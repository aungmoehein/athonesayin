//
//  PasswordKeyboard.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 17/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

protocol PasswordKeyboardDelegate: class {
    func keyWasTapped(text: String)
}

class PasswordKeyboard: UIView {
    // This variable will be set as the view controller so that
    // the keyboard can send messages to the view controller.
    weak var delegate: PasswordKeyboardDelegate?


    // MARK:- keyboard initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }

    func initializeSubviews() {
        let keyboardNib = UINib(nibName: "PasswordKeyboard", bundle: nil)
        let view = keyboardNib.instantiate(withOwner: self, options: nil)[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
    }
    
    @IBAction func btn0(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn1(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn2(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn3(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn4(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn5(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn6(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn7(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn8(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn9(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btnDelete(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: "DELETE_CHARACTER")
    }
}
