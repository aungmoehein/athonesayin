//
//  CategoryChooseCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 10/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class CategoryChooseCell: UICollectionViewCell {
    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var name: UILabel!
    
    let localizer = LocalizationSystem.Localizer
    
    var category_name: String! {
        didSet {
            name.text = localizer.localizedStringForKey(key: self.category_name, comment: "")
        }
    }
    
    var icon_name: String! {
        didSet{
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.setImage(image, for: .normal)
            icon.makeRounded()
            icon.contentMode = .center
            icon.backgroundColor = UIColor(hex: Color.colorPrimary.rawValue)
            icon.tintColor = UIColor.white
            icon.layer.shadowColor = UIColor.gray.cgColor
            icon.layer.shadowOffset = CGSize(width: 0, height: 0)
            icon.layer.shadowOpacity = 0.7
            icon.layer.shadowRadius = 2.0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        icon.isUserInteractionEnabled = false
    }

}
