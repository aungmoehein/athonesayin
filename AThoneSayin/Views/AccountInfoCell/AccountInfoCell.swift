//
//  AccountInfoCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 15/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import PopupDialog
import Firebase

class AccountInfoCell: UITableViewCell {
    @IBOutlet weak var btnLogin: UIButton!
    
    var localizer = LocalizationSystem.Localizer
    var showMsgDialog: PopupDialog!
    var vc: SettingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        btnLogin.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        Analytics.logEvent(Events.BTN_LOGIN, parameters: nil)
        let image = self.generateQRCode(from: String(describing: UIDevice.current.identifierForVendor!.uuidString))
        showInfo(image: image!)
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.isoLatin1)
        if let filter = CIFilter(name: "CIQRCodeGenerator"){
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 15, y: 15)
            
            if let output = filter.outputImage?.transformed(by: transform).tinted(using: .white){
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    func showInfo(image: UIImage){
        let popUp = QRCodeDialog(nibName: "QRCodeDialog", bundle: nil)
        
        popUp.image = image
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        vc.present(showMsgDialog, animated: true, completion: nil)
    }
}
