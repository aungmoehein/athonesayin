//
//  MarkedUsagesCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 19/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class MarkedUsagesCell: UITableViewCell {
    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    @IBOutlet weak var checked: UISwitch!
    
    var icon_name: String = "" {
        didSet {
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.setImage(image, for: .normal)
            icon.makeRounded()
            icon.contentMode = .center
            icon.backgroundColor = UIColor(hex: Color.colorPrimary.rawValue)
            icon.tintColor = UIColor.white
        }
    }
    
    var title: String = "" {
        didSet {
            lbTitle.text = self.title
        }
    }
    
    var amount: String = "" {
        didSet {
            lbAmount.text = self.amount
        }
    }
    
    var callBackSwitchState:((Bool) -> (Void))?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    @IBAction func checked(_ sender: UISwitch) {
        callBackSwitchState?(sender.isOn)
    }
}
