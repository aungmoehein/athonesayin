//
//  MarkedUsagesView.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 19/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import TTGSnackbar
import Firebase

protocol MarkedUsagesViewDelegate {
    func recentlyUsedAdded()
}

class MarkedUsagesView: UIViewController, UINavigationControllerDelegate  {
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var usagesTableView: UITableView!
    @IBOutlet weak var btnAddPage: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    
    var db: DBHelper!
    var localizer = LocalizationSystem.Localizer
    var markedUsagesViewDelegate: MarkedUsagesViewDelegate!
    
    var vc: HomeView!
    var viaDashboard: Bool = false
    var dashboard: DashboardView!
    var todayDate: String = ""
    var status: Int = Parameter.EXPENSE
    
    fileprivate var dateformatter = DateFormatter()
    fileprivate var isDataAdded: Bool = false
    fileprivate var items: [Item] = []
    
    var recentlyUseds: [RecentlyUsed] = [] {
        didSet {
            recentlyUseds.forEach() {
                recentlyUsed in
                
                items.append(Item(rc: recentlyUsed))
            }
        }
    }
    
    fileprivate var switchOnIndexes: [Int] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.delegate = self
        
        btnAdd.layer.borderColor = UIColor(hex: Color.colorPrimary.rawValue).cgColor
        btnAdd.layer.borderWidth = 1
        btnAdd.layer.cornerRadius = 10
        
        dateformatter.locale = Locale.init(identifier: "us")
        dateformatter.timeZone = .current
        dateformatter.dateStyle = .medium
        
        usagesTableView.register(UINib(nibName: "MarkedUsagesCell", bundle: nil), forCellReuseIdentifier: "marked_usages_cell")
        usagesTableView.dataSource = self
        usagesTableView.delegate = self
        usagesTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        lbTitle.text = status == Parameter.EXPENSE ? String(format: localizer.localizedStringForKey(key: "daily", comment: ""), localizer.localizedStringForKey(key: "expense", comment: "")) : String(format: localizer.localizedStringForKey(key: "daily", comment: ""), localizer.localizedStringForKey(key: "income", comment: ""))
        
        btnAdd.setTitle(localizer.localizedStringForKey(key: "daily_save", comment: ""), for: .normal)
        btnAddPage.setTitle(localizer.localizedStringForKey(key: "daily_other", comment: ""), for: .normal)
    }
    
    @IBAction func btnAddPage(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        goToAddView(status: self.status)
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        let date = dateformatter.string(from: Date())
        let selectedYear = date.split(separator: ",")[1]
        let selectedMonth = date.split(separator: ",")[0].split(separator: " ")[0]
        let selectedDay = date.split(separator: ",")[0].split(separator: " ")[1]
        let short_time = "\(selectedDay) \(selectedMonth),\(selectedYear)"
        let usage_time = date.dbDate()!.todbString()
        items.forEach() {
            item in
            if item.isSelected {
                db.insertUsage(usage_note: item.rc.usage_note, usage_amount: item.rc.usage_amount, category_id: item.rc.category_id, usage_status: item.rc.usage_status, usage_time: usage_time, short_time: short_time)
                isDataAdded = true
            }
        }
        markedUsagesViewDelegate.recentlyUsedAdded()
        self.dismiss(animated: true, completion: nil)
        
        if isDataAdded {
            Analytics.logEvent(status == Parameter.EXPENSE ? Events.ADD_DAILY_EXPENCE : Events.ADD_DAILY_INCOME, parameters: nil)
            showSnackbar(text: "အချက်အလက်များမှတ်ပြီးပါပြီ")
        }
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func goToAddView(status: Int) {
        let view = AddModule().build(db: db) as! AddView
        view.selectedSegment = status
        
        viaDashboard ? dashboard.navigationController?.pushViewController(view, animated: true) : vc.navigationController?.pushViewController(view, animated: true)
        
    }
    
    func showSnackbar(text: String) {
        let snackbar = TTGSnackbar()
        snackbar.message = text
        snackbar.duration = .middle
        snackbar.onTapBlock = { (snackbar) in snackbar.dismiss()}
        snackbar.onSwipeBlock = { (snackbar, direction) in
            if direction == .right {
                snackbar.animationType = .slideFromLeftToRight
            } else if direction == .left {
                snackbar.animationType = .slideFromRightToLeft
            }
            snackbar.dismiss()
        }
        snackbar.backgroundColor = UIColor(hex: Color.colorAccent.rawValue)
        snackbar.actionTextColor = UIColor.white
        snackbar.messageTextColor = UIColor.white
        snackbar.show()
    }
}

extension MarkedUsagesView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentlyUseds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "marked_usages_cell", for: indexPath) as! MarkedUsagesCell
        cell.icon_name = getCategoryById(id: recentlyUseds[indexPath.row].category_id).category_icon
        cell.title = localizer.localizedStringForKey(key: getCategoryById(id: recentlyUseds[indexPath.row].category_id).category_name, comment: "")
        cell.amount = String(format: localizer.localizedStringForKey(key: "ks", comment: ""), Int(recentlyUseds[indexPath.row].usage_amount).formattedWithSeparator)

        cell.callBackSwitchState = { isOn in
            self.items[indexPath.row].isSelected = isOn
        }
        return cell
    }
    
    func getCategoryById(id: Int) -> Category {
        return db.readCategoryById(id: id)
    }
}

struct Item {
    var rc : RecentlyUsed
    var isSelected : Bool

    init(rc : RecentlyUsed, isSelected : Bool = false) {
        self.rc = rc
        self.isSelected = isSelected
    }
}
