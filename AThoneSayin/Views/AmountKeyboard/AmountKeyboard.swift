//
//  AmountKeyboard.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 11/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

protocol AmountKeyboardDelegate: class {
    func keyWasTapped(text: String)
}

class AmountKeyboard: UIView {
    // This variable will be set as the view controller so that
    // the keyboard can send messages to the view controller.
    @IBOutlet weak var btnDismiss: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
    
    weak var delegate: AmountKeyboardDelegate?


    // MARK:- keyboard initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initializeSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initializeSubviews()
    }

    func initializeSubviews() {
        let keyboardNib = UINib(nibName: "AmountKeyboard", bundle: nil)
        let view = keyboardNib.instantiate(withOwner: self, options: nil)[0] as! UIView
        self.addSubview(view)
        view.frame = self.bounds
        
        var image = UIImage(named: "ic_key_dismiss")?.withRenderingMode(.alwaysTemplate)
        btnDismiss.setImage(image, for: .normal)
        btnDismiss.tintColor = .white
        
        image = UIImage(named: "ic_backspace")?.withRenderingMode(.alwaysTemplate)
        btnDelete.setImage(image, for: .normal)
        btnDelete.tintColor = .white
    }
    
    @IBAction func btn0(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn1(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn2(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn3(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn4(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn5(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn6(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn7(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn8(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn9(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btnC(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn00(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btn000(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: sender.titleLabel!.text!)
    }
    
    @IBAction func btndot(_ sender: UIButton) {
    }
    
    @IBAction func btnDelete(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: "DELETE_CHARACTER")
    }
    
    @IBAction func btnDismiss(_ sender: UIButton) {
        self.delegate?.keyWasTapped(text: "DISMISS_KEYBOARD")
    }
}
