//
//  CalendarRangeView.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 18/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

protocol CalendarRangeViewDelegate {
    func done()
}

class CalendarRangeView: UIViewController {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var delegate : CalendarRangeViewDelegate?
    let dateFormatter = DateFormatter()
    var vc: ReportPagerStripViewController!
    var fromDate: String = ""
    var toDate: String = ""
    var status: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.locale = Locale.init(identifier: "us")
        datePicker.timeZone = .current
        dateFormatter.locale = Locale(identifier: "us")
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "dd MMM, yyyy"
        fromDate = dateFormatter.string(from: Date())
        toDate = dateFormatter.string(from: Date())
        
        self.datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -50, to: Date())
        self.datePicker.maximumDate = Date()

        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], for: UIControl.State.selected)
        
        datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        if self.status == 0 {
            self.fromDate = dateFormatter.string(from: sender.date)
        }else {
            self.toDate = dateFormatter.string(from: sender.date)
        }
    }
    
    @IBAction func segmentedControl(_ sender: Any) {
        self.status = segmentedControl.selectedSegmentIndex
        
        self.datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -50, to: Date())
        if segmentedControl.selectedSegmentIndex == 1 {
            self.datePicker.minimumDate = self.fromDate.toDate(withFormat: "dd MMM, yyyy")
            self.datePicker.date = self.fromDate.toDate(withFormat: "dd MMM, yyyy")!
        }
    }
    
    @IBAction func btnDone(_ sender: Any) {
        vc.fromDate = self.fromDate
        vc.toDate = self.toDate
        delegate?.done()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
