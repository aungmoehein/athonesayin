//
//  EditCollectionCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class EditCategoryCell: UICollectionViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var removeBtn: UIButton!
    
    var isAnimate: Bool! = true
    
    var icon_name: String = "ic_bill" {
        didSet {
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.setImage(image, for: .normal)
            
        }
    }
    
    var title: String = "Ph bill" {
        didSet {
            name.text = self.title
        }
    }
    
    func startAnimate() {
        let shakeAnimation = CABasicAnimation(keyPath: "transform.rotation")
        shakeAnimation.duration = 0.05
        shakeAnimation.repeatCount = 4
        shakeAnimation.autoreverses = true
        shakeAnimation.duration = 0.2
        shakeAnimation.repeatCount = 99999
        
        let startAngle: Float = (-2) * 3.14159/180
        let stopAngle = -startAngle
        
        shakeAnimation.fromValue = NSNumber(value: startAngle as Float)
        shakeAnimation.toValue = NSNumber(value: 3 * stopAngle as Float)
        shakeAnimation.autoreverses = true
        shakeAnimation.timeOffset = 290 * drand48()
        
        let layer: CALayer = self.layer
        layer.add(shakeAnimation, forKey:"animate")
        removeBtn.isHidden = false
        isAnimate = true
    }
    
    func stopAnimate() {
        let layer: CALayer = self.layer
        layer.removeAnimation(forKey: "animate")
        self.removeBtn.isHidden = true
        isAnimate = false
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        cellView.layer.borderWidth = 1
        cellView.layer.cornerRadius = 7
        cellView.layer.borderColor = UIColor(hex: Color.colorAccent.rawValue).cgColor
        
        let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
        icon.setImage(image, for: .normal)
        icon.isUserInteractionEnabled = false
        icon.tintColor = UIColor(hex: Color.colorAccent.rawValue)
    }

}
