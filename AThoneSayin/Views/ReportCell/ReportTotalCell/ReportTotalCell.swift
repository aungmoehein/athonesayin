//
//  ReportTotalCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 19/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class ReportTotalCell: UITableViewCell {
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    var localizer = LocalizationSystem.Localizer
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var total: String = "" {
        didSet {
            lbTotal.text = localizer.localizedStringForKey(key: "lb_total", comment: "")
            lbAmount.text = self.total
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
