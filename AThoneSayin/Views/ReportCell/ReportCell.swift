//
//  ReportCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 18/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class ReportCell: UITableViewCell {
    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    
    var localizer = LocalizationSystem.Localizer
    var icon_name: String = "" {
        didSet {
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.setImage(image, for: .normal)
            icon.tintColor = UIColor(hex: Color.colorAccent.rawValue)
        }
    }
    
    var title: String = "" {
        didSet {
            lbTitle.text = self.title
        }
    }
    
    var amount: String = "" {
        didSet {
            lbAmount.text = self.amount
        }
    }
    
    var date: String = "" {
        didSet {
            lbDate.text = self.date
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
