//
//  ShowMsgDialog.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 12/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class ShowMsgDialog: UIViewController {

    @IBOutlet weak var lbMessage: UILabel!
    var msg: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewDidAppear(_ animated: Bool) {
        
        lbMessage.text = String.localizedStringWithFormat(NSLocalizedString(msg, comment: ""))
        
    }
}
