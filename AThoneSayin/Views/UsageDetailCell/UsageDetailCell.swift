//
//  UsageDetailCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 12/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class UsageDetailCell: UITableViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var redCircle: UIButton!
    @IBOutlet weak var lbShortNote: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    @IBOutlet weak var lbTime: UILabel!
    
    var localizer = LocalizationSystem.Localizer
    var note: String = "" {
        didSet {
            lbShortNote.text = localizer.localizedStringForKey(key: "no_note", comment: "")
            if self.note != "" {
                lbShortNote.text = self.note
            }
        }
    }
    
    var amount: String! {
        didSet {
            lbAmount.text = String(format: localizer.localizedStringForKey(key: "ks", comment: ""), amount)
        }
    }
    
    var time: String! {
        didSet {
            lbTime.text = self.time
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        redCircle.makeRounded()
        cellView.layer.cornerRadius = 15
        cellView.layer.borderWidth = 1
        cellView.layer.borderColor = UIColor.gray.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
