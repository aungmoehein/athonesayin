//
//  NoteDialog.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 11/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class NoteDialog: UIViewController, UITextViewDelegate {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    var note: String = "" 
    
    var addView: AddView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textView.text = self.note
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textView.becomeFirstResponder()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        addView.shortNote = textView.text
    }
}
