//
//  TableViewCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 08/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class HomeExpenseCell: UITableViewCell {
    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var iconCount: UIButton!
    
    let localizer = LocalizationSystem.Localizer
    
    var category_name: String! {
        didSet {
            name.text = localizer.localizedStringForKey(key: self.category_name, comment: "")
        }
    }
    
    var icon_name: String! {
        didSet{
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.setImage(image, for: .normal)
            icon.makeRounded()
            icon.contentMode = .center
            icon.backgroundColor = UIColor(hex: Color.colorPrimary.rawValue)
            icon.tintColor = UIColor.white
        }
    }
    
    var usage_amount: String! {
        didSet{
            amount.text = String(format: localizer.localizedStringForKey(key: "ks", comment: ""), self.usage_amount)
        }
    }
    
    var addedDate: String! {
        didSet{
            let time = self.addedDate.split(separator: ",")[0]
            date.text = "\(time)"
        }
    }
    
    var count: Int! {
        didSet{
            iconCount.makeRounded()
            iconCount.backgroundColor = UIColor(hex: Color.colorAccent.rawValue)
            iconCount.setTitle("\(self.count!)", for: .normal)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
