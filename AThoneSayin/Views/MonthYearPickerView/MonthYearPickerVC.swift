//
//  MonthYearPickerView.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 15/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class MonthYearPickerVC: UIViewController {
    
    @IBOutlet weak var calendarView: MonthYearPickerView!
    @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var homeView: HomeView!
    var dashboardView: DashboardView!
    let dateFormatter = DateFormatter()
    var vc_name: String = ""
    var chosenMonth: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.locale = Locale(identifier: "us")
        dateFormatter.timeZone = .current
        self.chosenMonth = "\(dateFormatter.monthSymbols[Calendar.current.component(.month, from: Date()) - 1].substring(fromIndex: 0, count: 3)), \(Calendar.current.component(.year, from: Date()))"
        
        calendarView.onDateSelected = { (month: Int, year: Int) in
            self.chosenMonth = "\(self.dateFormatter.monthSymbols[month - 1].substring(fromIndex: 0, count: 3)), \(year)"
        }
    }
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnDone(_ sender: Any) {
        switch vc_name {
        case "home":
            homeView.chosenMonth = chosenMonth
        default:
            dashboardView.chosenMonth = chosenMonth
        }
        
        self.dismiss(animated: true, completion: nil)
    }
}
