//
//  PasswordDialog.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 09/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

protocol PasswordDialogDelegate {
    func check(pass: Bool, isDismiss: Bool)
}

class PasswordDialog: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var txtFieldPassword: UITextField!
    @IBOutlet weak var btnEnter: UIButton!
    
    var delegate: PasswordDialogDelegate!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        btnEnter.layer.cornerRadius = 10
        btnEnter.layer.borderWidth = 1
        btnEnter.layer.borderColor = UIColor(hex: 0x459a0b).cgColor
    }

    @IBAction func btnEnter(_ sender: Any) {
        dismiss(animated: true, completion: nil)
        delegate.check(pass: txtFieldPassword.text == UserDefaults.standard.string(forKey: UserDefaults.Keys.password_string), isDismiss: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        delegate.check(pass: UserDefaults.standard.bool(forKey: UserDefaults.Keys.password_enabled), isDismiss: true)
    }
}
