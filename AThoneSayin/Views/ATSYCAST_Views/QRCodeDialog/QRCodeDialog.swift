//
//  QRCodeDialog.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 09/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class QRCodeDialog: UIViewController {
    @IBOutlet weak var imgQR: UIImageView!
    @IBOutlet weak var btnCopyID: UIButton!
    
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        imgQR.image = image
    }
    
    @IBAction func btnCopyID(_ sender: Any) {
        UIPasteboard.general.string = String(describing: UIDevice.current.identifierForVendor!.uuidString)
        self.view.makeToast("Copied to clipboard", duration: 2, position: .bottom)
    }
}
