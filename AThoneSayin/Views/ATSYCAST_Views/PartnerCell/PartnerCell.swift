//
//  PartnerCell.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 23/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class PartnerCell: UITableViewCell {
    @IBOutlet weak var partnerImg: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    
    var img: String = "" {
        didSet {
            partnerImg.layer.cornerRadius = 10
            
            partnerImg.sd_setImage(with: URL(string: self.img), placeholderImage: UIImage(named: "movie_poster.png"))
            if img == "m_sub" || img == "z_channel" {
                partnerImg.image = UIImage(named: img)
            }
        }
    }
    
    var title: String = "" {
        didSet {
            lbTitle.text = title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
