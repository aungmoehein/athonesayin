//
//  AnimeHomeTableCell.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 04/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

protocol AnimeHomeTableCellDelegate {
    func click(category: String, aniData: AniData?, aniLatestData: AniLatestData?, categoryMore: String?)
}

class AnimeHomeTableCell: UITableViewCell {
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var btnMore: UIButton!
    @IBOutlet weak var categoryCV: UICollectionView!
    
    var delegate: AnimeHomeTableCellDelegate!
    
    var aniLatestDatas: [AniLatestData] = [] {
        didSet {
            categoryCV.reloadData()
        }
    }
    
    var aniDatas: [AniData] = [] {
        didSet {
            categoryCV.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        categoryCV.register(UINib(nibName: "AnimeHomeLatestCell", bundle: nil), forCellWithReuseIdentifier: "anime_home_latest_cell")
        categoryCV.register(UINib(nibName: "ContentCell", bundle: nil), forCellWithReuseIdentifier: "content_cell")
        categoryCV.delegate = self
        categoryCV.dataSource = self
        categoryCV.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnMore(_ sender: Any) {
        delegate.click(category: "More", aniData: nil, aniLatestData: nil, categoryMore: categoryTitle.text)
    }
}

extension AnimeHomeTableCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if categoryTitle.text == "Latest" {
            return aniLatestDatas.count
        }else {
            return aniDatas.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if categoryTitle.text == "Latest" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "anime_home_latest_cell", for: indexPath) as! AnimeHomeLatestCell
            cell.displayImg = aniLatestDatas[indexPath.row].thumbnail!
            cell.displayTitle = aniLatestDatas[indexPath.row].title!
            cell.displayEpisode = aniLatestDatas[indexPath.row].episode_num!
            cell.displayType = aniLatestDatas[indexPath.row].type!
            cell.displayDate = aniLatestDatas[indexPath.row].created_at!
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "content_cell", for: indexPath) as! ContentCell
            cell.image = aniDatas[indexPath.row].cover_photo!
            cell.title = aniDatas[indexPath.row].title!
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if categoryTitle.text == "Latest" {
            return CGSize(width: 200, height: 162.5)
        }else {
            return CGSize(width: 90, height: 182.5)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if categoryTitle.text == "Latest" {
            delegate.click(category: categoryTitle.text!, aniData: nil, aniLatestData: aniLatestDatas[indexPath.row], categoryMore: nil)
        }else {
            delegate.click(category: categoryTitle.text!, aniData: aniDatas[indexPath.row], aniLatestData: nil, categoryMore: nil)
        }
    }
}
