//
//  EditStream.swift
//  ATSY Cast
//
//  Created by Aung Moe Hein on 12/07/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class EditStream: UIViewController {
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldURL: UITextField!
    @IBOutlet weak var btnPlay: UIButton!
    
    var vc: StreamTestView!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var stream: Stream!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txtFieldName.delegate = self
        self.txtFieldURL.delegate = self
        self.txtFieldName.text = stream.name
        self.txtFieldURL.text = stream.url
        
        btnPlay.layer.cornerRadius = 5
        btnPlay.layer.borderColor = UIColor(hex: 0x459a0b).cgColor
        btnPlay.layer.borderWidth = 1
    }

    @IBAction func btnPlay(_ sender: Any) {
        appDelegate.db.updateStream(id: stream.id, name: txtFieldName.text!, url: txtFieldURL.text!)
        self.txtFieldName.endEditing(true)
        self.txtFieldURL.endEditing(true)
        self.dismiss(animated: true, completion: {
            self.vc.reloadView()
        })
    }
}

extension EditStream: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
