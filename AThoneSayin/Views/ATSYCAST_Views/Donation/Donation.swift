//
//  Donation.swift
//  ATSY Cast
//
//  Created by Aung Moe Hein on 30/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class Donation: UIViewController {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbPhone: UILabel!
    @IBOutlet weak var lbAYA: UILabel!
    @IBOutlet weak var lbKBZ: UILabel!
    @IBOutlet weak var lbCB: UILabel!
    @IBOutlet weak var btnCopyAYA: UIButton!
    @IBOutlet weak var btnCopyKBZ: UIButton!
    @IBOutlet weak var btnCopyCB: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        lbAYA.text = "AYA - 0096223010010669"
        lbKBZ.text = "KBZ - 23430123400709201"
        lbCB.text =  "CB - 0010600100483966"
        
        lbTitle.text = UserDefaults.standard.string(forKey: UserDefaults.Keys.donation_title)
        
        let image = UIImage(named: "ic_clipboard")?.withRenderingMode(.alwaysTemplate)
        btnCopyAYA.setImage(image, for: .normal)
        btnCopyAYA.tintColor = UIColor(hex: 0x459a0b)
        btnCopyKBZ.setImage(image, for: .normal)
        btnCopyKBZ.tintColor = UIColor(hex: 0x459a0b)
        btnCopyCB.setImage(image, for: .normal)
        btnCopyCB.tintColor = UIColor(hex: 0x459a0b)
    }
    @IBAction func btnCopyAYA(_ sender: Any) {
        UIPasteboard.general.string = lbAYA.text?.replacingOccurrences(of: "AYA - ", with: "")
        self.view.makeToast("Copied to clipboard", duration: 2, position: .bottom)
    }
    
    @IBAction func btnCopyKBZ(_ sender: Any) {
        UIPasteboard.general.string = lbKBZ.text?.replacingOccurrences(of: "KBZ - ", with: "")
        self.view.makeToast("Copied to clipboard", duration: 2, position: .bottom)
    }
    
    @IBAction func btnCopyCB(_ sender: Any) {
        UIPasteboard.general.string = lbCB.text?.replacingOccurrences(of: "CB - ", with: "")
        self.view.makeToast("Copied to clipboard", duration: 2, position: .bottom)
    }
}
