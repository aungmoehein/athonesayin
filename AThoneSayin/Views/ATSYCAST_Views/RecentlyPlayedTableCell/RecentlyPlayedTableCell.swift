//
//  RecentlyPlayedTableCell.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 06/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

protocol RecentlyPlayedTableCellDelegate {
    func click(playBack: Playback)
}

class RecentlyPlayedTableCell: UITableViewCell {
    @IBOutlet weak var partnerTitle: UILabel!
    @IBOutlet weak var partnerCV: UICollectionView!
    
    var delegate: RecentlyPlayedTableCellDelegate!
    var playBacks: [Playback] = [] {
        didSet {
            partnerCV.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        partnerCV.register(UINib(nibName: "RecentlyPlayedCell", bundle: nil), forCellWithReuseIdentifier: "recently_played_cell")
        partnerCV.delegate = self
        partnerCV.dataSource = self
        partnerCV.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension RecentlyPlayedTableCell: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return playBacks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recently_played_cell", for: indexPath) as! RecentlyPlayedCell
        cell.image = playBacks[indexPath.row].imageURL
        cell.title = playBacks[indexPath.row].videoName
        cell.creditProgress(current: Int(playBacks[indexPath.row].position), max: Int(playBacks[indexPath.row].duration))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 90, height: 182.5)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate.click(playBack: playBacks[indexPath.row])
    }
}
