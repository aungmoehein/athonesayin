//
//  AboutDialog.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 29/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import Lottie

class AboutDialog: UIViewController {
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var lbDescription: UILabel!
    @IBOutlet weak var lbLinkMMTV: UILabel!
    @IBOutlet weak var lbLinkKH: UILabel!
    @IBOutlet weak var lbLInkDooFree: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animationView.animation = Animation.named("walking_people")
        animationView.loopMode = .loop
        animationView.play()
    }
}
