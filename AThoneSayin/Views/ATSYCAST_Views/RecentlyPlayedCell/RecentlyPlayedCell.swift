//
//  RecentlyPlayedCell.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 27/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class RecentlyPlayedCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    var image: String = "" {
        didSet {
            imgView.layer.cornerRadius = 5
            imgView.sd_setImage(with: URL(string: self.image), placeholderImage: UIImage(named: "movie_poster.png"))
        }
    }
    
    var title: String = "" {
        didSet {
            lbTitle.text = self.title
        }
    }
    
    func creditProgress(current: Int = 0, max: Int = 0) {
         //Compute ratio of 0 to 1 for progress.
        let ratio = Float(current) / Float(max)
        progressBar.progress = Float(ratio)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
