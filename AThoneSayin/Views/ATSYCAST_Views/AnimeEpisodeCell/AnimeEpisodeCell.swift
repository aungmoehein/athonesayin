//
//  TableViewCell.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 05/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class AnimeEpisodeCell: UITableViewCell {
    @IBOutlet weak var episodeNo: UILabel!
    @IBOutlet weak var episodeTitle: UILabel!
    @IBOutlet weak var airingDate: UILabel!
    @IBOutlet weak var btnDub: UIButton!
    @IBOutlet weak var btnSub: UIButton!
    @IBOutlet weak var btnDubWidth: NSLayoutConstraint!
    @IBOutlet weak var btnDubTrailing: NSLayoutConstraint!
    
    var episode_num: String = "" {
        didSet {
            episodeNo.text = "Episode " + self.episode_num
        }
    }
    
    var title: String = "" {
        didSet {
            episodeTitle.text = self.title
        }
    }
    
    var date: String = "" {
        didSet {
            airingDate.text = self.date
        }
    }
    
    func showVideoBtn(sub: Int, dub: Int) {
        btnSub.isHidden = sub != 1
        if dub == 0 {
            btnDub.isHidden = true
            btnDubWidth.constant = 0
            btnDubTrailing.constant = 0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
