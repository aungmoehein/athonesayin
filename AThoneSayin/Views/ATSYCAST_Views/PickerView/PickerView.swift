//
//  PickerView.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 16/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class PickerView: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOK: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    var status: Int = 0
    var categoryView: CategoryView!
    var selectedData: String = ""
    var dataArray: [String] = ["Latest", "Longest", "Shortest", "Top Rated", "Most Popular", "Top Weekly", "Top Monthly"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnOK.layer.borderColor = UIColor(hex: 0x459a0b).cgColor
        btnOK.layer.borderWidth = 1
        btnOK.layer.cornerRadius = 10
        
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)], for: UIControl.State.selected)
        segmentedControl.tintColor = UIColor(hex: 0x459a0b)

        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.setValue(UIColor.white, forKeyPath: "textColor")
        selectedData = dataArray[0]
    }
    @IBAction func segmentedControl(_ sender: Any) {
        self.status = segmentedControl.selectedSegmentIndex
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return dataArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return dataArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedData = dataArray[row]
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnOK(_ sender: Any) {
        categoryView.epOrder = selectedData
        UserDefaults.standard.set(status, forKey: UserDefaults.Keys.e_porner_gc)
        self.dismiss(animated: true, completion: nil)
    }
}
