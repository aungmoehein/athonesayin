//
//  AnimeSearchCell.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 04/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class AnimeSearchCell: UITableViewCell {
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var status: UILabel!
    
    var displayImg: String = "" {
        didSet {
            img.sd_setImage(with: URL(string: self.displayImg), placeholderImage: UIImage(named: "movie_poster.png"))
        }
    }
    
    var displayTitle: String = "" {
        didSet {
            title.text = self.displayTitle
        }
    }
    
    var displayYear: String = "" {
        didSet {
            year.text = self.displayYear
        }
    }
    
    var displayType: String = "" {
        didSet {
            type.text = self.displayType
        }
    }
    
    var displayStatus: String = "" {
        didSet {
            status.text = self.displayStatus
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
