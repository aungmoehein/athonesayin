//
//  ContentCell.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 21/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import SDWebImage

class ContentCell: UICollectionViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lbTitle: UILabel!
    
    var image: String = "" {
        didSet {
            imgView.layer.cornerRadius = 5
            imgView.sd_setImage(with: URL(string: self.image), placeholderImage: UIImage(named: "movie_poster.png"))
        }
    }
    
    var title: String = "" {
        didSet {
            lbTitle.text = self.title
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
