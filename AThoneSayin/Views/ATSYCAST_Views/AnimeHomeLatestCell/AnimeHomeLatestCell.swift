//
//  AnimeHomeLatestCell.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 04/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import SDWebImage

class AnimeHomeLatestCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var episode: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var date: UILabel!
    
    var displayImg: String = "" {
        didSet {
            image.sd_setImage(with: URL(string: self.displayImg), placeholderImage: UIImage(named: "movie_poster.png"))
        }
    }
    
    var displayTitle: String = "" {
        didSet {
            title.text = self.displayTitle
        }
    }
    
    var displayEpisode: String = "" {
        didSet {
            episode.text = "Ep. " + self.displayEpisode
        }
    }
    
    var displayType: String = "" {
        didSet {
            type.text = self.displayType.uppercased()
            type.textColor = self.displayType == "dub" ? UIColor(hex: 0x459A0B) : UIColor(hex: 0x007AFF)
        }
    }

    var displayDate: String = "" {
        didSet {
            date.text = self.displayDate.toDate(withFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSSSS'Z'")?.timeAgoDisplay()
        }
    }


    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
