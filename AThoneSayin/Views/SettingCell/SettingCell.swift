//
//  SettingCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 15/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbSecondaryTitle: UILabel!
    @IBOutlet weak var passwordEnable: UISwitch!
    @IBOutlet weak var icon: UIButton!
    
    var vc: SettingView!
    var db: DBHelper!
    
    var icon_name: String = "" {
        didSet {
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.setImage(image, for: .normal)
            
        }
    }
    
    var title: String = "" {
        didSet {
            lbTitle.text = self.title
            passwordEnable.isHidden = self.title != "Password Protection"
        }
    }
    
    var secondaryTitle: String = "" {
        didSet {
            lbSecondaryTitle.text = self.secondaryTitle
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func passwordEnable(_ sender: Any) {
        let passVC = PasscodeModule().build(db: db) as! PasscodeView
        passVC.isLogin = false
        vc.navigationController?.pushViewController(passVC, animated: true)
    }
}
