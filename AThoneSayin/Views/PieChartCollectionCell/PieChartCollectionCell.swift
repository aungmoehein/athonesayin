//
//  PieChartCollectionCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 15/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class PieChartCollectionCell: UICollectionViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var progressBar: CustomProgressView!
    @IBOutlet weak var lbAmount: UILabel!
    
    var localizer = LocalizationSystem.Localizer
    var color: UIColor! {
        didSet {
            self.progressBar.tintColor = color
            self.contentView.layer.cornerRadius = 15.0
            self.contentView.layer.borderWidth = 1.0
            self.contentView.layer.borderColor = color.cgColor
            self.contentView.layer.masksToBounds = true

            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOffset = CGSize(width: 0, height: 2.0)
            self.layer.shadowRadius = 2.0
            self.layer.shadowOpacity = 0.7
            self.layer.masksToBounds = false
        }
    }
    
    var icon_name: String = "" {
        didSet {
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.isUserInteractionEnabled = false
            icon.setImage(image, for: .normal)
            icon.contentMode = .center
            icon.tintColor = UIColor(hex: Color.colorAccent.rawValue)
        }
    }
    
    var name: String = "" {
        didSet {
            lbName.text = self.name
        }
    }
    
    var totalAmount: Float = 0
    var amount: Float = 0 {
        didSet {
            lbAmount.text = String(format: localizer.localizedStringForKey(key: "ks", comment: ""), "\(Int(amount).formattedWithSeparator)")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func progress(current: Float, max: Float) {
         //Compute ratio of 0 to 1 for progress.
        let ratio = current / max
        
        progressBar.progress = Float(ratio)
    }
    
}
