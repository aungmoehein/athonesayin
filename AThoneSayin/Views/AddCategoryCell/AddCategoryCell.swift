//
//  AddCategoryCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 13/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class AddCategoryCell: UICollectionViewCell {
    @IBOutlet weak var catView: UIView!
    @IBOutlet weak var icon: UIButton!
    
    override var isSelected: Bool {
        didSet {
            self.catView.backgroundColor = isSelected ?  UIColor(hex: Color.colorAccent.rawValue) : UIColor.white
            self.icon.tintColor = isSelected ? UIColor.white : UIColor(hex: Color.colorAccent.rawValue)
        }
    }
    
    var icon_name: String = "ic_bill" {
        didSet{
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.setImage(image, for: .normal)
            icon.tintColor = UIColor(hex: Color.colorAccent.rawValue)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        catView.layer.borderWidth = 1
        catView.layer.cornerRadius = 7
        catView.layer.borderColor = UIColor(hex: Color.colorAccent.rawValue).cgColor
        
        
        let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
        icon.setImage(image, for: .normal)
        icon.isUserInteractionEnabled = false
    }

}
