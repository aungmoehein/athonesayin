//
//  EditRecentlyUsedCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 20/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class EditRecentlyUsedCell: UITableViewCell {
    @IBOutlet weak var icon: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbAmount: UILabel!
    @IBOutlet weak var btnDelete: UIButton!
    
    var icon_name: String = "" {
        didSet {
            let image = UIImage(named: self.icon_name)?.withRenderingMode(.alwaysTemplate)
            icon.setImage(image, for: .normal)
            icon.contentMode = .center
            icon.tintColor = UIColor(hex: Color.colorAccent.rawValue)
        }
    }
    
    var title: String = "" {
        didSet {
            lbTitle.text = self.title
        }
    }
    
    var amount: String = "" {
        didSet {
            lbAmount.text = self.amount
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let image = UIImage(named: "close")?.withRenderingMode(.alwaysTemplate)
        btnDelete.setImage(image, for: .normal)
        btnDelete.contentMode = .center
        btnDelete.tintColor = .red
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
