//
//  LoginCell.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 07/09/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit

class LoginCell: UITableViewCell {
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var lbEmail: UILabel!
    @IBOutlet weak var credentialView: UIStackView!
    
    var name: String = "" {
        didSet {
            self.lbName.text = name
        }
    }
    
    var email: String = "" {
        didSet {
            self.lbEmail.text = email
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        btnLogin.layer.cornerRadius = 10
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
