//
//  LocalizationSystem.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 07/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import UIKit

class LocalizationSystem: NSObject {

    static let Localizer = LocalizationSystem()
    var bundle = Bundle.main
    
    // MARK:- For localize string
    func localizedStringForKey(key: String, comment: String) -> String {
           return bundle.localizedString(forKey: key, value: comment, table: nil)
       }
    
    // MARK:- Set user selected language
    func setSelectedLanguage(languageCode: String) {
//        let appleLanguages = UserDefaults.standard.object(forKey: UserDefaults.Keys.apple_languages) as! String
//        UserDefaults.standard.set(appleLanguages, forKey: UserDefaults.Keys.apple_languages)
//
//        guard let langPath = Bundle.main.path(forResource: languageCode, ofType: "lproj") else {
//            bundle = Bundle.main
//            return
//        }
//        bundle = Bundle(path: langPath)!
        var appleLanguages = UserDefaults.standard.object(forKey: UserDefaults.Keys.apple_languages) as! [String]
        appleLanguages.remove(at: 0)
        appleLanguages.insert(languageCode, at: 0)
        UserDefaults.standard.set(appleLanguages, forKey: UserDefaults.Keys.apple_languages)

        guard let langPath = Bundle.main.path(forResource: languageCode, ofType: "lproj") else {
            bundle = Bundle.main
            return
        }
        bundle = Bundle(path: langPath)!
    }
    
    // MARK:- Get current language
    func getLanguage() -> String {
        let appleLanguages = UserDefaults.standard.object(forKey: UserDefaults.Keys.apple_languages) as! String
        let prefferedLanguage = appleLanguages
        if prefferedLanguage.contains("-") {
            let array = prefferedLanguage.components(separatedBy: "-")
            return array[0]
        }
        return prefferedLanguage
    }
}
