//
//  Date.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 13/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension Date {
    func timeAgoDisplay(withFormat format: String = "yyyy-mm-dd") -> String {

        let calendar = Calendar.current
        let minuteAgo = calendar.date(byAdding: .minute, value: -1, to: Date())!
        let hourAgo = calendar.date(byAdding: .hour, value: -1, to: Date())!
        let dayAgo = calendar.date(byAdding: .day, value: -1, to: Date())!
        let weekAgo = calendar.date(byAdding: .day, value: -7, to: Date())!

        if minuteAgo < self {
            let diff = Calendar.current.dateComponents([.second], from: self, to: Date()).second ?? 0
            return String(format: "%@ sec ago", String(diff))
        } else if hourAgo < self {
            let diff = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
            return String(format: "%@ min ago", String(diff))
        } else if dayAgo < self {
            let diff = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
            return String(format: "%@ hr ago", String(diff))
        } else if weekAgo < self {
            let diff = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
            return String(format: "%@ days ago", String(diff))
        }
        
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "us")
        formatter.timeZone = .current
        formatter.dateFormat = "dd/MM 'at' h:mm a"
        formatter.amSymbol = "AM"
        formatter.pmSymbol = "PM"
        return formatter.string(from: self)
//        let diff = Calendar.current.dateComponents([.weekOfYear], from: self, to: Date()).weekOfYear ?? 0
//        return String(format: "%@ weeks ago", String(diff))
    }
    
    func toString(withFormat format: String = "yyyy-mm-dd") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "us")
        formatter.timeZone = .current
        let myString = formatter.string(from: Date())

        return myString
    }
    
    func todbString(withFormat format: String = "yyyy-MM-dd") -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        formatter.locale = Locale(identifier: "us")
        let myString = formatter.string(from: self)

        return myString

    }
}

extension String {
    
    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Yangon")
        dateFormatter.locale = Locale(identifier: "us")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)!.addingTimeInterval((3600 * 6) + (3600 / 2)) // Add 6:30
        
        return date
        
    }
    
    func dbDate(withFormat format: String = "MMM dd, yyyy")-> Date?{
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(identifier: "Asia/Yangon")
        dateFormatter.locale = Locale(identifier: "us")
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)!.addingTimeInterval((3600 * 6) + (3600 / 2))
        
        return date
    }
}
