//
//  MonthYearPicker.swift
//
//  Created by Ben Dodson on 15/04/2015.
//  Modified by Jiayang Miao on 24/10/2016 to support Swift 3
//  Modified by David Luque on 24/01/2018 to get default date
//
import UIKit

class MonthYearPickerView: UIPickerView, UIPickerViewDelegate, UIPickerViewDataSource {
    var months: [String]!
    var years: [Int]!
    
    var calendar = Calendar.current
    var month = Calendar.current.component(.month, from: Date()) {
        didSet {
            selectRow(month-1, inComponent: 0, animated: false)
        }
    }
    
    var year = Calendar.current.component(.year, from: Date()) {
        didSet {
            selectRow(years.firstIndex(of: year)!, inComponent: 1, animated: true)
        }
    }
    
    var onDateSelected: ((_ month: Int, _ year: Int) -> Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.addPickerUI()
        self.commonSetup()
    }
    
    func addPickerUI() {
        self.backgroundColor = UIColor.white
        self.setValue(UIColor.black, forKey: "textColor")
        self.autoresizingMask = .flexibleWidth
        self.contentMode = .center
        self.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
        
        let currentMonth = NSCalendar(identifier: NSCalendar.Identifier.gregorian)!.component(.month, from: NSDate() as Date)
        self.selectRow(currentMonth - 1, inComponent: 0, animated: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonSetup()
    }
    
    func commonSetup() {
        // population years
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "us")
        dateFormatter.timeZone = .current
        calendar.locale = Locale(identifier: "us")
        calendar.timeZone = .current
        var years: [Int] = []
        if years.count == 0 {
            var year = calendar.component(.year, from: NSDate() as Date)
            for _ in 0...50 {
                years.append(year)
                year -= 1
            }
        }
        self.years = years
        
        // population months with localized names
        var months: [String] = []
        var month = 0
        for _ in 1...12 {
            months.append(dateFormatter.monthSymbols[month].capitalized)
            month += 1
        }
        self.months = months
        
        self.delegate = self
        self.dataSource = self
        
        let currentMonth = calendar.component(.month, from: NSDate() as Date)
        self.selectRow(currentMonth - 1, inComponent: 0, animated: false)
    }
    
    // Mark: UIPicker Delegate / Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return months[row]
        case 1:
            return "\(years[row])"
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return months.count
        case 1:
            return years.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let month = self.selectedRow(inComponent: 0)+1
        let year = years[self.selectedRow(inComponent: 1)]

        self.month = month
        self.year = year
        
        calendar.locale = Locale(identifier: "us")
        calendar.timeZone = .current
        let currentMonth = calendar.component(.month, from: Date()) - 1
        if year == calendar.component(.year, from: Date()) {
            if month > calendar.component(.month, from: Date()) {
                self.selectRow(currentMonth, inComponent: 0, animated: true)
                self.month = currentMonth + 1
            }
        }
        
        if let block = onDateSelected {
            block(self.month, self.year)
        }
    }
    
}
