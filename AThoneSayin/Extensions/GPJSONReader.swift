//
//  GPJsonReader.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 10/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

public class GPJSONReader {
    
    /** Get the whole JSON object from a file
     *  @returns an optional dictionary [String: Any]?
     */
    
    public class func readJson(fileName: String, withBlock completion: ([String : Any]?) -> Void) {
        do {
            if let file = Bundle.main.url(forResource: fileName, withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String : Any] {
                    debugPrint("JSON found as dictionary")
                    completion(object)
                } else {
                    debugPrint("JSON is invalid")
                    completion(nil)
                }
            } else {
                debugPrint("No file found!")
                completion(nil)
            }
        } catch {
            debugPrint(error.localizedDescription)
            completion(nil)
        }
    }
}
