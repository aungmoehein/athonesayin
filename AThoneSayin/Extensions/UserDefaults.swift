//
//  UserDefaults.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 07/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension UserDefaults {
    enum Keys {
        static let apple_languages: String = "AppleLanguages"
        static let first_time: String = "first_time"
        static let default_category_count: String = "default_category_count"
        static let is_secure: String = "is_secure"
        static let passcode: String = "passcode"
        static let signin: String = "signin"
        static let appleAuthorizedUserIdKey: String = "appleAuthorizedUserIdKey"
        
        // ATSY CAST
        // Device Utility
        static let atsycast_first_time: String = "atsycast_first_time"
        static let first_open: String = "first_open"
        
        // Donation Config
        static let donation_title: String = "donation_title"
        
        // Partner Sheet ID
        static let partner_sheet_id: String = "partner_sheet_id"
        
        // Give access to specific device
        static let user_name: String = "user_name"
        static let give_access: String = "give_access"
        static let device_adult: String = "device_adule"
        static let all_channel: String = "all_channel"
        static let anime_access: String = "anime_access"
        static let password_string: String = "password_string"
        static let password_enabled: String = "password_enabled"
        static let has_donation: String = "has_donation"
        
        // Partner List
        static let partnerZChannel: String = "partnerZChannel"
        static let partnerMovieMM: String = "partnerMovieMM"
        static let partnerMDark: String = "partnerMDark"
        static let partnerMSub: String = "partnerMSub"
        static let partnerDoujin: String = "partnerDoujin"
        static let partnerAnimeflix: String = "partnerAnimeflix"
        static let partnerEPorner: String = "partnerEPorner"
        
        // Partner ID
        static let idZChannel: String = "idZChannel"
        static let idMovieMM: String = "idMovieMM"
        static let idMDark: String = "idMDark"
        static let idMSub: String = "idMSub"
        static let idDoujin: String = "idDoujin"
        static let idAnimeflix: String = "idAnimeflix"
        static let idEPorner: String = "idEPorner"
        
        // Partner Name Keys
        static let z_channel: String = "z_channel"
        static let movie_mm: String = "movie_mm"
        static let m_dark: String = "m_dark"
        static let m_sub: String = "m_sub"
        static let a_flix: String = "a_flix"
        static let e_porner: String = "e_porner"
        static let kh_analytics: String = "kh_analytics"
        
        static let m_sub_key_route: String = "m_sub_key_route"
        static let m_sub_key: String = "m_sub_key"
        
        static let hentai_b: String = "hentai_b"
        static let hentai_m: String = "hentai_m"
        static let hentai_k: String = "hentai_k"
        static let hentai_v: String = "hentai_v"
        
        static let e_porner_v: String = "e_porner_v"
        static let e_porner_gc: String = "e_porner_gc"
        static let is_initially_landscape: String = "is_initially_landscape"
        static let kh_analytics_version: String = "kh_analytics_version"
    }
}
