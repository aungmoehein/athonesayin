//
//  PieChartView.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 14/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Charts

extension PieChartView {
    func setup(date: String, amount: String) {
        self.usePercentValuesEnabled = true
        self.drawSlicesUnderHoleEnabled = false
        self.holeRadiusPercent = 0.93
        self.transparentCircleRadiusPercent = 0
        self.chartDescription?.enabled = false
        self.setExtraOffsets(left: 5, top: 10, right: 5, bottom: 5)
        
        self.drawCenterTextEnabled = true
        
        let paragraphStyle = NSParagraphStyle.default.mutableCopy() as! NSMutableParagraphStyle
        paragraphStyle.lineBreakMode = .byTruncatingTail
        paragraphStyle.alignment = .center
        paragraphStyle.lineSpacing = 2
        
        let mutableAttributedString = NSMutableAttributedString()
        let centerTextString = String(format: LocalizationSystem.Localizer.localizedStringForKey(key: "piechart_total_balance", comment: ""), date)
        let centerText = NSMutableAttributedString(string: centerTextString)
        centerText.setAttributes([.font : UIFont(name: "HelveticaNeue-Light", size: 16)!, .foregroundColor : UIColor(hex: Color.colorPrimary.rawValue), .paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: centerText.length))
        
        mutableAttributedString.append(centerText)
        
        let amount = NSMutableAttributedString(string: amount)
        amount.setAttributes([.font : UIFont(name: "HelveticaNeue-Light", size: 16)!, .foregroundColor : UIColor(hex: "33CCFF"), .paragraphStyle : paragraphStyle], range: NSRange(location: 0, length: amount.length))
        mutableAttributedString.append(amount)
        
        self.centerAttributedText =  mutableAttributedString
        self.rotationAngle = 0
        self.rotationEnabled = true
        self.highlightPerTapEnabled = true
    }
    
    func updateChartData(hideData: Bool, label: [String], value: [Int], colors: [UIColor]) {
        if hideData {
            self.data = nil
            return
        }

        self.setDataCount(label, value, colors)
    }
    
    func setDataCount(_ label: [String], _ value: [Int], _ colors: [UIColor]) {
        let entries = (0..<label.count).map { (i) -> PieChartDataEntry in
            return PieChartDataEntry(value: Double(value[i]),
                label: label[i])
        }
        
        let set = PieChartDataSet(entries: entries, label: "Election Results")
        set.sliceSpace = 2
        set.selectionShift = 10
        set.colors = colors
        
        set.xValuePosition = .outsideSlice
        if #available(iOS 13.0, *) {
            set.valueTextColor = .systemBackground
        } else {
            set.valueTextColor = .black
        }
        set.yValuePosition = .insideSlice
        set.valueLinePart1Length = 0.2
        if #available(iOS 13.0, *) {
            set.valueLineColor = .label
        } else {
            set.valueLineColor = .black
        }
        
        let data = PieChartData(dataSet: set)
        data.setValueTextColor(.clear)
        self.data = data
        if #available(iOS 13.0, *) {
            self.entryLabelColor = .label
        } else {
            self.entryLabelColor = .black
        }
        self.highlightValues(nil)
    }
}
