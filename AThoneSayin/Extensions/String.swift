//
//  String.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 08/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension String {
    
    var parseJSONString: AnyObject? {
        
        let data = self.data(using: String.Encoding.utf8, allowLossyConversion: false)
        
        if let jsonData = data {
            // Will return an object or nil if JSON decoding fails
            do{
                let string = try JSONSerialization.jsonObject(with: jsonData, options: JSONSerialization.ReadingOptions.mutableContainers)
                
                return string as AnyObject?
                
            } catch {
                return nil
            }
            
        } else {
            // Lossless conversion of the string was not possible
            return nil
        }
    }

    func substring(fromIndex : Int,count : Int) -> String{
        let startIndex = self.index(self.startIndex, offsetBy: fromIndex)
        let endIndex = self.index(self.startIndex, offsetBy: fromIndex + count)
        let range = startIndex..<endIndex
        return String(self[range])
    }
    
    var length: Int {
        return self.count
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    func toNumber() -> String {
        var string: String = ""
        let stringArray = self.components(separatedBy: CharacterSet.decimalDigits.inverted)
        for item in stringArray {
            if let number = Int(item) {
                string.append("\(number)")
            }
        }
        return string
    }
}

