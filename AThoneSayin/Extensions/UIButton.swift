//
//  UIImage.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 10/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func makeRounded() {
        self.layer.masksToBounds = false
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
