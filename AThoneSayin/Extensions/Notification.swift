//
//  Notification.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension Notification {
    enum Noti {
        static let language_did_change: String = "language_did_change"
        static let adult_enabled: String = "adult_enabled"
        static let add_favourite: String = "add_favourite"
    }
}
