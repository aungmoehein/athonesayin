//
//  Sheet2JSON.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 29/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta

public class Sheet2JSON {
    
    public class func toJSON(_ data: Entity<Any>) -> String {
        let feed = data.jsonDict["feed"] as! NSDictionary
        let entries = feed.object(forKey: "entry") as! NSArray
        var jsonString: Dictionary<String, String> = [:]
        var jsonStringArray: [Dictionary<String, String>] = []
        entries.forEach() {
            entry in
            let data = entry as! NSDictionary
            let prefix = "gsx$"
            
            data.allKeys.forEach() {
                value in
                let key = value as! String
                if key.contains(prefix) {
                    let keyData = data.object(forKey: key) as! NSDictionary
                    jsonString[String(key.split(separator: "$")[1])] = keyData.object(forKey: "$t") as? String
                }
            }
            jsonStringArray.append(jsonString)
        }
        
        guard let data = try? JSONSerialization.data(withJSONObject: jsonStringArray, options: []) else {
            return ""
        }
        return String(data: data, encoding: String.Encoding.utf8)!
    }
}

