//
//  AnimationView.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 07/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Lottie

extension AnimationView {
    func playAnimation(filename: String) {
        self.animation = Animation.named(filename)
        self.loopMode = .loop
        self.play()
    }
}
