//
//  UIViewController.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func showToast(message : String, font: UIFont) {
        
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-100, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 15;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 3.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }

    func showErrorAlert(text: String) {
        let uiAlert = UIAlertController(title: text, message: nil, preferredStyle: UIAlertController.Style.alert)
        
        uiAlert.addAction(
            UIAlertAction(
                title: LocalizationSystem.Localizer.localizedStringForKey(key: "Ok", comment: ""),
                style: .default,
                handler: nil
            )
        )
        
        self.present(uiAlert, animated: true, completion: nil)
    }
}

extension UIAlertController {
    func pruneNegativeWidthConstraints() {
        for subView in self.view.subviews {
            for constraint in subView.constraints where constraint.debugDescription.contains("width == - 16") {
                subView.removeConstraint(constraint)
            }
        }
    }
}
