//
//  Parameter.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 11/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

enum Parameter {
    static let EXPENSE: Int = 0
    static let INCOME: Int = 1
}
