//
//  Icons.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 07/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

enum LightIcons: String {
    case mode = "light"
    case homeIcon = "light_home"
    case dashboardIcon = "light_dashboard"
    case balanceIcon = "light_eye"
}
