//
//  Configuration.swift
//  ATSY Cast
//
//  Created by Aung Moe Hein on 29/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct Configuration {
    lazy var sheetNo: String = Bundle.main.infoDictionary?["sheetNo"] as! String
}
