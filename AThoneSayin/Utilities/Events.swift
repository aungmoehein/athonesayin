//
//  Events.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 11/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

enum Events {
    // Screens
    static let HOME_SCREEN = "home_screen" // fin
    static let DASHBOARD_SCREEN = "dashboard_screen" // fin
    static let REPORT_SCREEN = "report_screen" // fin
    static let PASSWORD_SCREEN = "password_screen" // fin
    static let DONATION_SCREEN = "donation_screen" // fin
    static let SETTING_CATEGORY_SCREEN = "setting_category_screen" // fin
    static let SETTING_DAILY_SCREEN = "setting_daily_screen" // fin
    static let USAGE_DETAIL_SCREEN = "usage_detail_screen" // fin
    
    // Add
    static let ADD_DAILY_EXPENCE = "add_daily_expence" // fin
    static let ADD_DAILY_INCOME = "add_daily_income" // fin
    static let ADD_EXPENCE = "add_expence" // fin
    static let ADD_INCOME = "add_income" // fin
    static let ADD_EXPENCE_CATEGORY = "add_expence_category" // fin
    static let ADD_INCOME_CATEGORY = "add_income_category" // fin
    
    // Edit
    static let UPDATE_EXPENCE_CATEGORY = "update_expence_category" // fin
    static let UPDATE_INCOME_CATEGORY = "update_income_category" // fin
    static let DELETE_EXPENCE_CATEGORY = "delete_expence_category" // fin
    static let DELETE_INCOME_CATEGORY = "delete_income_category" // fin
    static let UPDATE_EXPENCE = "update_expence" // fin
    static let UPDATE_INCOME = "update_income" // fin
    
    // Button
    static let BTN_REPORT_FILTER = "btn_report_filter" // fin
    static let BTN_LOGIN = "btn_login" // fin
    static let BTN_COPY_AYA = "btn_copy_aya" // fin
    static let BTN_COPY_KBZ = "btn_copy_kbz" // fin
    static let BTN_COPY_CB = "btn_copy_cb" // fin
    static let BTN_WATCH_ATSYCAST = "btn_watch_atsycast" // fin
    
    // Setting
    static let CHECK_UPDATE = "check_update" // fin
    static let CHANGE_LANGUAGE = "change_language" // fin
    static let FACEBOOK_CHECK = "facebook_check"
}
