//
//  Color.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 08/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

enum Color: String {
    case colorPrimary = "459a0b"
    case colorPrimaryDark = "2e5911"
    case colorAccent = "1e3a7b"
}
