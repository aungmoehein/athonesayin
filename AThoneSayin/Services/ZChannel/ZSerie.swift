//
//  ZSerie.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 25/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct ZSerie : Codable {
    let id: Int?
    let title: String?
    let episodes : [ZEpisode]?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case title = "title"
        case episodes = "episodes"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        episodes = try values.decodeIfPresent([ZEpisode].self, forKey: .episodes)
    }
    
}
