//
//  ZGenreList.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 25/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct ZGenreList : Codable {
    let genres : [ZGenre]?
    
    enum CodingKeys: String, CodingKey {
        
        case genres = "genres"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        genres = try values.decodeIfPresent([ZGenre].self, forKey: .genres)
    }
    
}
