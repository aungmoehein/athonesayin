//
//  ZSerieData.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 25/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

//
// MARK: - Section Data Structure
//
struct ZSection {
    var serie: ZSerie
    var collapsed: Bool
    
    public init(serie: ZSerie, collapsed: Bool = true) {
        self.serie = serie
        self.collapsed = collapsed
    }
}
