//
//  ZChannel.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 25/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta
import CoreData

class ZChannel: Service {
    static let sharedInstance: ZChannel = { ZChannel() } ()
    
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        let jsonDecoder = JSONDecoder()
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: UserDefaults.standard.string(forKey: UserDefaults.Keys.z_channel))
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
        self.configure("api/first/*/*") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/poster/by/filtres/*/created/*/*/*") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/poster/by/filtres/0/rating/*/*/*") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/season/by/serie/*/*/*") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configureTransformer("api/first/*/*"){
            try jsonDecoder.decode(ZGenreList.self, from: $0.content)
        }
        
        self.configureTransformer("api/poster/by/filtres/*/created/*/*/*"){
            try jsonDecoder.decode([ZPost].self, from: $0.content)
        }
        
        self.configureTransformer("api/poster/by/filtres/0/rating/*/*/*"){
            try jsonDecoder.decode([ZPost].self, from: $0.content)
        }
        
        self.configureTransformer("api/season/by/serie/*/*/*"){
            try jsonDecoder.decode([ZSerie].self, from: $0.content)
        }
    }
    
    var api: Resource { return resource("/api") }
    
    func genreList(apiKey: String) -> Resource {
        return api.child("/first/" + apiKey)
    }
    
    func moreDataByPage(page: Int, id: Int, apiKey: String) -> Resource {
        return api.child("/poster/by/filtres/\(id)/created/\(page)/\(apiKey)")
    }
    
    func topRatedPage(page: Int, apiKey: String) -> Resource {
        return api.child("/poster/by/filtres/0/rating/\(page)/\(apiKey)")
    }
    
    func getSeries(id: Int, apiKey: String) -> Resource {
        return api.child("/season/by/serie/\(id)/\(apiKey)")
    }
}

