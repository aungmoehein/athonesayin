//
//  ZPost.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 25/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct ZPost : Codable {
    let id: Int?
    let type: String?
    let title: String?
    let description: String?
    let image: String?
    let sources: [ZSource]?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case title = "title"
        case description = "description"
        case image = "image"
        case sources = "sources"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        sources = try values.decodeIfPresent([ZSource].self, forKey: .sources)
    }
}
