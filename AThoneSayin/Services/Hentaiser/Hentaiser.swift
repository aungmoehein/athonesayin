//
//  Hentaiser.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 29/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta
import CoreData

class Hentaiser: Service {
    static let sharedInstance: Hentaiser = { Hentaiser() } ()
    
    let version = UserDefaults.standard.string(forKey: UserDefaults.Keys.hentai_v)!
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        let jsonDecoder = JSONDecoder()
        
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: UserDefaults.standard.string(forKey: UserDefaults.Keys.hentai_b))
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
        configure("**/videos/**") {
            $0.headers["App-Package"] = UserDefaults.standard.string(forKey: UserDefaults.Keys.hentai_k)!
        }
        
        configure("**/video/**") {
            $0.headers["App-Package"] = UserDefaults.standard.string(forKey: UserDefaults.Keys.hentai_k)!
        }
        
        self.configure("\(version)/videos/*/page/*") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("\(version)/videos/*") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configureTransformer("\(version)/videos/*/page/*"){
            try jsonDecoder.decode(HVideoList.self, from: $0.content)
        }
        
        self.configureTransformer("\(version)/videos/*"){
            try jsonDecoder.decode([HMovie].self, from: $0.content)
        }
    }
    
    var api: Resource { return resource("/\(version)/videos") }
    
    var videoDetail: Resource { return resource("/\(version)/video") }
    
    func getVideoList(category: String, page: Int) -> Resource {
        if category == "hot" {
            return api.child("/\(category)")
        }else {
            return api.child("/\(category)/page/\(page)")
        }
    }
    
    func getVideo(gid: String) -> Resource {
        return videoDetail.child("\(gid)/user/getVideoDetail")
    }
}


