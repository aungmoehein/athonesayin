//
//  HVideo.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 29/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct HMovie : Codable {
    let gid: String?
    let title: String?
    let cover: String?
    
    enum CodingKeys: String, CodingKey {
        case gid = "gid"
        case title = "title"
        case cover = "cover"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        gid = try values.decodeIfPresent(String.self, forKey: .gid)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        cover = try values.decodeIfPresent(String.self, forKey: .cover)
    }
}

