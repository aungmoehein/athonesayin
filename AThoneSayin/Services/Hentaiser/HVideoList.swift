//
//  HVideoList.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 29/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct HVideoList : Codable {
    let videos : [HMovie]?
    
    enum CodingKeys: String, CodingKey {
        
        case videos = "videos"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        videos = try values.decodeIfPresent([HMovie].self, forKey: .videos)
    }
    
}
