//
//  MSSeriesData.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 02/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MSSection {
    var title: String
    var episodes: [MSEpisode]
    var collapsed: Bool
    
    public init(title: String, episodes: [MSEpisode], collapsed: Bool = true) {
        self.title = title
        self.episodes = episodes
        self.collapsed = collapsed
    }
}
