//
//  MSubMovie.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 02/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta
import CoreData

class MSubMovie: Service {
    static let sharedInstance: MSubMovie = { MSubMovie() } ()
    
    let apikey = UserDefaults.standard.string(forKey: UserDefaults.Keys.m_sub_key)!
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        let jsonDecoder = JSONDecoder()
        
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: UserDefaults.standard.string(forKey: UserDefaults.Keys.m_sub))
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
        configure("**/api/**") {
            $0.headers["APIKEY"] = self.apikey
        }
        
        print("APIKEY:: ", self.apikey)
        self.configure("api/*/all") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/series/*/episode") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configureTransformer("api/movie/all"){
            try jsonDecoder.decode([MSMovie].self, from: $0.content)
        }
        
        self.configureTransformer("api/series/all"){
            try jsonDecoder.decode([MSSerie].self, from: $0.content)
        }
        
        self.configureTransformer("api/series/*/episode"){
            try jsonDecoder.decode([MSEpisode].self, from: $0.content)
        }

    }
    
    var api: Resource { return resource("/api") }
    
    func getAllMovie() -> Resource {
        return api.child("/movie/all")
    }
    
    func getAllSeries() -> Resource {
        return api.child("/series/all")
    }
    
    func getEpisode(id: Int) -> Resource {
        return api.child("/series/\(id)/episode")
    }
}
