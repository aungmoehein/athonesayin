//
//  MSMovie.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 02/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MSMovie : Codable {
    let id: Int?
    let movietitle: String?
    let image: String?
    let moviegenres: String?
    let review: String?
    let stream: String?
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case movietitle = "movietitle"
        case image = "image"
        case moviegenres = "moviegenres"
        case review = "review"
        case stream = "stream"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        movietitle = try values.decodeIfPresent(String.self, forKey: .movietitle)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        moviegenres = try values.decodeIfPresent(String.self, forKey: .moviegenres)
        review = try values.decodeIfPresent(String.self, forKey: .review)
        stream = try values.decodeIfPresent(String.self, forKey: .stream)
    }
}
