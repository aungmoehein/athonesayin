//
//  MSSerie.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 02/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MSSerie : Codable {
    let id: Int?
    let seriestitle: String?
    let image: String?
    let seriesgenres: String?
    let review: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case seriestitle = "seriestitle"
        case image = "image"
        case seriesgenres = "seriesgenres"
        case review = "review"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        seriestitle = try values.decodeIfPresent(String.self, forKey: .seriestitle)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        seriesgenres = try values.decodeIfPresent(String.self, forKey: .seriesgenres)
        review = try values.decodeIfPresent(String.self, forKey: .review)
    }
}
