//
//  MSEpisode.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 02/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MSEpisode : Codable {
    let id: Int?
    let episodename: String?
    let stream: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case episodename = "episodename"
        case stream = "stream"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        episodename = try values.decodeIfPresent(String.self, forKey: .episodename)
        stream = try values.decodeIfPresent(String.self, forKey: .stream)
    }
}
