//
//  MyanDark.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 23/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta
import CoreData

class MyanDark: Service {
    static let sharedInstance: MyanDark = { MyanDark() } ()
    
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        let jsonDecoder = JSONDecoder()
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: UserDefaults.standard.string(forKey: UserDefaults.Keys.m_dark))
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
        self.configure("api/get_category_index") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/get_category_videos") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configureTransformer("api/get_category_index"){
            try jsonDecoder.decode(MDCategoryList.self, from: $0.content)
        }
        
        self.configureTransformer("api/get_category_videos"){
            try jsonDecoder.decode(MDPostList.self, from: $0.content)
        }
    }
    
    var api: Resource { return resource("/api") }
    
    func getMDCategoryList(apiKey: String) -> Resource {
        return api.child("/get_category_index").withParam("api_key", apiKey)
    }
    
    func getVideoList(page: Int, count: Int, apiKey: String, id: Int) -> Resource {
        return api.child("/get_category_videos").withParams(["api_key":apiKey, "page": "\(page)", "count": "\(count)", "sort": "n.id DESC", "id":"\(id)"])
    }
}

