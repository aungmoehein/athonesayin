//
//  MDCategoryList.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 23/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MDCategoryList : Codable {
    let categories : [MDCategory]?
    
    enum CodingKeys: String, CodingKey {
        
        case categories = "categories"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        categories = try values.decodeIfPresent([MDCategory].self, forKey: .categories)
    }
    
}
