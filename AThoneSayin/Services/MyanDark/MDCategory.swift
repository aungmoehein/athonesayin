//
//  MDCategory.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 23/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MDCategory : Codable {
    let cid  : Int?
    let category_name : String?
    let category_image : String?
    let video_count : Int?
    
    
    enum CodingKeys: String, CodingKey {
        case cid = "cid"
        case category_name = "category_name"
        case category_image = "category_image"
        case video_count = "video_count"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        cid = try values.decodeIfPresent(Int.self, forKey: .cid)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        category_image = try values.decodeIfPresent(String.self, forKey: .category_image)
        video_count = try values.decodeIfPresent(Int.self, forKey: .video_count)
    }
    
}

