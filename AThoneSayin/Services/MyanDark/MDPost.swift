//
//  MDPost.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 23/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MDPost : Codable {
    let vid: Int?
    let cat_id: Int?
    let video_title  : String?
    let video_url : String?
    let video_id: String?
    let video_thumbnail : String?
    let video_duration: String?
    let video_description: String?
    let video_type: String?
    let size: String?
    let total_views: Int?
    let date_time: String?
    let category_name: String?
    
    enum CodingKeys: String, CodingKey {
        case vid = "vid"
        case cat_id = "cat_id"
        case video_title = "video_title"
        case video_url = "video_url"
        case video_id = "video_id"
        case video_thumbnail = "video_thumbnail"
        case video_duration = "video_duration"
        case video_description = "video_description"
        case video_type = "video_type"
        case size = "size"
        case total_views = "total_views"
        case date_time = "date_time"
        case category_name = "category_name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        vid = try values.decodeIfPresent(Int.self, forKey: .vid)
        cat_id = try values.decodeIfPresent(Int.self, forKey: .cat_id)
        video_title = try values.decodeIfPresent(String.self, forKey: .video_title)
        video_url = try values.decodeIfPresent(String.self, forKey: .video_url)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        video_thumbnail = try values.decodeIfPresent(String.self, forKey: .video_thumbnail)
        video_duration = try values.decodeIfPresent(String.self, forKey: .video_duration)
        video_description = try values.decodeIfPresent(String.self, forKey: .video_description)
        video_type = try values.decodeIfPresent(String.self, forKey: .video_type)
        size = try values.decodeIfPresent(String.self, forKey: .size)
        total_views = try values.decodeIfPresent(Int.self, forKey: .total_views)
        date_time = try values.decodeIfPresent(String.self, forKey: .date_time)
        category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
    }
    
}
