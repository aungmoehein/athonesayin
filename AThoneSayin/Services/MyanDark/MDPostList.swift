//
//  MDPostList.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 23/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MDPostList : Codable {
    let posts : [MDPost]?
    
    enum CodingKeys: String, CodingKey {
        
        case posts = "posts"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        posts = try values.decodeIfPresent([MDPost].self, forKey: .posts)
    }
    
}
