//
//  Category.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 09/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

class Category
{
    var category_id: Int = 0
    var category_name: String = ""
    var category_icon: String = ""
    var category_type: Int = 0
    
    init(category_id:Int, category_name:String, category_icon:String, category_type:Int)
    {
        self.category_id = category_id
        self.category_name = category_name
        self.category_icon = category_icon
        self.category_type = category_type
    }
    
}
