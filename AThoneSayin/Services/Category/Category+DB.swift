//
//  Category+DB.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 09/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import SQLite3

extension DBHelper {
    
    func createCategoryTable() {
        let createTableString = "CREATE TABLE IF NOT EXISTS category(category_id INTEGER PRIMARY KEY AUTOINCREMENT,category_name TEXT,category_icon TEXT,category_type INTEGER);"
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK
        {
            if sqlite3_step(createTableStatement) == SQLITE_DONE
            {
                print("category table created.")
            } else {
                print("category table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    func readAllCategory() -> [Category] {
        let queryStatementString = "SELECT * FROM category;"
        var queryStatement: OpaquePointer? = nil
        var categories : [Category] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let category_id = sqlite3_column_int(queryStatement, 0)
                let category_name = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let category_icon = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let category_type = sqlite3_column_int(queryStatement, 3)
                categories.append(Category(category_id: Int(category_id), category_name: category_name, category_icon: category_icon, category_type: Int(category_type)))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return categories
    }
    
    func readCategoryCount() -> Int {
        let queryStatementString = "SELECT COUNT(*) FROM category;"
        var queryStatement: OpaquePointer? = nil
        var count: Int = 0
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let category_count = sqlite3_column_int(queryStatement, 0)
                count = Int(category_count)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return count
    }
    
    func readCategoryByType(type: Int) -> [Category] {
        let queryStatementString = "SELECT * FROM category WHERE category_type = ?;"
                var queryStatement: OpaquePointer? = nil
                var categories : [Category] = []
                if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                    sqlite3_bind_int(queryStatement, 1, Int32(type))
                    while sqlite3_step(queryStatement) == SQLITE_ROW {
                        let category_id = sqlite3_column_int(queryStatement, 0)
                        let category_name = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                        let category_icon = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                        let category_type = sqlite3_column_int(queryStatement, 3)
                        categories.append(Category(category_id: Int(category_id), category_name: category_name, category_icon: category_icon, category_type: Int(category_type)))
                    }
                } else {
                    print("SELECT statement could not be prepared")
                }
                sqlite3_finalize(queryStatement)
                return categories
    }
    
    func readCategoryById(id: Int) -> Category {
        let queryStatementString = "SELECT * FROM category WHERE category_id = ?;"
                var queryStatement: OpaquePointer? = nil
                var category:Category!
                if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
                    sqlite3_bind_int(queryStatement, 1, Int32(id))
                    while sqlite3_step(queryStatement) == SQLITE_ROW {
                        let category_id = sqlite3_column_int(queryStatement, 0)
                        let category_name = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                        let category_icon = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                        let category_type = sqlite3_column_int(queryStatement, 3)
                        category = Category(category_id: Int(category_id), category_name: category_name, category_icon: category_icon, category_type: Int(category_type))
                    }
                } else {
                    print("SELECT statement could not be prepared")
                }
                sqlite3_finalize(queryStatement)
                return category
    }
    
    func readCategoryIcons() -> [String] {
        let queryStatementString = "SELECT category_icon FROM category GROUP BY category_icon;"
        var queryStatement: OpaquePointer? = nil
        var icons : [String] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let category_icon = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                icons.append(category_icon)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return icons
    }
    
    func readCategoryForEdit(status: Int) -> [Category] {
        let defaultCategoryCount = UserDefaults.standard.integer(forKey: UserDefaults.Keys.default_category_count)
        
        let queryStatementString = "SELECT * FROM category WHERE category_id > ? AND category_type = ?;"
        var queryStatement: OpaquePointer? = nil
        var categories : [Category] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(defaultCategoryCount))
            sqlite3_bind_int(queryStatement, 2, Int32(status))
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let category_id = sqlite3_column_int(queryStatement, 0)
                let category_name = String(describing: String(cString: sqlite3_column_text(queryStatement, 1)))
                let category_icon = String(describing: String(cString: sqlite3_column_text(queryStatement, 2)))
                let category_type = sqlite3_column_int(queryStatement, 3)
                categories.append(Category(category_id: Int(category_id), category_name: category_name, category_icon: category_icon, category_type: Int(category_type)))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return categories
    }
    
    func insertCategory(category_name:String, category_icon:String, category_type:Int) {
        let insertStatementString = "INSERT INTO category (category_name, category_icon, category_type) VALUES (?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (category_name as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 2, (category_icon as NSString).utf8String, -1, nil)
            sqlite3_bind_int(insertStatement, 3, Int32(category_type))
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    func updateCategory(category_id:Int, category_name:String, category_icon:String) {
        let updateStatementString = "UPDATE category SET category_name = ?, category_icon = ? WHERE category_id = ?;"
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(updateStatement, 1, (category_name as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 2, (category_icon as NSString).utf8String, -1, nil)
            sqlite3_bind_int(updateStatement, 3, Int32(category_id))
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared.")
        }
        sqlite3_finalize(updateStatement)
    }
    
    func deleteCategoryById(category_id:Int) {
        let deleteStatementStirng = "DELETE FROM category WHERE category_id = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(category_id))
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
    }
}
