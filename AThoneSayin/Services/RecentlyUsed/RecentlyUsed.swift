//
//  RecentlyUsed.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 20/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

class RecentlyUsed {
    var id: Int = 0
    var usage_note: String = ""
    var usage_amount: Float = 0
    var category_id: Int = 0
    var usage_status: Int = 0
    
    init(id:Int, usage_note:String, usage_amount:Float, category_id:Int, usage_status:Int)
    {
        self.id = id
        self.usage_note = usage_note
        self.usage_amount = usage_amount
        self.category_id = category_id
        self.usage_status = usage_status
    }
    
}

