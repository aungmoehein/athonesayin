//
//  RecentlyUsed+DB.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 20/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

import SQLite3

extension DBHelper {
    func createRecentlyUsedTable() {
        let createTableString = "CREATE TABLE IF NOT EXISTS recently_used(id INTEGER PRIMARY KEY AUTOINCREMENT,usage_note TEXT,usage_amount DOUBLE,category_id INT, usage_status INT);"
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK
        {
            if sqlite3_step(createTableStatement) == SQLITE_DONE
            {
                print("recently_used table created.")
            } else {
                print("recently_used table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    func readAllRecentlyUsed() -> [RecentlyUsed] {
        let queryStatementString = "SELECT * FROM recently_used;"
        var queryStatement: OpaquePointer? = nil
        var recentlyUseds : [RecentlyUsed] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                recentlyUseds.append(selectAllRecentlyUsed(stmt: queryStatement!))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return recentlyUseds
    }
    
    func readAllRecentlyUsedByStatus(status: Int) -> [RecentlyUsed] {
        let queryStatementString = "SELECT * FROM recently_used WHERE usage_status = ?;"
        var queryStatement: OpaquePointer? = nil
        var recentlyUseds : [RecentlyUsed] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(status))
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                recentlyUseds.append(selectAllRecentlyUsed(stmt: queryStatement!))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return recentlyUseds
    }
    
    func insertRecentlyUsed(usage_note:String, usage_amount:Float, category_id:Int, usage_status:Int) {
        let insertStatementString = "INSERT INTO recently_used (usage_note, usage_amount, category_id, usage_status) VALUES (?, ?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (usage_note as NSString).utf8String, -1, nil)
            sqlite3_bind_double(insertStatement, 2, Double(usage_amount))
            sqlite3_bind_int(insertStatement, 3, Int32(category_id))
            sqlite3_bind_int(insertStatement, 4, Int32(usage_status))
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    func deleteRecentlyUsedById(id:Int) {
        let deleteStatementStirng = "DELETE FROM recently_used WHERE id = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(id))
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
    }
    
    private func selectAllRecentlyUsed(stmt: OpaquePointer) -> RecentlyUsed {
        var recentlyUsed: RecentlyUsed
        let id = sqlite3_column_int(stmt, 0)
        let usage_note = String(describing: String(cString: sqlite3_column_text(stmt, 1)))
        let usage_amount = sqlite3_column_double(stmt, 2)
        let category_id = sqlite3_column_int(stmt, 3)
        let usage_status = sqlite3_column_int(stmt, 4)
        recentlyUsed = RecentlyUsed(id: Int(id), usage_note: usage_note, usage_amount: Float(usage_amount), category_id: Int(category_id), usage_status: Int(usage_status))
        return recentlyUsed
    }
}
