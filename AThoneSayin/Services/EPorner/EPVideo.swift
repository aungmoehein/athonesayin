//
//  EPVideo.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 15/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct EPVideo : Codable {
    let id: String?
    let title: String?
    let keywords: String?
    let url: String?
    let embed: String?
    let default_thumb: EPThumb?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case keywords = "keywords"
        case url = "url"
        case embed = "embed"
        case default_thumb = "default_thumb"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        keywords = try values.decodeIfPresent(String.self, forKey: .keywords)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        embed = try values.decodeIfPresent(String.self, forKey: .embed)
        default_thumb = try values.decodeIfPresent(EPThumb.self, forKey: .default_thumb)
    }
}
