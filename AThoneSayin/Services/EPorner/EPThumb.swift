//
//  EPThumb.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 15/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct EPThumb : Codable {
    let src: String?
    
    enum CodingKeys: String, CodingKey {
        case src = "src"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        src = try values.decodeIfPresent(String.self, forKey: .src)
    }
}
