//
//  EPorner.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 15/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta
import CoreData

class EPorner: Service {
    static let sharedInstance: EPorner = { EPorner() } ()
    let apiVersion = UserDefaults.standard.string(forKey: UserDefaults.Keys.e_porner_v)
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        let jsonDecoder = JSONDecoder()
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: UserDefaults.standard.string(forKey: UserDefaults.Keys.e_porner), standardTransformers: [.text, .image])
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
        self.configure("api/" + apiVersion! + "/video/search/") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configureTransformer("api/" + apiVersion! + "/video/search/"){
            try jsonDecoder.decode(EPVideoList.self, from: $0.content)
        }
    }
    
    func videoList(query: String, order: String, page: Int) -> Resource {
        return resource("/api/" + apiVersion! + "/video/search/").withParams(["query": query, "page": "\(page)", "thumbsize": "big", "order": order, "gay": "\(UserDefaults.standard.integer(forKey: UserDefaults.Keys.e_porner_gc))", "lq": "0", "format": "json"])
    }
}


