//
//  EVideoList.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 15/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct EPVideoList : Codable {
    let count: Int?
    let per_page: Int?
    let videos: [EPVideo]?
    
    enum CodingKeys: String, CodingKey {
        case count = "count"
        case per_page = "per_page"
        case videos = "videos"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        count = try values.decodeIfPresent(Int.self, forKey: .count)
        per_page = try values.decodeIfPresent(Int.self, forKey: .per_page)
        videos = try values.decodeIfPresent([EPVideo].self, forKey: .videos)
    }
}
