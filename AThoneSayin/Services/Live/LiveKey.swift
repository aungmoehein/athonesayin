//
//  LiveKey.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 29/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

class LiveKey {
    var name: String = ""
    var status: String = ""
    var number: String = ""
    
    init(name:String, status:String, number:String)
    {
        self.name = name
        self.status = status
        self.number = number
    }
}

