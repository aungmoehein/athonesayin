//
//  Live.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 21/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

class Live {
    var channel_name: String = ""
    var channel_img: String = ""
    var stream_url: String = ""
    
    init(channel_name:String, channel_img:String, stream_url:String)
    {
        self.channel_name = channel_name
        self.channel_img = channel_img
        self.stream_url = stream_url
    }
}
