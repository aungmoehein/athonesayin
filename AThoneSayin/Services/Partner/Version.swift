//
//  Version.swift
//  ATSY Cast
//
//  Created by Aung Moe Hein on 22/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct Version : Codable {
    let version_name  : String?
    let update_status : String?
    let new_apk_url : String?
    let type : String?
    
    enum CodingKeys: String, CodingKey {
        case version_name = "versionname"
        case update_status = "updatestatus"
        case new_apk_url = "newapkurl"
        case type = "type"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        version_name = try values.decodeIfPresent(String.self, forKey: .version_name)
        update_status = try values.decodeIfPresent(String.self, forKey: .update_status)
        new_apk_url = try values.decodeIfPresent(String.self, forKey: .new_apk_url)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }
    
}
