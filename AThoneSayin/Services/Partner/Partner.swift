//
//  Partner.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 23/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct Partner : Codable {
    let comment  : String?
    let logo : String?
    let status : String?
    let apikey : String?
    let key : String?
    let ioskey : String?
    let name : String?
    let password: String?
    
    enum CodingKeys: String, CodingKey {
        case comment = "comment"
        case logo = "logo"
        case status = "status"
        case apikey = "apikey"
        case key = "key"
        case ioskey = "ioskey"
        case name = "name"
        case password = "password"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        comment = try values.decodeIfPresent(String.self, forKey: .comment)
        logo = try values.decodeIfPresent(String.self, forKey: .logo)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        apikey = try values.decodeIfPresent(String.self, forKey: .apikey)
        key = try values.decodeIfPresent(String.self, forKey: .key)
        ioskey = try values.decodeIfPresent(String.self, forKey: .ioskey)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        password = try values.decodeIfPresent(String.self, forKey: .password)
    }
    
}
