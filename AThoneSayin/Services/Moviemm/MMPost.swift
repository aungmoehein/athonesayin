//
//  MMPost.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 27/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MMPost : Codable {
    let title: String?
    let image: String?
    let links: [String]?
    let categories : [String]?
    
    enum CodingKeys: String, CodingKey {
        case title = "title"
        case image = "image"
        case links = "links"
        case categories = "categories"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        links = try values.decodeIfPresent([String].self, forKey: .links)
        categories = try values.decodeIfPresent([String].self, forKey: .categories)
    }
}
