//
//  MMPostList.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 27/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MMPostList : Codable {
    let postPerPage: String?
    let posts : [MMPost]?
    
    enum CodingKeys: String, CodingKey {
        case postPerPage = "postPerPage"
        case posts = "posts"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        postPerPage = try values.decodeIfPresent(String.self, forKey: .postPerPage)
        posts = try values.decodeIfPresent([MMPost].self, forKey: .posts)
    }
}
