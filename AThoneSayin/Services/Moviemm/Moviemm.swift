//
//  Moviemm.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 27/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta
import CoreData

class Moviemm: Service {
    static let sharedInstance: Moviemm = { Moviemm() } ()
    
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        let jsonDecoder = JSONDecoder()
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: UserDefaults.standard.string(forKey: UserDefaults.Keys.movie_mm))
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
        self.configure("movies6mm/vgt/categories.json") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("moviesmm/gcat/*/*") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configureTransformer("movies6mm/vgt/categories.json"){
            try jsonDecoder.decode([MMCategory].self, from: $0.content)
        }
        
        self.configureTransformer("moviesmm/gcat/*/*"){
            try jsonDecoder.decode(MMData.self, from: $0.content)
        }
    }
    
    func categoryList() -> Resource {
        return resource("/movies6mm").child("/vgt/categories.json")
    }
    
    func moviesList(category: String, page: Int) -> Resource {
        return resource("/moviesmm").child("/gcat/\(category)/\(page).json")
    }
}

