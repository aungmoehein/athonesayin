//
//  MMCategory.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 27/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MMCategory : Codable {
    let name : String?
    let image : String?
    let url : String?
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
        case url = "url"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        image = try values.decodeIfPresent(String.self, forKey: .image)
        url = try values.decodeIfPresent(String.self, forKey: .url)
    }
}
