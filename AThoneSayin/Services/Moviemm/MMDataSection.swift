//
//  MMDataSection.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 28/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

//
// MARK: - Section Data Structure
//
struct MMDataSection {
    var title: String
    var episodes: [String]
    var collapsed: Bool
    
    public init(title: String, episodes: [String], collapsed: Bool = true) {
        self.title = title
        self.episodes = episodes
        self.collapsed = collapsed
    }
}

