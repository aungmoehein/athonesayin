//
//  MMData.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 27/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct MMData : Codable {
    let today : MMPostList?
    
    enum CodingKeys: String, CodingKey {
        
        case today = "today"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        today = try values.decodeIfPresent(MMPostList.self, forKey: .today)
    }
}
