//
//  CMDownloader.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 13/08/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta
import var CommonCrypto.CC_MD5_DIGEST_LENGTH
import func CommonCrypto.CC_MD5
import typealias CommonCrypto.CC_LONG

class CMDownloader: Service {
    static let sharedInstance: CMDownloader = { CMDownloader() } ()
    var stringBuilder = [String : String]()
    let randomSalt = Int.random(in: 0..<901)

    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }

    init() {
        func MD5(string: String) -> Data {
            let length = Int(CC_MD5_DIGEST_LENGTH)
            let messageData = string.data(using:.utf8)!
            var digestData = Data(count: length)

            _ = digestData.withUnsafeMutableBytes { digestBytes -> UInt8 in
                messageData.withUnsafeBytes { messageBytes -> UInt8 in
                    if let messageBytesBaseAddress = messageBytes.baseAddress, let digestBytesBlindMemory = digestBytes.bindMemory(to: UInt8.self).baseAddress {
                        let messageLength = CC_LONG(messageData.count)
                        CC_MD5(messageBytesBaseAddress, messageLength, digestBytesBlindMemory)
                    }
                    return 0
                }
            }
            return digestData
        }
        
        stringBuilder["salt"] = String(randomSalt)
        stringBuilder["sign"] = MD5(string: "viaviweb\(randomSalt)").map { String(format: "%02x", $0) }.joined()
        stringBuilder["method_name"] = "get_home"
        
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: "http://cmdownloader.com")
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
    }
    
    func getHome() -> Request {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: stringBuilder)
            let theJSONText = String(data: jsonData, encoding: .ascii)
            let encodedData = theJSONText?.data(using: .utf8)
            
            if let base64Encoded = encodedData?.base64EncodedString() {
                print("HEADER JWT DATA:: \(theJSONText!)")
                print("HEADER JWT DATA:: \(base64Encoded)")
                return resource("/admin").child("/api.php").request(.post, urlEncoded: ["data": base64Encoded])
            }
        } catch {
            print("could not make data")
        }
        
        return resource("").request(.get)
    }
}


