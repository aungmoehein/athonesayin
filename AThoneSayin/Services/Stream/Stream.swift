//
//  Stream.swift
//  ATSY Cast
//
//  Created by Aung Moe Hein on 10/07/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

class Stream {
    var id: Int = 0
    var name: String = ""
    var url: String = ""
    
    init(id: Int, name: String, url: String) {
        self.id = id
        self.name = name
        self.url = url
    }
}
