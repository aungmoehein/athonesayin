//
//  AniPopularData.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 03/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct AniData : Codable {
    let id: Int?
    let title: String?
    let slug: String?
    let status: String?
    let description: String?
    let year: String?
    let type: String?
    let cover_photo: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case slug = "slug"
        case status = "status"
        case description = "description"
        case year = "year"
        case type = "type"
        case cover_photo = "cover_photo"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        slug = try values.decodeIfPresent(String.self, forKey: .slug)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        year = try values.decodeIfPresent(String.self, forKey: .year)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        cover_photo = try values.decodeIfPresent(String.self, forKey: .cover_photo)
    }
}
