//
//  AniVideo.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 03/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct AniVideo : Codable {
    let id: String?
    let provider: String?
    let file: String?
    let lang: String?
    let type: String?
    let thumbnail: String?
    let resolution: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case provider = "provider"
        case file = "file"
        case lang = "lang"
        case type = "type"
        case thumbnail = "thumbnail"
        case resolution = "resolution"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(String.self, forKey: .id)
        provider = try values.decodeIfPresent(String.self, forKey: .provider)
        file = try values.decodeIfPresent(String.self, forKey: .file)
        lang = try values.decodeIfPresent(String.self, forKey: .lang)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        thumbnail = try values.decodeIfPresent(String.self, forKey: .thumbnail)
        resolution = try values.decodeIfPresent(String.self, forKey: .resolution)
    }
}
