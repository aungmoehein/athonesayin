//
//  AniPopularList.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 03/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct AniList : Codable {
    let data: [AniData]?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent([AniData].self, forKey: .data)
    }
}
