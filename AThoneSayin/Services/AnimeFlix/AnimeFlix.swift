//
//  AnimeFlix.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 04/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta
import CoreData

class Animeflix: Service {
    static let sharedInstance: Animeflix = { Animeflix() } ()
    
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        let jsonDecoder = JSONDecoder()
        
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: UserDefaults.standard.string(forKey: UserDefaults.Keys.a_flix))
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
        self.configure("api/episodes/latest") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/anime/popular") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/anime/recent") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/search") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/episodes") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/episode") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configure("api/videos") {
            $0.pipeline[.parsing].removeTransformers()
        }
        
        self.configureTransformer("api/episodes/latest"){
            try jsonDecoder.decode(AniLatestList.self, from: $0.content)
        }

        self.configureTransformer("api/anime/popular"){
            try jsonDecoder.decode(AniList.self, from: $0.content)
        }
        
        self.configureTransformer("api/anime/recent"){
            try jsonDecoder.decode(AniList.self, from: $0.content)
        }
        
        self.configureTransformer("api/search"){
            try jsonDecoder.decode(AniList.self, from: $0.content)
        }
        
        self.configureTransformer("api/episodes"){
            try jsonDecoder.decode(AniEpisodeList.self, from: $0.content)
        }
        
        self.configureTransformer("api/episode"){
            try jsonDecoder.decode(AniLatestEpisode.self, from: $0.content)
        }
        
        self.configureTransformer("api/videos"){
            try jsonDecoder.decode([AniVideo].self, from: $0.content)
        }
    }
    
    var api: Resource { return resource("/api") }
    
    func getLatestAnime(page: Int) -> Resource {
        if page == 1 {
            return api.child("/episodes/latest").withParam("limit", "12")
        }else {
            return api.child("/episodes/latest").withParam("limit", "12").withParam("page", "\(page)")
        }
    }
    
    func getPopularAnime(page: Int) -> Resource {
        if page == 1 {
            return api.child("/anime/popular").withParam("limit", "12")
        }else {
            return api.child("/anime/popular").withParam("limit", "12").withParam("page", "\(page)")
        }
    }
    
    func getAnimeMovie(page: Int) -> Resource {
        if page == 1 {
            return api.child("/anime/popular").withParams(["limit": "12", "type": "Movie"])
        }else {
            return api.child("/anime/popular").withParams(["limit": "12", "type": "Movie"]).withParam("page", "\(page)")
        }
    }
    
    func getRecentAnime(page: Int) -> Resource {
        if page == 1 {
            return api.child("/anime/recent").withParam("limit", "12")
        }else {
            return api.child("/anime/recent").withParam("limit", "12").withParam("page", "\(page)")
        }
    }
    
    func getSearchResult(text: String) -> Resource {
        return api.child("/search").withParam("q", text)
    }
    
    func getAnimeEpisodes(id: Int, page: Int, sort: String) -> Resource {
        if page == 1 {
            return api.child("/episodes").withParams(["limit":"30", "anime_id":"\(id)", "sort":sort])
        }else {
            return api.child("/episodes").withParams(["limit":"30", "anime_id":"\(id)", "sort":sort]).withParam("page", "\(page)")
        }
    }
    
    func getLatestAnimeEpisode(episodeNo: String, slug: String) -> Resource {
        return api.child("/episode").withParams(["episode_num":episodeNo, "slug":slug])
    }
    
    func getAnimeVideo(episodeID: Int) -> Resource {
        return api.child("/videos").withParam("episode_id", "\(episodeID)")
    }
}
