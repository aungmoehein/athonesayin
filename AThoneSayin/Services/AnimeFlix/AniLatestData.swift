//
//  AniLatestData.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 03/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct AniLatestData : Codable {
    let id: Int?
    let title: String?
    let episode_num: String?
    let type: String?
    let created_at: String?
    let thumbnail: String?
    let anime: AniLatestAnime?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case episode_num = "episode_num"
        case type = "type"
        case created_at = "created_at"
        case thumbnail = "thumbnail"
        case anime = "anime"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        episode_num = try values.decodeIfPresent(String.self, forKey: .episode_num)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        thumbnail = try values.decodeIfPresent(String.self, forKey: .thumbnail)
        anime = try values.decodeIfPresent(AniLatestAnime.self, forKey: .anime)
    }
}
