//
//  AniLatestEpisode.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 05/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct AniLatestEpisodeData : Codable {
    let anime: AniData?
    
    enum CodingKeys: String, CodingKey {
        case anime = "anime"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        anime = try values.decodeIfPresent(AniData.self, forKey: .anime)
    }
}
