//
//  AniLatestEpisode.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 05/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct AniLatestEpisode : Codable {
    let data: AniLatestEpisodeData?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent(AniLatestEpisodeData.self, forKey: .data)
    }
}
