//
//  AniEpisode.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 03/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct AniEpisode : Codable {
    let id: Int?
    let title: String?
    let episode_num: String?
    let airing_date: String?
    let sub: Int?
    let dub: Int?
    let thumbnail: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case episode_num = "episode_num"
        case airing_date = "airing_date"
        case sub = "sub"
        case dub = "dub"
        case thumbnail = "thumbnail"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decodeIfPresent(Int.self, forKey: .id)
        title = try values.decodeIfPresent(String.self, forKey: .title)
        episode_num = try values.decodeIfPresent(String.self, forKey: .episode_num)
        airing_date = try values.decodeIfPresent(String.self, forKey: .airing_date)
        sub = try values.decodeIfPresent(Int.self, forKey: .sub)
        dub = try values.decodeIfPresent(Int.self, forKey: .dub)
        thumbnail = try values.decodeIfPresent(String.self, forKey: .thumbnail)
    }
}
