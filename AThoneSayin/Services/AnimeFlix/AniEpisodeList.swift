//
//  AniEpisodeList.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 03/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct AniEpisodeList : Codable {
    let data: [AniEpisode]?
    let anime: AniData?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case anime = "anime"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent([AniEpisode].self, forKey: .data)
        anime = try values.decodeIfPresent(AniData.self, forKey: .anime)
    }
}
