//
//  Analytic.swift
//  ATSY Cast
//
//  Created by Aung Moe Hein on 23/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta

class AnalyticService: Service {
    static let sharedInstance: AnalyticService = { AnalyticService() } ()
    
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: UserDefaults.standard.string(forKey: UserDefaults.Keys.kh_analytics))
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
    }
    var api: Resource { return resource("/api") }
    
    func sendAnalytic() -> Resource {
        return api.child("/analytics-" + UserDefaults.standard.string(forKey: UserDefaults.Keys.kh_analytics_version)! + ".php")
    }
}
