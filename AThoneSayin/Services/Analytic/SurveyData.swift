//
//  SurveyData.swift
//  ATSY Cast
//
//  Created by Aung Moe Hein on 24/06/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct SurveyData : Codable {
    let status: Int?
    
    enum CodingKeys: String, CodingKey {
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(Int.self, forKey: .status)
    }
}
