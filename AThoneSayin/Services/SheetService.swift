//
//  SheetService.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 29/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import Siesta

class SheetService: Service {
    static let sharedInstance: SheetService = { SheetService() } ()
    var envConfig = Configuration()
    let configuration = URLSessionConfiguration.default
    var authToken: String? {
        didSet {
            invalidateConfiguration()
            wipeResources()
        }
    }
    
    init() {
        configuration.allowsCellularAccess = true
        
        super.init(baseURL: "https://spreadsheets.google.com/feeds")
        
        SiestaLog.Category.enabled = SiestaLog.Category.common
        
    }
    var config: Resource { return resource("/list/" + envConfig.sheetNo) }
    var partnerList: Resource { return resource("/list/" + UserDefaults.standard.string(forKey: UserDefaults.Keys.partner_sheet_id)!) }
}

