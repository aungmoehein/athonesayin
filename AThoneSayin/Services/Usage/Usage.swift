//
//  Usage.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 09/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

class Usage
{
    var usage_id: Int = 0
    var usage_note: String = ""
    var usage_amount: Float = 0
    var category_id: Int = 0
    var usage_status: Int = 0
    var usage_time: String = ""
    var short_time: String = ""
    var created_time: String = ""
    var updated_time: String = ""
    
    init(usage_id:Int, usage_note:String, usage_amount:Float, category_id:Int, usage_status:Int, usage_time:String, short_time:String, created_time:String, updated_time:String)
    {
        self.usage_id = usage_id
        self.usage_note = usage_note
        self.usage_amount = usage_amount
        self.category_id = category_id
        self.usage_status = usage_status
        self.usage_time = usage_time
        self.short_time = short_time
        self.created_time = created_time
        self.updated_time = updated_time
    }
    
}
