//
//  Usage+DB.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 09/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

import SQLite3

extension DBHelper {
    func createUsageTable() {
        let createTableString = "CREATE TABLE IF NOT EXISTS usage(usage_id INTEGER PRIMARY KEY AUTOINCREMENT,usage_note TEXT,usage_amount DOUBLE,category_id INT, usage_status INT, usage_time TIMESTAMP, short_time TEXT, created_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, updated_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP);"
        var createTableStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, createTableString, -1, &createTableStatement, nil) == SQLITE_OK
        {
            if sqlite3_step(createTableStatement) == SQLITE_DONE
            {
                print("usage table created.")
            } else {
                print("usage table could not be created.")
            }
        } else {
            print("CREATE TABLE statement could not be prepared.")
        }
        sqlite3_finalize(createTableStatement)
    }
    
    func readAllUsage() -> [Usage] {
        let queryStatementString = "SELECT * FROM usage;"
        var queryStatement: OpaquePointer? = nil
        var usages : [Usage] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                usages.append(selectAllUsage(stmt: queryStatement!))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return usages
    }
    
    func readUsageByStatus_SDate(status: Int, short_date: String) -> ([Int], [Int], [Float], [String]) {
        let queryStatementString = "SELECT category_id, COUNT(category_id), SUM(usage_amount), short_time FROM usage WHERE usage_status = ? AND short_time LIKE ? GROUP BY category_id, usage_status, usage_time ORDER BY usage_time DESC;"
        var queryStatement: OpaquePointer? = nil
        var cat_id: [Int] = []
        var cat_id_count: [Int] = []
        var amount: [Float] = []
        var time: [String] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(status))
            sqlite3_bind_text(queryStatement, 2, ("%\(short_date)%" as NSString).utf8String, -1, nil)
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let category_id = sqlite3_column_int(queryStatement, 0)
                let category_count = sqlite3_column_int(queryStatement, 1)
                let usage_amount = sqlite3_column_double(queryStatement, 2)
                let short_time = String(describing: String(cString: sqlite3_column_text(queryStatement, 3)))
                
                cat_id.append(Int(category_id))
                cat_id_count.append(Int(category_count))
                amount.append(Float(usage_amount))
                time.append(short_time)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return (cat_id, cat_id_count, amount, time)
    }
    
    func readUsagesForDetail(status: Int, short_date: String, cid: Int) -> [Usage] {
        let queryStatementString = "SELECT * FROM usage WHERE usage_status = ? AND short_time = ? AND category_id = ? ORDER BY usage_time ASC;"
        var queryStatement: OpaquePointer? = nil
        var usages : [Usage] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(status))
            sqlite3_bind_text(queryStatement, 2, (short_date as NSString).utf8String, -1, nil)
            sqlite3_bind_int(queryStatement, 3, Int32(cid))
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                usages.append(selectAllUsage(stmt: queryStatement!))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return usages
    }
    
    func readUsagesForDashboard(status: Int, short_date: String) -> ([Int], [Float]) {
        let queryStatementString = "SELECT category_id, SUM(usage_amount) FROM usage WHERE usage_status = ? AND short_time LIKE ? GROUP BY category_id;"
        var queryStatement: OpaquePointer? = nil
        var cat_id: [Int] = []
        var amount: [Float] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(queryStatement, 1, Int32(status))
            sqlite3_bind_text(queryStatement, 2, ("%\(short_date)%" as NSString).utf8String, -1, nil)
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let category_id = sqlite3_column_int(queryStatement, 0)
                let usage_amount = sqlite3_column_double(queryStatement, 1)
                
                cat_id.append(Int(category_id))
                amount.append(Float(usage_amount))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return (cat_id, amount)
    }
    
    func readUsagesBetweenDates(fromDate: String, endDate: String) -> [Usage] {
        let queryStatementString = "SELECT * FROM usage WHERE usage_time BETWEEN ? AND ? ORDER BY usage_time ASC;"
        var queryStatement: OpaquePointer? = nil
        var usages : [Usage] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, (fromDate as NSString).utf8String, -1, nil)
            sqlite3_bind_text(queryStatement, 2, (endDate as NSString).utf8String, -1, nil)
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                usages.append(selectAllUsage(stmt: queryStatement!))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return usages
    }
    
    func readUsagesByDate(date: String) -> [Usage] {
        let queryStatementString = "SELECT * FROM usage WHERE short_time = ? ORDER BY usage_time ASC;"
        var queryStatement: OpaquePointer? = nil
        var usages : [Usage] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, (date as NSString).utf8String, -1, nil)
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                usages.append(selectAllUsage(stmt: queryStatement!))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return usages
    }
    
    func readUsageAmountsByDate(date: String) -> (Float, Float) {
        let queryStatementString = "SELECT (SELECT SUM(usage_amount) FROM usage WHERE usage_status = 0 AND short_time = ? GROUP BY usage_time), (SELECT SUM(usage_amount) FROM usage WHERE usage_status = 1 AND short_time = ? GROUP BY usage_time);"
        var queryStatement: OpaquePointer? = nil
        var expense: Float = 0
        var income: Float = 0
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, (date as NSString).utf8String, -1, nil)
            sqlite3_bind_text(queryStatement, 2, (date as NSString).utf8String, -1, nil)
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let status_0 = sqlite3_column_double(queryStatement, 0)
                let status_1 = sqlite3_column_double(queryStatement, 1)
                expense = Float(status_0)
                income = Float(status_1)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return (expense, income)
    }
    
    func readUsagesByMonth(month: String, status: Int, cid: Int) -> [Usage] {
        let queryStatementString = "SELECT * FROM usage WHERE short_time LIKE ? AND usage_status = ? AND category_id = ? ORDER BY usage_time ASC;"
        var queryStatement: OpaquePointer? = nil
        var usages : [Usage] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, ("%\(month)%" as NSString).utf8String, -1, nil)
            sqlite3_bind_int(queryStatement, 2, Int32(status))
            sqlite3_bind_int(queryStatement, 3, Int32(cid))
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                usages.append(selectAllUsage(stmt: queryStatement!))
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return usages
    }
    
    func readShortTimeBetweenDates(from: String, to: String) -> [String] {
        let queryStatementString = "SELECT short_time FROM usage WHERE usage_time BETWEEN ? AND ? GROUP BY short_time ORDER BY usage_time ASC;"
        var queryStatement: OpaquePointer? = nil
        var dates : [String] = []
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(queryStatement, 1, (from as NSString).utf8String, -1, nil)
            sqlite3_bind_text(queryStatement, 2, (to as NSString).utf8String, -1, nil)
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let date = String(describing: String(cString: sqlite3_column_text(queryStatement, 0)))
                dates.append(date)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return dates
    }
    
    func calcUsageAmountByDate(date: String) -> (Float, Float) {
        let queryStatementString = "SELECT (SELECT SUM(usage_amount) FROM usage WHERE usage_status = 1) AS income, (SELECT SUM(usage_amount) FROM usage WHERE usage_status = 0) AS expense;"
        var queryStatement: OpaquePointer? = nil
        var income: Float = 0
        var expense: Float = 0
        if sqlite3_prepare_v2(db, queryStatementString, -1, &queryStatement, nil) == SQLITE_OK {
//            sqlite3_bind_text(queryStatement, 1, ("%\(date)%" as NSString).utf8String, -1, nil)
//            sqlite3_bind_text(queryStatement, 2, ("%\(date)%" as NSString).utf8String, -1, nil)
            while sqlite3_step(queryStatement) == SQLITE_ROW {
                let inc = sqlite3_column_int(queryStatement, 0)
                let exp = sqlite3_column_int(queryStatement, 1)
                income = Float(inc)
                expense = Float(exp)
            }
        } else {
            print("SELECT statement could not be prepared")
        }
        sqlite3_finalize(queryStatement)
        return (income, expense)
    }
    
    func insertUsage(usage_note:String, usage_amount:Float, category_id:Int, usage_status:Int, usage_time:String, short_time:String) {
        let insertStatementString = "INSERT INTO usage (usage_note, usage_amount, category_id, usage_status, usage_time, short_time) VALUES (?, ?, ?, ?, ?, ?);"
        var insertStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, insertStatementString, -1, &insertStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(insertStatement, 1, (usage_note as NSString).utf8String, -1, nil)
            sqlite3_bind_double(insertStatement, 2, Double(usage_amount))
            sqlite3_bind_int(insertStatement, 3, Int32(category_id))
            sqlite3_bind_int(insertStatement, 4, Int32(usage_status))
            sqlite3_bind_text(insertStatement, 5, (usage_time as NSString).utf8String, -1, nil)
            sqlite3_bind_text(insertStatement, 6, (short_time as NSString).utf8String, -1, nil)
            
            if sqlite3_step(insertStatement) == SQLITE_DONE {
                print("Successfully inserted row.")
            } else {
                print("Could not insert row.")
            }
        } else {
            print("INSERT statement could not be prepared.")
        }
        sqlite3_finalize(insertStatement)
    }
    
    func updateUsage(usage_note:String, usage_amount:Float, category_id:Int, usage_status:Int, usage_time:String, short_time:String, usage_id: Int) {
        let updateStatementString = "UPDATE usage SET usage_note = ?, usage_amount = ?, category_id = ?, usage_status = ?, usage_time = ?, short_time = ?, created_time = CURRENT_TIMESTAMP WHERE usage_id = ?;"
        var updateStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, updateStatementString, -1, &updateStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(updateStatement, 1, (usage_note as NSString).utf8String, -1, nil)
            sqlite3_bind_double(updateStatement, 2, Double(usage_amount))
            sqlite3_bind_int(updateStatement, 3, Int32(category_id))
            sqlite3_bind_int(updateStatement, 4, Int32(usage_status))
            sqlite3_bind_text(updateStatement, 5, (usage_time as NSString).utf8String, -1, nil)
            sqlite3_bind_text(updateStatement, 6, (short_time as NSString).utf8String, -1, nil)
            sqlite3_bind_int(updateStatement, 7, Int32(usage_id))
            
            if sqlite3_step(updateStatement) == SQLITE_DONE {
                print("Successfully updated row.")
            } else {
                print("Could not update row.")
            }
        } else {
            print("UPDATE statement could not be prepared.")
        }
        sqlite3_finalize(updateStatement)
    }
    
    func deleteUsageById(usage_id:Int) {
        let deleteStatementStirng = "DELETE FROM usage WHERE usage_id = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(usage_id))
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
    }
    
    func deleteUsagesByCategoryId(category_id: Int) {
        let deleteStatementStirng = "DELETE FROM usage WHERE category_id = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_int(deleteStatement, 1, Int32(category_id))
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
    }
    
    func deleteUsageByStatus_SDate(status: Int, date: String, cid: Int) {
        let deleteStatementStirng = "DELETE FROM usage WHERE short_time = ? AND usage_status = ? AND category_id = ?;"
        var deleteStatement: OpaquePointer? = nil
        if sqlite3_prepare_v2(db, deleteStatementStirng, -1, &deleteStatement, nil) == SQLITE_OK {
            sqlite3_bind_text(deleteStatement, 1, (date as NSString).utf8String, -1, nil)
            sqlite3_bind_int(deleteStatement, 2, Int32(status))
            sqlite3_bind_int(deleteStatement, 3, Int32(cid))
            if sqlite3_step(deleteStatement) == SQLITE_DONE {
                print("Successfully deleted row.")
            } else {
                print("Could not delete row.")
            }
        } else {
            print("DELETE statement could not be prepared")
        }
        sqlite3_finalize(deleteStatement)
    }
    
    private func selectAllUsage(stmt: OpaquePointer) -> Usage {
        var usage: Usage
        let usage_id = sqlite3_column_int(stmt, 0)
        let usage_note = String(describing: String(cString: sqlite3_column_text(stmt, 1)))
        let usage_amount = sqlite3_column_double(stmt, 2)
        let category_id = sqlite3_column_int(stmt, 3)
        let usage_status = sqlite3_column_int(stmt, 4)
        let usage_time = String(describing: String(cString: sqlite3_column_text(stmt, 5)))
        let short_time = String(describing: String(cString: sqlite3_column_text(stmt, 6)))
        let created_time = String(describing: String(cString: sqlite3_column_text(stmt, 7)))
        let updated_time = String(describing: String(cString: sqlite3_column_text(stmt, 8)))
        usage = Usage(usage_id: Int(usage_id), usage_note: usage_note, usage_amount: Float(usage_amount), category_id: Int(category_id), usage_status: Int(usage_status), usage_time: usage_time, short_time: short_time, created_time: created_time, updated_time: updated_time)
        return usage
    }
}
