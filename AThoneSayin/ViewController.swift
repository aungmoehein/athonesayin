//
//  ViewController.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 07/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialBottomNavigation
import MaterialComponents.MaterialContainerScheme
import MaterialComponents.MaterialTypographyScheme

class ViewController: UITabBarController, UINavigationControllerDelegate, MDCBottomNavigationBarDelegate {
    
    var homeIcon, dashboardIcon, balanceIcon: String!
    let localizer = LocalizationSystem.Localizer
    var db: DBHelper!
    
    @objc var containerScheme: MDCContainerScheming = {
        let containerScheme = MDCContainerScheme()
        containerScheme.colorScheme.backgroundColor = .white
        containerScheme.colorScheme.primaryColor = UIColor(hex: Color.colorPrimary.rawValue)
        return containerScheme
    }()
    
    let bottomNavBar = MDCBottomNavigationBar()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.delegate = self
        addLeftBarIcon(name: Image.titleImage.rawValue)
        addRightBarButton()
        
        view.backgroundColor = containerScheme.colorScheme.backgroundColor
        homeIcon = LightIcons.homeIcon.rawValue
        dashboardIcon = LightIcons.dashboardIcon.rawValue
        balanceIcon = LightIcons.balanceIcon.rawValue
        
        UserDefaults.standard.set(false, forKey: UserDefaults.Keys.first_time)
        
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let reportView = Storyboard.instantiateViewController(withIdentifier: "report_pager_strip") as! ReportPagerStripViewController
        reportView.db = db
        if db != nil {
            self.viewControllers = [HomeModule().build(db: db), DashboardModule().build(db: db), reportView]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        createTabbarItem()
    }
    
    func addLeftBarIcon(name: String) {
        let logoImage = UIImage.init(named: name)
        let logoImageView = UIImageView.init(image: logoImage)
        logoImageView.frame = CGRect(x: 0.0, y:0.0, width: 100, height:40)
        logoImageView.contentMode = .scaleAspectFit
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant: 100)
        let heightConstraint = logoImageView.heightAnchor.constraint(equalToConstant: 40)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        navigationItem.leftBarButtonItem = imageItem
    }
    
    func addRightBarButton() {
        let setting = UIButton()
        let image = UIImage(named: "ic_setting")?.withRenderingMode(.alwaysTemplate)
        setting.setImage(image, for: .normal)
        setting.frame = CGRect(x: 0.0, y:0.0, width: 30, height:40)
        setting.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        setting.contentMode = .center
        setting.addTarget(self, action: #selector(settingAction), for: .touchUpInside)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: setting)
    }
    
    @objc func settingAction(sender: UIButton!) {
        let vc = SettingModule().build(db: db) as! SettingView
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func createTabbarItem() {
        let tabBarItem1 = UITabBarItem(title: localizer.localizedStringForKey(key: "Home", comment: ""), image: UIImage(named: homeIcon), tag: 0)
        
        let tabBarItem2 = UITabBarItem(title: localizer.localizedStringForKey(key: "Dashboard", comment: ""), image: UIImage(named: dashboardIcon), tag: 1)
        
        let tabBarItem3 = UITabBarItem(title: localizer.localizedStringForKey(key: "Balance", comment: ""), image: UIImage(named: balanceIcon), tag: 2)
        
        bottomNavBar.items = [ tabBarItem1, tabBarItem2, tabBarItem3 ]
        bottomNavBar.selectedItem = tabBarItem1
        bottomNavBar.delegate = self
        bottomNavBar.applyPrimaryTheme(withScheme: containerScheme)
        
        view.addSubview(bottomNavBar)
    }
    
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem){
        guard let fromView = selectedViewController?.view, let toView = customizableViewControllers?[item.tag].view else {
            return
        }

        if fromView != toView {
            UIView.transition(from: fromView, to: toView, duration: 0.2, options: [.transitionCrossDissolve], completion: nil)
        }
        self.selectedIndex = item.tag
    }
    
    func layoutBottomNavBar() {
      let size = bottomNavBar.sizeThatFits(view.bounds.size)
      var bottomNavBarFrame = CGRect(x: 0,
                                     y: view.bounds.height - size.height,
                                     width: size.width,
                                     height: size.height)
      if #available(iOS 11.0, *) {
        bottomNavBarFrame.size.height += view.safeAreaInsets.bottom
        bottomNavBarFrame.origin.y -= view.safeAreaInsets.bottom
      }
      bottomNavBar.frame = bottomNavBarFrame
    }

    override func viewWillLayoutSubviews() {
      super.viewWillLayoutSubviews()
      layoutBottomNavBar()
    }
    
    @available(iOS 11, *)
    override func viewSafeAreaInsetsDidChange() {
      super.viewSafeAreaInsetsDidChange()
      layoutBottomNavBar()
    }
}

