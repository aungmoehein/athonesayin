//
//  AppDelegate.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 07/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import CoreData
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    let localizer = LocalizationSystem.Localizer
    var db = DBHelper()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        #if DEBUG
            print("DEBUG MODE")
        #else
            print("RELEASE MODE")
            Analytics.setAnalyticsCollectionEnabled(true)
        #endif
        
        UserDefaults.standard.set(false, forKey: UserDefaults.Keys.password_enabled)
        UserDefaults.standard.register(defaults: [
            UserDefaults.Keys.apple_languages: "my-MM",
            UserDefaults.Keys.first_time: true,
            UserDefaults.Keys.atsycast_first_time: true,
            UserDefaults.Keys.default_category_count: 0,
            UserDefaults.Keys.is_secure: false,
            UserDefaults.Keys.passcode: "",
            UserDefaults.Keys.first_open: true,
            UserDefaults.Keys.password_enabled: false,
            UserDefaults.Keys.is_initially_landscape: false,
            UserDefaults.Keys.signin: false
        ])
        
        let isFirstTime = UserDefaults.standard.bool(forKey: UserDefaults.Keys.first_time)
        if isFirstTime {
            localizer.setSelectedLanguage(languageCode: "my-MM")
            insertDefaultCategories()
            UserDefaults.standard.set(db.readCategoryCount(), forKey: UserDefaults.Keys.default_category_count)
        }
        
        // localization -> addObserver
        NotificationCenter.default.addObserver(self, selector: #selector(self.onCurrentLocaleDidChange(notification:)), name: Notification.Name(Notification.Noti.language_did_change), object: nil)
        
        if #available(iOS 9.0, *){
            guard let window =  self.window else {return false}
            
            let is_secure = UserDefaults.standard.bool(forKey: UserDefaults.Keys.is_secure)
            if is_secure {
                let vc = PasscodeModule().build(db: db) as! PasscodeView
                vc.isLogin = true
                window.rootViewController = UINavigationController(rootViewController: vc)
                window.makeKeyAndVisible()
            }else {
                let Storyboard = UIStoryboard(name: "Main", bundle: nil)
                let view = Storyboard.instantiateViewController(withIdentifier: "host") as! ViewController
                view.db = db
                window.rootViewController = UINavigationController(rootViewController: view)
                window.makeKeyAndVisible()
            }
        }
        
        return true
    }
    
    func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
      if let rootViewController = self.topViewControllerWithRootViewController(rootViewController: window?.rootViewController) {
        if (rootViewController.responds(to: Selector(("canRotate")))) {
          // Unlock landscape view orientations for this view controller
          return .allButUpsideDown;
        }
      }
      
      // Only allow portrait (standard behaviour)
      return .portrait;
    }
    
    private func topViewControllerWithRootViewController(rootViewController: UIViewController!) -> UIViewController? {
      if (rootViewController == nil) { return nil }
      if (rootViewController.isKind(of: UITabBarController.self)) {
        return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UITabBarController).selectedViewController)
      } else if (rootViewController.isKind(of: UINavigationController.self)) {
        return topViewControllerWithRootViewController(rootViewController: (rootViewController as! UINavigationController).visibleViewController)
      } else if (rootViewController.presentedViewController != nil) {
        return topViewControllerWithRootViewController(rootViewController: rootViewController.presentedViewController)
      }
      return rootViewController
    }
    
    // localization -> onCurrentLocaleDidChange
    @objc func onCurrentLocaleDidChange(notification: Notification) {
        let selectedLanguage = notification.object as! String
        localizer.setSelectedLanguage(languageCode: selectedLanguage)
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    // MARK: - Core Data stack
    
        lazy var applicationDocumentsDirectory: URL = {
        // The directory the application uses to store the Core Data store file. This code uses a directory named "com.razeware.HitList" in the application's documents Application Support directory.
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    lazy var managedObjectModel: NSManagedObjectModel = {
        // The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
        let modelURL = Bundle.main.url(forResource: "ASoneTote", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true])
        } catch {
            // Report any error we got.
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            // Replace this with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        
        return coordinator
    }()
    
    
    lazy var managedObjectContext: NSManagedObjectContext = {
        // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        if managedObjectContext.hasChanges {
            do {
                try managedObjectContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    
    func insertDefaultCategories() {
        if let path = Bundle.main.path(forResource: "category_json", ofType: "json") {
            do {
                  let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                  let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [NSDictionary]{
                    jsonResult.forEach{ json in
                        let name = json.object(forKey: "category_name") as! String
                        let icon = json.object(forKey: "category_icon") as! String
                        let type = json.object(forKey: "category_type") as! Int
                        db.insertCategory(category_name: name, category_icon: icon, category_type: type)
                    }
                }
              } catch {
                   // handle error
              }
        }
    }
    
}

