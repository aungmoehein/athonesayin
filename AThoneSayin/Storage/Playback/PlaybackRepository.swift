//
//  PlaybackRepository.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 26/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import CoreData

class PlaybackRepository {
    
    var context: NSManagedObjectContext!
    
    init(context: NSManagedObjectContext) {
        self.context = context
    }
    
    func raw() -> Playback {
        return NSEntityDescription.insertNewObject(forEntityName: "Playback", into: context) as! Playback
    }
    
    func create(videoWrapper: VideoWrapper, position: Int, duration: Int) {
        let playBack = self.raw()
        playBack.id = Int64(videoWrapper.id)
        playBack.position = Int16(position)
        playBack.duration = Int16(duration)
        playBack.partner = videoWrapper.partner
        playBack.imageURL = videoWrapper.imageURL
        playBack.videoURL = videoWrapper.videoURL
        playBack.videoName = videoWrapper.videoName
        playBack.updatedAt = Date()
        
        self.persist()
    }
    
    func findVideo(id: Int, partner: String) -> [Playback]? {
        let request = NSFetchRequest<Playback>(entityName: "Playback")
        
        let idPredicate = NSPredicate(format: "id = %d", id)
        let partnerPredicate = NSPredicate(format: "partner = %@", partner)
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [idPredicate, partnerPredicate])
        
        do {
            let response = try context.fetch(request)
            let playBack: [Playback] = response
            
            return playBack
        } catch let error as NSError {
            debugPrint(error)
        }
        
        return nil
    }
    
    func findLiveVideo(name: String, partner: String) -> [Playback]? {
        let request = NSFetchRequest<Playback>(entityName: "Playback")
        
        let idPredicate = NSPredicate(format: "videoName = %@", name)
        let partnerPredicate = NSPredicate(format: "partner = %@", partner)
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [idPredicate, partnerPredicate])
        
        do {
            let response = try context.fetch(request)
            let playBack: [Playback] = response
            
            return playBack
        } catch let error as NSError {
            debugPrint(error)
        }
        
        return nil
    }
    
    func findPlayBackCategories() -> [String]? {
        let column = "partner"
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Playback")
        request.resultType = .dictionaryResultType
        request.returnsDistinctResults = true
        let sort = NSSortDescriptor(key: #keyPath(Playback.updatedAt), ascending: false)
        request.sortDescriptors = [sort]
        request.propertiesToFetch = [column]
        
        do {
            let response = try context.fetch(request) as? [[String: String]]
            let distinctValues = response?.compactMap { $0[column] }
            
            return distinctValues
        } catch let error as NSError {
            debugPrint(error)
        }
        
        return nil
    }
    
    func findPlayBacks(partner: String, count: Int) -> [Playback]? {
        let request = NSFetchRequest<Playback>(entityName: "Playback")
        
        request.fetchLimit = count
        let partnerPredicate = NSPredicate(format: "partner = %@", partner)
        request.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [partnerPredicate])
        let sort = NSSortDescriptor(key: #keyPath(Playback.updatedAt), ascending: false)
        request.sortDescriptors = [sort]
        
        do {
            let response = try context.fetch(request)
            let playBack: [Playback] = response
            
            return playBack
        } catch let error as NSError {
            debugPrint(error)
        }
        
        return nil
    }
    
    func delete(playBack: Playback) {
        context.delete(playBack)
        persist()
    }
    
    func deleteAll() {
        let delAllReqVar = NSBatchDeleteRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>(entityName: "Playback"))
        
        do {
            try context.execute(delAllReqVar)
        } catch {
            debugPrint(error)
        }
    }
    
    func persist(){
        do{
            try context.save()
        } catch let error as NSError {
            debugPrint(error)
        }
    }
}
