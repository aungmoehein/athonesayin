//
//  Playback+CoreDataProperties.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 26/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import CoreData

extension Playback {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Playback> {
        return NSFetchRequest<Playback>(entityName: "Playback")
    }
    
    @NSManaged public var id: Int64
    @NSManaged public var position: Int16
    @NSManaged public var duration: Int16
    @NSManaged public var updatedAt: Date
    @NSManaged public var imageURL: String
    @NSManaged public var partner: String
    @NSManaged public var videoURL: String
    @NSManaged public var videoName: String
}
