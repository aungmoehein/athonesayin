//
//  UsageDetailRouter.swift
//  UsageDetail
//
//  Created by Aung Moe Hein on 12/05/2020.
//

import Foundation
import UIKit

final class UsageDetailRouter: RouterInterface {

    weak var presenter: UsageDetailPresenterRouterInterface!

    weak var viewController: UIViewController?
    
    var db: DBHelper!
    init(db: DBHelper) {
        self.db = db
    }
}

extension UsageDetailRouter: UsageDetailRouterPresenterInterface {
    func goToAddView(id: Int, status: Int, cid: Int, amount: Float, date: String, note: String) {
        let vc = AddModule().build(db: db) as! AddView
        vc.modalPresentationStyle = .fullScreen
        vc.usageID = id
        vc.selectedSegment = status
        vc.cat_id = cid
        vc.amount = Int(amount).formattedWithSeparator
        vc.updatedDate = date
        vc.createdDate = date
        vc.shortNote = note
        vc.isUpdate = true
        self.viewController?.navigationController?.pushViewController(vc, animated: true)
//        viewController?.present(vc, animated: true, completion: nil)
    }
}
