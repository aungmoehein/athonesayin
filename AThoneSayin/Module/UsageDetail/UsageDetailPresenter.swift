//
//  UsageDetailPresenter.swift
//  UsageDetail
//
//  Created by Aung Moe Hein on 12/05/2020.
//

import Foundation

final class UsageDetailPresenter: PresenterInterface {

    var router: UsageDetailRouterPresenterInterface!
    var interactor: UsageDetailInteractorPresenterInterface!
    weak var view: UsageDetailViewPresenterInterface!

}

extension UsageDetailPresenter: UsageDetailPresenterRouterInterface {

}

extension UsageDetailPresenter: UsageDetailPresenterInteractorInterface {
    func onFetchUsagesByMonth(usages: [Usage]) {
        self.view.displayUsagesByMonth(usages: usages)
    }
    
    func onFetchCategoryById(category: Category) {
        self.view.displayCategoryById(category: category)
    }
    
    func onFetchUsagesDetail(usages: [Usage]) {
        self.view.displayUsages(usages: usages)
    }
}

extension UsageDetailPresenter: UsageDetailPresenterViewInterface {

    func start() {

    }
    
    func deleteCategoryById(id: Int) {
        interactor.deleteUsageById(id: id)
    }
    
    func getCategoryById(id: Int) {
        interactor.fetchCategoryById(id: id)
    }
    
    func getUsagesDetail(status: Int, short_date: String, cid: Int) {
        self.interactor.fetchUsagesDetail(status: status, short_date: short_date, cid: cid)
    }
    
    func getUsagesByMonth(month: String, status: Int, cid: Int) {
        self.interactor.fetchUsagesByMonth(month: month, status: status, cid: cid)
    }
    
    func addView(id: Int, status: Int, cid: Int, amount: Float, date: String, note: String) {
        self.router.goToAddView(id: id, status: status, cid: cid, amount: amount, date: date, note: note)
    }

}
