//
//  UsageDetailView.swift
//  UsageDetail
//
//  Created by Aung Moe Hein on 12/05/2020.
//

import Foundation
import UIKit
import PopupDialog
import Firebase

final class UsageDetailView: UIViewController, ViewInterface, UINavigationControllerDelegate {
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var detailTable: UITableView!
    @IBOutlet weak var lbTotal: UILabel!
    @IBOutlet weak var imgTotal: UIImageView!
    
    var localizer = LocalizationSystem.Localizer
    var presenter: UsageDetailPresenterViewInterface!
    
    var showMsgDialog: PopupDialog!
    
    var category: Category! {
        didSet {
            addNavBarItems(icon: self.category.category_icon, title: localizer.localizedStringForKey(key: self.category.category_name, comment: ""), date: String(self.date.split(separator: ",")[0]))
        }
    }
    
    var status: Int!
    var cat_id: Int!
    var date: String!
    var totalAmount: Float = 0
    var viaDashboard: Bool = false

    var usages: [Usage] = [] {
        didSet {
            detailTable.reloadData()
            
            var total: Float = 0
            usages.forEach{usage in
                total += usage.usage_amount
            }
            lbTotal.text = String(format: localizer.localizedStringForKey(key: "total_usage_amount", comment: ""), "\(Int(total).formattedWithSeparator)")
            
            if usages.count == 0 {
                navigationController?.popViewController(animated: true)
                dismiss(animated: true, completion: nil)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
        
        let image = UIImage(named: "ic_document")?.withRenderingMode(.alwaysTemplate)
        imgTotal.image = image
        imgTotal.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        
        footerView.layer.shadowColor = UIColor.gray.cgColor
        footerView.layer.shadowOffset = CGSize(width: 0, height: -2)
        footerView.layer.shadowOpacity = 0.7
        footerView.layer.shadowRadius = 2.0
        
        detailTable.register(UINib(nibName: "UsageDetailCell", bundle: nil), forCellReuseIdentifier: "usage_detail_cell")
        detailTable.dataSource = self
        detailTable.delegate = self
        detailTable.reloadData()

        self.presenter.start()
        Analytics.logEvent(Events.USAGE_DETAIL_SCREEN, parameters: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.presenter.getCategoryById(id: cat_id!)
        if viaDashboard {
            self.presenter.getUsagesByMonth(month: date, status: status!, cid: cat_id!)
        }else {
            self.presenter.getUsagesDetail(status: status!, short_date: date!, cid: cat_id!)
        }
        
        if usages.count == 0 {
            navigationController?.popViewController(animated: true)
            dismiss(animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.navigationController!.viewControllers.count > 1
    }
    
    func addNavBarItems(icon: String, title: String, date: String) {
        let categoryIcon = UIButton()
        categoryIcon.frame = CGRect(x: 0.0, y:0.0, width: 40, height:40)
        categoryIcon.contentEdgeInsets = UIEdgeInsets(top: 10,left: 10,bottom: 10,right: 10)
        categoryIcon.setImage(UIImage(named: icon)?.withRenderingMode(.alwaysTemplate), for: .normal)
        categoryIcon.makeRounded()
        categoryIcon.contentMode = .center
        categoryIcon.backgroundColor = UIColor.white
        categoryIcon.tintColor = UIColor(hex: Color.colorAccent.rawValue)
        categoryIcon.layer.shadowColor = UIColor.gray.cgColor
        categoryIcon.layer.shadowOffset = CGSize(width: 0, height: 0)
        categoryIcon.layer.shadowOpacity = 0.7
        categoryIcon.layer.shadowRadius = 2.0
        categoryIcon.layer.masksToBounds = false
        categoryIcon.isUserInteractionEnabled = false
        let imageItem = UIBarButtonItem.init(customView: categoryIcon)
        let widthConstraint = categoryIcon.widthAnchor.constraint(equalToConstant: 40)
        let heightConstraint = categoryIcon.heightAnchor.constraint(equalToConstant: 40)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        
        self.navigationItem.title = title
        
        let rightLabel = UILabel()
        rightLabel.text = date
        rightLabel.font = .systemFont(ofSize: 15)
        
        let dateItem = UIBarButtonItem.init(customView: rightLabel)
        
        navigationItem.rightBarButtonItem = dateItem
        navigationItem.leftBarButtonItem = imageItem
    }
}

extension UsageDetailView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension UsageDetailView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "usage_detail_cell", for: indexPath) as! UsageDetailCell
        
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        
        if status == Parameter.INCOME {
            cell.lbAmount.textColor = UIColor(hex: Color.colorPrimary.rawValue)
            cell.redCircle.backgroundColor = UIColor(hex: Color.colorAccent.rawValue)
        }
        
        cell.amount = usages[indexPath.row].usage_amount.formattedWithSeparator
        cell.time = usages[indexPath.row].created_time.toDate()?.timeAgoDisplay()
        cell.note = usages[indexPath.row].usage_note
        
        if viaDashboard {
            cell.time = usages[indexPath.row].short_time
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !viaDashboard {
            let date = usages[indexPath.row].short_time
            let selectedYear = date.split(separator: ",")[1]
            let selectedMonth = date.split(separator: ",")[0].split(separator: " ")[1]
            let selectedDay = date.split(separator: ",")[0].split(separator: " ")[0]
            let short_time = "\(selectedMonth) \(selectedDay),\(selectedYear)"
            
            presenter.addView(id: usages[indexPath.row].usage_id, status: status!, cid: cat_id!, amount: usages[indexPath.row].usage_amount, date: short_time, note: usages[indexPath.row].usage_note)
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
                if !viaDashboard {
                    let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                        self.showMsg(msg: "အချက်အလက်များ ဖျက်ရန်သေချာပါသလား။", tableView: tableView, indexPath: indexPath)
                        
                        completionHandler(true)
                    }
                    if #available(iOS 13.0, *) {
                        deleteAction.image = UIImage(systemName: "trash")
                    } else {
                        deleteAction.image = UIImage(named: "ic_trash")
                    }
                    deleteAction.backgroundColor = .systemRed
                    
                    let editAction = UIContextualAction(style: .normal, title: nil) { (_, _, completionHandler)
                        in
                        if !self.viaDashboard {
                            let date = self.usages[indexPath.row].short_time
                            let selectedYear = date.split(separator: ",")[1]
                            let selectedMonth = date.split(separator: ",")[0].split(separator: " ")[1]
                            let selectedDay = date.split(separator: ",")[0].split(separator: " ")[0]
                            let short_time = "\(selectedMonth) \(selectedDay),\(selectedYear)"
                            
                            self.presenter.addView(id: self.usages[indexPath.row].usage_id, status: self.status!, cid: self.cat_id!, amount: self.usages[indexPath.row].usage_amount, date: short_time, note: self.usages[indexPath.row].usage_note)
                        }
                        
                        completionHandler(true)
                    }
                    if #available(iOS 13.0, *) {
                        editAction.image = UIImage(systemName: "square.and.pencil")
                    } else {
                        editAction.image = UIImage(named: "ic_pen_box")
                    }
                    editAction.backgroundColor = .systemYellow
                    
                    let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
                    configuration.performsFirstActionWithFullSwipe = false
                    return configuration
                }
                return nil
    }
    
    private func showMsg(msg: String, tableView: UITableView, indexPath: IndexPath) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
            self.presenter.deleteCategoryById(id: self.usages[indexPath.row].usage_id)
            self.usages.remove(at: indexPath.row)
            tableView.reloadData()
            self.presenter.getUsagesDetail(status: self.status!, short_date: self.date!, cid: self.cat_id!)
        }
        let buttonCancel = CancelButton(title: NSLocalizedString("Cancel", comment: "")) {
        }
        showMsgDialog.addButtons([buttonOk, buttonCancel])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}

extension UsageDetailView: UsageDetailViewPresenterInterface {
    func displayCategoryById(category: Category) {
        self.category = category
    }
    
    func displayUsages(usages: [Usage]) {
        self.usages = usages
    }
    
    func displayUsagesByMonth(usages: [Usage]) {
        self.usages = usages
    }
}
