//
//  UsageDetailModule.swift
//  UsageDetail
//
//  Created by Aung Moe Hein on 12/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol UsageDetailRouterPresenterInterface: RouterPresenterInterface {
    func goToAddView(id: Int, status: Int, cid: Int, amount: Float, date: String, note: String)
}

// MARK: - presenter

protocol UsageDetailPresenterRouterInterface: PresenterRouterInterface {

}

protocol UsageDetailPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchUsagesDetail(usages: [Usage])
    func onFetchUsagesByMonth(usages: [Usage])
    func onFetchCategoryById(category: Category)
}

protocol UsageDetailPresenterViewInterface: PresenterViewInterface {
    func start()
    func addView(id: Int, status: Int, cid: Int, amount: Float, date: String, note: String)
    func getCategoryById(id: Int)
    func getUsagesDetail(status: Int, short_date: String, cid: Int)
    func getUsagesByMonth(month: String, status: Int, cid: Int)
    func deleteCategoryById(id: Int)
}

// MARK: - interactor

protocol UsageDetailInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchCategoryById(id: Int)
    func fetchUsagesDetail(status: Int, short_date: String, cid: Int)
    func fetchUsagesByMonth(month: String, status: Int, cid: Int)
    func deleteUsageById(id: Int)
}

// MARK: - view

protocol UsageDetailViewPresenterInterface: ViewPresenterInterface {
    func displayCategoryById(category: Category)
    func displayUsages(usages: [Usage])
    func displayUsagesByMonth(usages: [Usage])
}


// MARK: - module builder

final class UsageDetailModule: ModuleInterface {

    typealias View = UsageDetailView
    typealias Presenter = UsageDetailPresenter
    typealias Router = UsageDetailRouter
    typealias Interactor = UsageDetailInteractor

    func build(db: DBHelper, status: Int, date: String, cid: Int) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "usage_detail") as! View
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router(db: db)

        view.status = status
        view.date = date
        view.cat_id = cid
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
