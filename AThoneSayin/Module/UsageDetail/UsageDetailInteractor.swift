//
//  UsageDetailInteractor.swift
//  UsageDetail
//
//  Created by Aung Moe Hein on 12/05/2020.
//

import Foundation

final class UsageDetailInteractor: InteractorInterface {

    weak var presenter: UsageDetailPresenterInteractorInterface!
    var db: DBHelper!
    
    init(db: DBHelper) {
        self.db = db
    }
}

extension UsageDetailInteractor: UsageDetailInteractorPresenterInterface {
    func fetchUsagesDetail(status: Int, short_date: String, cid: Int) {
        self.presenter.onFetchUsagesDetail(usages: db.readUsagesForDetail(status: status, short_date: short_date, cid: cid))
    }
    
    func fetchUsagesByMonth(month: String, status: Int, cid: Int) {
        self.presenter.onFetchUsagesByMonth(usages: db.readUsagesByMonth(month: month, status: status, cid: cid))
    }
    
    func fetchCategoryById(id: Int) {
        self.presenter.onFetchCategoryById(category: db.readCategoryById(id: id))
    }
    
    func deleteUsageById(id: Int) {
        db.deleteUsageById(usage_id: id)
    }
}
