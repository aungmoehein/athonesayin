//
//  SettingRouter.swift
//  Setting
//
//  Created by Aung Moe Hein on 15/05/2020.
//

import Foundation
import UIKit

final class SettingRouter: RouterInterface {

    weak var presenter: SettingPresenterRouterInterface!

    weak var viewController: UIViewController?
    
    var db: DBHelper!
    init(db: DBHelper) {
        self.db = db
    }
}

extension SettingRouter: SettingRouterPresenterInterface {
    func goToEditCategory() {
        let vc = EditCategoryModule().build(db: db) as! EditCategoryView
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToEditRecentlyUsed() {
        let vc = EditRecentlyUsedModule().build(db: db) as! EditRecentlyUsedView
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToDonate() {
        let vc = DonateModule().build() as! DonateView
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToATSYCast() {
        let view = WelcomeModule().build() as! WelcomeView
        UIApplication.shared.windows.first?.rootViewController = UINavigationController(rootViewController: view)
        UIView.transition(with: UIApplication.shared.windows.first!, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion:
        { completed in
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        })
    }
}
