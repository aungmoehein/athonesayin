//
//  SettingView.swift
//  Setting
//
//  Created by Aung Moe Hein on 15/05/2020.
//

import Foundation
import PopupDialog
import UIKit
import Firebase
import NVActivityIndicatorView

final class SettingView: UITableViewController, ViewInterface, UINavigationControllerDelegate, NVActivityIndicatorViewable {

    var presenter: SettingPresenterViewInterface!
    var setting: [[[String]]] = []
    let Login: [[String]] = [["Login"], ["Aung Moe Hein"]]
    let Account: [[String]] = [["Account"], ["Aung Moe Hein"]]
    let General: [[String]] = [["General"], ["Password Protection", "လျှို့ဝှက်နံပါတ်ဖြင့်ထားမည်", "ic_lock"], ["Category List", "သုံးစွဲမှု အမျိုးအစားများပြင်ဆင်မည်", "ic_category_list"], ["Daily Activity", "နေ့စဥ် အသုံးပြုမှုများ", "ic_activity"], ["Check Update", "ဆော့ဖ်ဝဲလ်ဗားရှင်းအသစ် စစ်ဆေးမည်", "ic_update"], ["Change Language", "ဘာသာစကားပြောင်းမည်", "ic_language"]]
    let AtsyCast: [[String]] = [["ATSY Cast"],["ATSY Cast ကြည့်ရန်", "ic_tv"]]
    let About: [[String]] = [["ကျွန်ုပ်တို့အကြောင်း"], ["Version", "ic_info"]]
    let Donate: [[String]] = [["Donate"], ["Buy me a cup of coffee", "ကော်ဖီတိုက်နိုင်ပါသည်", "ic_donate"]]
    let Develop: [[String]] = [["Developed by"], ["Aung Moe Hein", "ic_dev"]]
    
    var db: DBHelper!
    let localizer = LocalizationSystem.Localizer
    var showMsgDialog: PopupDialog!
    
    var version: String = "" {
        didSet {
            self.stopAnimating()
            let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            showMsgDialog(msg: self.version > currentVersion ? "Update Available (Version: \(self.version))" : "No new update.", isUpdate: self.version > currentVersion)
        }
    }
    
    var downloadUrl: String = ""
    
    var is_Secure: Bool = false {
        didSet{
            tableView.reloadData()
        }
    }
    
    var atsyCastAccess: String = "" {
        didSet {
            self.stopAnimating()
            if UserDefaults.standard.string(forKey: UserDefaults.Keys.give_access)! == "TRUE" {
                self.presenter.atsyCastView()
            }else {
                self.showMsgDialog(msg: "လက်ရှိကြည့်ရှု၍ မရနိုင်သေးပါ။ ကြည့်ရှုရန် Developer ထံတွင် Form ဖြည့်ပါ။", isUpdate: false)
            }
        }
    }
    
    private var fullName: String = ""
    private var email: String = ""


    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        setting.append(Login)
        setting.append(Account)
        setting.append(General)
        setting.append(AtsyCast)
        setting.append(About)
        setting.append(Donate)
        setting.append(Develop)
        
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
        self.navigationItem.title = "Setting"
        self.navigationController?.navigationBar.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        
        tableView.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "setting_cell")
        tableView.register(UINib(nibName: "LoginCell", bundle: nil), forCellReuseIdentifier: "login_cell")
        tableView.register(UINib(nibName: "AccountInfoCell", bundle: nil), forCellReuseIdentifier: "account_info_cell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()

        self.presenter.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        is_Secure = UserDefaults.standard.bool(forKey: UserDefaults.Keys.is_secure)

        tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.navigationController!.viewControllers.count > 1
    }
    
    func changeLanguage() {
        // localization
        let titleText = localizer.localizedStringForKey(key: "Change Language", comment: "")
        let myanmarText = localizer.localizedStringForKey(key: "Myanmar", comment: "")
        let englishText = localizer.localizedStringForKey(key: "English", comment: "")
        
        let uiAlert = UIAlertController(title: titleText, message: nil, preferredStyle: .alert)
        
        uiAlert.addAction(
            UIAlertAction(
                title: myanmarText,
                style: .default,
                handler: {_ in self.forceChangeLanguage(selectedLanguage: "my-MM")}
        ))
        
        uiAlert.addAction(
            UIAlertAction(
                title: englishText,
                style: .default,
                handler: {_ in self.forceChangeLanguage(selectedLanguage: "en")}
        ))
        
        uiAlert.addAction(
            UIAlertAction(
                title: "Cancel",
                style: .cancel,
                handler: nil
        ))
        
        uiAlert.pruneNegativeWidthConstraints()
        self.present(uiAlert, animated: true, completion: nil)
    }

    func forceChangeLanguage(selectedLanguage: String) {
        NotificationCenter.default.post(name: Notification.Name(Notification.Noti.language_did_change), object: selectedLanguage)
    }
    
    private func showMsgDialog(msg: String, isUpdate: Bool) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        var confirmButtons: [PopupDialogButton] = []
        
        if isUpdate {
            let buttonUpdate = DefaultButton(title: NSLocalizedString("Update", comment: "")) {
                UIApplication.shared.open(URL(string: self.downloadUrl)!, options: [:], completionHandler: nil)
            }
            
            let buttonLater = CancelButton(title: NSLocalizedString("Later", comment: "")) {
                
            }
            
            confirmButtons.append(buttonUpdate)
            confirmButtons.append(buttonLater)
        }else {
            let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
                
            }
            confirmButtons.append(buttonOk)
        }
        
        showMsgDialog.addButtons(confirmButtons)
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    @IBAction func btnLogin(_ sender: UIButton) {
        let popUp = SigninDialog(nibName: "SigninDialog", bundle: nil)
        popUp.appleCredentialDelegate = self
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}

extension SettingView {
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return setting[section].count - 1
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return setting.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "login_cell", for: indexPath) as! LoginCell
            cell.selectionStyle = .none
            cell.credentialView.isHidden = !UserDefaults.standard.bool(forKey: UserDefaults.Keys.signin)
            cell.btnLogin.isHidden = UserDefaults.standard.bool(forKey: UserDefaults.Keys.signin)
            cell.name = fullName
            cell.email = email
            cell.btnLogin.addTarget(self, action: #selector(btnLogin(_:)), for: .touchUpInside)
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "account_info_cell", for: indexPath) as! AccountInfoCell
            cell.selectionStyle = .none
            cell.vc = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "setting_cell", for: indexPath) as! SettingCell
            cell.selectionStyle = .none
            cell.vc = self
            cell.lbSecondaryTitle.isHidden = false
            cell.title = setting[indexPath.section][indexPath.row + 1][0]
            cell.secondaryTitle = setting[indexPath.section][indexPath.row + 1][1]
            cell.icon_name = setting[indexPath.section][indexPath.row + 1][2]
            cell.db = db
            cell.passwordEnable.setOn(is_Secure, animated: false)
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "setting_cell", for: indexPath) as! SettingCell
            cell.selectionStyle = .none
            cell.lbSecondaryTitle.isHidden = false
            cell.title = setting[indexPath.section][indexPath.row + 1][0]
            cell.secondaryTitle = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            cell.icon_name = setting[indexPath.section][indexPath.row + 1][1]
            return cell
        case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "setting_cell", for: indexPath) as! SettingCell
            cell.selectionStyle = .none
            cell.vc = self
            cell.title = setting[indexPath.section][indexPath.row + 1][0]
            cell.lbSecondaryTitle.isHidden = false
            cell.secondaryTitle = setting[indexPath.section][indexPath.row + 1][1]
            cell.icon_name = setting[indexPath.section][indexPath.row + 1][2]
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "setting_cell", for: indexPath) as! SettingCell
            cell.selectionStyle = .none
            cell.lbSecondaryTitle.isHidden = true
            cell.title = setting[indexPath.section][indexPath.row + 1][0]
            cell.icon_name = setting[indexPath.section][indexPath.row + 1][1]
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 2:
            switch indexPath.row {
            case 0:
                print("Password")
            case 1:
                presenter.editCategory()
            case 2:
                presenter.editRecentlyUsed()
            case 3:
                Analytics.logEvent(Events.CHECK_UPDATE, parameters: nil)
                self.startAnimating()
                presenter.getVersion()
            default:
                Analytics.logEvent(Events.CHANGE_LANGUAGE, parameters: nil)
                self.changeLanguage()
            }
        case 3:
            self.startAnimating()
            Analytics.logEvent(Events.BTN_WATCH_ATSYCAST, parameters: nil)
            self.presenter.getATSYAccess()
        case 5:
            switch indexPath.row {
            case 0:
                presenter.donateView()
            default:
                break
            }
        case 6:
            switch indexPath.row {
            case 0:
                Analytics.logEvent(Events.FACEBOOK_CHECK, parameters: nil)
                if UIApplication.shared.canOpenURL(URL(string: "fb://profile/100000983343830")!) {
                    UIApplication.shared.open(URL(string: "fb://profile/100000983343830")!, options: [:])
                } else {
                    UIApplication.shared.open(URL(string: "https://www.facebook.com/aung.moehein.330")!, options: [:])
                }
            default:
                break
            }
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 20, y: 8, width: 320, height: 20)
        myLabel.font = UIFont.systemFont(ofSize: 18)
        myLabel.textColor = UIColor(hex: Color.colorAccent.rawValue)
        myLabel.text = self.tableView(tableView, titleForHeaderInSection: section)

        let headerView = UIView()
        if #available(iOS 13.0, *) {
            headerView.backgroundColor = .systemBackground
        } else {
            headerView.backgroundColor = .white
        }
        headerView.addSubview(myLabel)

        return headerView
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return setting[section][0][0]
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}

extension SettingView: AppleCredentialDelegate {
    func finishedSignin(userIdentifier: String, fullName: String, email: String) {
        self.fullName = fullName
        self.email = email
        tableView.reloadData()
    }
}

extension SettingView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension SettingView: SettingViewPresenterInterface {
    func displayVersion(version: String, url: String) {
        self.version = version
        self.downloadUrl = url
    }
    
    func displayATSYCastAccess(access: String) {
        self.atsyCastAccess = access
    }
    
    func connectionError() {
        self.stopAnimating()
        self.showMsgDialog(msg: "Check your internet connection.", isUpdate: false)
    }
}
