//
//  SettingInteractor.swift
//  Setting
//
//  Created by Aung Moe Hein on 15/05/2020.
//

import Foundation
import UIKit

final class SettingInteractor: InteractorInterface {

    weak var presenter: SettingPresenterInteractorInterface!
}

extension SettingInteractor: SettingInteractorPresenterInterface {
    func getVersion() {
        SheetService.sharedInstance.partnerList.child("/1/public/values").withParam("alt", "json").load()
            .onSuccess(){data in
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        jsonDictionary.forEach() {
                            json in
                            let type = json["type"]!
                            let version = json["versionname"]!
                            let url = json["downloadurl"]!
                            
                            if type == "ios" {
                                self.presenter.onFetchVersion(version: version, url: url)
                            }
                        }
                    }catch {
                        debugPrint(error)
                    }
                }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
    
    func fetchVersion() {
        UserDefaults.standard.set("", forKey: UserDefaults.Keys.partner_sheet_id)
        SheetService.sharedInstance.config.child("/1/public/values").withParam("alt", "json").load()
            .onSuccess(){data in
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        jsonDictionary.forEach() {
                            json in
                            let sheet = json["sheet"]!
                            
                            UserDefaults.standard.set(sheet, forKey: UserDefaults.Keys.partner_sheet_id)
                        }
                    } catch {
                        // Handle error
                        print(error)
                    }
                   
                    self.getVersion()
                }
        }.onFailure(){
            error in
            self.presenter.onFetchError()
            debugPrint(error.userMessage)
        }
    }
    
    func fetchDeviceID() {
        SheetService.sharedInstance.config.child("/3/public/values").withParam("alt", "json").load()
            .onSuccess(){data in
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        UserDefaults.standard.set("FALSE", forKey: UserDefaults.Keys.give_access)
                        UserDefaults.standard.set("FALSE", forKey: UserDefaults.Keys.device_adult)
                        UserDefaults.standard.set("FALSE", forKey: UserDefaults.Keys.all_channel)
                        UserDefaults.standard.set("FALSE", forKey: UserDefaults.Keys.anime_access)
                        UserDefaults.standard.set("letmein", forKey: UserDefaults.Keys.password_string)
                        UserDefaults.standard.set("", forKey: UserDefaults.Keys.user_name)
                        UserDefaults.standard.set("FALSE", forKey: UserDefaults.Keys.has_donation)
                        print("DEVICE: ", String(describing: UIDevice.current.identifierForVendor!.uuidString))
                        jsonDictionary.forEach() {
                            json in
                            let name = json["name"]!
                            let access = json["partners"]!
                            let device = json["device"]!
                            let adult = json["adult"]!
                            let all_channel = json["allchannel"]!
                            let anime = json["anime"]!
                            let password = json["password"]!
                            let donation = json["donation"]!
                            
                            if device == String(describing: UIDevice.current.identifierForVendor!.uuidString) {
                                UserDefaults.standard.set(name, forKey: UserDefaults.Keys.user_name)
                                UserDefaults.standard.set(all_channel, forKey: UserDefaults.Keys.all_channel)
                                UserDefaults.standard.set(adult, forKey: UserDefaults.Keys.device_adult)
                                UserDefaults.standard.set(access, forKey: UserDefaults.Keys.give_access)
                                UserDefaults.standard.set(anime, forKey: UserDefaults.Keys.anime_access)
                                UserDefaults.standard.set(donation, forKey: UserDefaults.Keys.has_donation)
                                if adult == "FALSE" {
                                    UserDefaults.standard.set(false, forKey: UserDefaults.Keys.password_enabled)
                                }
                                if password != "" {
                                    UserDefaults.standard.set(password, forKey: UserDefaults.Keys.password_string)
                                }
                            }
                        }
                        
                        self.presenter.onFetchATSYCastAccess(access: UserDefaults.standard.string(forKey: UserDefaults.Keys.give_access)!)
                    } catch {
                        // Handle error
                        print(error)
                    }
                }
        }.onFailure(){
            error in
            self.presenter.onFetchError()
            debugPrint(error.userMessage)
        }
    }
}
