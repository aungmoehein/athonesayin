//
//  SettingPresenter.swift
//  Setting
//
//  Created by Aung Moe Hein on 15/05/2020.
//

import Foundation

final class SettingPresenter: PresenterInterface {

    var router: SettingRouterPresenterInterface!
    var interactor: SettingInteractorPresenterInterface!
    weak var view: SettingViewPresenterInterface!

}

extension SettingPresenter: SettingPresenterRouterInterface {
    func editCategory() {
        router.goToEditCategory()
    }
    
    func editRecentlyUsed() {
        router.goToEditRecentlyUsed()
    }
    
    func donateView() {
        router.goToDonate()
    }
    
    func atsyCastView() {
        router.goToATSYCast()
    }
}

extension SettingPresenter: SettingPresenterInteractorInterface {
    func onFetchVersion(version: String, url: String) {
        view.displayVersion(version: version, url: url)
    }
    
    func onFetchATSYCastAccess(access: String) {
        view.displayATSYCastAccess(access: access)
    }
    
    func onFetchError() {
        view.connectionError()
    }
}

extension SettingPresenter: SettingPresenterViewInterface {
    func start() {

    }
    
    func getVersion() {
        self.interactor.fetchVersion()
    }
    
    func getATSYAccess() {
        self.interactor.fetchDeviceID()
    }
}
