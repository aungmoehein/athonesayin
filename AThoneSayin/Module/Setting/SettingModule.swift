//
//  SettingModule.swift
//  Setting
//
//  Created by Aung Moe Hein on 15/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol SettingRouterPresenterInterface: RouterPresenterInterface {
    func goToEditCategory()
    func goToEditRecentlyUsed()
    func goToDonate()
    func goToATSYCast()
}

// MARK: - presenter

protocol SettingPresenterRouterInterface: PresenterRouterInterface {
    
}

protocol SettingPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchVersion(version: String, url: String)
    func onFetchATSYCastAccess(access: String)
    func onFetchError()
}

protocol SettingPresenterViewInterface: PresenterViewInterface {
    func start()
    func editCategory()
    func editRecentlyUsed()
    func getVersion()
    func getATSYAccess()
    func donateView()
    func atsyCastView()
}

// MARK: - interactor

protocol SettingInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchVersion()
    func fetchDeviceID()
}

// MARK: - view

protocol SettingViewPresenterInterface: ViewPresenterInterface {
    func displayVersion(version: String, url: String)
    func displayATSYCastAccess(access: String)
    func connectionError()
}


// MARK: - module builder

final class SettingModule: ModuleInterface {

    typealias View = SettingView
    typealias Presenter = SettingPresenter
    typealias Router = SettingRouter
    typealias Interactor = SettingInteractor

    func build(db: DBHelper) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "setting") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router(db: db)

        view.db = db
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
