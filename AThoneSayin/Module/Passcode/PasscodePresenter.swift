//
//  PasscodePresenter.swift
//  Passcode
//
//  Created by Aung Moe Hein on 17/05/2020.
//

import Foundation

final class PasscodePresenter: PresenterInterface {

    var router: PasscodeRouterPresenterInterface!
    var interactor: PasscodeInteractorPresenterInterface!
    weak var view: PasscodeViewPresenterInterface!

}

extension PasscodePresenter: PasscodePresenterRouterInterface {

}

extension PasscodePresenter: PasscodePresenterInteractorInterface {

}

extension PasscodePresenter: PasscodePresenterViewInterface {

    func start() {

    }
    
    func home() {
        router.goToHome()
    }

}
