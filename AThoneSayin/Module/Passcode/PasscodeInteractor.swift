//
//  PasscodeInteractor.swift
//  Passcode
//
//  Created by Aung Moe Hein on 17/05/2020.
//

import Foundation

final class PasscodeInteractor: InteractorInterface {

    weak var presenter: PasscodePresenterInteractorInterface!
}

extension PasscodeInteractor: PasscodeInteractorPresenterInterface {

}
