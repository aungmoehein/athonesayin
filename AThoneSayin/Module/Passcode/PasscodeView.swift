//
//  PasscodeView.swift
//  Passcode
//
//  Created by Aung Moe Hein on 17/05/2020.
//

import Foundation
import UIKit
import Firebase

final class PasscodeView: UIViewController, ViewInterface, UINavigationControllerDelegate {
    @IBOutlet weak var iconLock: UIButton!
    @IBOutlet weak var lbTitle: UILabel!
    
    var localizer = LocalizationSystem.Localizer
    var presenter: PasscodePresenterViewInterface!
    
    let passcode = Passcode()
    var notConfirmCode: Bool = true
    var passCode: String = ""
    
    var is_secure: Bool = false
    var keyPasscode: String = ""
    var isLogin: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let image = UIImage(named: "ic_lock")?.withRenderingMode(.alwaysTemplate)
        iconLock.setImage(image, for: .normal)
        iconLock.makeRounded()
        iconLock.backgroundColor = UIColor(hex: Color.colorPrimary.rawValue)
        iconLock.tintColor = .white
        
        is_secure = UserDefaults.standard.bool(forKey: UserDefaults.Keys.is_secure)
        keyPasscode = UserDefaults.standard.string(forKey: UserDefaults.Keys.passcode)!
        
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
        
        setupUI()
        self.presenter.start()
        Analytics.logEvent(Events.PASSWORD_SCREEN, parameters: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lbTitle.text = localizer.localizedStringForKey(key: "enter_passcode", comment: "")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.navigationController!.viewControllers.count > 1
    }

    private func setupUI() {
        passcode.frame = CGRect(x: 100, y: 100, width: 200, height: 40)
        passcode.center = CGPoint(x: self.view.center.x, y: passcode.center.y)
        passcode.backgroundColor = UIColor.clear
        passcode.becomeFirstResponder()
        passcode.didFinishedEnterCode = {code in
            if self.is_secure {
                if code == self.keyPasscode {
                    if !self.isLogin {
                        UserDefaults.standard.set(false, forKey: UserDefaults.Keys.is_secure)
                        UserDefaults.standard.set("", forKey: UserDefaults.Keys.passcode)
                        self.navigationController?.popViewController(animated: true)
                        self.dismiss(animated: true, completion: nil)
                    } else {
                        self.presenter.home()
                    }
                }else {
                    self.wrongPasscode()
                }
            }else {
                self.registerPasscode(code)
            }
        }
        view.addSubview(passcode)
        
        passcode.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = passcode.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        let verticalConstraint = passcode.topAnchor.constraint(equalTo: lbTitle.bottomAnchor, constant: 25)
        let widthConstraint = passcode.widthAnchor.constraint(equalToConstant: 200)
        let heightConstraint = passcode.heightAnchor.constraint(equalToConstant: 40)
        
        view.addConstraints([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
    }
    
    func registerPasscode(_ code: String) {
        if self.notConfirmCode {
            self.passCode = code
            self.notConfirmCode = false
            self.passcode.code = ""
            self.passcode.becomeFirstResponder()
            self.lbTitle.text = localizer.localizedStringForKey(key: "confirm_passcode", comment: "")
        }else {
            if self.passCode == code {
                print("Finished Setting code")
                UserDefaults.standard.set(true, forKey: UserDefaults.Keys.is_secure)
                UserDefaults.standard.set(code, forKey: UserDefaults.Keys.passcode)
                self.navigationController?.popViewController(animated: true)
                self.dismiss(animated: true, completion: nil)
            }else {
                self.wrongPasscode()
                self.passCode = ""
                self.notConfirmCode = true
            }
        }
    }
    
    func wrongPasscode() {
        self.shakePasscode()
        self.lbTitle.text = localizer.localizedStringForKey(key: "enter_passcode", comment: "")
        self.passcode.code = ""
        self.passcode.becomeFirstResponder()
    }
    
    func shakePasscode() {
        let shake = CABasicAnimation(keyPath: "position")
        let xDelta = CGFloat(5)
        shake.duration = 0.05
        shake.repeatCount = 2
        shake.autoreverses = true

        let from_point = CGPoint(x: passcode.center.x - xDelta, y: passcode.center.y)
        let from_value = NSValue(cgPoint: from_point)

        let to_point = CGPoint(x: passcode.center.x + xDelta, y: passcode.center.y)
        let to_value = NSValue(cgPoint: to_point)

        shake.fromValue = from_value
        shake.toValue = to_value
        shake.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeInEaseOut)
        passcode.layer.add(shake, forKey: "position")
    }
}

extension PasscodeView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension PasscodeView: PasscodeViewPresenterInterface {

}
