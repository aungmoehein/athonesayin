//
//  PasscodeModule.swift
//  Passcode
//
//  Created by Aung Moe Hein on 17/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol PasscodeRouterPresenterInterface: RouterPresenterInterface {
    func goToHome()
}

// MARK: - presenter

protocol PasscodePresenterRouterInterface: PresenterRouterInterface {

}

protocol PasscodePresenterInteractorInterface: PresenterInteractorInterface {

}

protocol PasscodePresenterViewInterface: PresenterViewInterface {
    func start()
    func home()
}

// MARK: - interactor

protocol PasscodeInteractorPresenterInterface: InteractorPresenterInterface {

}

// MARK: - view

protocol PasscodeViewPresenterInterface: ViewPresenterInterface {

}


// MARK: - module builder

final class PasscodeModule: ModuleInterface {

    typealias View = PasscodeView
    typealias Presenter = PasscodePresenter
    typealias Router = PasscodeRouter
    typealias Interactor = PasscodeInteractor

    func build(db: DBHelper) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "passcode") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router(db: db)

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
