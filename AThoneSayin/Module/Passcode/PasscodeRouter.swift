//
//  PasscodeRouter.swift
//  Passcode
//
//  Created by Aung Moe Hein on 17/05/2020.
//

import Foundation
import UIKit

final class PasscodeRouter: RouterInterface {

    weak var presenter: PasscodePresenterRouterInterface!

    weak var viewController: UIViewController?
    var db: DBHelper!
    init(db: DBHelper) {
        self.db = db
    }
}

extension PasscodeRouter: PasscodeRouterPresenterInterface {
    func goToHome() {
        let Storyboard = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "host") as! ViewController
        view.db = db
        UIApplication.shared.windows.first?.rootViewController = UINavigationController(rootViewController: view)
        UIView.transition(with: UIApplication.shared.windows.first!, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion:
        { completed in
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        })
    }
}
