//
//  EditRecentlyUsedModule.swift
//  EditRecentlyUsed
//
//  Created by Aung Moe Hein on 20/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol EditRecentlyUsedRouterPresenterInterface: RouterPresenterInterface {

}

// MARK: - presenter

protocol EditRecentlyUsedPresenterRouterInterface: PresenterRouterInterface {

}

protocol EditRecentlyUsedPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchCategoryById(category: Category)
    func onFetchRecentlyUsedsByStatus(recentlyUseds: [RecentlyUsed])
}

protocol EditRecentlyUsedPresenterViewInterface: PresenterViewInterface {
    func start()
    func getCategoryById(id: Int)
    func getRecentlyUsedsByStatus(status: Int)
    func removeRecentlyUsedById(id: Int)
}

// MARK: - interactor

protocol EditRecentlyUsedInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchCategoryById(id: Int)
    func fetchRecentlyUsedsByStatus(status: Int)
    func deleteRecentlyUsedById(id: Int)
}

// MARK: - view

protocol EditRecentlyUsedViewPresenterInterface: ViewPresenterInterface {
    func displayCategoryById(category: Category)
    func displayRecentlyUsed(recentlyUseds: [RecentlyUsed])
}


// MARK: - module builder

final class EditRecentlyUsedModule: ModuleInterface {

    typealias View = EditRecentlyUsedView
    typealias Presenter = EditRecentlyUsedPresenter
    typealias Router = EditRecentlyUsedRouter
    typealias Interactor = EditRecentlyUsedInteractor

    func build(db: DBHelper) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "edit_recently_used") as! View
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
