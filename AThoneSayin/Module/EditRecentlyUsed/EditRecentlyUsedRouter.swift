//
//  EditRecentlyUsedRouter.swift
//  EditRecentlyUsed
//
//  Created by Aung Moe Hein on 20/05/2020.
//

import Foundation
import UIKit

final class EditRecentlyUsedRouter: RouterInterface {

    weak var presenter: EditRecentlyUsedPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension EditRecentlyUsedRouter: EditRecentlyUsedRouterPresenterInterface {

}