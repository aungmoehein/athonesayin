//
//  EditRecentlyUsedInteractor.swift
//  EditRecentlyUsed
//
//  Created by Aung Moe Hein on 20/05/2020.
//

import Foundation

final class EditRecentlyUsedInteractor: InteractorInterface {

    weak var presenter: EditRecentlyUsedPresenterInteractorInterface!
    var db: DBHelper!
    
    init(db: DBHelper) {
        self.db = db
    }
}

extension EditRecentlyUsedInteractor: EditRecentlyUsedInteractorPresenterInterface {
    func fetchRecentlyUsedsByStatus(status: Int) {
        self.presenter.onFetchRecentlyUsedsByStatus(recentlyUseds: db.readAllRecentlyUsedByStatus(status: status))
    }
    
    func fetchCategoryById(id: Int) {
        self.presenter.onFetchCategoryById(category: db.readCategoryById(id: id))
    }
    
    func deleteRecentlyUsedById(id: Int) {
        db.deleteRecentlyUsedById(id: id)
    }
}
