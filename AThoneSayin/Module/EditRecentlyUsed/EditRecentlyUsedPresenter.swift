//
//  EditRecentlyUsedPresenter.swift
//  EditRecentlyUsed
//
//  Created by Aung Moe Hein on 20/05/2020.
//

import Foundation

final class EditRecentlyUsedPresenter: PresenterInterface {

    var router: EditRecentlyUsedRouterPresenterInterface!
    var interactor: EditRecentlyUsedInteractorPresenterInterface!
    weak var view: EditRecentlyUsedViewPresenterInterface!

}

extension EditRecentlyUsedPresenter: EditRecentlyUsedPresenterRouterInterface {

}

extension EditRecentlyUsedPresenter: EditRecentlyUsedPresenterInteractorInterface {
    func onFetchRecentlyUsedsByStatus(recentlyUseds: [RecentlyUsed]) {
        self.view.displayRecentlyUsed(recentlyUseds: recentlyUseds)
    }
    
    func onFetchCategoryById(category: Category) {
        self.view.displayCategoryById(category: category)
    }
}

extension EditRecentlyUsedPresenter: EditRecentlyUsedPresenterViewInterface {

    func start() {

    }
    
    func getCategoryById(id: Int) {
        interactor.fetchCategoryById(id: id)
    }

    func getRecentlyUsedsByStatus(status: Int) {
        interactor.fetchRecentlyUsedsByStatus(status: status)
    }
    
    func removeRecentlyUsedById(id: Int) {
        interactor.deleteRecentlyUsedById(id: id)
    }
}
