//
//  EditRecentlyUsedView.swift
//  EditRecentlyUsed
//
//  Created by Aung Moe Hein on 20/05/2020.
//

import Foundation
import PopupDialog
import UIKit
import Firebase

final class EditRecentlyUsedView: UIViewController, ViewInterface {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var tbView: UITableView!
    
    var localizer = LocalizationSystem.Localizer
    var presenter: EditRecentlyUsedPresenterViewInterface!
    
    var showMsgDialog: PopupDialog!
    var category: Category!
    var recentlyUseds: [RecentlyUsed] = [] {
        didSet {
            tbView.reloadData()
        }
    }
    
    var status: Int = Parameter.EXPENSE

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tbView.register(UINib(nibName: "EditRecentlyUsedCell", bundle: nil), forCellReuseIdentifier: "edit_recently_used_cell")
        tbView.delegate = self
        tbView.dataSource = self
        tbView.reloadData()
        
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], for: UIControl.State.selected)

        self.presenter.start()
        presenter.getRecentlyUsedsByStatus(status: self.status)
        Analytics.logEvent(Events.SETTING_DAILY_SCREEN, parameters: nil)
    }

    @IBAction func segmentedControl(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
        case Parameter.EXPENSE:
            self.status = Parameter.EXPENSE
        default:
            self.status = Parameter.INCOME
        }
        
        self.presenter.getRecentlyUsedsByStatus(status: self.status)
    }
}

extension EditRecentlyUsedView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recentlyUseds.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "edit_recently_used_cell", for: indexPath) as! EditRecentlyUsedCell
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        
        presenter.getCategoryById(id: recentlyUseds[indexPath.row].category_id)
        cell.icon_name = self.category.category_icon
        cell.title = localizer.localizedStringForKey(key: self.category.category_name, comment: "")
        cell.amount = "(" + String(format: localizer.localizedStringForKey(key: "ks", comment: ""), Int(recentlyUseds[indexPath.row].usage_amount).formattedWithSeparator) + ")"
        cell.btnDelete.tag = recentlyUseds[indexPath.row].id
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteClick(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let msg = recentlyUseds[indexPath.row].usage_note == "" ? localizer.localizedStringForKey(key: "no_note", comment: "") : recentlyUseds[indexPath.row].usage_note
        showMsgDialog(msg: msg)
    }
    
    private func showMsgDialog(msg: String) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
            
        }
        showMsgDialog.addButtons([buttonOk])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    @objc func btnDeleteClick(_ sender: UIButton) {
        showMsg(msg: self.localizer.localizedStringForKey(key: "confirm_delete", comment: ""), sender: sender)
    }
    
    private func showMsg(msg: String,sender: UIButton) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
            let hitPoint = sender.convert(CGPoint.zero, to: self.tbView)
            let hitIndex = self.tbView.indexPathForRow(at: hitPoint)
            
            //remove the image and refresh the collection view
            self.recentlyUseds.remove(at: (hitIndex?.row)!)
            self.presenter.removeRecentlyUsedById(id: sender.tag)
            self.tbView.reloadData()
        }
        let buttonCancel = CancelButton(title: NSLocalizedString("Cancel", comment: "")) {
        }
        showMsgDialog.addButtons([buttonOk, buttonCancel])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}

extension EditRecentlyUsedView: EditRecentlyUsedViewPresenterInterface {
    func displayRecentlyUsed(recentlyUseds: [RecentlyUsed]) {
        self.recentlyUseds = recentlyUseds
    }
    
    func displayCategoryById(category: Category) {
        self.category = category
    }
}
