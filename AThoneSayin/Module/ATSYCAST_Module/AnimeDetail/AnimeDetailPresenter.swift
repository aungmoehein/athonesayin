//
//  AnimeDetailPresenter.swift
//  AnimeDetail
//
//  Created by Aung Moe Hein on 05/06/2020.
//

import Foundation

final class AnimeDetailPresenter: PresenterInterface {

    var router: AnimeDetailRouterPresenterInterface!
    var interactor: AnimeDetailInteractorPresenterInterface!
    weak var view: AnimeDetailViewPresenterInterface!

}

extension AnimeDetailPresenter: AnimeDetailPresenterRouterInterface {

}

extension AnimeDetailPresenter: AnimeDetailPresenterInteractorInterface {
    func onFetchAnimeVideos(animeVideos: [AniVideo]) {
        self.view.displayAnimeVideos(animeVideos: animeVideos)
    }
    
    func onFetchMoreEpisodes(aniEpisodes: [AniEpisode]) {
        self.view.displayMoreEpisodes(episodes: aniEpisodes)
    }
    
    func onFetchError(type: String) {
        self.view.displayError(type: type)
    }
}

extension AnimeDetailPresenter: AnimeDetailPresenterViewInterface {

    func start() {

    }

    func getAnimeVideos(episodeID: Int, type: String) {
        self.interactor.fetchAnimeVideos(episodeID: episodeID, type: type)
    }
    
    func goToVideoPlayer(videoWrapper: VideoWrapper) {
        self.router.goToVideoPlayer(videoWrapper: videoWrapper)
    }
    
    func getMoreEpisodes(id: Int, filter: String, page: Int) {
        self.interactor.fetchAnimeEpisodes(id: id, filter: filter, page: page)
    }
}
