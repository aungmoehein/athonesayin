//
//  AnimeDetailView.swift
//  AnimeDetail
//
//  Created by Aung Moe Hein on 05/06/2020.
//

import Foundation
import UIKit
import SDWebImage
import ReadMoreTextView
import PopupDialog
import NVActivityIndicatorView

final class AnimeDetailView: UIViewController, ViewInterface, NVActivityIndicatorViewable {
    @IBOutlet weak var animeImage: UIImageView!
    @IBOutlet weak var animeTitle: UILabel!
    @IBOutlet weak var releasedYear: UILabel!
    @IBOutlet weak var animeType: UILabel!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var animeStatus: UILabel!
    @IBOutlet weak var animeDescription: ReadMoreTextView!
    @IBOutlet weak var titleEpisodes: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var episodesTV: UITableView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var overLayViewHeight: NSLayoutConstraint!
    
    let headerViewMaxHeight: CGFloat = 300
    let headerViewMinHeight: CGFloat = 0
    
    fileprivate var showMsgDialog: PopupDialog!
    var presenter: AnimeDetailPresenterViewInterface!
    
    var currentPage = 1
    var currentDisplayRow = 0
    var isLastPage: Bool = false
    var isLoading = false

    var videoTitle: String = ""
    var animeData: AniData!
    var animeEpisodes: [AniEpisode] = []
    var newEpisodes: [AniEpisode] = [] {
        didSet {
            episodesTV.reloadData()
        }
    }
    
    var sort: String = "DESC" {
        didSet {
            newEpisodes = []
            self.currentPage = 1
            self.presenter.getMoreEpisodes(id: animeData.id!, filter: sort, page: self.currentPage)
        }
    }
    
    var retryEpisodeID: Int = 0
    var retryEpisodeType: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        self.stopAnimating()
        newEpisodes = animeEpisodes
        
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)], for: UIControl.State.selected)
        segmentedControl.tintColor = UIColor(hex: 0x459a0b)
        
        statusView.layer.cornerRadius = statusView.frame.size.width/2
        statusView.clipsToBounds = true
        statusView.backgroundColor = animeData.status == "Finished" ? .green : .yellow
        
        animeTitle.text = animeData.title
        releasedYear.text = animeData.year
        animeType.text = animeData.type
        animeStatus.text = animeData.status
        animeDescription.text = animeData.description
        animeImage.sd_setImage(with: URL(string: animeData.cover_photo!), placeholderImage: UIImage(named: "movie_poster.png"))
        animeImage.layer.cornerRadius = 3
        
        episodesTV.register(UINib(nibName: "AnimeEpisodeCell", bundle: nil), forCellReuseIdentifier: "anime_episode_cell")
        episodesTV.delegate = self
        episodesTV.dataSource = self
        episodesTV.reloadData()
        
        self.presenter.start()
    }
    
    @objc func canRotate() -> Void {}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            
            if !isLastPage {
                self.currentPage = self.currentPage + 1
                self.presenter.getMoreEpisodes(id: animeData.id!, filter: self.sort, page: self.currentPage)
            }
        }
    }

    @IBAction func segmentedControl(_ sender: Any) {
        self.sort = segmentedControl.selectedSegmentIndex == 0 ? "DESC" : "ASC"
    }
    
    @IBAction func btnSub(_ sender: UIButton) {
        self.startAnimating()
        self.videoTitle = animeData.title! + ":" + "Episode " + newEpisodes[sender.tag].episode_num!
        self.retryEpisodeID = newEpisodes[sender.tag].id!
        self.retryEpisodeType = "sub"
        self.presenter.getAnimeVideos(episodeID: newEpisodes[sender.tag].id!, type: "sub")
    }
    
    @IBAction func btnDub(_ sender: UIButton) {
        self.startAnimating()
        self.videoTitle = animeData.title! + ":" + "Episode " + newEpisodes[sender.tag].episode_num!
        self.retryEpisodeID = newEpisodes[sender.tag].id!
        self.retryEpisodeType = "dub"
        self.presenter.getAnimeVideos(episodeID: newEpisodes[sender.tag].id!, type: "dub")
    }
}

extension AnimeDetailView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        currentDisplayRow = indexPath.row
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newEpisodes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = episodesTV.dequeueReusableCell(withIdentifier: "anime_episode_cell", for: indexPath) as! AnimeEpisodeCell
        cell.selectionStyle = .none
        cell.episode_num = newEpisodes[indexPath.row].episode_num!
        cell.title = newEpisodes[indexPath.row].title == nil ? "" : newEpisodes[indexPath.row].title!
        cell.date = newEpisodes[indexPath.row].airing_date == nil ? "" : newEpisodes[indexPath.row].airing_date!
        cell.showVideoBtn(sub: newEpisodes[indexPath.row].sub!, dub: newEpisodes[indexPath.row].dub!)
        cell.btnSub.tag = indexPath.row
        cell.btnDub.tag = indexPath.row
        cell.btnSub.addTarget(self, action: #selector(btnSub(_:)), for: .touchUpInside)
        cell.btnDub.addTarget(self, action: #selector(btnDub(_:)), for: .touchUpInside)
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = overLayViewHeight.constant - y

        if newHeaderViewHeight > headerViewMaxHeight {
            self.scrollView.alpha = 1
            overLayViewHeight.constant = headerViewMaxHeight
        } else if newHeaderViewHeight < headerViewMinHeight {
            self.scrollView.alpha = 0
            overLayViewHeight.constant = headerViewMinHeight
        } else {
            self.scrollView.alpha = newHeaderViewHeight / 170
            overLayViewHeight.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
        }
        
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading && currentDisplayRow == newEpisodes.count-1 {
            loadMoreData()
        }
    }
}

extension AnimeDetailView: AnimeDetailViewPresenterInterface {
    func displayAnimeVideos(animeVideos: [AniVideo]) {
        self.stopAnimating()
        let videoWrapper = VideoWrapper(id: retryEpisodeID, partner: UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerAnimeflix)!, imageURL: animeData.cover_photo!, videoURL: animeVideos[0].file!, videoName: videoTitle)
        self.presenter.goToVideoPlayer(videoWrapper: videoWrapper)
    }
    
    func displayMoreEpisodes(episodes: [AniEpisode]) {
        self.stopAnimating()
        isLastPage = episodes.count == 0
        newEpisodes += episodes
        self.isLoading = false
    }
    
    func displayError(type: String) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = "Something went wrong!"
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonRetry = DefaultButton(title: NSLocalizedString("Retry", comment: "")) {
            if type == "Episodes" {
                self.presenter.getMoreEpisodes(id: self.animeData.id!, filter: self.sort, page: self.currentPage)
            }else {
                self.presenter.getAnimeVideos(episodeID: self.retryEpisodeID, type: self.retryEpisodeType)
            }
        }
        
        let buttonCancel = CancelButton(title: NSLocalizedString("Cancel", comment: "")) {
            
        }
        showMsgDialog.addButtons([buttonCancel, buttonRetry])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}
