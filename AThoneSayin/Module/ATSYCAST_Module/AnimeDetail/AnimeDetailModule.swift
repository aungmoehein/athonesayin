//
//  AnimeDetailModule.swift
//  AnimeDetail
//
//  Created by Aung Moe Hein on 05/06/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol AnimeDetailRouterPresenterInterface: RouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper)
}

// MARK: - presenter

protocol AnimeDetailPresenterRouterInterface: PresenterRouterInterface {
    
}

protocol AnimeDetailPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchAnimeVideos(animeVideos: [AniVideo])
    func onFetchMoreEpisodes(aniEpisodes: [AniEpisode])
    func onFetchError(type: String)
}

protocol AnimeDetailPresenterViewInterface: PresenterViewInterface {
    func start()
    func getAnimeVideos(episodeID: Int, type: String)
    func goToVideoPlayer(videoWrapper: VideoWrapper)
    func getMoreEpisodes(id: Int, filter: String, page: Int)
}

// MARK: - interactor

protocol AnimeDetailInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchAnimeVideos(episodeID: Int, type: String)
    func fetchAnimeEpisodes(id: Int, filter: String, page: Int)
}

// MARK: - view

protocol AnimeDetailViewPresenterInterface: ViewPresenterInterface {
    func displayAnimeVideos(animeVideos: [AniVideo])
    func displayMoreEpisodes(episodes: [AniEpisode])
    func displayError(type: String)
}


// MARK: - module builder

final class AnimeDetailModule: ModuleInterface {

    typealias View = AnimeDetailView
    typealias Presenter = AnimeDetailPresenter
    typealias Router = AnimeDetailRouter
    typealias Interactor = AnimeDetailInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "anime_detail") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
