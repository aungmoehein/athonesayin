//
//  AnimeDetailInteractor.swift
//  AnimeDetail
//
//  Created by Aung Moe Hein on 05/06/2020.
//

import Foundation

final class AnimeDetailInteractor: InteractorInterface {

    weak var presenter: AnimeDetailPresenterInteractorInterface!
}

extension AnimeDetailInteractor: AnimeDetailInteractorPresenterInterface {
    func fetchAnimeEpisodes(id: Int, filter: String, page: Int) {
        Animeflix.sharedInstance.getAnimeEpisodes(id: id, page: page, sort: filter).load()
            .onSuccess() {data in
            if let aniEpisodeList: AniEpisodeList = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchMoreEpisodes(aniEpisodes: aniEpisodeList.data!)
                }
            }
        }.onFailure() {error in
            self.presenter.onFetchError(type: "Episodes")
        }
    }
    
    func fetchAnimeVideos(episodeID: Int, type: String) {
        Animeflix.sharedInstance.getAnimeVideo(episodeID: episodeID).load()
            .onSuccess() {data in
            if let aniVideos: [AniVideo] = data.typedContent() {
                if self.presenter != nil {
                    var videos: [AniVideo] = []
                    for aniVideo in aniVideos {
                        if aniVideo.lang == type && aniVideo.provider == "AUEngine" && aniVideo.type == "mp4" {
                            videos.append(aniVideo)
                        }
                    }
                    
                    if videos.count == 0 {
                        for aniVideo in aniVideos {
                            if aniVideo.lang == type && aniVideo.provider == "FastStream" && aniVideo.type == "hls" {
                                videos.append(aniVideo)
                            }
                        }
                    }
                    self.presenter.onFetchAnimeVideos(animeVideos: videos)
                }
            }
        }.onFailure() {error in
            self.presenter.onFetchError(type: "Videos")
        }
    }
}
