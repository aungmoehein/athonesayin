//
//  AnimeDetailRouter.swift
//  AnimeDetail
//
//  Created by Aung Moe Hein on 05/06/2020.
//

import Foundation
import UIKit

final class AnimeDetailRouter: RouterInterface {

    weak var presenter: AnimeDetailPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension AnimeDetailRouter: AnimeDetailRouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "video_play_view") as! VideoPlayView
        vc.isVideo = videoWrapper.isVideo
        vc.isHentai = videoWrapper.isHentai
        vc.videoWrapper = videoWrapper
        vc.videoName = videoWrapper.videoName
        vc.streamURL = videoWrapper.videoURL
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
