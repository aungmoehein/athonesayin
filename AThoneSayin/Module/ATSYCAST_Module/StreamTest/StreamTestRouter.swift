//
//  StreamTestRouter.swift
//  StreamTest
//
//  Created by Aung Moe Hein on 09/07/2020.
//

import Foundation
import UIKit

final class StreamTestRouter: RouterInterface {

    weak var presenter: StreamTestPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension StreamTestRouter: StreamTestRouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "video_play_view") as! VideoPlayView
        vc.videoWrapper = videoWrapper
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
