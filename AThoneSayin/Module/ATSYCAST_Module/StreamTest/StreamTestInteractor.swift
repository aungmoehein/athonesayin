//
//  StreamTestInteractor.swift
//  StreamTest
//
//  Created by Aung Moe Hein on 09/07/2020.
//

import Foundation
import UIKit

final class StreamTestInteractor: InteractorInterface {

    weak var presenter: StreamTestPresenterInteractorInterface!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
}

extension StreamTestInteractor: StreamTestInteractorPresenterInterface {
    func fetchStreams() {
        self.presenter.onFetchStreams(streams: appDelegate.db.readAllStream())
    }
    
    func deleteStream(id: Int) {
        appDelegate.db.deleteStreamByID(id: id)
    }
    
    func updateStream(id: Int, name: String, url: String) {
        appDelegate.db.updateStream(id: id, name: name, url: url)
    }
    
    func insertStream(name: String, url: String) {
        appDelegate.db.insertStream(name: name, url: url)
    }
}
