//
//  StreamTestPresenter.swift
//  StreamTest
//
//  Created by Aung Moe Hein on 09/07/2020.
//

import Foundation

final class StreamTestPresenter: PresenterInterface {

    var router: StreamTestRouterPresenterInterface!
    var interactor: StreamTestInteractorPresenterInterface!
    weak var view: StreamTestViewPresenterInterface!

}

extension StreamTestPresenter: StreamTestPresenterRouterInterface {

}

extension StreamTestPresenter: StreamTestPresenterInteractorInterface {
    func onFetchStreams(streams: [Stream]) {
        self.view.displayStreams(streams: streams)
    }
}

extension StreamTestPresenter: StreamTestPresenterViewInterface {

    func start() {

    }

    func getStreamsFromDB() {
        self.interactor.fetchStreams()
    }
    
    func removeStreamFromDB(id: Int) {
        self.interactor.deleteStream(id: id)
    }
    
    func updateStreamInDB(id: Int, name: String, url: String) {
        self.interactor.updateStream(id: id, name: name, url: url)
    }
    
    func saveStreamToDB(name: String, url: String) {
        self.interactor.insertStream(name: name, url: url)
    }
    
    func videoPlayer(videoWrapper: VideoWrapper) {
        self.router.goToVideoPlayer(videoWrapper: videoWrapper)
    }
}
