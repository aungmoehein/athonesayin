//
//  StreamTestModule.swift
//  StreamTest
//
//  Created by Aung Moe Hein on 09/07/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol StreamTestRouterPresenterInterface: RouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper)
}

// MARK: - presenter

protocol StreamTestPresenterRouterInterface: PresenterRouterInterface {

}

protocol StreamTestPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchStreams(streams: [Stream])
}

protocol StreamTestPresenterViewInterface: PresenterViewInterface {
    func start()
    func getStreamsFromDB()
    func removeStreamFromDB(id: Int)
    func updateStreamInDB(id: Int, name: String, url: String)
    func saveStreamToDB(name: String, url: String)
    func videoPlayer(videoWrapper: VideoWrapper)
}

// MARK: - interactor

protocol StreamTestInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchStreams()
    func deleteStream(id: Int)
    func updateStream(id: Int, name: String, url: String)
    func insertStream(name: String, url: String)
}

// MARK: - view

protocol StreamTestViewPresenterInterface: ViewPresenterInterface {
    func displayStreams(streams: [Stream])
}


// MARK: - module builder

final class StreamTestModule: ModuleInterface {

    typealias View = StreamTestView
    typealias Presenter = StreamTestPresenter
    typealias Router = StreamTestRouter
    typealias Interactor = StreamTestInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "stream_test") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
