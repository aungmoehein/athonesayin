//
//  StreamTestView.swift
//  StreamTest
//
//  Created by Aung Moe Hein on 09/07/2020.
//

import Foundation
import UIKit
import PopupDialog

final class StreamTestView: UIViewController, ViewInterface, UINavigationControllerDelegate {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbStreamName: UILabel!
    @IBOutlet weak var lbStreamURL: UILabel!
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldURL: UITextField!
    @IBOutlet weak var tbExample: UITableView!
    @IBOutlet weak var btnPlay: UIButton!
    
    var showMsgDialog: PopupDialog!
    var presenter: StreamTestPresenterViewInterface!
    
    var testStreamNames: [Stream] = [] {
        didSet {
            tbExample.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.delegate = self
        self.navigationItem.title = Bundle.main.infoDictionary!["CFBundleName"] as? String
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x050505)
        self.navigationController?.navigationBar.tintColor = .white
        addNavButtons()
        
        txtFieldName.delegate = self
        txtFieldURL.delegate = self
        
        tbExample.delegate = self
        tbExample.dataSource = self
        tbExample.reloadData()
        
        btnPlay.layer.cornerRadius = 5
        btnPlay.layer.borderColor = UIColor(hex: 0x459a0b).cgColor
        btnPlay.layer.borderWidth = 1

        self.presenter.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        reloadView()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func reloadView() {
        self.presenter.getStreamsFromDB()
    }
    
    func addNavButtons() {
        let info = UIButton()
        let image = UIImage(named: "ic_info")?.withRenderingMode(.alwaysTemplate)
        info.setImage(image, for: .normal)
        info.frame = CGRect(x: 0.0, y:0.0, width: 30, height:40)
        info.tintColor = .white
        info.contentMode = .center
        info.addTarget(self, action: #selector(infoAction), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: info)
    }
    
    @objc func infoAction(sender: UIButton!) {
        let view = ATSYCAST_SettingModule().build() as! ATSYCAST_SettingView
        self.navigationController?.pushViewController(view, animated: true)
    }

    @IBAction func btnPlay(_ sender: Any) {
        if txtFieldName.text!.isEmpty {
            self.showMsg(msg: "Stream name cannot be empty.")
        }else if txtFieldURL.text!.isEmpty {
            self.showMsg(msg: "Stream url cannot be empty.")
        }else {
            let videoWrapper = VideoWrapper(id: 0, partner: "stream", imageURL: "" , videoURL: txtFieldURL.text!, videoName: txtFieldName.text!, isVideo: false, isHentai: false)
            self.presenter.saveStreamToDB(name: txtFieldName.text!, url: txtFieldURL.text!)
            self.presenter.videoPlayer(videoWrapper: videoWrapper)
            
            txtFieldURL.endEditing(true)
            txtFieldURL.text = ""
            
            txtFieldName.endEditing(true)
            txtFieldName.text = ""
        }
    }
    
    private func showMsg(msg: String) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
            
        }
        showMsgDialog.addButtons([buttonOk])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    private func showMsg(msg: String, tableView: UITableView, indexPath: IndexPath) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
            self.presenter.removeStreamFromDB(id: self.testStreamNames[indexPath.row].id)
            self.testStreamNames.remove(at: indexPath.row)
            tableView.reloadData()
            self.presenter.getStreamsFromDB()
        }
        let buttonCancel = CancelButton(title: NSLocalizedString("Cancel", comment: "")) {
        }
        showMsgDialog.addButtons([buttonOk, buttonCancel])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    private func showEditDialog(stream: Stream) {
        let popUp = EditStream(nibName: "EditStream", bundle: nil)
        popUp.vc = self
        popUp.stream = stream
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}

extension StreamTestView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension StreamTestView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return testStreamNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .value1, reuseIdentifier: "Cell")
        cell.textLabel?.text = testStreamNames[indexPath.row].name
        cell.textLabel?.textColor = .white
        cell.accessoryType = .disclosureIndicator
        cell.tintColor = .white
        cell.backgroundColor = UIColor(hex: 0x111111)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoWrapper = VideoWrapper(id: 0, partner: "stream", imageURL: "" , videoURL: testStreamNames[indexPath.row].url, videoName: testStreamNames[indexPath.row].name, isVideo: false, isHentai: false)
        
        self.presenter.videoPlayer(videoWrapper: videoWrapper)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
        let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
            self.showMsg(msg: "Are you sure want to delete?", tableView: tableView, indexPath: indexPath)
            
            completionHandler(true)
        }
        if #available(iOS 13.0, *) {
            deleteAction.image = UIImage(systemName: "trash")
        } else {
            deleteAction.image = UIImage(named: "ic_trash")
        }
        deleteAction.backgroundColor = .systemRed
        
        let editAction = UIContextualAction(style: .normal, title: nil) { (_, _, completionHandler)
            in
            
            self.showEditDialog(stream: self.testStreamNames[indexPath.row])
            
            completionHandler(true)
        }
        if #available(iOS 13.0, *) {
            editAction.image = UIImage(systemName: "square.and.pencil")
        } else {
            editAction.image = UIImage(named: "ic_pen_box")
        }
        editAction.backgroundColor = .systemYellow
        
        let configuration = UISwipeActionsConfiguration(actions: [deleteAction, editAction])
        configuration.performsFirstActionWithFullSwipe = false
        return configuration
    }
}

extension StreamTestView: StreamTestViewPresenterInterface {
    func displayStreams(streams: [Stream]) {
        self.testStreamNames = streams
    }
}
