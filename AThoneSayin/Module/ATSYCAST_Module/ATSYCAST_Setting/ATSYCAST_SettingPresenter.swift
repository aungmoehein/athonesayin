//
//  SettingPresenter.swift
//  Setting
//
//  Created by Aung Moe Hein on 19/06/2020.
//

import Foundation

final class ATSYCAST_SettingPresenter: PresenterInterface {

    var router: ATSYCAST_SettingRouterPresenterInterface!
    var interactor: ATSYCAST_SettingInteractorPresenterInterface!
    weak var view: ATSYCAST_SettingViewPresenterInterface!

}

extension ATSYCAST_SettingPresenter: ATSYCAST_SettingPresenterRouterInterface {

}

extension ATSYCAST_SettingPresenter: ATSYCAST_SettingPresenterInteractorInterface {
    func onFetchVersion(version: Version) {
        self.view.displayVersion(version: version)
    }
}

extension ATSYCAST_SettingPresenter: ATSYCAST_SettingPresenterViewInterface {

    func start() {

    }
    
    func showFavorite() {
        self.router.goToHome()
    }

    func getVersion() {
        self.interactor.fetchVersion()
    }
}
