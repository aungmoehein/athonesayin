//
//  SettingModule.swift
//  Setting
//
//  Created by Aung Moe Hein on 19/06/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol ATSYCAST_SettingRouterPresenterInterface: RouterPresenterInterface {
    func goToHome()
}

// MARK: - presenter

protocol ATSYCAST_SettingPresenterRouterInterface: PresenterRouterInterface {

}

protocol ATSYCAST_SettingPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchVersion(version: Version)
}

protocol ATSYCAST_SettingPresenterViewInterface: PresenterViewInterface {
    func start()
    func showFavorite()
    func getVersion()
}

// MARK: - interactor

protocol ATSYCAST_SettingInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchVersion()
}

// MARK: - view

protocol ATSYCAST_SettingViewPresenterInterface: ViewPresenterInterface {
    func displayVersion(version: Version)
}


// MARK: - module builder

final class ATSYCAST_SettingModule: ModuleInterface {

    typealias View = ATSYCAST_SettingView
    typealias Presenter = ATSYCAST_SettingPresenter
    typealias Router = ATSYCAST_SettingRouter
    typealias Interactor = ATSYCAST_SettingInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "atsycast_setting") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
