//
//  SettingRouter.swift
//  Setting
//
//  Created by Aung Moe Hein on 19/06/2020.
//

import Foundation
import UIKit

final class ATSYCAST_SettingRouter: RouterInterface {

    weak var presenter: ATSYCAST_SettingPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension ATSYCAST_SettingRouter: ATSYCAST_SettingRouterPresenterInterface {
    func goToHome() {
        let live = ATSYCAST_HomeModule().build(liveKey: LiveKey(name: "", status: "", number: "")) as! ATSYCAST_HomeView
        live.isFavorite = true
        self.viewController?.navigationController?.pushViewController(live, animated: true)
    }
}
