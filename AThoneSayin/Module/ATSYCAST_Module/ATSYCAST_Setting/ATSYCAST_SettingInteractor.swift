//
//  SettingInteractor.swift
//  Setting
//
//  Created by Aung Moe Hein on 19/06/2020.
//

import Foundation

final class ATSYCAST_SettingInteractor: InteractorInterface {

    weak var presenter: ATSYCAST_SettingPresenterInteractorInterface!
}

extension ATSYCAST_SettingInteractor: ATSYCAST_SettingInteractorPresenterInterface {
    func fetchVersion() {
        SheetService.sharedInstance.partnerList.child("/3/public/values").withParam("alt", "json").load()
        .onSuccess(){ data in
            let response = Sheet2JSON.toJSON(data)
            if let data = response.data(using: String.Encoding.utf8) {
                do {
                    let decoder = JSONDecoder()
                    let jsonDictionary = try decoder.decode([Version].self, from: data)
                    jsonDictionary.forEach() {
                        version in
                        if version.type == "iOS" {
                            self.presenter.onFetchVersion(version: version)
                        }
                    }
                }catch {
                    // Handle error
                    print(error)
                }
            }
        }
    }
}
