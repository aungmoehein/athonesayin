//
//  SettingView.swift
//  Setting
//
//  Created by Aung Moe Hein on 19/06/2020.
//

import Foundation
import UIKit
import PopupDialog

final class ATSYCAST_SettingView: UIViewController, ViewInterface, PasswordDialogDelegate {
    @IBOutlet weak var appLogoImg: UIImageView!
    @IBOutlet weak var lbName: UILabel!
    @IBOutlet weak var btnAppID: UIButton!
    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var versionView: UIView!
    @IBOutlet weak var donationView: UIView!
    @IBOutlet weak var creditView: UIView!
    @IBOutlet weak var passwordSwitch: UISwitch!
    @IBOutlet weak var btnCheckUpdate: UIButton!
    @IBOutlet weak var lbVersion: UILabel!
    @IBOutlet weak var lbDesign: UILabel!
    
    var presenter: ATSYCAST_SettingPresenterViewInterface!
    var passwordDialog: PopupDialog!
    var showMsgDialog: PopupDialog!
    
    var downloadUrl: String = ""
    var version: Version! {
        didSet {
            self.downloadUrl = version.new_apk_url!
            let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String
            showMsgDialog(msg: self.version.version_name! > currentVersion ? "Update Available (Version: " + self.version.version_name! + ")" : "No new update.", isUpdate: self.version.version_name! > currentVersion)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.string(forKey: UserDefaults.Keys.user_name) != "" {
            lbName.text = UserDefaults.standard.string(forKey: UserDefaults.Keys.user_name)
        }
        
        btnAppID.layer.borderWidth = 1
        btnAppID.layer.borderColor = UIColor(hex: 0x459a0b).cgColor
        btnAppID.layer.cornerRadius = 7
        
        profileView.layer.cornerRadius = 10
        passwordView.layer.cornerRadius = 10
        versionView.layer.cornerRadius = 10
        donationView.layer.cornerRadius = 10
        creditView.layer.cornerRadius = 10
        
        donationView.isHidden = true
        donationView.isHidden = UserDefaults.standard.string(forKey: UserDefaults.Keys.give_access) != "TRUE"
        
        if donationView.isHidden {
            creditView.topAnchor.constraint(equalTo: versionView.bottomAnchor, constant: CGFloat(10)).isActive = true
        }
        
        creditView.isHidden = true
        creditView.isHidden = UserDefaults.standard.string(forKey: UserDefaults.Keys.give_access) != "TRUE"
        
        if creditView.isHidden {
            lbDesign.topAnchor.constraint(equalTo: versionView.bottomAnchor, constant: CGFloat(15)).isActive = true
        }
        
        passwordView.isHidden = true
        if UserDefaults.standard.string(forKey: UserDefaults.Keys.has_donation) == "TRUE" {
            passwordView.isHidden = UserDefaults.standard.string(forKey: UserDefaults.Keys.device_adult) != "TRUE"
        }
        passwordSwitch.isOn = UserDefaults.standard.bool(forKey: UserDefaults.Keys.password_enabled)
        
        if passwordView.isHidden {
            versionView.topAnchor.constraint(equalTo: profileView.bottomAnchor, constant: CGFloat(10)).isActive = true
        }
        
        lbVersion.text = "Version (\(Bundle.main.infoDictionary?["CFBundleShortVersionString"] ?? ""))"
        
        let gestureCredit = UITapGestureRecognizer(target: self, action:  #selector (self.creditDialog(_:)))
        self.creditView.addGestureRecognizer(gestureCredit)
        
        let gestureDonation = UITapGestureRecognizer(target: self, action:  #selector (self.donationDialog(_:)))
        self.donationView.addGestureRecognizer(gestureDonation)

        self.presenter.start()
    }
    
    @objc func canRotate() -> Void {}
    
    @objc func creditDialog(_ sender:UITapGestureRecognizer){
       let popUp = AboutDialog(nibName: "AboutDialog", bundle: nil)
       showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
       self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    @objc func donationDialog(_ sender:UITapGestureRecognizer){
        self.presenter.showFavorite()
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.isoLatin1)
        if let filter = CIFilter(name: "CIQRCodeGenerator"){
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 15, y: 15)
            
            if let output = filter.outputImage?.transformed(by: transform).tinted(using: .white){
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    func showInfo(image: UIImage){
        let popUp = QRCodeDialog(nibName: "QRCodeDialog", bundle: nil)
        
        popUp.image = image
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    @IBAction func passwordSwitch(_ sender: Any) {
        if passwordSwitch.isOn {
            self.showPasswordDialog()
        }else {
            UserDefaults.standard.set(false, forKey: UserDefaults.Keys.password_enabled)
            NotificationCenter.default.post(name: Notification.Name(Notification.Noti.adult_enabled), object: false)
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func showPasswordDialog() {
        let popUp = PasswordDialog(nibName: "PasswordDialog", bundle: nil)
        popUp.delegate = self
        passwordDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        passwordDialog.viewController = self
        passwordDialog.buttonAlignment = .horizontal
        self.present(passwordDialog, animated: true, completion: nil)
    }
    
    func MessageDialog() {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = "Wrong Password!"
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        let buttonRetry = DefaultButton(title: NSLocalizedString("Retry", comment: "")) {
            self.showPasswordDialog()
        }
        
        showMsgDialog.addButtons([buttonRetry])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    func check(pass: Bool, isDismiss: Bool) {
        if isDismiss {
            passwordSwitch.isOn = pass
//            darkModeDelegate.darkEnabled(status: pass)
            NotificationCenter.default.post(name: Notification.Name(Notification.Noti.adult_enabled), object: pass)
            self.navigationController?.popViewController(animated: true)
        }else {
            if pass {
                UserDefaults.standard.set(true, forKey: UserDefaults.Keys.password_enabled)
            }else {
                MessageDialog()
            }
        }
    }
    
    @IBAction func btnAppID(_ sender: Any) {
        let image = self.generateQRCode(from: String(describing: UIDevice.current.identifierForVendor!.uuidString))
        showInfo(image: image!)
    }
    
    @IBAction func btnCheckUpdate(_ sender: Any) {
        self.presenter.getVersion()
    }
    
    private func showMsgDialog(msg: String, isUpdate: Bool) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        var confirmButtons: [PopupDialogButton] = []
        
        if isUpdate {
            let buttonUpdate = DefaultButton(title: NSLocalizedString("Update", comment: "")) {
                UIApplication.shared.open(URL(string: self.downloadUrl)!, options: [:], completionHandler: nil)
            }
            
            let buttonLater = CancelButton(title: NSLocalizedString("Later", comment: "")) {
                
            }
            
            confirmButtons.append(buttonUpdate)
            confirmButtons.append(buttonLater)
        }else {
            let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
                
            }
            confirmButtons.append(buttonOk)
        }
        
        showMsgDialog.addButtons(confirmButtons)
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}

extension ATSYCAST_SettingView: ATSYCAST_SettingViewPresenterInterface {
    func displayVersion(version: Version) {
        self.version = version
    }
}
