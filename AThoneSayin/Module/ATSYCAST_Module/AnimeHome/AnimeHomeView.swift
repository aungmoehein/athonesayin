//
//  AnimeHomeView.swift
//  AnimeHome
//
//  Created by Aung Moe Hein on 04/06/2020.
//

import Foundation
import UIKit
import PopupDialog
import NVActivityIndicatorView

final class AnimeHomeView: UIViewController, ViewInterface, NVActivityIndicatorViewable {
    @IBOutlet weak var animeTV: UITableView!
    
    fileprivate var showMsgDialog: PopupDialog!
    let categories: [String] = ["Latest", "Popular", "Movies", "Recently Added"]
    
    var aniLatestDatas: [AniLatestData] = [] {
        didSet {
            animeTV.reloadData()
        }
    }
    
    var aniPopularDatas: [AniData] = [] {
        didSet {
            animeTV.reloadData()
        }
    }
    
    var aniMovies: [AniData] = [] {
        didSet {
            animeTV.reloadData()
        }
    }
    
    var aniRecents: [AniData] = [] {
        didSet {
            animeTV.reloadData()
        }
    }
    
    var aniEpisodeList: AniEpisodeList! {
        didSet {
            self.presenter.goToDetail(aniEpisodeList: self.aniEpisodeList)
        }
    }
    
    var aniLatestEpisodeData: AniLatestEpisodeData! {
        didSet {
            self.presenter.getAnimeEpisodeList(id: (aniLatestEpisodeData.anime?.id)!)
        }
    }
    
    var presenter: AnimeHomePresenterViewInterface!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        addRightBarButton()
        
        animeTV.register(UINib(nibName: "AnimeHomeTableCell", bundle: nil), forCellReuseIdentifier: "anime_home_table_cell")
        animeTV.delegate = self
        animeTV.dataSource = self
        animeTV.reloadData()

        self.presenter.start()
        self.refresh()
    }
    
    @objc func canRotate() -> Void {}
    
    func refresh() {
        self.presenter.getLatestAnime()
        self.presenter.getPopularAnime()
        self.presenter.getAnimeMovies()
        self.presenter.getRecentAnime()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }

    func addRightBarButton() {
        let info = UIButton()
        let image = UIImage(named: "ic_search")?.withRenderingMode(.alwaysTemplate)
        info.setImage(image, for: .normal)
        info.frame = CGRect(x: 0.0, y:0.0, width: 30, height:40)
        info.tintColor = UIColor(hex: 0x459a0b)
        info.contentMode = .center
        info.addTarget(self, action: #selector(searchAction), for: .touchUpInside)
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: info)
    }
    
    @objc func searchAction(sender: UIButton!) {
        self.presenter.goToSearch()
    }
}

extension AnimeHomeView: AnimeHomeTableCellDelegate {
    func click(category: String, aniData: AniData?, aniLatestData: AniLatestData?, categoryMore: String?) {
        self.startAnimating()
        if category == "Latest" {
            self.presenter.getLatestAnimeEpisode(episodeNo: (aniLatestData?.episode_num)!, slug: (aniLatestData?.anime?.slug)!)
        }else if category == "More" {
            self.presenter.goToMore(categoryTitle: categoryMore!)
        }else {
            self.presenter.getAnimeEpisodeList(id: (aniData?.id)!)
        }
    }
}

extension AnimeHomeView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = animeTV.dequeueReusableCell(withIdentifier: "anime_home_table_cell", for: indexPath) as! AnimeHomeTableCell
        cell.selectionStyle = .none
        cell.categoryTitle.text = categories[indexPath.row]
        cell.delegate = self
        switch categories[indexPath.row] {
        case "Latest":
            cell.aniLatestDatas = aniLatestDatas
        case "Popular":
            cell.aniDatas = aniPopularDatas
        case "Movies":
            cell.aniDatas = aniMovies
        default:
            cell.aniDatas = aniRecents
        }
        return cell
    }
}

extension AnimeHomeView: AnimeHomeViewPresenterInterface {
    func displayLatestAnime(aniLatestDatas: [AniLatestData]) {
        self.aniLatestDatas = aniLatestDatas
    }
    
    func displayPopularAnime(aniPopularDatas: [AniData]) {
        self.aniPopularDatas = aniPopularDatas
    }
    
    func displayAnimeMovies(aniMovies: [AniData]) {
        self.aniMovies = aniMovies
    }
    
    func displayAnimeRecents(aniRecents: [AniData]) {
        self.aniRecents = aniRecents
    }
    
    func displayAnimeEpisodeList(aniEpisodeList: AniEpisodeList) {
        self.stopAnimating()
        self.aniEpisodeList = aniEpisodeList
    }
    
    func displayAnimeLatestEpisode(aniLatestEpisodeData: AniLatestEpisodeData) {
        self.stopAnimating()
        self.aniLatestEpisodeData = aniLatestEpisodeData
    }
    
    func displayError() {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = "Something went wrong!"
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonRetry = DefaultButton(title: NSLocalizedString("Retry", comment: "")) {
            self.refresh()
        }
        
        let buttonCancel = CancelButton(title: NSLocalizedString("Cancel", comment: "")) {
            
        }
        showMsgDialog.addButtons([buttonCancel, buttonRetry])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}
