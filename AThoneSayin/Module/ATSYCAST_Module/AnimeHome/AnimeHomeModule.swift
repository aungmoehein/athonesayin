//
//  AnimeHomeModule.swift
//  AnimeHome
//
//  Created by Aung Moe Hein on 04/06/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol AnimeHomeRouterPresenterInterface: RouterPresenterInterface {
    func goToSearch()
    func goToDetail(aniEpisodeList: AniEpisodeList)
    func goToMore(categoryTitle: String)
}

// MARK: - presenter

protocol AnimeHomePresenterRouterInterface: PresenterRouterInterface {

}

protocol AnimeHomePresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchLatestAnime(aniLatestDatas: [AniLatestData])
    func onFetchPopularAnime(aniPopularDatas: [AniData])
    func onFetchAnimeMovies(aniMovies: [AniData])
    func onFetchAnimeRecents(aniRecents: [AniData])
    func onFetchAnimeEpisodeList(aniEpisodeList: AniEpisodeList)
    func onFetchAnimeLatestEpisode(aniLatestEpisodeData: AniLatestEpisodeData)
    func onFetchError()
}

protocol AnimeHomePresenterViewInterface: PresenterViewInterface {
    func start()
    func getLatestAnime()
    func getPopularAnime()
    func getAnimeMovies()
    func getRecentAnime()
    func goToSearch()
    func getAnimeEpisodeList(id: Int)
    func goToDetail(aniEpisodeList: AniEpisodeList)
    func getLatestAnimeEpisode(episodeNo: String, slug: String)
    func goToMore(categoryTitle: String)
}

// MARK: - interactor

protocol AnimeHomeInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchLatestAnime()
    func fetchPopularAnime()
    func fetchAnimeMovies()
    func fetchRecentAnime()
    func fetchAnimeEpisodes(id: Int)
    func fetchAnimeLatestEpisode(episodeNo: String, slug: String)
}

// MARK: - view

protocol AnimeHomeViewPresenterInterface: ViewPresenterInterface {
    func displayLatestAnime(aniLatestDatas: [AniLatestData])
    func displayPopularAnime(aniPopularDatas: [AniData])
    func displayAnimeMovies(aniMovies: [AniData])
    func displayAnimeRecents(aniRecents: [AniData])
    func displayAnimeEpisodeList(aniEpisodeList: AniEpisodeList)
    func displayAnimeLatestEpisode(aniLatestEpisodeData: AniLatestEpisodeData)
    func displayError()
}


// MARK: - module builder

final class AnimeHomeModule: ModuleInterface {

    typealias View = AnimeHomeView
    typealias Presenter = AnimeHomePresenter
    typealias Router = AnimeHomeRouter
    typealias Interactor = AnimeHomeInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "anime_home") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
