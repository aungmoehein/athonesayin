//
//  AnimeHomeInteractor.swift
//  AnimeHome
//
//  Created by Aung Moe Hein on 04/06/2020.
//

import Foundation

final class AnimeHomeInteractor: InteractorInterface {

    weak var presenter: AnimeHomePresenterInteractorInterface!
}

extension AnimeHomeInteractor: AnimeHomeInteractorPresenterInterface {
    func fetchLatestAnime() {
        Animeflix.sharedInstance.getLatestAnime(page: 1).load()
            .onSuccess() {data in
            if let aniLatestList: AniLatestList = data.typedContent() {
                self.presenter.onFetchLatestAnime(aniLatestDatas: aniLatestList.data!)
            }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
    
    func fetchPopularAnime() {
        Animeflix.sharedInstance.getPopularAnime(page: 1).load()
            .onSuccess() {data in
            if let aniList: AniList = data.typedContent() {
                self.presenter.onFetchPopularAnime(aniPopularDatas: aniList.data!)
            }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
    
    func fetchAnimeMovies() {
        Animeflix.sharedInstance.getAnimeMovie(page: 1).load()
            .onSuccess() {data in
            if let aniList: AniList = data.typedContent() {
                self.presenter.onFetchAnimeMovies(aniMovies: aniList.data!)
            }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
    
    func fetchRecentAnime() {
        Animeflix.sharedInstance.getRecentAnime(page: 1).load()
            .onSuccess() {data in
            if let aniList: AniList = data.typedContent() {
                self.presenter.onFetchAnimeRecents(aniRecents: aniList.data!)
            }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
    
    func fetchAnimeEpisodes(id: Int) {
        Animeflix.sharedInstance.getAnimeEpisodes(id: id, page: 1, sort: "DESC").load()
            .onSuccess() {data in
            if let aniEpisodeList: AniEpisodeList = data.typedContent() {
                self.presenter.onFetchAnimeEpisodeList(aniEpisodeList: aniEpisodeList)
            }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
    
    func fetchAnimeLatestEpisode(episodeNo: String, slug: String) {
        Animeflix.sharedInstance.getLatestAnimeEpisode(episodeNo: episodeNo, slug: slug).load()
            .onSuccess() {data in
            if let aniLatestEpisode: AniLatestEpisode = data.typedContent() {
                self.presenter.onFetchAnimeLatestEpisode(aniLatestEpisodeData: aniLatestEpisode.data!)
            }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
}
