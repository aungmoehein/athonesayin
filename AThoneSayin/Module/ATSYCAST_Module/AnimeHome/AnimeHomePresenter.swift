//
//  AnimeHomePresenter.swift
//  AnimeHome
//
//  Created by Aung Moe Hein on 04/06/2020.
//

import Foundation

final class AnimeHomePresenter: PresenterInterface {

    var router: AnimeHomeRouterPresenterInterface!
    var interactor: AnimeHomeInteractorPresenterInterface!
    weak var view: AnimeHomeViewPresenterInterface!

}

extension AnimeHomePresenter: AnimeHomePresenterRouterInterface {

}

extension AnimeHomePresenter: AnimeHomePresenterInteractorInterface {
    func onFetchLatestAnime(aniLatestDatas: [AniLatestData]) {
        self.view.displayLatestAnime(aniLatestDatas: aniLatestDatas)
    }
    
    func onFetchPopularAnime(aniPopularDatas: [AniData]) {
        self.view.displayPopularAnime(aniPopularDatas: aniPopularDatas)
    }
    
    func onFetchAnimeMovies(aniMovies: [AniData]) {
        self.view.displayAnimeMovies(aniMovies: aniMovies)
    }
    
    func onFetchAnimeRecents(aniRecents: [AniData]) {
        self.view.displayAnimeRecents(aniRecents: aniRecents)
    }
    
    func onFetchAnimeEpisodeList(aniEpisodeList: AniEpisodeList) {
        self.view.displayAnimeEpisodeList(aniEpisodeList: aniEpisodeList)
    }
    
    func onFetchAnimeLatestEpisode(aniLatestEpisodeData: AniLatestEpisodeData) {
        self.view.displayAnimeLatestEpisode(aniLatestEpisodeData: aniLatestEpisodeData)
    }
    
    func onFetchError() {
        self.view.displayError()
    }
}

extension AnimeHomePresenter: AnimeHomePresenterViewInterface {
    func start() {

    }

    func getLatestAnime() {
        self.interactor.fetchLatestAnime()
    }
    
    func getPopularAnime() {
        self.interactor.fetchPopularAnime()
    }
    
    func getAnimeMovies() {
        self.interactor.fetchAnimeMovies()
    }
    
    func getRecentAnime() {
        self.interactor.fetchRecentAnime()
    }
    
    func goToSearch() {
        self.router.goToSearch()
    }
    
    func getAnimeEpisodeList(id: Int) {
        self.interactor.fetchAnimeEpisodes(id: id)
    }
    
    func goToDetail(aniEpisodeList: AniEpisodeList) {
        self.router.goToDetail(aniEpisodeList: aniEpisodeList)
    }
    
    func getLatestAnimeEpisode(episodeNo: String, slug: String) {
        self.interactor.fetchAnimeLatestEpisode(episodeNo: episodeNo, slug: slug)
    }
    
    func goToMore(categoryTitle: String) {
        self.router.goToMore(categoryTitle: categoryTitle)
    }
}
