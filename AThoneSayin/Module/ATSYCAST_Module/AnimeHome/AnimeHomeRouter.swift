//
//  AnimeHomeRouter.swift
//  AnimeHome
//
//  Created by Aung Moe Hein on 04/06/2020.
//

import Foundation
import UIKit

final class AnimeHomeRouter: RouterInterface {

    weak var presenter: AnimeHomePresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension AnimeHomeRouter: AnimeHomeRouterPresenterInterface {
    func goToSearch() {
        let vc = AnimeSearchModule().build() as! AnimeSearchView
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToDetail(aniEpisodeList: AniEpisodeList) {
        let vc = AnimeDetailModule().build() as! AnimeDetailView
        vc.animeData = aniEpisodeList.anime
        vc.animeEpisodes = aniEpisodeList.data!
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToMore(categoryTitle: String) {
        let vc = AnimeMoreModule().build() as! AnimeMoreView
        vc.navTitle = categoryTitle
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
