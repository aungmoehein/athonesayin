//
//  PartnerPageView.swift
//  PartnerPage
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation
import UIKit
import XLPagerTabStrip
import NVActivityIndicatorView
import PopupDialog
import Firebase

final class PartnerPageView: UIViewController, ViewInterface, IndicatorInfoProvider, NVActivityIndicatorViewable {
    @IBOutlet weak var tbPartner: UITableView!
    
    var itemInfo = IndicatorInfo("ATSY Cast")
    var presenter: PartnerPagePresenterViewInterface!
    
    fileprivate var refresher: UIRefreshControl!
    var partnerCount: Int = 0
    var msubTitle: String = ""
    var msubImg: String = ""
    var animeTitle: String = ""
    var animeImg: String = ""
    var epTitle: String = ""
    var epImg: String = ""
    var partners: [Partner] = [] {
        didSet{
            partnerCount = partners.count
            self.tbPartner.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        self.tbPartner.register(UINib(nibName: "PartnerCell", bundle: nil), forCellReuseIdentifier: "partner_cell")
        self.tbPartner.delegate = self
        self.tbPartner.dataSource = self
        self.tbPartner.reloadData()
        
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(self.onRefresh), for: UIControl.Event.valueChanged)
        tbPartner.addSubview(refresher)

        self.startAnimating()
        self.presenter.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func canRotate() -> Void {}
    
    @objc private func onRefresh() {
        self.startAnimating()
        self.presenter.start()
        refresher.endRefreshing()
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension PartnerPageView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partnerCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "partner_cell", for: indexPath) as! PartnerCell
        cell.selectionStyle = .none
        cell.title = partners[indexPath.row].name!
        
        switch partners[indexPath.row].name {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            cell.img = "z_channel"
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            cell.img = "m_sub"
        default:
            cell.img = partners[indexPath.row].logo!
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Analytics.logEvent(partners[indexPath.row].name!, parameters: nil)
        self.presenter.catchAnalytics(actionType: partners[indexPath.row].name!, deviceID: String(describing: UIDevice.current.identifierForVendor!.uuidString), deviceName: UIDevice.modelName)
        if partners[indexPath.row].key! == UserDefaults.standard.string(forKey: UserDefaults.Keys.idAnimeflix) {
            self.presenter.animeView()
        }else {
            self.presenter.categoryView(title: partners[indexPath.row].name!, apiKey: partners[indexPath.row].apikey!, partnerKey: partners[indexPath.row].key!)
        }
    }
}

extension PartnerPageView: PartnerPageViewPresenterInterface {
    func displayPartners(partners: [Partner]) {
        self.stopAnimating()
        self.partners = partners
    }
    
    func onError() {
        self.stopAnimating()
        let title = "Something went wrong."
        let message = "Check your internet connection."
        let image = UIImage(named: "no_wifi")

        let popup = PopupDialog(title: title, message: message, image: image)
        let retryBtn = DefaultButton(title: "Retry", dismissOnTap: false) {
            self.onRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        
        popup.addButtons([retryBtn])
        self.present(popup, animated: true, completion: nil)
    }
}
