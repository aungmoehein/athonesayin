//
//  PartnerPageRouter.swift
//  PartnerPage
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation
import UIKit

final class PartnerPageRouter: RouterInterface {

    weak var presenter: PartnerPagePresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension PartnerPageRouter: PartnerPageRouterPresenterInterface {
    func goToMyanDark(title: String, apiKey: String, partnerKey: String) {
        let vc = CategoryModule().build() as! CategoryView
        vc.categoryTitle = title
        vc.apiKey = apiKey
        vc.partnerKey = partnerKey
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToAnime() {
        let vc = AnimeHomeModule().build() as! AnimeHomeView
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
