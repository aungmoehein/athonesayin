//
//  PartnerPagePresenter.swift
//  PartnerPage
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation

final class PartnerPagePresenter: PresenterInterface {

    var router: PartnerPageRouterPresenterInterface!
    var interactor: PartnerPageInteractorPresenterInterface!
    weak var view: PartnerPageViewPresenterInterface!

}

extension PartnerPagePresenter: PartnerPagePresenterRouterInterface {

}

extension PartnerPagePresenter: PartnerPagePresenterInteractorInterface {
    func onFetchPartners(partners: [Partner]) {
        self.view.displayPartners(partners: partners)
    }
    
    func onFetchError() {
        self.view.onError()
    }
}

extension PartnerPagePresenter: PartnerPagePresenterViewInterface {

    func start() {
        self.interactor.fetchPartners()
    }

    func categoryView(title: String, apiKey: String, partnerKey: String) {
        router.goToMyanDark(title: title, apiKey: apiKey, partnerKey: partnerKey)
    }
    
    func animeView() {
        router.goToAnime()
    }
    
    func catchAnalytics(actionType: String, deviceID: String, deviceName: String) {
        self.interactor.sendAnalytics(actionType: actionType, deviceID: deviceID, deviceName: deviceName)
    }
}
