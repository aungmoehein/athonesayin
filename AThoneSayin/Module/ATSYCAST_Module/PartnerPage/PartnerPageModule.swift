//
//  PartnerPageModule.swift
//  PartnerPage
//
//  Created by Aung Moe Hein on 23/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol PartnerPageRouterPresenterInterface: RouterPresenterInterface {
    func goToMyanDark(title: String, apiKey: String, partnerKey: String)
    func goToAnime()
}

// MARK: - presenter

protocol PartnerPagePresenterRouterInterface: PresenterRouterInterface {

}

protocol PartnerPagePresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchPartners(partners: [Partner])
    func onFetchError()
}

protocol PartnerPagePresenterViewInterface: PresenterViewInterface {
    func start()
    func categoryView(title: String, apiKey: String, partnerKey: String)
    func animeView()
    func catchAnalytics(actionType: String, deviceID: String, deviceName: String)
}

// MARK: - interactor

protocol PartnerPageInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchPartners()
    func sendAnalytics(actionType: String, deviceID: String, deviceName: String)
}

// MARK: - view

protocol PartnerPageViewPresenterInterface: ViewPresenterInterface {
    func displayPartners(partners: [Partner])
    func onError()
}


// MARK: - module builder

final class PartnerPageModule: ModuleInterface {

    typealias View = PartnerPageView
    typealias Presenter = PartnerPagePresenter
    typealias Router = PartnerPageRouter
    typealias Interactor = PartnerPageInteractor

    func build() -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "partner_page") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
