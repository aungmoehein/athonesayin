//
//  PartnerPageInteractor.swift
//  PartnerPage
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation

final class PartnerPageInteractor: InteractorInterface {
    fileprivate var hasDonation = UserDefaults.standard.string(forKey: UserDefaults.Keys.has_donation)
    weak var presenter: PartnerPagePresenterInteractorInterface!
}

extension PartnerPageInteractor: PartnerPageInteractorPresenterInterface {
    func fetchPartnerKeys() {
        var partners: [Partner] = []
        SheetService.sharedInstance.partnerList.child("/7/public/values").withParam("alt", "json").load()
            .onSuccess(){data in
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Partner].self, from: data)
                        jsonDictionary.forEach() {
                            partner in
                            if partner.ioskey == "TRUE" {
                                if UserDefaults.standard.string(forKey: UserDefaults.Keys.device_adult) == "TRUE" && self.hasDonation == "TRUE" && UserDefaults.standard.bool(forKey: UserDefaults.Keys.password_enabled) {
                                    if UserDefaults.standard.string(forKey: UserDefaults.Keys.anime_access) == "TRUE" && self.hasDonation == "TRUE" {
                                        partners.append(partner)
                                    }else {
                                        if partner.comment != "anime" {
                                            partners.append(partner)
                                        }
                                    }
                                } else {
                                    if partner.password == "FALSE" {
                                        if UserDefaults.standard.string(forKey: UserDefaults.Keys.anime_access) == "TRUE" && self.hasDonation == "TRUE" {
                                            partners.append(partner)
                                        }else {
                                            if partner.comment != "anime" {
                                                partners.append(partner)
                                            }
                                        }
                                    }
                                }
                            }
                            
                            switch partner.key {
                            case "1":
                                UserDefaults.standard.set(partner.name, forKey: UserDefaults.Keys.partnerZChannel)
                            case "2":
                                UserDefaults.standard.set(partner.name, forKey: UserDefaults.Keys.partnerMovieMM)
                            case "3":
                                UserDefaults.standard.set(partner.name, forKey: UserDefaults.Keys.partnerMDark)
                            case "7":
                                UserDefaults.standard.set(partner.name, forKey: UserDefaults.Keys.partnerMSub)
                            case "5":
                                UserDefaults.standard.set(partner.name, forKey: UserDefaults.Keys.partnerDoujin)
                            case "10":
                                UserDefaults.standard.set(partner.name, forKey: UserDefaults.Keys.partnerAnimeflix)
                            case "9":
                                UserDefaults.standard.set(partner.name, forKey: UserDefaults.Keys.partnerEPorner)
                            default:
                                break
                            }
                        }
                        self.presenter.onFetchPartners(partners: partners)
                    } catch {
                        // Handle error
                        print(error)
                    }
                }
        }.onFailure(){error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchPartners() {
        SheetService.sharedInstance.config.child("/2/public/values").withParam("alt", "json").load()
            .onSuccess(){ data in
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        jsonDictionary.forEach() {
                            json in
                            let id = json["id"]!
                            let url = json["url"]!
                            
                            switch id {
                            case "1":
                                UserDefaults.standard.set(id, forKey: UserDefaults.Keys.idZChannel)
                                UserDefaults.standard.set(url, forKey: UserDefaults.Keys.z_channel)
                            case "2":
                                UserDefaults.standard.set(id, forKey: UserDefaults.Keys.idMovieMM)
                                UserDefaults.standard.set(url, forKey: UserDefaults.Keys.movie_mm)
                            case "3":
                                UserDefaults.standard.set(id, forKey: UserDefaults.Keys.idMDark)
                                UserDefaults.standard.set(url, forKey: UserDefaults.Keys.m_dark)
                            case "7":
                                UserDefaults.standard.set(id, forKey: UserDefaults.Keys.idMSub)
                                UserDefaults.standard.set(url, forKey: UserDefaults.Keys.m_sub)
                                UserDefaults.standard.set(json["apikey"]!, forKey: UserDefaults.Keys.m_sub_key_route)
                            case "5":
                                UserDefaults.standard.set(id, forKey: UserDefaults.Keys.idDoujin)
                                UserDefaults.standard.set(url, forKey: UserDefaults.Keys.hentai_b)
                                UserDefaults.standard.set(json["media"]!, forKey: UserDefaults.Keys.hentai_m)
                                UserDefaults.standard.set(json["apikey"]!, forKey: UserDefaults.Keys.hentai_k)
                                UserDefaults.standard.set(json["apiversion"]!, forKey: UserDefaults.Keys.hentai_v)
                            case "10":
                                UserDefaults.standard.set(id, forKey: UserDefaults.Keys.idAnimeflix)
                                UserDefaults.standard.set(url, forKey: UserDefaults.Keys.a_flix)
                            case "9":
                                UserDefaults.standard.set(id, forKey: UserDefaults.Keys.idEPorner)
                                UserDefaults.standard.set(url, forKey: UserDefaults.Keys.e_porner)
                                UserDefaults.standard.set(json["apiversion"]!, forKey: UserDefaults.Keys.e_porner_v)
                            default:
                                break
                            }
                        }
                        self.fetchMSubMovieKey()
                        self.fetchPartnerKeys()
                    } catch {
                        // Handle error
                        print(error)
                    }
                }
        }.onFailure(){error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchMSubMovieKey() {
        guard let url = URL(string: "\(UserDefaults.standard.string(forKey: UserDefaults.Keys.m_sub_key_route)!)/download/api2.txt") else { return }
        let task = URLSession.shared.dataTask(with: url) { data, response, error in

          guard let data = data, error == nil else { return }

            UserDefaults.standard.set(NSString(data: data, encoding: String.Encoding.utf8.rawValue)!, forKey: UserDefaults.Keys.m_sub_key)
        }

        task.resume()
    }
    
    func sendAnalytics(actionType: String, deviceID: String, deviceName: String) {
        AnalyticService.sharedInstance.sendAnalytic().request(.post, urlEncoded: ["actionType": actionType, "deviceID": deviceID, "deviceName": deviceName, "action": "analytics", "appID": Bundle.main.bundleIdentifier!]).onSuccess() {
                data in
            debugPrint("Send Success")
        }.onFailure() {
            error in
            print(error)
        }
    }
}
