//
//  HomeTabController.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 21/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import PopupDialog
import XLPagerTabStrip

class HomeTabController: ButtonBarPagerTabStripViewController, UINavigationControllerDelegate {
    var isReload = false
    var initialPosition: Bool = true
    var showMsgDialog: PopupDialog!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let giveAccess = UserDefaults.standard.string(forKey: UserDefaults.Keys.give_access)
    let hasDonation = UserDefaults.standard.string(forKey: UserDefaults.Keys.has_donation)
    var pages: [UIViewController] = []
    
    var liveKeys: [LiveKey] = []

    override func viewDidLoad() {
        tabbarSetting()
        super.viewDidLoad()
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.delegate = self
        self.navigationItem.title = "ATSY Cast"
        let textAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = textAttributes
        self.navigationController?.navigationBar.barTintColor = UIColor(hex: 0x050505)
        self.navigationController?.navigationBar.tintColor = .white

        addNavButtons()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onCurrentAdultEnabled(notification:)), name: Notification.Name(Notification.Noti.adult_enabled), object: nil)
        
        self.containerView.delegate = self
        buttonBarView.selectedBar.backgroundColor = UIColor(hex: 0x459a0b)
        
        CMDownloader.sharedInstance.getHome().onSuccess() {
            data in
            
            debugPrint("DATA::: \(data)")
            
        }.onFailure() {
            error in
            
            debugPrint("error:: \(error.localizedDescription)")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }
    
    @objc func canRotate() -> Void {}
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func addNavButtons() {
        let info = UIButton()
        let image = UIImage(named: "ic_info_atsy")?.withRenderingMode(.alwaysTemplate)
        info.setImage(image, for: .normal)
        info.frame = CGRect(x: 0.0, y:0.0, width: 30, height:40)
        info.tintColor = .white
        info.contentMode = .center
        info.addTarget(self, action: #selector(infoAction), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: info)
        
        let athoneSayin = UIButton()
        let atsy_image = UIImage(named: "ic_atsy")
        athoneSayin.setImage(atsy_image, for: .normal)
        athoneSayin.frame = CGRect(x: 0.0, y:0.0, width: 30, height:40)
        athoneSayin.contentMode = .center
        athoneSayin.addTarget(self, action: #selector(athoneSayinAction), for: .touchUpInside)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: athoneSayin)
    }
    
    @objc func athoneSayinAction(sender: UIButton!) {
        UIApplication.shared.statusBarStyle = .default
        let is_secure = UserDefaults.standard.bool(forKey: UserDefaults.Keys.is_secure)
        if is_secure {
            let vc = PasscodeModule().build(db: appDelegate.db) as! PasscodeView
            vc.isLogin = true
            UIApplication.shared.windows.first?.rootViewController = UINavigationController(rootViewController: vc)
            UIView.transition(with: UIApplication.shared.windows.first!, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion:
            { completed in
                UIApplication.shared.windows.first?.makeKeyAndVisible()
            })
        }else {
            let Storyboard = UIStoryboard(name: "Main", bundle: nil)
            let view = Storyboard.instantiateViewController(withIdentifier: "host") as! ViewController
            view.db = appDelegate.db
            UIApplication.shared.windows.first?.rootViewController = UINavigationController(rootViewController: view)
            UIView.transition(with: UIApplication.shared.windows.first!, duration: 0.3, options: .transitionCrossDissolve, animations: {}, completion:
            { completed in
                UIApplication.shared.windows.first?.makeKeyAndVisible()
            })
        }
    }
    
    @objc func infoAction(sender: UIButton!) {
        let view = ATSYCAST_SettingModule().build() as! ATSYCAST_SettingView
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.isoLatin1)
        if let filter = CIFilter(name: "CIQRCodeGenerator"){
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 15, y: 15)
            
            if let output = filter.outputImage?.transformed(by: transform).tinted(using: .white){
                return UIImage(ciImage: output)
            }
        }
        return nil
    }
    
    func showInfo(image: UIImage){
        let popUp = QRCodeDialog(nibName: "QRCodeDialog", bundle: nil)
        
        popUp.image = image
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: true)
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    override func viewDidLayoutSubviews () {
        super.viewDidLayoutSubviews ()
        
        if initialPosition && giveAccess == "TRUE" {
            self.moveToViewController(at: 1, animated: false)
            initialPosition = false
        }
    }
    
    func tabbarSetting() {
        settings.style.buttonBarBackgroundColor = UIColor(hex: 0x1c1c1c)
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarHeight = 2.0
        settings.style.buttonBarMinimumLineSpacing = 0
        settings.style.buttonBarItemTitleColor = .white
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 10
        settings.style.buttonBarRightContentInset = 10
//        settings.style.buttonBarMinimumInteritemSpacing = 10
        
        changeCurrentIndexProgressive = {(oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            oldCell?.label.textColor = .white
            newCell?.label.textColor = UIColor(hex: 0x459a0b)
        }
    }
    
    @objc func onCurrentAdultEnabled(notification: Notification) {
        pages.removeAll()
        reloadPagerTabStripView()
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        pages = []
        if  giveAccess == "TRUE" {
            let recent = RecentlyPlayedModule().build() as! RecentlyPlayedView
            pages.append(recent)
            
            let partner = PartnerPageModule().build() as! PartnerPageView
            pages.append(partner)
        }
        
        liveKeys.forEach() {
            liveKey in
            if UserDefaults.standard.string(forKey: UserDefaults.Keys.all_channel) == "TRUE" {
                if liveKey.name == "21+" {
                    if UserDefaults.standard.string(forKey: UserDefaults.Keys.device_adult) == "TRUE" && hasDonation == "TRUE" && UserDefaults.standard.bool(forKey: UserDefaults.Keys.password_enabled) {
                        let live = ATSYCAST_HomeModule().build(liveKey: liveKey) as! ATSYCAST_HomeView
                        pages.append(live)
                    }
                }else {
                    let live = ATSYCAST_HomeModule().build(liveKey: liveKey) as! ATSYCAST_HomeView
                    pages.append(live)
                }
            }else {
                if liveKey.name == "Free to Air" {
                    let live = ATSYCAST_HomeModule().build(liveKey: liveKey) as! ATSYCAST_HomeView
                    pages.append(live)
                }
            }
        }
        
        guard isReload else {
            return pages
        }
        return pages
    }
    
    override func reloadPagerTabStripView() {
        isReload = true
        if arc4random() % 2 == 0 {
            pagerBehaviour = .progressive(skipIntermediateViewControllers: arc4random() % 2 == 0, elasticIndicatorLimit: arc4random() % 2 == 0 )
        } else {
            pagerBehaviour = .common(skipIntermediateViewControllers: arc4random() % 2 == 0)
        }
        super.reloadPagerTabStripView()
    }
}
