//
//  WelcomeView.swift
//  Welcome
//
//  Created by Aung Moe Hein on 28/05/2020.
//

import Foundation
import UIKit
import PopupDialog
import Lottie
import Firebase

final class WelcomeView: UIViewController, ViewInterface, UIWebViewDelegate {
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var imgLogo: UIImageView!
    @IBOutlet weak var lbAppName: UILabel!
    
    var passwordDialog: PopupDialog!
    var showMsgDialog: PopupDialog!
    var presenter: WelcomePresenterViewInterface!
    
    var giveAccess: String = ""
    
    var liveKeys: [LiveKey] = [] {
        didSet {
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2)) {
                self.presenter.showHomePage(liveKeys: self.liveKeys)
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        UIApplication.shared.statusBarStyle = .lightContent
        animationView.animation = Animation.named("movie_loading")
        animationView.loopMode = .loop
        animationView.play()

        if presenter != nil {
            self.presenter.start()
            self.presenter.getDeviceIDList()
        }
    }
    
    @objc func canRotate() -> Void {}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        imgLogo.alpha = 0.0
        lbAppName.alpha = 0.0
        animationView.alpha = 0.0
        
        self.navigationController?.navigationBar.isHidden = true
        
        let isInitiallyLandscape = UIApplication.shared.statusBarOrientation.isLandscape
        
        if presenter != nil {
            presenter.getDeviceOrientation(isInitiallyLandscape: isInitiallyLandscape)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        UIView.animate(withDuration: 1, animations: {
            self.imgLogo.alpha = 1
            self.imgLogo.frame.origin.y += 20
           
            self.lbAppName.alpha = 1
            self.lbAppName.frame.origin.y -= 20
        }, completion: {
            _ in
            self.animationView.alpha = 1
        })
    }
    
     override var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}

extension WelcomeView: WelcomeViewPresenterInterface {
    func displayLiveKeys(liveKeys: [LiveKey]) {
        self.liveKeys = liveKeys
    }
}
