//
//  WelcomeInteractor.swift
//  Welcome
//
//  Created by Aung Moe Hein on 28/05/2020.
//

import Foundation
import UIKit
import AesEverywhere

final class WelcomeInteractor: InteractorInterface {
    
    weak var presenter: WelcomePresenterInteractorInterface!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var isInitiallyLandscape: Bool {
        get {
            return UserDefaults.standard.bool(forKey: UserDefaults.Keys.is_initially_landscape)
        }
        set(newValue) {
            UserDefaults.standard.set(newValue, forKey: UserDefaults.Keys.is_initially_landscape)
        }
    }
}

extension WelcomeInteractor: WelcomeInteractorPresenterInterface {
    func fetchDeviceID() {
        //Fetch Partner Sheet ID
        self.fetchPartnerSheetID()
        
        //Fetch Analytics URL
        self.fetchAnalyticURL()
        
        //Fetch Donation Title
        self.fetchDonationTitle()
    }
    
    func fetchPartnerSheetID() {
        UserDefaults.standard.set("", forKey: UserDefaults.Keys.partner_sheet_id)
        SheetService.sharedInstance.config.child("/1/public/values").withParam("alt", "json").load()
            .onSuccess(){data in
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        jsonDictionary.forEach() {
                            json in
                            let sheet = json["sheet"]!
                            
                            UserDefaults.standard.set(sheet, forKey: UserDefaults.Keys.partner_sheet_id)
                        }
                    } catch {
                        // Handle error
                        print(error)
                    }
                    
                    self.fetchLiveKeys()
                }
        }.onFailure(){
            error in
            debugPrint(error.userMessage)
        }
    }
    
    func fetchAnalyticURL() {
        SheetService.sharedInstance.config.child("/4/public/values").withParam("alt", "json").load()
            .onSuccess() {data in
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        jsonDictionary.forEach() {
                            json in
                            UserDefaults.standard.set(json["url"]!, forKey: UserDefaults.Keys.kh_analytics)
                            UserDefaults.standard.set(json["apiversion"]!, forKey: UserDefaults.Keys.kh_analytics_version)
                        }
                    }catch {
                        print(error)
                    }
                }
                
                let isFirstTime = UserDefaults.standard.bool(forKey: UserDefaults.Keys.first_time)
                if isFirstTime {
                    self.sendSurvey()
                }
        }.onFailure() {
            error in
            debugPrint(error.userMessage)
        }
    }
    
    func fetchLiveKeys() {
        SheetService.sharedInstance.partnerList.child("/4/public/values").withParam("alt", "json").load()
            .onSuccess(){data in
                var liveKeys: [LiveKey] = []
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        jsonDictionary.forEach() {
                            json in
                            let name = json["name"]!
                            let status = json["status"]!
                            let number = json["number"]!
                            
                            if Int(number) != nil {
                                liveKeys.append(LiveKey(name: name, status: status, number: number))
                            }
                        }
                    } catch {
                        // Handle error
                        print(error)
                    }
                }
                
                if self.presenter != nil {
                    self.presenter.onFetchLiveKeys(liveKeys: liveKeys)
                }
        }.onFailure(){
            error in
            debugPrint(error.userMessage)
        }
    }
    
    func fetchDonationTitle() {
        SheetService.sharedInstance.config.child("/5/public/values").withParam("alt", "json").load()
            .onSuccess(){data in
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        jsonDictionary.forEach() {
                            json in
                            let title = json["title"]!
                            
                            UserDefaults.standard.set(title, forKey: UserDefaults.Keys.donation_title)
                        }
                    } catch {
                        // Handle error
                        print(error)
                    }
                }
        }.onFailure(){
            error in
            debugPrint(error.userMessage)
        }
    }
    
    func sendSurvey() {
        AnalyticService.sharedInstance.sendAnalytic().request(.post, urlEncoded: ["age": "All age", "gender": "All gender", "deviceID": String(describing: UIDevice.current.identifierForVendor!.uuidString), "deviceName": UIDevice.modelName, "action": "survey", "appID": Bundle.main.bundleIdentifier!]).onSuccess() {
                data in
            let decrypted = try! AES256.decrypt(input: data.jsonDict["value"] as! String, passphrase: "~pos@2020~")
            if let data = decrypted.data(using: String.Encoding.utf8) {
                do {
                    let decoder = JSONDecoder()
                    let jsonDictionary = try decoder.decode(SurveyData.self, from: data)
                    if jsonDictionary.status == 200 {
                        UserDefaults.standard.set(false, forKey: UserDefaults.Keys.first_time)
                    }
                }catch {
                
                }
            }
        }.onFailure() {
            error in
            print(error)
        }
    }
    
    func insertDefaultStream() {
        let bigBuckBunny: Stream = Stream(id: 0, name: "BigBuckBunny", url: "https://multiplatform-f.akamaihd.net/i/multi/will/bunny/big_buck_bunny_,640x360_400,640x360_700,640x360_1000,950x540_1500,.f4v.csmil/index_1_av.m3u8")
        
        let elephantDream: Stream = Stream(id: 1, name: "Elephant Dream", url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ElephantsDream.mp4")
        
        let issLive: Stream = Stream(id: 2, name: "ISS HD Live", url: "http://hls.ums.ustream.tv/playlist/directhls/channel/17074538/playlist.m3u8?sgn=23d01ff63b69b7f4c64f8b170e0cbf8b196af2da")
        
        let Sintel: Stream = Stream(id: 3, name: "Sintel", url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/Sintel.mp4")
        
        let tearOfSteel: Stream = Stream(id: 4, name: "Tear of Steel", url: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/TearsOfSteel.mp4")
        
        let wildLife: Stream = Stream(id: 5, name: "Wild Life", url: "http://playertest.longtailvideo.com/adaptive/wowzaid3/chunklist_w249832652.m3u8")
        
        let defaultStreams: [Stream] = [bigBuckBunny, elephantDream, issLive, Sintel, tearOfSteel, wildLife]
        
        defaultStreams.forEach() {
            defaultStream in
            
            appDelegate.db.insertStream(name: defaultStream.name, url: defaultStream.url)
        }
    }
}
