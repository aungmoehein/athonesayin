//
//  WelcomeRouter.swift
//  Welcome
//
//  Created by Aung Moe Hein on 28/05/2020.
//

import Foundation
import UIKit

final class WelcomeRouter: RouterInterface {

    weak var presenter: WelcomePresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension WelcomeRouter: WelcomeRouterPresenterInterface {
    func goToHome(liveKeys: [LiveKey]) {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "home_tab") as! HomeTabController
        view.liveKeys = liveKeys
        let navigationController = UINavigationController(rootViewController: view)
        navigationController.modalPresentationStyle = .fullScreen
        viewController?.present(navigationController, animated: true, completion: nil)
    }
    
    func goToStreamTest() {
        let view = StreamTestModule().build() as! StreamTestView
        let navigationController = UINavigationController(rootViewController: view)
        navigationController.modalPresentationStyle = .fullScreen
        viewController?.present(navigationController, animated: true, completion: nil)
    }
}
