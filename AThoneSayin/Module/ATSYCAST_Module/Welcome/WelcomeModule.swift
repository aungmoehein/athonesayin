//
//  WelcomeModule.swift
//  Welcome
//
//  Created by Aung Moe Hein on 28/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol WelcomeRouterPresenterInterface: RouterPresenterInterface {
    func goToHome(liveKeys: [LiveKey])
    func goToStreamTest()
}

// MARK: - presenter

protocol WelcomePresenterRouterInterface: PresenterRouterInterface {

}

protocol WelcomePresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchLiveKeys(liveKeys: [LiveKey])
}

protocol WelcomePresenterViewInterface: PresenterViewInterface {
    func start()
    func getDeviceIDList()
    func getLiveKeys()
    func showHomePage(liveKeys: [LiveKey])
    func getDeviceOrientation(isInitiallyLandscape: Bool)
    func sendSurvey()
    func showStreamTest()
    func addDefaultStreamURLs()
}

// MARK: - interactor

protocol WelcomeInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchDeviceID()
    func fetchLiveKeys()
    var isInitiallyLandscape: Bool { get set }
    func sendSurvey()
    func insertDefaultStream()
}

// MARK: - view

protocol WelcomeViewPresenterInterface: ViewPresenterInterface {
    func displayLiveKeys(liveKeys: [LiveKey])
}


// MARK: - module builder

final class WelcomeModule: ModuleInterface {

    typealias View = WelcomeView
    typealias Presenter = WelcomePresenter
    typealias Router = WelcomeRouter
    typealias Interactor = WelcomeInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "welcome") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
