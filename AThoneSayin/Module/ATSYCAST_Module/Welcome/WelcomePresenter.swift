//
//  WelcomePresenter.swift
//  Welcome
//
//  Created by Aung Moe Hein on 28/05/2020.
//

import Foundation

final class WelcomePresenter: PresenterInterface {

    var router: WelcomeRouterPresenterInterface!
    var interactor: WelcomeInteractorPresenterInterface!
    weak var view: WelcomeViewPresenterInterface!

}

extension WelcomePresenter: WelcomePresenterRouterInterface {

}

extension WelcomePresenter: WelcomePresenterInteractorInterface {
    func onFetchLiveKeys(liveKeys: [LiveKey]) {
        self.view.displayLiveKeys(liveKeys: liveKeys)
    }
}

extension WelcomePresenter: WelcomePresenterViewInterface {

    func start() {
        
    }
    
    func getDeviceIDList() {
        self.interactor.fetchDeviceID()
    }
    
    func showHomePage(liveKeys: [LiveKey]) {
        self.router.goToHome(liveKeys: liveKeys)
    }
    
    func getLiveKeys() {
        self.interactor.fetchLiveKeys()
    }
    
    func getDeviceOrientation(isInitiallyLandscape: Bool) {
        interactor.isInitiallyLandscape = isInitiallyLandscape
    }

    func sendSurvey() {
        self.interactor.sendSurvey()
    }
    
    func showStreamTest() {
        self.router.goToStreamTest()
    }
    
    func addDefaultStreamURLs() {
        self.interactor.insertDefaultStream()
    }
}
