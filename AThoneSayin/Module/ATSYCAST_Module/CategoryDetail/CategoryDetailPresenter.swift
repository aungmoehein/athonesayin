//
//  CategoryDetailPresenter.swift
//  CategoryDetail
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation

final class CategoryDetailPresenter: PresenterInterface {

    var router: CategoryDetailRouterPresenterInterface!
    var interactor: CategoryDetailInteractorPresenterInterface!
    weak var view: CategoryDetailViewPresenterInterface!

}

extension CategoryDetailPresenter: CategoryDetailPresenterRouterInterface {

}

extension CategoryDetailPresenter: CategoryDetailPresenterInteractorInterface {
    func onFetchMSMovies(msMovies: [MSMovie]) {
        self.view.displayMSMovies(msMovies: msMovies)
    }
    
    func onFetchMSSeries(msSeries: [MSSerie]) {
        self.view.displayMSSeries(msSeries: msSeries)
    }
    
    func onFetchMDPosts(mdPosts: [MDPost]) {
        self.view.displayMDPosts(mdPosts: mdPosts)
    }
    
    func onFetchZPosts(zPosts: [ZPost]) {
        self.view.displayZPosts(zPosts: zPosts)
    }
    
    func onFetchMMPosts(mmPosts: [MMPost], count: Int) {
        self.view.displayMMPosts(mmPosts: mmPosts, count: count)
    }
    
    func onFetchHMovies(hMovies: [HMovie]) {
        self.view.displayHMovies(hMovies: hMovies)
    }
    
    func onFetchHVideo(title: String, url: String) {
        self.view.displayHVideo(title: title, url: url)
    }
    
    func onFetchEPVideo(epVideoList: EPVideoList) {
        self.view.displayEPVideos(epVideoList: epVideoList)
    }
    
    func onFetchError() {
        self.view.onError()
    }
}

extension CategoryDetailPresenter: CategoryDetailPresenterViewInterface {

    func start() {

    }
    
    func getMSMovies(category: String) {
        self.interactor.fetchMSubMovies(category: category)
    }

    func getMDPosts(page: Int, count: Int, apiKey: String, id: Int) {
        self.interactor.fetchMDPosts(page: page, count: count, apiKey: apiKey, id: id)
    }
    
    func videoPlayer(videoWrapper: VideoWrapper) {
        self.router.goToVideoPlayer(videoWrapper: videoWrapper)
    }
    
    func getZTopRatings(page: Int, apiKey: String) {
        self.interactor.fetchZTopRatingPosts(page: page, apiKey: apiKey)
    }

    func getZPosts(page: Int, id: Int, apiKey: String) {
        self.interactor.fetchZPosts(page: page, id: id, apiKey: apiKey)
    }
    
    func seriesView(seriesParser: SeriesParser) {
        self.router.goToSeries(seriesParser: seriesParser)
    }
    
    func getMMPosts(category: String, page: Int) {
        self.interactor.fetchMMPosts(category: category, page: page)
    }
    
    func getHVideos(category: String, page: Int) {
        self.interactor.fetchHMovies(category: category, page: page)
    }
    
    func getHVideoURL(gid: String) {
        self.interactor.fetchHVideo(gid: gid)
    }
    
    func getEPVideoList(query: String, order: String, page: Int) {
        self.interactor.fetchEPornerVideos(query: query, order: order, page: page)
    }
}
