//
//  CategoryDetailView.swift
//  CategoryDetail
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import PopupDialog
import SafariServices

final class CategoryDetailView: UIViewController, ViewInterface, UINavigationControllerDelegate, NVActivityIndicatorViewable {
    @IBOutlet weak var detailCV: UICollectionView!
    
    var presenter: CategoryDetailPresenterViewInterface!
    
    var cid: Int = 0
    var apiKey : String = ""
    var listTitle : String = ""
    var categoryTitle: String = ""
    
    var currentDisplayRow = 0
    var isLastPage: Bool = false
    var isLoading = false
    var currentPage = 0
    
    var msMovies: [MSMovie] = [] {
        didSet {
            detailCV.reloadData()
        }
    }
    
    var msSeries: [MSSerie] = [] {
        didSet {
            detailCV.reloadData()
        }
    }
    
    var mdPosts: [MDPost] = [] {
        didSet {
            detailCV.reloadData()
        }
    }
    
    var zPosts: [ZPost] = [] {
        didSet {
            detailCV.reloadData()
        }
    }
    
    var mmPosts: [MMPost] = [] {
        didSet {
            detailCV.reloadData()
        }
    }
    
    var hMovies: [HMovie] = [] {
        didSet {
            detailCV.reloadData()
        }
    }
    
    var epVideos: [EPVideo] = [] {
        didSet {
            detailCV.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        self.navigationController?.delegate = self
        self.navigationItem.title = listTitle
        
        detailCV.register(UINib(nibName: "ContentCell", bundle: nil), forCellWithReuseIdentifier: "content_cell")
        detailCV.register(UINib(nibName: "LoadingCell", bundle: nil), forCellWithReuseIdentifier: "loading_cell")
        detailCV.delegate = self
        detailCV.dataSource = self
        detailCV.reloadData()
        
        self.startAnimating()
        self.presenter.start()
        onRefresh()
    }
    
    @objc func canRotate() -> Void {}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            self.currentPage = self.currentPage + 1
            if !isLastPage {
                onRefresh()
            }
        }
    }
    
    @objc private func onRefresh() {
        switch categoryTitle {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
            self.presenter.getMDPosts(page: self.currentPage + 1, count: 30, apiKey: apiKey, id: cid)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            if cid == -1 {
                self.presenter.getZTopRatings(page: self.currentPage, apiKey: apiKey)
            }else {
                self.presenter.getZPosts(page: self.currentPage, id: cid, apiKey: apiKey)
            }
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
            let category = listTitle.uppercased()
            self.presenter.getMMPosts(category: category, page: self.currentPage)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            self.presenter.getMSMovies(category: listTitle)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerEPorner):
            self.presenter.getEPVideoList(query: listTitle.lowercased(), order: apiKey.lowercased().replacingOccurrences(of: " ", with: "-"), page: self.currentPage + 1)
        default:
            let category = listTitle.lowercased().replacingOccurrences(of: " ", with: "_")
            self.presenter.getHVideos(category: category, page: self.currentPage + 1)
        }
    }
}

extension CategoryDetailView: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        currentDisplayRow = indexPath.row
        
        var displayRow = false
        switch categoryTitle {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
            displayRow = currentDisplayRow == mdPosts.count-1
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            displayRow = currentDisplayRow == zPosts.count-1
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
            displayRow = currentDisplayRow == mmPosts.count-1
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            displayRow = currentDisplayRow == msMovies.count-1
            if listTitle == "Series" {
                displayRow = currentDisplayRow == msSeries.count-1
            }
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerEPorner):
            displayRow = currentDisplayRow == epVideos.count - 1
        default:
            displayRow = currentDisplayRow == hMovies.count-2
        }
        if !isLoading && displayRow {
            loadMoreData()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        var displayRow = false
        switch categoryTitle {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
            displayRow = currentDisplayRow == mdPosts.count-1
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            displayRow = currentDisplayRow == zPosts.count-1
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
            displayRow = currentDisplayRow == mmPosts.count-1
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            displayRow = currentDisplayRow == msMovies.count-1
            if listTitle == "Series" {
                displayRow = currentDisplayRow == msSeries.count-1
            }
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerEPorner):
            displayRow = currentDisplayRow == epVideos.count - 1
        default:
            displayRow = currentDisplayRow == hMovies.count-2
        }
        
        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading && displayRow {
            loadMoreData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            switch categoryTitle {
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
                return mdPosts.count
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
                return zPosts.count
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
                return mmPosts.count
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
                if listTitle == "Series" {
                    return msSeries.count
                }else {
                    return msMovies.count
                }
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerEPorner):
                return epVideos.count
            default:
                return hMovies.count
            }
        }else if section == 1 {
            return 1
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            var title = ""
            var image = ""
            
            switch categoryTitle {
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
                let thumbnail =  mdPosts[indexPath.row].video_thumbnail!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                image = "\(UserDefaults.standard.string(forKey: UserDefaults.Keys.m_dark)!)/upload/" + thumbnail
                
                title = mdPosts[indexPath.row].video_title!
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
                image = zPosts[indexPath.row].image!
                title = zPosts[indexPath.row].title!
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
                image = mmPosts[indexPath.row].image!
                title = mmPosts[indexPath.row].title!
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
                if listTitle == "Series" {
                    image = msSeries[indexPath.row].image!
                    title = msSeries[indexPath.row].seriestitle!
                }else {
                    image = msMovies[indexPath.row].image!
                    title = msMovies[indexPath.row].movietitle!
                }
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerEPorner):
                image = (epVideos[indexPath.row].default_thumb?.src)!
                title = epVideos[indexPath.row].title!
            default:
                image = UserDefaults.standard.string(forKey: UserDefaults.Keys.hentai_m)! + hMovies[indexPath.row].cover!
                title = hMovies[indexPath.row].title!
            }
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "content_cell", for: indexPath) as! ContentCell
            cell.image = image
            cell.title = title
            return cell
        }else {
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "loading_cell", for: indexPath) as! LoadingCell
             
             cell.activityIndicator.startAnimating()
             cell.activityIndicator.isHidden = false
             if self.isLastPage && isLoading {
                 cell.activityIndicator.stopAnimating()
                 cell.activityIndicator.isHidden = true
             }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch categoryTitle {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
            let thumbnail =  mdPosts[indexPath.row].video_thumbnail!.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            let image = "\(UserDefaults.standard.string(forKey: UserDefaults.Keys.m_dark)!)/upload/" + thumbnail
            
            let videoWrapper = VideoWrapper(id: mdPosts[indexPath.row].vid!, partner: categoryTitle, imageURL: image, videoURL: mdPosts[indexPath.row].video_url!, videoName: mdPosts[indexPath.row].video_title!, isVideo: false, isHentai: true)
            self.presenter.videoPlayer(videoWrapper: videoWrapper)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            if zPosts[indexPath.row].type == "serie" {
                let seriesParser = SeriesParser(id: zPosts[indexPath.row].id!, apiKey: apiKey, seriesTitle: zPosts[indexPath.row].title!, partner: categoryTitle, image: zPosts[indexPath.row].image!)
                self.presenter.seriesView(seriesParser: seriesParser)
            }else {
                let videoWrapper = VideoWrapper(id: zPosts[indexPath.row].id!, partner: categoryTitle, imageURL: zPosts[indexPath.row].image!, videoURL: zPosts[indexPath.row].sources![0].url!, videoName: zPosts[indexPath.row].title!)
                self.presenter.videoPlayer(videoWrapper: videoWrapper)
            }
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
            if (mmPosts[indexPath.row].categories?.contains("SERIES"))! {
                let seriesParser = SeriesParser(apiKey: apiKey, seriesTitle: mmPosts[indexPath.row].title!, partner: categoryTitle, image: mmPosts[indexPath.row].image!, episodes: mmPosts[indexPath.row].links!)
                self.presenter.seriesView(seriesParser: seriesParser)
            }else {
                let v_link = mmPosts[indexPath.row].links![0].split(separator: "/")[2]
                let vid = Int(String(v_link).toNumber())
                let videoURL = String(format: apiKey, String(v_link))
                let videoWrapper = VideoWrapper(id: vid!, partner: categoryTitle, imageURL: mmPosts[indexPath.row].image!, videoURL: videoURL, videoName: mmPosts[indexPath.row].title!)
                self.presenter.videoPlayer(videoWrapper: videoWrapper)
            }
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            if listTitle == "Series" {
                let seriesParser = SeriesParser(id: msSeries[indexPath.row].id!, apiKey: apiKey, seriesTitle: msSeries[indexPath.row].seriestitle!, partner: categoryTitle, image: msSeries[indexPath.row].image!)
                self.presenter.seriesView(seriesParser: seriesParser)
            }else {
                let videoWrapper = VideoWrapper(id: msMovies[indexPath.row].id!, partner: categoryTitle, imageURL: msMovies[indexPath.row].image!, videoURL: msMovies[indexPath.row].stream!, videoName: msMovies[indexPath.row].movietitle!)
                self.presenter.videoPlayer(videoWrapper: videoWrapper)
            }
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerEPorner):
            if let url = URL(string: epVideos[indexPath.row].embed!) {
                let config = SFSafariViewController.Configuration()
                config.entersReaderIfAvailable = true

                let vc = SFSafariViewController(url: url, configuration: config)
                present(vc, animated: true)
            }
        default:
            self.presenter.getHVideoURL(gid: hMovies[indexPath.row].gid!)
        }
    }
}

extension CategoryDetailView: UICollectionViewDelegateFlowLayout {
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let viewWidth = view.frame.size.width
        let viewHeight = view.frame.size.height
        
        if indexPath.section == 0 {
            if UIDevice.current.model == "iPad" {
                let height = UIDevice.current.orientation.isLandscape || UIApplication.shared.statusBarOrientation.isLandscape ? (viewHeight-60)/3 : (viewHeight-60)/4
                return CGSize(width: (viewWidth-10)/6, height: height)
            } else {
                return CGSize(width: (viewWidth-60)/3, height: (viewHeight-60)/3)
            }
        } else {
            return CGSize(width: viewWidth-100, height: CGFloat(30))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}

extension CategoryDetailView: CategoryDetailViewPresenterInterface {
    func displayMSMovies(msMovies: [MSMovie]) {
        self.stopAnimating()
        isLastPage = true
        self.msMovies = msMovies
        self.isLoading = false
    }
    
    func displayMSSeries(msSeries: [MSSerie]) {
        self.stopAnimating()
        isLastPage = true
        self.msSeries = msSeries
        self.isLoading = false
    }
    
    func displayMDPosts(mdPosts: [MDPost]) {
        self.stopAnimating()
        isLastPage = mdPosts.count == 0
        self.mdPosts += mdPosts
        self.isLoading = false
    }
    
    func displayZPosts(zPosts: [ZPost]) {
        self.stopAnimating()
        isLastPage = zPosts.count == 0
        self.zPosts += zPosts
        self.isLoading = false
    }
    
    func displayMMPosts(mmPosts: [MMPost], count: Int) {
        self.stopAnimating()
        isLastPage = mmPosts.count < count
        self.mmPosts += mmPosts
        self.isLoading = false
    }
    
    func displayHMovies(hMovies: [HMovie]) {
        self.stopAnimating()
        isLastPage = hMovies.count == 0
        self.hMovies += hMovies
        self.isLoading = false
    }
    
    func displayHVideo(title: String, url: String) {
        self.stopAnimating()
        
        let v_link = UserDefaults.standard.string(forKey: UserDefaults.Keys.hentai_m)! + url
        let videoWrapper = VideoWrapper(videoURL: v_link, videoName: title, isVideo: false, isHentai: true)
        self.presenter.videoPlayer(videoWrapper: videoWrapper)
    }
    
    func displayEPVideos(epVideoList: EPVideoList) {
        self.stopAnimating()
        isLastPage = epVideoList.count! < epVideoList.per_page!
        self.epVideos += epVideoList.videos!
        self.isLoading = false
    }
    
    func onError() {
        self.stopAnimating()
        let title = "Something went wrong."
        let message = "Check your internet connection."
        let image = UIImage(named: "no_wifi")

        let popup = PopupDialog(title: title, message: message, image: image)
        let retryBtn = DefaultButton(title: "Retry", dismissOnTap: false) {
            self.startAnimating()
            self.onRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        
        popup.addButtons([retryBtn])
        self.present(popup, animated: true, completion: nil)
    }
}
