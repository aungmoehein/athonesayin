//
//  CategoryDetailRouter.swift
//  CategoryDetail
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation
import UIKit

final class CategoryDetailRouter: RouterInterface {

    weak var presenter: CategoryDetailPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension CategoryDetailRouter: CategoryDetailRouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "video_play_view") as! VideoPlayView
        vc.isVideo = videoWrapper.isVideo
        vc.isHentai = videoWrapper.isHentai
        vc.videoWrapper = videoWrapper
        vc.videoName = videoWrapper.videoName
        vc.streamURL = videoWrapper.videoURL
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToSeries(seriesParser: SeriesParser) {
        let vc = SeriesModule().build() as! SeriesView
        vc.seriesParser = seriesParser
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
