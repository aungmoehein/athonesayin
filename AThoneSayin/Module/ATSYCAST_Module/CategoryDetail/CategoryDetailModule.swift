//
//  CategoryDetailModule.swift
//  CategoryDetail
//
//  Created by Aung Moe Hein on 23/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol CategoryDetailRouterPresenterInterface: RouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper)
    func goToSeries(seriesParser: SeriesParser)
}

// MARK: - presenter

protocol CategoryDetailPresenterRouterInterface: PresenterRouterInterface {

}

protocol CategoryDetailPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchMSMovies(msMovies: [MSMovie])
    func onFetchMSSeries(msSeries: [MSSerie])
    func onFetchMDPosts(mdPosts: [MDPost])
    func onFetchZPosts(zPosts: [ZPost])
    func onFetchMMPosts(mmPosts: [MMPost], count: Int)
    func onFetchHMovies(hMovies: [HMovie])
    func onFetchHVideo(title: String, url: String)
    func onFetchEPVideo(epVideoList: EPVideoList)
    func onFetchError()
}

protocol CategoryDetailPresenterViewInterface: PresenterViewInterface {
    func start()
    func getMSMovies(category: String)
    func getMDPosts(page: Int, count: Int, apiKey: String, id: Int)
    func videoPlayer(videoWrapper: VideoWrapper)
    func getZTopRatings(page: Int, apiKey: String)
    func getZPosts(page: Int, id: Int, apiKey: String)
    func seriesView(seriesParser: SeriesParser)
    func getMMPosts(category: String, page: Int)
    func getHVideos(category: String, page: Int)
    func getHVideoURL(gid: String)
    func getEPVideoList(query: String, order: String, page: Int)
}

// MARK: - interactor

protocol CategoryDetailInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchMSubMovies(category: String)
    func fetchMDPosts(page: Int, count: Int, apiKey: String, id: Int)
    func fetchZPosts(page: Int, id: Int, apiKey: String)
    func fetchZTopRatingPosts(page: Int, apiKey: String)
    func fetchMMPosts(category: String, page: Int)
    func fetchHMovies(category: String, page: Int)
    func fetchEPornerVideos(query: String, order: String, page: Int)
    func fetchHVideo(gid: String)
}

// MARK: - view

protocol CategoryDetailViewPresenterInterface: ViewPresenterInterface {
    func displayMSMovies(msMovies: [MSMovie])
    func displayMSSeries(msSeries: [MSSerie])
    func displayMDPosts(mdPosts: [MDPost])
    func displayZPosts(zPosts: [ZPost])
    func displayMMPosts(mmPosts: [MMPost], count: Int)
    func displayHMovies(hMovies: [HMovie])
    func displayHVideo(title: String, url: String)
    func displayEPVideos(epVideoList: EPVideoList)
    func onError()
}


// MARK: - module builder

final class CategoryDetailModule: ModuleInterface {

    typealias View = CategoryDetailView
    typealias Presenter = CategoryDetailPresenter
    typealias Router = CategoryDetailRouter
    typealias Interactor = CategoryDetailInteractor

    func build() -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "category_detail") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
