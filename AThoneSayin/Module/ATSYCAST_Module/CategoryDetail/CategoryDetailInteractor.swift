//
//  CategoryDetailInteractor.swift
//  CategoryDetail
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation

final class CategoryDetailInteractor: InteractorInterface {

    weak var presenter: CategoryDetailPresenterInteractorInterface!
}

extension CategoryDetailInteractor: CategoryDetailInteractorPresenterInterface {
    func fetchZTopRatingPosts(page: Int, apiKey: String) {
        ZChannel.sharedInstance.topRatedPage(page: page, apiKey: apiKey).load()
            .onSuccess(){data in
            if let zPosts: [ZPost] = data.typedContent() {
                self.presenter.onFetchZPosts(zPosts: zPosts)
            }
        }.onFailure(){error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchZPosts(page: Int, id: Int, apiKey: String) {
        ZChannel.sharedInstance.moreDataByPage(page: page, id: id, apiKey: apiKey).load()
            .onSuccess(){data in
            if let zPosts: [ZPost] = data.typedContent() {
                self.presenter.onFetchZPosts(zPosts: zPosts)
            }
        }.onFailure(){error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchMMPosts(category: String, page: Int) {
        Moviemm.sharedInstance.moviesList(category: category, page: page).load()
            .onSuccess(){ data in
                if let mmData: MMData = data.typedContent() {
                    self.presenter.onFetchMMPosts(mmPosts: (mmData.today?.posts)!, count: Int((mmData.today?.postPerPage)!) ?? 0)
                }
        }.onFailure() {error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchHMovies(category: String, page: Int) {
        Hentaiser.sharedInstance.getVideoList(category: category, page: page).load()
            .onSuccess(){ data in
                if category == "hot" {
                    if let hVideoList: [HMovie] = data.typedContent() {
                        self.presenter.onFetchHMovies(hMovies: hVideoList)
                    }
                }else {
                    if let hVideoList: HVideoList = data.typedContent() {
                        self.presenter.onFetchHMovies(hMovies: hVideoList.videos!)
                    }
                }
        }.onFailure() {error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchHVideo(gid: String) {
        Hentaiser.sharedInstance.getVideo(gid: gid).load()
            .onSuccess() { data in
                let data = data.content as! NSDictionary
                let url = data.value(forKey: "url") as! String
                let title = data.value(forKey: "title") as! String

                self.presenter.onFetchHVideo(title: title, url: url)
        }.onFailure() { error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchMDPosts(page: Int, count: Int, apiKey: String, id: Int) {
        MyanDark.sharedInstance.getVideoList(page: page, count: count, apiKey: apiKey, id: id).load()
            .onSuccess(){data in
            if let mdPosts: MDPostList = data.typedContent() {
                self.presenter.onFetchMDPosts(mdPosts: mdPosts.posts!)
            }
        }.onFailure(){error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchMSubMovies(category: String) {
        if category == "Series" {
            MSubMovie.sharedInstance.getAllSeries().load()
                .onSuccess(){data in
                if let msSeries: [MSSerie] = data.typedContent() {
                    self.presenter.onFetchMSSeries(msSeries: msSeries)
                }
            }.onFailure(){error in
                debugPrint(error.userMessage)
                self.presenter.onFetchError()
            }

        }else {
            MSubMovie.sharedInstance.getAllMovie().load()
                .onSuccess(){data in
                if let msMovies: [MSMovie] = data.typedContent() {
                    var msMoviesByCat: [MSMovie] = []
                    for msMovie in msMovies {
                        if msMovie.moviegenres!.contains(category) {
                            msMoviesByCat.append(msMovie)
                        }
                    }
                    self.presenter.onFetchMSMovies(msMovies: msMoviesByCat)
                }
            }.onFailure(){error in
                debugPrint(error.userMessage)
                self.presenter.onFetchError()
            }
        }
    }
    
    func fetchEPornerVideos(query: String, order: String, page: Int) {
        EPorner.sharedInstance.videoList(query: query, order: order, page: page).load()
            .onSuccess(){data in
                if let epVideoList: EPVideoList = data.typedContent() {
                    self.presenter.onFetchEPVideo(epVideoList: epVideoList)
                }
        }.onFailure() { error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
}
