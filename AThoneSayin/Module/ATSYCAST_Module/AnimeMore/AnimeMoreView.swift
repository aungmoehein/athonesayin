//
//  AnimeMoreView.swift
//  AnimeMore
//
//  Created by Aung Moe Hein on 05/06/2020.
//

import Foundation
import NVActivityIndicatorView
import UIKit
import PopupDialog

final class AnimeMoreView: UIViewController, ViewInterface, UINavigationControllerDelegate, NVActivityIndicatorViewable {
    @IBOutlet weak var animeCV: UICollectionView!
    
    fileprivate var showMsgDialog: PopupDialog!
    var presenter: AnimeMorePresenterViewInterface!
    
    var currentDisplayRow = 0
    var currentPage = 1
    var lastPage: Bool = false
    var isLoading = false
    
    var navTitle: String = ""

    var aniLatestDatas: [AniLatestData] = [] {
        didSet {
            animeCV.reloadData()
        }
    }
    
    var aniDatas: [AniData] = [] {
        didSet {
            animeCV.reloadData()
        }
    }
    
    var aniEpisodeList: AniEpisodeList! {
        didSet {
            self.presenter.goToDetail(aniEpisodeList: self.aniEpisodeList)
        }
    }
    
    var aniLatestEpisodeData: AniLatestEpisodeData! {
        didSet {
            self.presenter.getAnimeEpisodeList(id: (aniLatestEpisodeData.anime?.id)!)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        self.stopAnimating()
        navigationController?.delegate = self
        self.navigationItem.title = navTitle
        
        animeCV.register(UINib(nibName: "AnimeHomeLatestCell", bundle: nil), forCellWithReuseIdentifier: "anime_home_latest_cell")
        animeCV.register(UINib(nibName: "ContentCell", bundle: nil), forCellWithReuseIdentifier: "content_cell")
        animeCV.register(UINib(nibName: "LoadingCell", bundle: nil), forCellWithReuseIdentifier: "loading_cell")
        animeCV.delegate = self
        animeCV.dataSource = self
        animeCV.reloadData()

        self.presenter.start()
        loadData(page: currentPage)
    }
    
    @objc func canRotate() -> Void {}
    
    func loadData(page: Int) {
        switch navTitle {
        case "Popular":
            self.presenter.getPopularAnime(page: page)
        case "Movies":
            self.presenter.getAnimeMovies(page: page)
        case "Recently Added":
            self.presenter.getRecentAnime(page: page)
        default:
            self.presenter.getLatestAnime(page: page)
        }
    }
}

extension AnimeMoreView: UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            if navTitle == "Latest" {
                return aniLatestDatas.count
            }
            return aniDatas.count
        }else if section == 1 {
            return 1
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.section == 0 {
            if navTitle != "Latest" {
                let cell = animeCV.dequeueReusableCell(withReuseIdentifier: "content_cell", for: indexPath) as! ContentCell
                cell.image = aniDatas[indexPath.row].cover_photo!
                cell.title = aniDatas[indexPath.row].title!
                return cell
            }else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "anime_home_latest_cell", for: indexPath) as! AnimeHomeLatestCell
                cell.displayImg = aniLatestDatas[indexPath.row].thumbnail!
                cell.displayTitle = aniLatestDatas[indexPath.row].title!
                cell.displayEpisode = aniLatestDatas[indexPath.row].episode_num!
                cell.displayType = aniLatestDatas[indexPath.row].type!
                cell.displayDate = aniLatestDatas[indexPath.row].created_at!
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "loading_cell", for: indexPath) as! LoadingCell
            
            cell.activityIndicator.startAnimating()
            cell.activityIndicator.isHidden = false
            if self.lastPage && isLoading {
                cell.activityIndicator.stopAnimating()
                cell.activityIndicator.isHidden = true
            }
           return cell
       }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        currentDisplayRow = indexPath.row
        if !isLoading && (currentDisplayRow == aniDatas.count-1) {
            loadMoreData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.startAnimating()
        if navTitle == "Latest" {
            self.presenter.getLatestAnimeEpisode(episodeNo: aniLatestDatas[indexPath.row].episode_num!, slug: (aniLatestDatas[indexPath.row].anime?.slug)!)
        }else {
            self.presenter.getAnimeEpisodeList(id: aniDatas[indexPath.row].id!)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if (offsetY > contentHeight - scrollView.frame.height * 4) && !isLoading && (currentDisplayRow == aniDatas.count-1) {
            loadMoreData()
        }
    }
    
    func loadMoreData() {
        if !self.isLoading {
            self.isLoading = true
            
            if !self.lastPage {
                self.currentPage = self.currentPage + 1
                self.loadData(page: self.currentPage)
            } else {
                print("Stop loading data")
            }
        }
    }
}

extension AnimeMoreView: UICollectionViewDelegateFlowLayout {
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let viewWidth = view.frame.size.width
        let viewHeight = view.frame.size.height
        
        if indexPath.section == 0 {
            if UIDevice.current.model == "iPad" {
                if navTitle == "Latest" {
                    return CGSize(width: (viewWidth-60)/3, height: 180)
                }
                let height = UIDevice.current.orientation.isLandscape || UIApplication.shared.statusBarOrientation.isLandscape ? (viewHeight-60)/3 : (viewHeight-60)/4
                return CGSize(width: (viewWidth-10)/6, height: height)
            } else {
                if navTitle == "Latest" {
                    return CGSize(width: (viewWidth-60)/2, height: (viewHeight-180)/3)
                }
                return CGSize(width: (viewWidth-60)/3, height: (viewHeight-60)/3)
            }
        } else {
            return CGSize(width: viewWidth-100, height: CGFloat(30))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}

extension AnimeMoreView: AnimeMoreViewPresenterInterface {
    func displayLatestAnime(aniLatestDatas: [AniLatestData]) {
        self.aniLatestDatas += aniLatestDatas
        self.lastPage = aniLatestDatas.count == 0
        self.isLoading = false
    }
    
    func displayPopularAnime(aniPopularDatas: [AniData]) {
        self.aniDatas += aniPopularDatas
        self.lastPage = aniPopularDatas.count == 0
        self.isLoading = false
    }
    
    func displayAnimeMovies(aniMovies: [AniData]) {
        self.aniDatas += aniMovies
        self.lastPage = aniMovies.count == 0
        self.isLoading = false
    }
    
    func displayAnimeRecents(aniRecents: [AniData]) {
        self.aniDatas += aniRecents
        self.lastPage = aniRecents.count == 0
        self.isLoading = false
    }
    
    func displayAnimeEpisodeList(aniEpisodeList: AniEpisodeList) {
        self.stopAnimating()
        self.aniEpisodeList = aniEpisodeList
    }
    
    func displayAnimeLatestEpisode(aniLatestEpisodeData: AniLatestEpisodeData) {
        self.stopAnimating()
        self.aniLatestEpisodeData = aniLatestEpisodeData
    }
    
    func displayError() {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = "Something went wrong!"
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonRetry = DefaultButton(title: NSLocalizedString("Retry", comment: "")) {
            self.loadData(page: self.currentPage)
        }
        
        let buttonCancel = CancelButton(title: NSLocalizedString("Cancel", comment: "")) {
            
        }
        showMsgDialog.addButtons([buttonCancel, buttonRetry])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}
