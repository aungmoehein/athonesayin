//
//  AnimeMoreModule.swift
//  AnimeMore
//
//  Created by Aung Moe Hein on 05/06/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol AnimeMoreRouterPresenterInterface: RouterPresenterInterface {
    func goToDetail(aniEpisodeList: AniEpisodeList)
}

// MARK: - presenter

protocol AnimeMorePresenterRouterInterface: PresenterRouterInterface {

}

protocol AnimeMorePresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchLatestAnime(aniLatestDatas: [AniLatestData])
    func onFetchPopularAnime(aniPopularDatas: [AniData])
    func onFetchAnimeMovies(aniMovies: [AniData])
    func onFetchAnimeRecents(aniRecents: [AniData])
    func onFetchAnimeEpisodeList(aniEpisodeList: AniEpisodeList)
    func onFetchAnimeLatestEpisode(aniLatestEpisodeData: AniLatestEpisodeData)
    func onFetchError()
}

protocol AnimeMorePresenterViewInterface: PresenterViewInterface {
    func start()
    func getLatestAnime(page: Int)
    func getPopularAnime(page: Int)
    func getAnimeMovies(page: Int)
    func getRecentAnime(page: Int)
    func getAnimeEpisodeList(id: Int)
    func goToDetail(aniEpisodeList: AniEpisodeList)
    func getLatestAnimeEpisode(episodeNo: String, slug: String)
}

// MARK: - interactor

protocol AnimeMoreInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchLatestAnime(page: Int)
    func fetchPopularAnime(page: Int)
    func fetchAnimeMovies(page: Int)
    func fetchRecentAnime(page: Int)
    func fetchAnimeEpisodes(id: Int)
    func fetchAnimeLatestEpisode(episodeNo: String, slug: String)
}

// MARK: - view

protocol AnimeMoreViewPresenterInterface: ViewPresenterInterface {
    func displayLatestAnime(aniLatestDatas: [AniLatestData])
    func displayPopularAnime(aniPopularDatas: [AniData])
    func displayAnimeMovies(aniMovies: [AniData])
    func displayAnimeRecents(aniRecents: [AniData])
    func displayAnimeEpisodeList(aniEpisodeList: AniEpisodeList)
    func displayAnimeLatestEpisode(aniLatestEpisodeData: AniLatestEpisodeData)
    func displayError()
}


// MARK: - module builder

final class AnimeMoreModule: ModuleInterface {

    typealias View = AnimeMoreView
    typealias Presenter = AnimeMorePresenter
    typealias Router = AnimeMoreRouter
    typealias Interactor = AnimeMoreInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "anime_more") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
