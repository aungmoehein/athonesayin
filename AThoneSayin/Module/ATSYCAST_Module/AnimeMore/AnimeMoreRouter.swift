//
//  AnimeMoreRouter.swift
//  AnimeMore
//
//  Created by Aung Moe Hein on 05/06/2020.
//

import Foundation
import UIKit

final class AnimeMoreRouter: RouterInterface {

    weak var presenter: AnimeMorePresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension AnimeMoreRouter: AnimeMoreRouterPresenterInterface {
    func goToDetail(aniEpisodeList: AniEpisodeList) {
        let vc = AnimeDetailModule().build() as! AnimeDetailView
        vc.animeData = aniEpisodeList.anime
        vc.animeEpisodes = aniEpisodeList.data!
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
