//
//  AnimeMorePresenter.swift
//  AnimeMore
//
//  Created by Aung Moe Hein on 05/06/2020.
//

import Foundation

final class AnimeMorePresenter: PresenterInterface {

    var router: AnimeMoreRouterPresenterInterface!
    var interactor: AnimeMoreInteractorPresenterInterface!
    weak var view: AnimeMoreViewPresenterInterface!

}

extension AnimeMorePresenter: AnimeMorePresenterRouterInterface {

}

extension AnimeMorePresenter: AnimeMorePresenterInteractorInterface {
    func onFetchLatestAnime(aniLatestDatas: [AniLatestData]) {
        self.view.displayLatestAnime(aniLatestDatas: aniLatestDatas)
    }
    
    func onFetchPopularAnime(aniPopularDatas: [AniData]) {
        self.view.displayPopularAnime(aniPopularDatas: aniPopularDatas)
    }
    
    func onFetchAnimeMovies(aniMovies: [AniData]) {
        self.view.displayAnimeMovies(aniMovies: aniMovies)
    }
    
    func onFetchAnimeRecents(aniRecents: [AniData]) {
        self.view.displayAnimeRecents(aniRecents: aniRecents)
    }
    
    func onFetchAnimeEpisodeList(aniEpisodeList: AniEpisodeList) {
        self.view.displayAnimeEpisodeList(aniEpisodeList: aniEpisodeList)
    }
    
    func onFetchAnimeLatestEpisode(aniLatestEpisodeData: AniLatestEpisodeData) {
        self.view.displayAnimeLatestEpisode(aniLatestEpisodeData: aniLatestEpisodeData)
    }
    
    func onFetchError() {
        self.view.displayError()
    }
}

extension AnimeMorePresenter: AnimeMorePresenterViewInterface {

    func start() {

    }

    func getLatestAnime(page: Int) {
        self.interactor.fetchLatestAnime(page: page)
    }
    
    func getPopularAnime(page: Int) {
        self.interactor.fetchPopularAnime(page: page)
    }
    
    func getAnimeMovies(page: Int) {
        self.interactor.fetchAnimeMovies(page: page)
    }
    
    func getRecentAnime(page: Int) {
        self.interactor.fetchRecentAnime(page: page)
    }
    
    func getAnimeEpisodeList(id: Int) {
        self.interactor.fetchAnimeEpisodes(id: id)
    }
    
    func goToDetail(aniEpisodeList: AniEpisodeList) {
        self.router.goToDetail(aniEpisodeList: aniEpisodeList)
    }
    
    func getLatestAnimeEpisode(episodeNo: String, slug: String) {
        self.interactor.fetchAnimeLatestEpisode(episodeNo: episodeNo, slug: slug)
    }
}
