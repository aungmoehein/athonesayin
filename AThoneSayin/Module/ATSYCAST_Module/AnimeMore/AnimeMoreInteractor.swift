//
//  AnimeMoreInteractor.swift
//  AnimeMore
//
//  Created by Aung Moe Hein on 05/06/2020.
//

import Foundation

final class AnimeMoreInteractor: InteractorInterface {

    weak var presenter: AnimeMorePresenterInteractorInterface!
}

extension AnimeMoreInteractor: AnimeMoreInteractorPresenterInterface {
    func fetchLatestAnime(page: Int) {
        Animeflix.sharedInstance.getLatestAnime(page: page).load()
            .onSuccess() {data in
            if let aniLatestList: AniLatestList = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchLatestAnime(aniLatestDatas: aniLatestList.data!)
                }
            }
        }.onFailure() {error in
            if self.presenter != nil {
                self.presenter.onFetchError()
            }
        }
    }
    
    func fetchPopularAnime(page: Int) {
        Animeflix.sharedInstance.getPopularAnime(page: page).load()
            .onSuccess() {data in
            if let aniList: AniList = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchPopularAnime(aniPopularDatas: aniList.data!)
                }
            }
        }.onFailure() {error in
            if self.presenter != nil {
                self.presenter.onFetchError()
            }
        }
    }
    
    func fetchAnimeMovies(page: Int) {
        Animeflix.sharedInstance.getAnimeMovie(page: page).load()
            .onSuccess() {data in
            if let aniList: AniList = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchAnimeMovies(aniMovies: aniList.data!)
                }
            }
        }.onFailure() {error in
            if self.presenter != nil {
                self.presenter.onFetchError()
            }
        }
    }
    
    func fetchRecentAnime(page: Int) {
        Animeflix.sharedInstance.getRecentAnime(page: page).load()
            .onSuccess() {data in
            if let aniList: AniList = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchAnimeRecents(aniRecents: aniList.data!)
                }
            }
        }.onFailure() {error in
            if self.presenter != nil {
                self.presenter.onFetchError()
            }
        }
    }
    
    func fetchAnimeEpisodes(id: Int) {
        Animeflix.sharedInstance.getAnimeEpisodes(id: id, page: 1, sort: "DESC").load()
            .onSuccess() {data in
            if let aniEpisodeList: AniEpisodeList = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchAnimeEpisodeList(aniEpisodeList: aniEpisodeList)
                }
            }
        }.onFailure() {error in
            if self.presenter != nil {
                self.presenter.onFetchError()
            }
        }
    }
    
    func fetchAnimeLatestEpisode(episodeNo: String, slug: String) {
        Animeflix.sharedInstance.getLatestAnimeEpisode(episodeNo: episodeNo, slug: slug).load()
            .onSuccess() {data in
            if let aniLatestEpisode: AniLatestEpisode = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchAnimeLatestEpisode(aniLatestEpisodeData: aniLatestEpisode.data!)
                }
            }
        }.onFailure() {error in
            if self.presenter != nil {
                self.presenter.onFetchError()
            }
        }
    }
}
