//
//  CategoryView.swift
//  Category
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import PopupDialog

final class CategoryView: UIViewController, ViewInterface, UINavigationControllerDelegate, NVActivityIndicatorViewable {
    @IBOutlet weak var categoryTV: UITableView!
    
    var showMsgDialog: PopupDialog!
    var presenter: CategoryPresenterViewInterface!
    
    var partnerKey: String = ""
    var categoryTitle: String = ""
    var apiKey: String = ""
    var mdCategories: [MDCategory] = [] {
        didSet {
            categoryTV.reloadData()
        }
    }
    
    var zGenreList: [ZGenre] = [] {
        didSet {
            categoryTV.reloadData()
        }
    }
    
    var mmCategories: [MMCategory] = [] {
        didSet {
            categoryTV.reloadData()
        }
    }
    
    var hentaiList: [String] = ["Hot", "Latest", "Top Commented", "Top Liked", "Top Rated", "Top Viewed"]
    
    var msubMovieList: [String] = ["Series", "Action", "Adventure", "Animation", "Biography", "Comedy", "Crime", "Drama", "Family", "Fantasy", "Game-Show", "Super Hero", "History", "Horror", "Jackie Chan", "Music", "Mystery", "Romance", "Sci-Fi", "Sport", "Terminator", "Thriller", "War", "18+"]
    
    let btnOrder = UIButton()
    var epOrder: String = "Latest" {
        didSet {
            btnOrder.setTitle(self.epOrder, for: .normal)
        }
    }
    
    var epCategories: [String] = ["ANAL", "TEEN", "POV", "THREESOME", "JAPANESE", "LESBIAN", "ASIAN", "REDHEAD", "MATURE", "INTERRACIAL", "BIG TITS", "MILF", "HARDCORE", "GROUP SEX", "VR PORN", "AMATEUR", "ORGY", "EBONY", "LATINA", "FOOTJOB", "INDIAN", "4K ULTRA HD", "HOMEMADE", "BIG DICK", "HENTAI", "SHEMALE", "UNIFORM", "BISEXUAL", "HOTEL", "MASTURBATION", "CREAMPIE", "BLOWJOB", "BIG ASS", "PORNSTAR", "DOUBLE PENETRATION", "BBW", "LINGERIE", "WEBCAM", "CUMSHOT", "MASSAGE", "BDSM", "PUBLIC", "PETITE", "STUDENTS", "STRIPTEASE", "HANDJOB", "GAY", "FAT", "FETISH", "HOUSEWIVES", "SWINGER", "BONDAGE", "BUKKAKE", "DOCTOR", "FOR WOMEN", "VINTAGE", "FISTING", "SLEEPING", "NURSES", "SMALL TITS", "TOYS", "60 FPS", "HD SEX", "OUTDOOR", "THROAT", "TAXI", "TEACHER", "VIRGIN", "STEPSISTER", "STEPMOM", "SPANISH", "SQUIRT", "SISTER", "SHORT HAIR", "SLAVE", "SAUNA", "RAPE", "ROMANTIC", "BLOWBANG", "COUPLE", "COLLEGE", "CZECH", "CAR", "DAD", "DADDY DAUGHTER", "DANCE", "DAUGHTER", "DOGGYSTYLE", "DRUNK", "EPORN", "FAKE AGENT", "FACE SITTING", "FAMILY", "FEET", "FORCED", "GANGBANG", "GLASSES", "GYM", "HAIRY", "HOT MOM", "JOHNNY SINS", "JILLIAN JANSON", "KITCHEN", "KOREAN", "MACHINE", "MIA KHALIFA", "MOM AND SON", "MONEY", "OILED", "ORGASM", "PARTY", "PEE", "PISSING", "POOL", "PREGNANT", "RIDING", "SECRETARY", "SENSUAL", "SHOPLIFTER", "STOCKINGS", "THAI", "UNDERWATER", "WHORE", "YOGA", "CHINESE"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        self.navigationController?.delegate = self
        self.navigationItem.title = categoryTitle
        
        if categoryTitle == UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerEPorner) {
            UserDefaults.standard.set(0, forKey: UserDefaults.Keys.e_porner_gc)
            addNavBarRightButton()
        }
        
        self.categoryTV.register(UINib(nibName: "CategoryCell", bundle: nil), forCellReuseIdentifier: "category_cell")
        categoryTV.delegate = self
        categoryTV.dataSource = self
        categoryTV.reloadData()

        self.presenter.start()
        
        onRefresh()
    }
    
    @objc func canRotate() -> Void {}
    
    func addNavBarRightButton() {
        btnOrder.translatesAutoresizingMaskIntoConstraints = false
        btnOrder.setTitle(epOrder, for: .normal)
        btnOrder.setTitleColor(UIColor(hex: 0x459a0b), for: .normal)
        btnOrder.tintColor = UIColor(hex: 0x459a0b)
        btnOrder.contentMode = .center
        btnOrder.addTarget(self, action: #selector(pickerAction), for: .touchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnOrder)
    }
    
    @objc func pickerAction(sender: UIButton!) {
        let popUp = PickerView(nibName: "PickerView", bundle: nil)
        popUp.categoryView = self
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown)
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.epCategories.sort()
        self.navigationController?.navigationBar.isHidden = false
    }

    @objc private func onRefresh() {
        self.startAnimating()
        switch partnerKey {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMDark):
            self.presenter.fetchMDCategories(apiKey: apiKey)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idZChannel):
            self.presenter.fetchZGenreList(apiKey: apiKey)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMovieMM):
            self.presenter.fetchMMCategories()
        default:
            self.stopAnimating()
            break
        }
    }
}

extension CategoryView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch partnerKey {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMDark):
            return mdCategories.count
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idZChannel):
            return zGenreList.count
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMovieMM):
            return mmCategories.count
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMSub):
            return msubMovieList.count
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idEPorner):
            return epCategories.count
        default:
            return hentaiList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var categoryName = ""
        switch partnerKey {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMDark):
            categoryName = mdCategories[indexPath.row].category_name!
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idZChannel):
            categoryName = zGenreList[indexPath.row].title!
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMovieMM):
            categoryName = mmCategories[indexPath.row].name!
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMSub):
            categoryName = msubMovieList[indexPath.row]
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idEPorner):
            categoryName = epCategories[indexPath.row]
        default:
            categoryName = hentaiList[indexPath.row]
        }
        
        let cell = categoryTV.dequeueReusableCell(withIdentifier: "category_cell", for: indexPath) as! CategoryCell
        cell.accessoryType = .disclosureIndicator
        cell.selectionStyle = .none
        cell.title = categoryName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch partnerKey {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMDark):
            self.presenter.videoDetail(cid: mdCategories[indexPath.row].cid!, title: mdCategories[indexPath.row].category_name!, apiKey: apiKey, categoryTitle: categoryTitle)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idZChannel):
            self.presenter.videoDetail(cid: zGenreList[indexPath.row].id!, title: zGenreList[indexPath.row].title!, apiKey: apiKey, categoryTitle: categoryTitle)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMovieMM):
            self.presenter.videoDetail(cid: 0, title: mmCategories[indexPath.row].name!, apiKey: apiKey, categoryTitle: categoryTitle)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idMSub):
            self.presenter.videoDetail(cid: 0, title: msubMovieList[indexPath.row], apiKey: apiKey, categoryTitle: categoryTitle)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.idEPorner):
            self.presenter.videoDetail(cid: 0, title: epCategories[indexPath.row], apiKey: epOrder, categoryTitle: categoryTitle)
        default:
            self.presenter.videoDetail(cid: 0, title: hentaiList[indexPath.row], apiKey: "", categoryTitle: categoryTitle)
        }
    }
}

extension CategoryView: CategoryViewPresenterInterface {
    func displayMDCategories(mdCategories: [MDCategory]) {
        self.stopAnimating()
        self.mdCategories = mdCategories
    }
    
    func displayZGenres(zGenreList: [ZGenre]) {
        self.stopAnimating()
        self.zGenreList = zGenreList
    }
    
    func displayMMCategories(mmCategories: [MMCategory]) {
        self.stopAnimating()
        self.mmCategories = mmCategories
    }
    
    func onError() {
        self.stopAnimating()
        let title = "Something went wrong."
        let message = "Check your internet connection."
        let image = UIImage(named: "no_wifi")

        let popup = PopupDialog(title: title, message: message, image: image)
        let retryBtn = DefaultButton(title: "Retry", dismissOnTap: false) {
            self.onRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        
        popup.addButtons([retryBtn])
        self.present(popup, animated: true, completion: nil)
    }
}
