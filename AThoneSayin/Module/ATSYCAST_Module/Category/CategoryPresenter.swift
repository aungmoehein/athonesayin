//
//  CategoryPresenter.swift
//  Category
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation

final class CategoryPresenter: PresenterInterface {

    var router: CategoryRouterPresenterInterface!
    var interactor: CategoryInteractorPresenterInterface!
    weak var view: CategoryViewPresenterInterface!

}

extension CategoryPresenter: CategoryPresenterRouterInterface {

}

extension CategoryPresenter: CategoryPresenterInteractorInterface {
    func onFetchMDCategories(mdCategories: [MDCategory]) {
        self.view.displayMDCategories(mdCategories: mdCategories)
    }
    
    func onFetchZGenreList(zGenreList: [ZGenre]) {
        self.view.displayZGenres(zGenreList: zGenreList)
    }
    
    func onFetchMMCategories(mmCategories: [MMCategory]) {
        self.view.displayMMCategories(mmCategories: mmCategories)
    }
    
    func onFetchError() {
        self.view.onError()
    }
}

extension CategoryPresenter: CategoryPresenterViewInterface {

    func start() {
        
    }
    
    func fetchMDCategories(apiKey: String) {
        self.interactor.fetchMDCategoryList(apiKey: apiKey)
    }

    func videoDetail(cid: Int, title: String, apiKey: String, categoryTitle: String) {
        self.router.goToVideoList(cid: cid, title: title, apiKey: apiKey, categoryTitle: categoryTitle)
    }
    
    func fetchZGenreList(apiKey: String) {
        self.interactor.fetchZChannelGenres(apiKey: apiKey)
    }
    
    func fetchMMCategories() {
        self.interactor.fetchMMCategoryList()
    }
}
