//
//  CategoryRouter.swift
//  Category
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation
import UIKit

final class CategoryRouter: RouterInterface {

    weak var presenter: CategoryPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension CategoryRouter: CategoryRouterPresenterInterface {
    func goToVideoList(cid: Int, title: String, apiKey: String, categoryTitle: String) {
        let vc = CategoryDetailModule().build() as! CategoryDetailView
        vc.cid = cid
        vc.listTitle = title
        vc.apiKey = apiKey
        vc.categoryTitle = categoryTitle
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
