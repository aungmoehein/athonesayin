//
//  CategoryModule.swift
//  Category
//
//  Created by Aung Moe Hein on 23/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol CategoryRouterPresenterInterface: RouterPresenterInterface {
    func goToVideoList(cid: Int, title: String, apiKey: String, categoryTitle: String)
}

// MARK: - presenter

protocol CategoryPresenterRouterInterface: PresenterRouterInterface {
    
}

protocol CategoryPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchMDCategories(mdCategories: [MDCategory])
    func onFetchZGenreList(zGenreList: [ZGenre])
    func onFetchMMCategories(mmCategories: [MMCategory])
    func onFetchError()
}

protocol CategoryPresenterViewInterface: PresenterViewInterface {
    func start()
    func fetchMDCategories(apiKey: String)
    func videoDetail(cid: Int, title: String, apiKey: String, categoryTitle: String)
    func fetchZGenreList(apiKey: String)
    func fetchMMCategories()
}

// MARK: - interactor

protocol CategoryInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchMDCategoryList(apiKey: String)
    func fetchZChannelGenres(apiKey: String)
    func fetchMMCategoryList()
}

// MARK: - view

protocol CategoryViewPresenterInterface: ViewPresenterInterface {
    func displayMDCategories(mdCategories: [MDCategory])
    func displayZGenres(zGenreList: [ZGenre])
    func displayMMCategories(mmCategories: [MMCategory])
    func onError()
}


// MARK: - module builder

final class CategoryModule: ModuleInterface {

    typealias View = CategoryView
    typealias Presenter = CategoryPresenter
    typealias Router = CategoryRouter
    typealias Interactor = CategoryInteractor

    func build() -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "category") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
