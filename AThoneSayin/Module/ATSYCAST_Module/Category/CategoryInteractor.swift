//
//  CategoryInteractor.swift
//  Category
//
//  Created by Aung Moe Hein on 23/05/2020.
//

import Foundation

final class CategoryInteractor: InteractorInterface {

    weak var presenter: CategoryPresenterInteractorInterface!
}

extension CategoryInteractor: CategoryInteractorPresenterInterface {
    func fetchZChannelGenres(apiKey: String) {
        ZChannel.sharedInstance.genreList(apiKey: apiKey).load()
            .onSuccess(){ data in
                if let zGenreList: ZGenreList = data.typedContent() {
                    self.presenter.onFetchZGenreList(zGenreList: zGenreList.genres!)
                }
        }.onFailure(){ error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchMDCategoryList(apiKey: String) {
        MyanDark.sharedInstance.getMDCategoryList(apiKey: apiKey).load()
            .onSuccess(){ data in
                if let mdList: MDCategoryList = data.typedContent() {
                    var categoryList: [MDCategory] = []
                    for mdcategory in mdList.categories! {
                        if mdcategory.category_name != "Demo Data" {
                            categoryList.append(mdcategory)
                        }
                    }
                    self.presenter.onFetchMDCategories(mdCategories: categoryList)
                }
        }.onFailure(){ error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchMMCategoryList() {
        Moviemm.sharedInstance.categoryList().load()
            .onSuccess(){ data in
                if let mmList: [MMCategory] = data.typedContent() {
                    self.presenter.onFetchMMCategories(mmCategories: mmList)
                }
        }.onFailure(){ error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
}
