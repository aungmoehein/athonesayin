//
//  SeriesView.swift
//  Series
//
//  Created by Aung Moe Hein on 25/05/2020.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import PopupDialog

final class SeriesView: UITableViewController, ViewInterface, NVActivityIndicatorViewable {
    
    var seriesParser: SeriesParser!
    var videoWrapper: VideoWrapper!
    
    var presenter: SeriesPresenterViewInterface!

    var zsections: [ZSection] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var mmsections: [MMDataSection] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var mssections: [MSSection] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        self.title = seriesParser.seriesTitle
        
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableView.automaticDimension
        tableView.reloadData()

        self.presenter.start()
        onRefresh()
    }
    
    @objc func canRotate() -> Void {}

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    @objc func onRefresh() {
        self.startAnimating()
        switch seriesParser.partner {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            self.presenter.getSeries(id: seriesParser.id, apiKey: seriesParser.apiKey)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            self.presenter.getMSEpisodes(id: seriesParser.id)
        default:
            self.stopAnimating()
            mmsections.append(MMDataSection(title: seriesParser.seriesTitle, episodes: seriesParser.episodes))
        }
    }
}

extension SeriesView  {
    override func numberOfSections(in tableView: UITableView) -> Int {
        switch seriesParser.partner {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            return zsections.count
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            return mssections.count
        default:
            return mmsections.count
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch seriesParser.partner {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            return zsections[section].collapsed ? 0 : zsections[section].serie.episodes!.count
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            return mssections[section].collapsed ? 0 : mssections[section].episodes.count
        default:
            return mmsections[section].collapsed ? 0 : mmsections[section].episodes.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var episodeName: String = ""
        switch seriesParser.partner {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            let item: ZEpisode = zsections[indexPath.section].serie.episodes![indexPath.row]
            episodeName = item.title!
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            let item: MSEpisode = mssections[indexPath.section].episodes[indexPath.row]
            episodeName = item.episodename!
        default:
            episodeName = "Episode \(indexPath.row + 1)"
        }
        
        let cell: SeriesViewCell = tableView.dequeueReusableCell(withIdentifier: "series_view_cell") as? SeriesViewCell ??
            SeriesViewCell(style: .default, reuseIdentifier: "series_view_cell")
        cell.selectionStyle = .none
        cell.nameLabel.text = episodeName
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var videoURL: String = ""
        var videoName: String = ""
        
        switch seriesParser.partner {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            let episode = zsections[indexPath.section].serie.episodes![indexPath.row]
            videoURL = episode.sources![0].url!
            videoName = "\(zsections[indexPath.section].serie.title!):\(episode.title!)"
            videoWrapper = VideoWrapper(id: episode.id!, partner: seriesParser.partner, imageURL: seriesParser.image, videoURL: videoURL, videoName: videoName)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            let episode = mssections[indexPath.section].episodes[indexPath.row]
            videoURL = episode.stream!
            videoName = "\(mssections[indexPath.section].title):\(episode.episodename!)"
            videoWrapper = VideoWrapper(id: episode.id!, partner: seriesParser.partner, imageURL: seriesParser.image, videoURL: videoURL, videoName: videoName)
        default:
            let v_link = mmsections[0].episodes[indexPath.row].split(separator: "/")[2]
            let vid = Int(String(v_link).toNumber())
            videoURL = String(format: seriesParser.apiKey, String(v_link))
            videoName = "\(mmsections[0].title):Episode \(indexPath.row + 1)"
            videoWrapper = VideoWrapper(id: vid!, partner: seriesParser.partner, imageURL: seriesParser.image, videoURL: videoURL, videoName: videoName)
        }
        
        self.presenter.playEpisode(videoWrapper: videoWrapper)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    // Header
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "series_header_view") as? SeriesHeaderView ?? SeriesHeaderView(reuseIdentifier: "series_header_view")
        
        var headerTitle: String = ""
        switch seriesParser.partner {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            headerTitle = zsections[section].serie.title!
            header.setCollapsed(zsections[section].collapsed)
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            headerTitle = mssections[section].title
            header.setCollapsed(mssections[section].collapsed)
        default:
            headerTitle = mmsections[section].title
            header.setCollapsed(mmsections[section].collapsed)
        }

        header.titleLabel.text = headerTitle
        header.arrowLabel.text = ">"

        header.section = section
        header.delegate = self

        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
}

extension SeriesView: SeriesHeaderViewDelegate {
    
    func toggleSection(_ header: SeriesHeaderView, section: Int) {
        var collapsed: Bool = false
        
        // Toggle collapse
        switch seriesParser.partner {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            collapsed = !zsections[section].collapsed
            zsections[section].collapsed = collapsed
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            collapsed = !mssections[section].collapsed
            mssections[section].collapsed = collapsed
        default:
            collapsed = !mmsections[section].collapsed
            mmsections[section].collapsed = collapsed
        }
        
        header.setCollapsed(collapsed)
        
        tableView.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
}

extension SeriesView: SeriesViewPresenterInterface {
    func displaySeries(zSeries: [ZSerie]) {
        self.stopAnimating()
        var sections: [ZSection] = []
        zSeries.forEach() {
            zSerie in
            sections.append(ZSection(serie: zSerie))
        }
        self.zsections = sections
    }
    
    func displayMSEpisodes(msEpisodes: [MSEpisode]) {
        self.stopAnimating()
        var sections: [MSSection] = []
        sections.append(MSSection(title: seriesParser.seriesTitle, episodes: msEpisodes))
        self.mssections = sections
    }
    
    func onError() {
        self.stopAnimating()
        let title = "Something went wrong."
        let message = "Check your internet connection."
        let image = UIImage(named: "no_wifi")

        let popup = PopupDialog(title: title, message: message, image: image)
        let retryBtn = DefaultButton(title: "Retry", dismissOnTap: false) {
            self.onRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        
        popup.addButtons([retryBtn])
        self.present(popup, animated: true, completion: nil)
    }
}
