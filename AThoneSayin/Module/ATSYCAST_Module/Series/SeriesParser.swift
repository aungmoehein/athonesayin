//
//  SeriesParser.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 28/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct SeriesParser {
    var id: Int = 0
    var apiKey: String = ""
    var seriesTitle: String = ""
    var partner: String = ""
    var image: String = ""
    var episodes: [String] = []
    
    public init(id: Int = 0, apiKey: String, seriesTitle: String, partner: String, image: String, episodes: [String] = []) {
        self.id = id
        self.apiKey = apiKey
        self.seriesTitle = seriesTitle
        self.partner = partner
        self.image = image
        self.episodes = episodes
    }
}
