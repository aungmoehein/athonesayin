//
//  SeriesModule.swift
//  Series
//
//  Created by Aung Moe Hein on 25/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol SeriesRouterPresenterInterface: RouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper)
}

// MARK: - presenter

protocol SeriesPresenterRouterInterface: PresenterRouterInterface {

}

protocol SeriesPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchSeries(zSeries: [ZSerie])
    func onFetchMSEpisodes(msEpisodes: [MSEpisode])
    func onFetchError()
}

protocol SeriesPresenterViewInterface: PresenterViewInterface {
    func start()
    func getSeries(id: Int, apiKey: String)
    func getMSEpisodes(id: Int)
    func playEpisode(videoWrapper: VideoWrapper)
}

// MARK: - interactor

protocol SeriesInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchSeries(id: Int, apiKey: String)
    func fetchMSEpisodes(id: Int)
}

// MARK: - view

protocol SeriesViewPresenterInterface: ViewPresenterInterface {
    func displaySeries(zSeries: [ZSerie])
    func displayMSEpisodes(msEpisodes: [MSEpisode])
    func onError()
}


// MARK: - module builder

final class SeriesModule: ModuleInterface {

    typealias View = SeriesView
    typealias Presenter = SeriesPresenter
    typealias Router = SeriesRouter
    typealias Interactor = SeriesInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "series") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
