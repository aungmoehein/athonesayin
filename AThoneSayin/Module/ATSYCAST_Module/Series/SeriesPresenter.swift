//
//  SeriesPresenter.swift
//  Series
//
//  Created by Aung Moe Hein on 25/05/2020.
//

import Foundation

final class SeriesPresenter: PresenterInterface {

    var router: SeriesRouterPresenterInterface!
    var interactor: SeriesInteractorPresenterInterface!
    weak var view: SeriesViewPresenterInterface!

}

extension SeriesPresenter: SeriesPresenterRouterInterface {

}

extension SeriesPresenter: SeriesPresenterInteractorInterface {
    func onFetchSeries(zSeries: [ZSerie]) {
        self.view.displaySeries(zSeries: zSeries)
    }
    
    func onFetchMSEpisodes(msEpisodes: [MSEpisode]) {
        self.view.displayMSEpisodes(msEpisodes: msEpisodes)
    }
    
    func onFetchError() {
        self.view.onError()
    }
}

extension SeriesPresenter: SeriesPresenterViewInterface {

    func start() {

    }

    func getSeries(id: Int, apiKey: String) {
        self.interactor.fetchSeries(id: id, apiKey: apiKey)
    }
    
    func getMSEpisodes(id: Int) {
        self.interactor.fetchMSEpisodes(id: id)
    }
    
    func playEpisode(videoWrapper: VideoWrapper) {
        self.router.goToVideoPlayer(videoWrapper: videoWrapper)
    }
}
