//
//  SeriesInteractor.swift
//  Series
//
//  Created by Aung Moe Hein on 25/05/2020.
//

import Foundation

final class SeriesInteractor: InteractorInterface {

    weak var presenter: SeriesPresenterInteractorInterface!
}

extension SeriesInteractor: SeriesInteractorPresenterInterface {
    func fetchSeries(id: Int, apiKey: String) {
        ZChannel.sharedInstance.getSeries(id: id, apiKey: apiKey).load()
            .onSuccess(){data in
            if let zSeries: [ZSerie] = data.typedContent() {
                self.presenter.onFetchSeries(zSeries: zSeries)
            }
        }.onFailure(){error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
    
    func fetchMSEpisodes(id: Int) {
        MSubMovie.sharedInstance.getEpisode(id: id).load()
            .onSuccess(){data in
            if let msEpisodes: [MSEpisode] = data.typedContent() {
                self.presenter.onFetchMSEpisodes(msEpisodes: msEpisodes)
            }
        }.onFailure(){error in
            debugPrint(error.userMessage)
            self.presenter.onFetchError()
        }
    }
}
