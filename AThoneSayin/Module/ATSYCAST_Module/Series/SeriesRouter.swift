//
//  SeriesRouter.swift
//  Series
//
//  Created by Aung Moe Hein on 25/05/2020.
//

import Foundation
import UIKit

final class SeriesRouter: RouterInterface {

    weak var presenter: SeriesPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension SeriesRouter: SeriesRouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "video_play_view") as! VideoPlayView
        vc.isVideo = true
        vc.videoWrapper = videoWrapper
        vc.videoName = videoWrapper.videoName
        vc.streamURL = videoWrapper.videoURL
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
