//
//  RecentlyPlayedView.swift
//  RecentlyPlayed
//
//  Created by Aung Moe Hein on 27/05/2020.
//

import Foundation
import UIKit
import XLPagerTabStrip

final class RecentlyPlayedView: UIViewController, ViewInterface, IndicatorInfoProvider, RecentlyPlayedTableCellDelegate {
    @IBOutlet weak var recentlyPlayedTB: UITableView!
    
    var itemInfo = IndicatorInfo(title: "Continue Watching")
    var presenter: RecentlyPlayedPresenterViewInterface!
    
    var zChannelPlayBacks: [Playback] = [] {
        didSet {
            recentlyPlayedTB.reloadData()
        }
    }
    
    var msMoviePlayBacks: [Playback] = [] {
        didSet {
            recentlyPlayedTB.reloadData()
        }
    }
    
    var movieMMPlayBacks: [Playback] = [] {
        didSet {
            recentlyPlayedTB.reloadData()
        }
    }
    
    var darkSitePlayBacks: [Playback] = [] {
        didSet {
            recentlyPlayedTB.reloadData()
        }
    }
    
    var animePlayBacks: [Playback] = [] {
        didSet {
            recentlyPlayedTB.reloadData()
        }
    }
    
    var partners: [String] = [] {
        didSet {
            partners.forEach() {
                partner in
                self.presenter.getPlayBacks(partner: partner)
            }
            recentlyPlayedTB.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        recentlyPlayedTB.register(UINib(nibName: "RecentlyPlayedTableCell", bundle: nil), forCellReuseIdentifier: "recently_played_table_cell")
        recentlyPlayedTB.delegate = self
        recentlyPlayedTB.dataSource = self
        recentlyPlayedTB.reloadData()

        self.presenter.start()
    }
    
    @objc func canRotate() -> Void {}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
        self.presenter.getPartners()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension RecentlyPlayedView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partners.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "recently_played_table_cell", for: indexPath) as! RecentlyPlayedTableCell
        cell.selectionStyle = .none
        cell.delegate = self
        cell.partnerTitle.text = partners[indexPath.row]
        switch partners[indexPath.row] {
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
            cell.playBacks = zChannelPlayBacks
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
            cell.playBacks = msMoviePlayBacks
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
            cell.playBacks = darkSitePlayBacks
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
            cell.playBacks = movieMMPlayBacks
        case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerAnimeflix):
            cell.playBacks = animePlayBacks
        default:
            break
        }
        return cell
    }
    
    func click(playBack: Playback) {
        let videoWrapper = VideoWrapper(id: Int(playBack.id), partner: playBack.partner, imageURL: playBack.imageURL, videoURL: playBack.videoURL, videoName: playBack.videoName)
        self.presenter.playVideo(videoWrapper: videoWrapper)
    }
}

extension RecentlyPlayedView: RecentlyPlayedViewPresenterInterface {
    func displayZChannelPlayBacks(playBacks: [Playback]) {
        self.zChannelPlayBacks = playBacks
    }
    
    func displayMSMoviePlayBacks(playBacks: [Playback]) {
        self.msMoviePlayBacks = playBacks
    }
    
    func displayMovieMMPlayBacks(playBacks: [Playback]) {
        self.movieMMPlayBacks = playBacks
    }
    
    func displayDarkSitePlayBacks(playBacks: [Playback]) {
        self.darkSitePlayBacks = playBacks
    }
    
    func displayAnimePlayBacks(playBacks: [Playback]) {
        self.animePlayBacks = playBacks
    }
    
    func displayPartners(partners: [String]) {
        self.partners = partners
    }
}
