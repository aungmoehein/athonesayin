//
//  RecentlyPlayedInteractor.swift
//  RecentlyPlayed
//
//  Created by Aung Moe Hein on 27/05/2020.
//

import Foundation

final class RecentlyPlayedInteractor: InteractorInterface {

    weak var presenter: RecentlyPlayedPresenterInteractorInterface!
    var playBackRepository: PlaybackRepository!
    init(playBackRepository: PlaybackRepository) {
        self.playBackRepository = playBackRepository
    }
}

extension RecentlyPlayedInteractor: RecentlyPlayedInteractorPresenterInterface {
    func fetchPlayBacks(partner: String) {
        if let playBacks = playBackRepository.findPlayBacks(partner: partner, count: 100) {
            switch partner {
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
                self.presenter.onZChannelPlayBacksReceived(playBacks: playBacks)
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
                self.presenter.onMSMoviePlayBacksReceived(playBacks: playBacks)
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
                self.presenter.onDarkSitePlayBacksReceived(playBacks: playBacks)
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
                self.presenter.onMovieMMPlayBacksReceived(playBacks: playBacks)
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerAnimeflix):
                self.presenter.onAnimePlayBacksReceived(playBacks: playBacks)
            default:
                break
            }
        } else {
            switch partner {
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerZChannel):
                self.presenter.onZChannelPlayBacksReceived(playBacks: [])
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMSub):
                self.presenter.onMSMoviePlayBacksReceived(playBacks: [])
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMDark):
                self.presenter.onDarkSitePlayBacksReceived(playBacks: [])
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM):
                self.presenter.onMovieMMPlayBacksReceived(playBacks: [])
            case UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerAnimeflix):
                self.presenter.onAnimePlayBacksReceived(playBacks: [])
            default:
                break
            }
        }
    }
    
    func fetchPartners() {
        if let partners = playBackRepository.findPlayBackCategories() {
            var atsyPartners: [String] = []
            partners.forEach() {
                partner in
                if partner != "live" {
                    atsyPartners.append(partner)
                }
            }
            presenter.onPartnesReceived(partners: atsyPartners)
        } else {
            presenter.onPartnesReceived(partners: [])
        }
    }
}
