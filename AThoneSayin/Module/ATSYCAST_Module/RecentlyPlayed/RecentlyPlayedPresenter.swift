//
//  RecentlyPlayedPresenter.swift
//  RecentlyPlayed
//
//  Created by Aung Moe Hein on 27/05/2020.
//

import Foundation

final class RecentlyPlayedPresenter: PresenterInterface {

    var router: RecentlyPlayedRouterPresenterInterface!
    var interactor: RecentlyPlayedInteractorPresenterInterface!
    weak var view: RecentlyPlayedViewPresenterInterface!

}

extension RecentlyPlayedPresenter: RecentlyPlayedPresenterRouterInterface {

}

extension RecentlyPlayedPresenter: RecentlyPlayedPresenterInteractorInterface {
    func onZChannelPlayBacksReceived(playBacks: [Playback]) {
        self.view.displayZChannelPlayBacks(playBacks: playBacks)
    }
    
    func onMSMoviePlayBacksReceived(playBacks: [Playback]) {
        self.view.displayMSMoviePlayBacks(playBacks: playBacks)
    }
    
    func onMovieMMPlayBacksReceived(playBacks: [Playback]) {
        self.view.displayMovieMMPlayBacks(playBacks: playBacks)
    }
    
    func onDarkSitePlayBacksReceived(playBacks: [Playback]) {
        self.view.displayDarkSitePlayBacks(playBacks: playBacks)
    }
    
    func onAnimePlayBacksReceived(playBacks: [Playback]) {
        self.view.displayAnimePlayBacks(playBacks: playBacks)
    }
    
    func onPartnesReceived(partners: [String]) {
        self.view.displayPartners(partners: partners)
    }
}

extension RecentlyPlayedPresenter: RecentlyPlayedPresenterViewInterface {

    func start() {

    }

    func getPlayBacks(partner: String) {
        self.interactor.fetchPlayBacks(partner: partner)
    }
    
    func playVideo(videoWrapper: VideoWrapper) {
        self.router.goToVideoPlayer(videoWrapper: videoWrapper)
    }
    
    func getPartners() {
        self.interactor.fetchPartners()
    }
}
