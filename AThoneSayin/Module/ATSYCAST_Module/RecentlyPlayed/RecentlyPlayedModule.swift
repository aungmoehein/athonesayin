//
//  RecentlyPlayedModule.swift
//  RecentlyPlayed
//
//  Created by Aung Moe Hein on 27/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol RecentlyPlayedRouterPresenterInterface: RouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper)
}

// MARK: - presenter

protocol RecentlyPlayedPresenterRouterInterface: PresenterRouterInterface {

}

protocol RecentlyPlayedPresenterInteractorInterface: PresenterInteractorInterface {
    func onZChannelPlayBacksReceived(playBacks: [Playback])
    func onMSMoviePlayBacksReceived(playBacks: [Playback])
    func onMovieMMPlayBacksReceived(playBacks: [Playback])
    func onDarkSitePlayBacksReceived(playBacks: [Playback])
    func onAnimePlayBacksReceived(playBacks: [Playback])
    func onPartnesReceived(partners: [String])
}

protocol RecentlyPlayedPresenterViewInterface: PresenterViewInterface {
    func start()
    func getPlayBacks(partner: String)
    func playVideo(videoWrapper: VideoWrapper)
    func getPartners()
}

// MARK: - interactor

protocol RecentlyPlayedInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchPlayBacks(partner: String)
    func fetchPartners()
}

// MARK: - view

protocol RecentlyPlayedViewPresenterInterface: ViewPresenterInterface {
    func displayZChannelPlayBacks(playBacks: [Playback])
    func displayMSMoviePlayBacks(playBacks: [Playback])
    func displayMovieMMPlayBacks(playBacks: [Playback])
    func displayDarkSitePlayBacks(playBacks: [Playback])
    func displayAnimePlayBacks(playBacks: [Playback])
    func displayPartners(partners: [String])
}


// MARK: - module builder

final class RecentlyPlayedModule: ModuleInterface {

    typealias View = RecentlyPlayedView
    typealias Presenter = RecentlyPlayedPresenter
    typealias Router = RecentlyPlayedRouter
    typealias Interactor = RecentlyPlayedInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "recently_played") as! View
        
        let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        let playBackRepository = PlaybackRepository(context: context)
        
        let interactor = Interactor(playBackRepository: playBackRepository)
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
