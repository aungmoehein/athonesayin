//
//  RecentlyPlayedRouter.swift
//  RecentlyPlayed
//
//  Created by Aung Moe Hein on 27/05/2020.
//

import Foundation
import UIKit

final class RecentlyPlayedRouter: RouterInterface {

    weak var presenter: RecentlyPlayedPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension RecentlyPlayedRouter: RecentlyPlayedRouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "video_play_view") as! VideoPlayView
        vc.isVideo = true
        vc.videoWrapper = videoWrapper
        vc.videoName = videoWrapper.videoName
        vc.streamURL = videoWrapper.videoURL
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
