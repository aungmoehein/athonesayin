//
//  VideoPlayView.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 21/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import BMPlayer
import AVFoundation
import NVActivityIndicatorView
import TTGSnackbar

func delay(_ seconds: Double, completion:@escaping ()->()) {
    let popTime = DispatchTime.now() + Double(Int64( Double(NSEC_PER_SEC) * seconds )) / Double(NSEC_PER_SEC)
    
    DispatchQueue.main.asyncAfter(deadline: popTime) {
        completion()
    }
}

class VideoPlayView: UIViewController {
//        @IBOutlet weak var player: BMCustomPlayer!
    
    var player: BMPlayer!
    
    var isVideo: Bool = false
    var isHentai: Bool = false
    var videoName: String = ""
    var streamURL: String = ""
    
    //for playback
    var videoEnded: Bool = false
    var videoWrapper: VideoWrapper!
    var playBack: Playback!
    let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    var playBackRepository: PlaybackRepository!
    var playerTotalDuration: TimeInterval!
    var playerCurrentTime: TimeInterval!
    
    private var snackbar = TTGSnackbar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPlayerManager()
        preparePlayer()
        
        let value = UIInterfaceOrientation.landscapeRight.rawValue
        UIDevice.current.setValue(value, forKey: "orientation")
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidEnterBackground),
                                               name: UIApplication.didEnterBackgroundNotification,
                                               object: nil)

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationWillEnterForeground),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.AddToFavourite(notification:)), name: Notification.Name(Notification.Noti.add_favourite), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if isVideo && !isHentai {
            addToPlayBack(videoWrapper: videoWrapper)
        }
        
        if playBack != nil {
            setupPlayerResource(position: Int(playBack.position))
        }else {
            setupPlayerResource()
        }
        player.autoPlay()
    }
    
    @objc func canRotate() -> Void {}
    
    func showSnackbar(text: String) {
        let snackbar = TTGSnackbar()
        snackbar.message = text
        snackbar.duration = .middle
        snackbar.onTapBlock = { (snackbar) in snackbar.dismiss()}
        snackbar.onSwipeBlock = { (snackbar, direction) in
            if direction == .right {
                snackbar.animationType = .slideFromLeftToRight
            } else if direction == .left {
                snackbar.animationType = .slideFromRightToLeft
            }
            snackbar.dismiss()
        }
        snackbar.backgroundColor = UIColor(hex: 0xF93E2A)
        snackbar.actionTextColor = UIColor.white
        snackbar.messageTextColor = UIColor.white
        snackbar.show()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        get {
            return .landscapeRight
        }
    }
    
    @objc func applicationWillEnterForeground() {
        if isVideo && !videoEnded && !isHentai && self.playerCurrentTime != nil {
            self.updatePlayBack(position: Int(self.playerCurrentTime), duration: Int(self.playerTotalDuration))
        }
    }
    
    @objc func applicationDidEnterBackground() {
        player.pause(allowAutoPlay: false)
        if isVideo && !videoEnded && !isHentai && self.playerCurrentTime != nil {
            self.updatePlayBack(position: Int(self.playerCurrentTime), duration: Int(self.playerTotalDuration))
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.default, animated: false)
        // If use the slide to back, remember to call this method
        // 使用手势返回的时候，调用下面方法
        player.pause(allowAutoPlay: true)
        if isVideo && !videoEnded && !isHentai && self.playerCurrentTime != nil {
            self.updatePlayBack(position: Int(self.playerCurrentTime), duration: Int(self.playerTotalDuration))
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = true
//        UIApplication.shared.setStatusBarStyle(UIStatusBarStyle.lightContent, animated: false)
        // If use the slide to back, remember to call this method
        // 使用手势返回的时候，调用下面方法
    }
    
    deinit {
        // If use the slide to back, remember to call this method
        // 使用手势返回的时候，调用下面方法手动销毁
        player.prepareToDealloc()
        print("VideoPlayViewController Deinit")
    }
    
}
