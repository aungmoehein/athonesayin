//
//  BMPlayerCustomControlView.swift
//  BMPlayer
//
//  Created by BrikerMan on 2017/4/4.
//  Copyright © 2017年 CocoaPods. All rights reserved.
//

import UIKit
import BMPlayer

class LiveChannelControlView: BMPlayerControlView {
    
    var btnFavourite = UIButton()
    var existFavorite: Bool = false
    
    override func customizeUIComponents() {
        player?.pause(allowAutoPlay: true)
        playButton.removeFromSuperview()
        currentTimeLabel.removeFromSuperview()
        totalTimeLabel.removeFromSuperview()
        
        timeSlider.removeFromSuperview()
        
        mainMaskView.backgroundColor   = UIColor.clear
        topMaskView.backgroundColor    = UIColor.black.withAlphaComponent(0.4)
        bottomMaskView.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        bottomMaskView.addSubview(btnFavourite)
        btnFavourite.snp.makeConstraints {
            $0.left.equalTo(self).offset(10)
            $0.bottom.equalTo(bottomMaskView).offset(-10)
        }
        
        btnFavourite.addTarget(self, action: #selector(addToFavourite), for: .touchUpInside)
        btnFavourite.setTitleColor(UIColor.white, for: .normal)
    }
    
    @objc func addToFavourite() {
        existFavorite.toggle()
        btnFavourite.setTitle(!existFavorite ? "+ Add To Favourite" : "- Remove From Favourite", for: .normal)
        NotificationCenter.default.post(name: Notification.Name(Notification.Noti.add_favourite), object: existFavorite)
    }
    
    override func updateUI(_ isForFullScreen: Bool) {
        super.updateUI(isForFullScreen)
        if let layer = player?.playerLayer {
            layer.frame = player!.bounds
        }
    }
    
    override func controlViewAnimation(isShow: Bool) {
        self.isMaskShowing = isShow
//        UIApplication.shared.setStatusBarHidden(!isShow, with: .fade)
        
        UIView.animate(withDuration: 0.24, animations: {
            self.topMaskView.snp.remakeConstraints {
                $0.top.equalTo(self.mainMaskView).offset(isShow ? 0 : -65)
                $0.left.right.equalTo(self.mainMaskView)
                $0.height.equalTo(65)
            }
            
            self.bottomMaskView.snp.remakeConstraints {
                $0.bottom.equalTo(self.mainMaskView).offset(isShow ? 0 : 50)
                $0.left.right.equalTo(self.mainMaskView)
                $0.height.equalTo(50)
            }
            self.layoutIfNeeded()
        }) { (_) in
            self.autoFadeOutControlViewWithAnimation()
        }
    }
}

class BMCustomPlayer: BMPlayer {
    override func storyBoardCustomControl() -> BMPlayerControlView? {
        return LiveChannelControlView()
    }
}
