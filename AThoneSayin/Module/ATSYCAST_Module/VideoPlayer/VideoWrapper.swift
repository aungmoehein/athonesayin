//
//  VideoWrapper.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 26/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

struct VideoWrapper {
    let id: Int
    let partner: String
    let imageURL: String
    let videoURL: String
    let videoName: String
    let isVideo: Bool
    let isHentai: Bool
    
    init(id: Int = 0, partner: String = "", imageURL: String = "", videoURL: String = "", videoName: String = "", isVideo: Bool = true, isHentai: Bool = false) {
        self.id = id
        self.partner = partner
        self.imageURL = imageURL
        self.videoURL = videoURL
        self.videoName = videoName
        self.isVideo = isVideo
        self.isHentai = isHentai
    }
}
