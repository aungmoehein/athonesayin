//
//  VideoPlayView+Player.swift
//  ASoneTote
//
//  Created by Aung Moe Hein on 22/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation
import BMPlayer
import AVKit
import NVActivityIndicatorView

extension VideoPlayView {
    /**
     prepare playerView
     */
    
    func addToPlayBack(videoWrapper: VideoWrapper) {
        self.playBackRepository = PlaybackRepository(context: context)
        switch videoWrapper.partner {
        case "live":
            if let mPlaybacks = self.playBackRepository.findVideo(id: videoWrapper.id, partner: videoWrapper.partner) {
                if mPlaybacks.count > 0 {
                    self.preparePlayback(videoWrapper: videoWrapper, playBacks: mPlaybacks)
                }else {
                    self.playBackRepository.create(videoWrapper: videoWrapper, position: 0, duration: 0)
                    self.addToPlayBack(videoWrapper: videoWrapper)
                }
            }
        case "stream":
            break
        default:
            if let mPlaybacks = self.playBackRepository.findLiveVideo(name: videoWrapper.videoName, partner: videoWrapper.partner) {
                if mPlaybacks.count > 0 {
                    self.preparePlayback(videoWrapper: videoWrapper, playBacks: mPlaybacks)
                }else {
                    self.playBackRepository.create(videoWrapper: videoWrapper, position: 0, duration: 0)
                    self.addToPlayBack(videoWrapper: videoWrapper)
                }
            }
        }
    }
    
    private func preparePlayback(videoWrapper: VideoWrapper, playBacks: [Playback]) {
        switch videoWrapper.partner {
        case "live":
            if let mPlayback = playBacks.first(where: { $0.id == videoWrapper.id }) {
                self.playBack = mPlayback
            }else {
                self.playBackRepository.create(videoWrapper: videoWrapper, position: 0, duration: 0)
            }
        case "stream":
            break
        default:
            if let mPlayback = playBacks.first(where: { $0.videoName == videoWrapper.videoName }) {
                self.playBack = mPlayback
            }else {
                self.playBackRepository.create(videoWrapper: videoWrapper, position: 0, duration: 0)
            }
        }
    }
    
    func updatePlayBack(position: Int, duration: Int) {
        if (self.playBack != nil) {
            playBack!.position = Int16(position)
            playBack!.duration = Int16(duration)
            playBack!.updatedAt = Date()
            self.playBackRepository.persist()
        }
    }
    
    func preparePlayer() {
        var controller: BMPlayerControlView? = nil
        
        if !self.isVideo && !isHentai {
            let liveChannelControlView = LiveChannelControlView()
            self.playBackRepository = PlaybackRepository(context: context)
            if let mPlaybacks = self.playBackRepository.findLiveVideo(name: videoWrapper.videoName, partner: videoWrapper.partner) {
                liveChannelControlView.existFavorite = mPlaybacks.count > 0
                if mPlaybacks.count > 0 {
                    self.playBack = mPlaybacks.first
                }
                liveChannelControlView.btnFavourite.setTitle(mPlaybacks.count <= 0 ? "+ Add To Favourite" : "- Remove From Favourite", for: .normal)
            }
            liveChannelControlView.btnFavourite.isHidden = videoWrapper.partner == "stream"
            controller = liveChannelControlView
        }
        
        player = BMPlayer(customControlView: controller)
        view.addSubview(player)
        player.snp.makeConstraints { (make) in
            make.centerY.equalTo(self.view)
            make.left.right.equalTo(self.view)
            make.height.equalTo(player.snp.width).multipliedBy(9.0/16.0).priority(750)
        }
        
        player.delegate = self
        player.backBlock = { [unowned self] (isFullScreen) in
            self.resetPlayerManager()
            let _ = self.navigationController?.popViewController(animated: true)
        }
        
        self.view.layoutIfNeeded()
    }
    
    func setupPlayerResource(position: Int = 0) {
        print("STREAM URL::: ",self.videoWrapper.videoURL)
        var url = URL(string: self.videoWrapper.videoURL.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!)!
        var options: [String: [String:String]] = [:]
        
        if isVideo && !isHentai {
            let decodedStreamURL = self.streamURL.removingPercentEncoding
            url = URL(string: decodedStreamURL!.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!)!
            
            if videoWrapper.partner == UserDefaults.standard.string(forKey: UserDefaults.Keys.partnerMovieMM) {
                print("HEREEEEEE")
                let apiKeys = url.absoluteString.split(separator: ",")
                url = URL(string: String(apiKeys[0]))!
                let user_agent = apiKeys[1]
                let header = ["User-Agent": UAString(name: String(user_agent))]
                options = ["AVURLAssetHTTPHeaderFieldsKey":header]
            }
        }
        
        let definition = BMPlayerResourceDefinition(url: url,
                                                    definition: "HD",
                                                    options: options)
        
        let asset = BMPlayerResource(name: self.videoName,
                                     definitions: [definition],
                                     cover: nil,
                                     subtitles: nil)
        
        // Play audio even device is in silent mode.
        do {
           try AVAudioSession.sharedInstance().setCategory(.playback)
        } catch(let error) {
            print(error.localizedDescription)
        }
        
        player.seek(TimeInterval(position))
        player.setVideo(resource: asset)
    }
    
    func setupPlayerManager() {
        resetPlayerManager()
    }
    
    func resetPlayerManager() {
        BMPlayerConf.allowLog = false
        BMPlayerConf.shouldAutoPlay = true
        BMPlayerConf.tintColor = UIColor.white
        BMPlayerConf.topBarShowInCase = .always
        BMPlayerConf.loaderType  = NVActivityIndicatorType.ballRotateChase
    }
}

// MARK:- BMPlayerDelegate example
extension VideoPlayView: BMPlayerDelegate {
    // Call when player orinet changed
    func bmPlayer(player: BMPlayer, playerOrientChanged isFullscreen: Bool) {
        player.snp.remakeConstraints { (make) in
            make.left.right.equalTo(self.view)
            if isFullscreen {
                make.top.equalTo(view.snp.top)
                make.bottom.equalTo(view.snp.bottom)
            } else {
                make.centerY.equalTo(self.view)
                make.height.equalTo(view.snp.width).multipliedBy(9.0/16.0).priority(750)
            }
        }
    }
    
    // Call back when playing state changed, use to detect is playing or not
    func bmPlayer(player: BMPlayer, playerIsPlaying playing: Bool) {
        print("| BMPlayerDelegate | playerIsPlaying | playing - \(playing)")
        
        // For Live Channel
        if !playing && !isVideo && !isHentai {
            player.play()
        }
    }
    
    // Call back when playing state changed, use to detect specefic state like buffering, bufferfinished
    func bmPlayer(player: BMPlayer, playerStateDidChange state: BMPlayerState) {
        print("| BMPlayerDelegate | playerStateDidChange | state - \(state)")
        if state == .error {
            self.navigationController?.popViewController(animated: true)
            showSnackbar(text: videoWrapper.partner != "stream" ? "This video is not supported in iOS." : "Unable to play this stream URL.")
            if playBack != nil {
                self.playBackRepository.delete(playBack: self.playBack)
            }
        }
        
        if state == .playedToTheEnd && isVideo && !isHentai {
            videoEnded = true
            self.playBackRepository.delete(playBack: self.playBack)
        }
    }
    
    // Call back when play time change
    func bmPlayer(player: BMPlayer, playTimeDidChange currentTime: TimeInterval, totalTime: TimeInterval) {
        //        print("| BMPlayerDelegate | playTimeDidChange | \(currentTime) of \(totalTime)")
        self.playerCurrentTime = currentTime
        self.playerTotalDuration = totalTime
    }
    
    // Call back when the video loaded duration changed
    func bmPlayer(player: BMPlayer, loadedTimeDidChange loadedDuration: TimeInterval, totalDuration: TimeInterval) {
        //        print("| BMPlayerDelegate | loadedTimeDidChange | \(loadedDuration) of \(totalDuration)")
    }
    
}

extension VideoPlayView {
    @objc func AddToFavourite(notification: Notification) {
        let add = notification.object as! Bool
        if add {
            addToPlayBack(videoWrapper: videoWrapper)
            showSnackbar(text: "Successfully added to favourites.")
        }else {
            self.playBackRepository.delete(playBack: self.playBack)
            showSnackbar(text: "Successfully removed from favourites.")
        }
    }
}
