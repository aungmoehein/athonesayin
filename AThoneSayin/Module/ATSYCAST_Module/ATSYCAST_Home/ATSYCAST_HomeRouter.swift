//
//  HomeRouter.swift
//  Home
//
//  Created by Aung Moe Hein on 21/05/2020.
//

import Foundation
import UIKit

final class ATSYCAST_HomeRouter: RouterInterface {

    weak var presenter: ATSYCAST_HomePresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension ATSYCAST_HomeRouter: ATSYCAST_HomeRouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "video_play_view") as! VideoPlayView
        vc.videoWrapper = videoWrapper
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
