//
//  HomeInteractor.swift
//  Home
//
//  Created by Aung Moe Hein on 21/05/2020.
//

import Foundation

final class ATSYCAST_HomeInteractor: InteractorInterface {

    weak var presenter: ATSYCAST_HomePresenterInteractorInterface!
    var playBackRepository: PlaybackRepository!
    init(playBackRepository: PlaybackRepository) {
        self.playBackRepository = playBackRepository
    }
}

extension ATSYCAST_HomeInteractor: ATSYCAST_HomeInteractorPresenterInterface {
    func fetchLiveChannels(sheetNo: String) {
        SheetService.sharedInstance.partnerList.child("/\(sheetNo)/public/values").withParam("alt", "json").load()
            .onSuccess(){data in
                var liveChannels: [Live] = []
                let response = Sheet2JSON.toJSON(data)
                if let data = response.data(using: String.Encoding.utf8) {
                    do {
                        let decoder = JSONDecoder()
                        let jsonDictionary = try decoder.decode([Dictionary<String, String>].self, from: data)
                        jsonDictionary.forEach() {
                            json in
                            let name = json["title"]!
                            let image = json["cover"]!
                            let stream = json["stream"]!
                            let status = json["iosstatus"]!
                            
                            if Bundle.main.infoDictionary?["Configuration"]  as? String == "Release" {
                                if status == "TRUE" {
                                    liveChannels.append(Live(channel_name: name, channel_img: image, stream_url: stream))
                                }
                            }else {
                                liveChannels.append(Live(channel_name: name, channel_img: image, stream_url: stream))
                            }
                        }
                    } catch {
                        // Handle error
                        print(error)
                    }
                }
                self.presenter.onFetchLiveChannels(liveChannels: liveChannels)
        }.onFailure(){
            error in
            self.presenter.onFetchError()
        }
    }
    
    func fetchFavorite() {
        var liveChannels: [Live] = []
        if let playBacks = playBackRepository.findPlayBacks(partner: "live", count: 100) {
            playBacks.forEach() {
                playBack in
                liveChannels.append(Live(channel_name: playBack.videoName, channel_img: playBack.imageURL, stream_url: playBack.videoURL))
            }
            self.presenter.onFetchLiveChannels(liveChannels: liveChannels)
        }
    }
    
    func sendAnalytics(actionType: String, deviceID: String, deviceName: String) {
        AnalyticService.sharedInstance.sendAnalytic().request(.post, urlEncoded: ["actionType": actionType, "deviceID": deviceID, "deviceName": deviceName, "action": "analytics", "appID": Bundle.main.bundleIdentifier!]).onSuccess() {
                data in
            debugPrint("Send Success")
        }.onFailure() {
            error in
            print(error)
        }
    }
}

