//
//  HomeView.swift
//  Home
//
//  Created by Aung Moe Hein on 21/05/2020.
//

import Foundation
import UIKit
import XLPagerTabStrip
import NVActivityIndicatorView
import PopupDialog
import Firebase

final class ATSYCAST_HomeView: UIViewController, ViewInterface, IndicatorInfoProvider, NVActivityIndicatorViewable {
    @IBOutlet weak var liveCV: UICollectionView!
    
    fileprivate var refresher : UIRefreshControl!
    private var cellSize: CGSize!
    
    var isFavorite: Bool = false
    var sheetNo: String = ""
    var itemInfo = IndicatorInfo(title: "Free To Air")
    var presenter: ATSYCAST_HomePresenterViewInterface!
    
    var liveChannels: [Live] = [] {
        didSet {
            liveCV.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        liveCV.register(UINib(nibName: "ContentCell", bundle: nil), forCellWithReuseIdentifier: "content_cell")
        liveCV.delegate = self
        liveCV.dataSource = self
        liveCV.reloadData()
        
        refresher = UIRefreshControl()
        refresher.addTarget(self, action: #selector(self.onRefresh), for: UIControl.Event.valueChanged)
        liveCV.addSubview(refresher)
        
        if !isFavorite {
            self.startAnimating()
            self.presenter.start(sheetNo: sheetNo)
        }
        
        cellSize = CGSize(width: ((view.frame.size.width)-30)/2, height: ((view.frame.size.height)-143)/3)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isFavorite {
            self.navigationItem.title = "Favorites"
            self.presenter.fetchFavorites()
        }
        
        self.navigationController?.navigationBar.isHidden = false
        
        if UIApplication.shared.statusBarOrientation.isLandscape {
            UserDefaults.standard.set(true, forKey: UserDefaults.Keys.is_initially_landscape)
        }
    }
    
    @objc func canRotate() -> Void {}
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        let size = view.frame.size
        
        guard let flowLayout = liveCV.collectionViewLayout as? UICollectionViewFlowLayout else {
            return
        }
        
        if UIDevice.current.orientation.isPortrait {
            cellSize = CGSize(width: ((size.width)-30)/2, height: ((size.height)-40)/3)
        }
        
        if UIDevice.current.orientation.isLandscape {
            cellSize = CGSize(width: ((size.width)-40)/3, height: 180)
        }
        
        if UserDefaults.standard.bool(forKey: UserDefaults.Keys.is_initially_landscape) {
            cellSize = CGSize(width: ((size.width)-40)/3, height: 180)
            UserDefaults.standard.set(false, forKey: UserDefaults.Keys.is_initially_landscape)
        }
        
        flowLayout.invalidateLayout()
    }
    
    @objc private func onRefresh() {
        self.startAnimating()
        self.presenter.start(sheetNo: sheetNo)
        refresher.endRefreshing()
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
}

extension ATSYCAST_HomeView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return liveChannels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "content_cell", for: indexPath) as! ContentCell
        cell.image = liveChannels[indexPath.row].channel_img
        cell.title = liveChannels[indexPath.row].channel_name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Analytics.logEvent(liveChannels[indexPath.row].channel_name, parameters: nil)
        self.presenter.catchAnalytics(actionType: liveChannels[indexPath.row].channel_name, deviceID: String(describing: UIDevice.current.identifierForVendor!.uuidString), deviceName: UIDevice.modelName)
        
        let videoWrapper = VideoWrapper(id: 0, partner: "live", imageURL: liveChannels[indexPath.row].channel_img, videoURL: liveChannels[indexPath.row].stream_url, videoName: liveChannels[indexPath.row].channel_name, isVideo: false, isHentai: false)
        self.presenter.videoPlayer(videoWrapper: videoWrapper)
    }
}

extension ATSYCAST_HomeView: UICollectionViewDelegateFlowLayout {
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = view.frame.size
        
        if indexPath.section == 0 {
            return cellSize
        } else {
            return CGSize(width: (size.width)-100, height: CGFloat(30))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 16, left: 10, bottom: 16, right: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}

extension ATSYCAST_HomeView: ATSYCAST_HomeViewPresenterInterface {
    func displayLiveChannels(liveChannels: [Live]) {
        self.stopAnimating()
        self.liveChannels = liveChannels
    }
    
    func onError() {
        self.stopAnimating()
        let title = "Something went wrong."
        let message = "Check your internet connection."
        let image = UIImage(named: "no_wifi")
        
        let popup = PopupDialog(title: title, message: message, image: image)
        let retryBtn = DefaultButton(title: "Retry", dismissOnTap: false) {
            self.onRefresh()
            self.dismiss(animated: true, completion: nil)
        }
        
        popup.addButtons([retryBtn])
        self.present(popup, animated: true, completion: nil)
    }
}
