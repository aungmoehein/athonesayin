//
//  HomeModule.swift
//  Home
//
//  Created by Aung Moe Hein on 21/05/2020.
//
import Foundation
import UIKit
import XLPagerTabStrip

// MARK: - router

protocol ATSYCAST_HomeRouterPresenterInterface: RouterPresenterInterface {
    func goToVideoPlayer(videoWrapper: VideoWrapper)
}

// MARK: - presenter

protocol ATSYCAST_HomePresenterRouterInterface: PresenterRouterInterface {

}

protocol ATSYCAST_HomePresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchLiveChannels(liveChannels: [Live])
    func onFetchError()
}

protocol ATSYCAST_HomePresenterViewInterface: PresenterViewInterface {
    func start(sheetNo: String)
    func fetchFavorites()
    func videoPlayer(videoWrapper: VideoWrapper)
    func catchAnalytics(actionType: String, deviceID: String, deviceName: String)
}

// MARK: - interactor

protocol ATSYCAST_HomeInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchLiveChannels(sheetNo: String)
    func fetchFavorite()
    func sendAnalytics(actionType: String, deviceID: String, deviceName: String)
}

// MARK: - view

protocol ATSYCAST_HomeViewPresenterInterface: ViewPresenterInterface {
    func displayLiveChannels(liveChannels: [Live])
    func onError()
}

// MARK: - module builder

final class ATSYCAST_HomeModule: ModuleInterface {

    typealias View = ATSYCAST_HomeView
    typealias Presenter = ATSYCAST_HomePresenter
    typealias Router = ATSYCAST_HomeRouter
    typealias Interactor = ATSYCAST_HomeInteractor

    func build(liveKey: LiveKey) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "atsycast_home") as! View
        
        let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
        let playBackRepository = PlaybackRepository(context: context)
        let interactor = Interactor(playBackRepository: playBackRepository)
        let presenter = Presenter()
        let router = Router()
        
        view.itemInfo = IndicatorInfo(title: liveKey.name)
        view.sheetNo = liveKey.number

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
