//
//  HomePresenter.swift
//  Home
//
//  Created by Aung Moe Hein on 21/05/2020.
//

import Foundation

final class ATSYCAST_HomePresenter: PresenterInterface {

    var router: ATSYCAST_HomeRouterPresenterInterface!
    var interactor: ATSYCAST_HomeInteractorPresenterInterface!
    weak var view: ATSYCAST_HomeViewPresenterInterface!

}

extension ATSYCAST_HomePresenter: ATSYCAST_HomePresenterRouterInterface {

}

extension ATSYCAST_HomePresenter: ATSYCAST_HomePresenterInteractorInterface {
    func onFetchLiveChannels(liveChannels: [Live]) {
        self.view.displayLiveChannels(liveChannels: liveChannels)
    }
    
    func onFetchError() {
        self.view.onError()
    }
}

extension ATSYCAST_HomePresenter: ATSYCAST_HomePresenterViewInterface {

    func start(sheetNo: String) {
        self.interactor.fetchLiveChannels(sheetNo: sheetNo)
    }
    
    func fetchFavorites() {
        self.interactor.fetchFavorite()
    }
    
    func videoPlayer(videoWrapper: VideoWrapper) {
        self.router.goToVideoPlayer(videoWrapper: videoWrapper)
    }

    func catchAnalytics(actionType: String, deviceID: String, deviceName: String) {
        self.interactor.sendAnalytics(actionType: actionType, deviceID: deviceID, deviceName: deviceName)
    }
}
