//
//  AnimeSearchRouter.swift
//  AnimeSearch
//
//  Created by Aung Moe Hein on 04/06/2020.
//

import Foundation
import UIKit

final class AnimeSearchRouter: RouterInterface {

    weak var presenter: AnimeSearchPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension AnimeSearchRouter: AnimeSearchRouterPresenterInterface {
    func goToDetail(aniEpisodeList: AniEpisodeList) {
        let vc = AnimeDetailModule().build() as! AnimeDetailView
        vc.animeData = aniEpisodeList.anime
        vc.animeEpisodes = aniEpisodeList.data!
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
}
