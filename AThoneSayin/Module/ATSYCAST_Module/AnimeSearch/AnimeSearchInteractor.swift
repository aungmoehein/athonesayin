//
//  AnimeSearchInteractor.swift
//  AnimeSearch
//
//  Created by Aung Moe Hein on 04/06/2020.
//

import Foundation

final class AnimeSearchInteractor: InteractorInterface {

    weak var presenter: AnimeSearchPresenterInteractorInterface!
}

extension AnimeSearchInteractor: AnimeSearchInteractorPresenterInterface {
    func fetchAnimeEpisodes(id: Int) {
        Animeflix.sharedInstance.getAnimeEpisodes(id: id, page: 1, sort: "DESC").load()
            .onSuccess() {data in
            if let aniEpisodeList: AniEpisodeList = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchAnimeEpisodeList(aniEpisodeList: aniEpisodeList)
                }
            }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
    
    func fetchSearchResult(text: String) {
        Animeflix.sharedInstance.getSearchResult(text: text).load()
            .onSuccess() {data in
            if let aniList: AniList = data.typedContent() {
                if self.presenter != nil {
                    self.presenter.onFetchSearchResult(aniDatas: aniList.data!)
                }
            }
        }.onFailure() {error in
            self.presenter.onFetchError()
        }
    }
}
