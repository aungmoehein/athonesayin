//
//  AnimeSearchModule.swift
//  AnimeSearch
//
//  Created by Aung Moe Hein on 04/06/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol AnimeSearchRouterPresenterInterface: RouterPresenterInterface {
    func goToDetail(aniEpisodeList: AniEpisodeList)
}

// MARK: - presenter

protocol AnimeSearchPresenterRouterInterface: PresenterRouterInterface {

}

protocol AnimeSearchPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchSearchResult(aniDatas: [AniData])
    func onFetchAnimeEpisodeList(aniEpisodeList: AniEpisodeList)
    func onFetchError()
}

protocol AnimeSearchPresenterViewInterface: PresenterViewInterface {
    func start()
    func getSearchResult(text: String)
    func getAnimeEpisodeList(id: Int)
    func goToDetail(aniEpisodeList: AniEpisodeList)
}

// MARK: - interactor

protocol AnimeSearchInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchSearchResult(text: String)
    func fetchAnimeEpisodes(id: Int)
}

// MARK: - view

protocol AnimeSearchViewPresenterInterface: ViewPresenterInterface {
    func displayAnimes(aniDatas: [AniData])
    func displayAnimeEpisodeList(aniEpisodeList: AniEpisodeList)
    func displayError()
}


// MARK: - module builder

final class AnimeSearchModule: ModuleInterface {

    typealias View = AnimeSearchView
    typealias Presenter = AnimeSearchPresenter
    typealias Router = AnimeSearchRouter
    typealias Interactor = AnimeSearchInteractor

    func build() -> UIViewController {
        let view = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "anime_search") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
