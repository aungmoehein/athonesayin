//
//  AnimeSearchView.swift
//  AnimeSearch
//
//  Created by Aung Moe Hein on 04/06/2020.
//

import Foundation
import UIKit
import PopupDialog
import NVActivityIndicatorView

final class AnimeSearchView: UIViewController, ViewInterface, UISearchResultsUpdating, UISearchBarDelegate, NVActivityIndicatorViewable {
    @IBOutlet weak var animeTV: UITableView!
    
    fileprivate var showMsgDialog: PopupDialog!
    var presenter: AnimeSearchPresenterViewInterface!
    var loadingView: UIView!
    let searchController = UISearchController(searchResultsController: nil)
    
    var aniDatas: [AniData] = [] {
        didSet {
            self.removeLoading(loadingView: loadingView)
            animeTV.reloadData()
        }
    }
    
    var aniEpisodeList: AniEpisodeList! {
        didSet {
            self.presenter.goToDetail(aniEpisodeList: self.aniEpisodeList)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NVActivityIndicatorView.DEFAULT_TYPE = .ballClipRotateMultiple
        
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        self.definesPresentationContext = true
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.tintColor = UIColor(hex: 0x459a0b)
        searchController.searchBar.barTintColor = UIColor(hex: 0x111111)
        let textFieldInsideSearchBar = searchController.searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = .white
        
        animeTV.tableHeaderView = searchController.searchBar
        animeTV.register(UINib(nibName: "AnimeSearchCell", bundle: nil), forCellReuseIdentifier: "anime_search_cell")
        animeTV.delegate = self
        animeTV.dataSource = self
        animeTV.reloadData()

        self.presenter.start()
    }
    
    @objc func canRotate() -> Void {}
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func showLoading(onView : UIView) -> UIView {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        if #available(iOS 13.0, *) {
            let activityIndicator = UIActivityIndicatorView.init(style: UIActivityIndicatorView.Style.medium)
            
            activityIndicator.color = .white
            activityIndicator.startAnimating()
            activityIndicator.center = spinnerView.center
            
            DispatchQueue.main.async {
                spinnerView.addSubview(activityIndicator)
                onView.addSubview(spinnerView)
            }
        } else {
            // Fallback on earlier versions
        }
        
        return spinnerView
    }
    
    func removeLoading(loadingView: UIView) {
        DispatchQueue.main.async {
            loadingView.removeFromSuperview()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if searchController.searchBar.text != "" {
            loadingView = self.showLoading(onView: animeTV)
            self.presenter.getSearchResult(text: searchBar.text!)
            searchController.dismiss(animated: true, completion: nil)
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        searchBar.resignFirstResponder()
    }

    func updateSearchResults(for searchController: UISearchController) {
        
    }
}

extension AnimeSearchView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aniDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = animeTV.dequeueReusableCell(withIdentifier: "anime_search_cell", for: indexPath) as! AnimeSearchCell
        cell.selectionStyle = .none
        cell.displayImg = aniDatas[indexPath.row].cover_photo!
        cell.displayTitle = aniDatas[indexPath.row].title!
        cell.displayYear = aniDatas[indexPath.row].year!
        cell.displayType = aniDatas[indexPath.row].type!
        cell.displayStatus = aniDatas[indexPath.row].status!
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.startAnimating()
        self.presenter.getAnimeEpisodeList(id: aniDatas[indexPath.row].id!)
    }
}

extension AnimeSearchView: AnimeSearchViewPresenterInterface {
    func displayAnimeEpisodeList(aniEpisodeList: AniEpisodeList) {
        self.aniEpisodeList = aniEpisodeList
    }
    
    func displayAnimes(aniDatas: [AniData]) {
        self.stopAnimating()
        self.aniDatas = aniDatas
    }
    
    func displayError() {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = "Something went wrong!"
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonRetry = DefaultButton(title: NSLocalizedString("Ok", comment: "")) {
            
        }
        
        showMsgDialog.addButtons([buttonRetry])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        self.present(showMsgDialog, animated: true, completion: nil)
    }
}
