//
//  AnimeSearchPresenter.swift
//  AnimeSearch
//
//  Created by Aung Moe Hein on 04/06/2020.
//

import Foundation

final class AnimeSearchPresenter: PresenterInterface {

    var router: AnimeSearchRouterPresenterInterface!
    var interactor: AnimeSearchInteractorPresenterInterface!
    weak var view: AnimeSearchViewPresenterInterface!

}

extension AnimeSearchPresenter: AnimeSearchPresenterRouterInterface {

}

extension AnimeSearchPresenter: AnimeSearchPresenterInteractorInterface {
    func onFetchAnimeEpisodeList(aniEpisodeList: AniEpisodeList) {
        self.view.displayAnimeEpisodeList(aniEpisodeList: aniEpisodeList)
    }
    
    func onFetchSearchResult(aniDatas: [AniData]) {
        self.view.displayAnimes(aniDatas: aniDatas)
    }
    
    func onFetchError() {
        self.view.displayError()
    }
}

extension AnimeSearchPresenter: AnimeSearchPresenterViewInterface {
    func getAnimeEpisodeList(id: Int) {
        self.interactor.fetchAnimeEpisodes(id: id)
    }
    
    func goToDetail(aniEpisodeList: AniEpisodeList) {
        self.router.goToDetail(aniEpisodeList: aniEpisodeList)
    }
    

    func start() {

    }

    func getSearchResult(text: String) {
        self.interactor.fetchSearchResult(text: text)
    }
}
