//
//  BalancePresenter.swift
//  Balance
//
//  Created by Aung Moe Hein on 07/05/2020.
//

import Foundation

final class BalancePresenter: PresenterInterface {

    var router: BalanceRouterPresenterInterface!
    var interactor: BalanceInteractorPresenterInterface!
    weak var view: BalanceViewPresenterInterface!

}

extension BalancePresenter: BalancePresenterRouterInterface {

}

extension BalancePresenter: BalancePresenterInteractorInterface {
    func onFetchUsagesByDate(usages: [Usage]) {
        self.view.displayUsagesByDate(usages: usages)
    }
    
    func onFetchUsagesBetweenDates(usages: [Usage]) {
        self.view.displayUsagesBetweenDates(usages: usages)
    }
    
    func onFetchCategoryById(category: Category) {
        self.view.displayCategoryById(category: category)
    }
}

extension BalancePresenter: BalancePresenterViewInterface {

    func start() {

    }
    
    func getCategoryById(id: Int) {
        interactor.fetchCategoryById(id: id)
    }
    
    func getUsagesByDate(date:String) {
        self.interactor.fetchUsagesByDate(date: date)
    }

    func getUsagesBetweenDates(from: String, to: String) {
        self.interactor.fetchUsagesBetweenDates(from: from, to: to)
    }
}
