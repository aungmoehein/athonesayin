//
//  ReportPagerStripViewController.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 18/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import UIKit
import PopupDialog
import Charts
import XLPagerTabStrip
import Firebase

class ReportPagerStripViewController: ButtonBarPagerTabStripViewController, CalendarRangeViewDelegate, ChartViewDelegate, BalanceViewDelegate {
    @IBOutlet weak var btnFilter: UIButton!
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var OverLayViewHeight: NSLayoutConstraint!
    
    var isReload = false
    
    var db: DBHelper!
    var pages: [UIViewController] = []
    var dateFormatter = DateFormatter()
    
    var calendarDialog: PopupDialog!
    var fromDate: String = ""
    var toDate: String = ""
    
    var days: [String] = []
    var incomes: [Float] = []
    var expenses: [Float] = []
    
    let OverLayViewMaxHeight: CGFloat = 200
    let OverLayViewMinHeight: CGFloat = 0

    override func viewDidLoad() {
        tabbarSetting()
        super.viewDidLoad()
        
        self.containerView.delegate = self
        
        barChart()
        let image = UIImage(named: "ic_filter")?.withRenderingMode(.alwaysTemplate)
        btnFilter.setImage(image, for: .normal)
        btnFilter.tintColor = .white
        btnFilter.makeRounded()

        buttonBarView.selectedBar.backgroundColor = UIColor(hex: Color.colorPrimary.rawValue)
        buttonBarView.backgroundColor = UIColor.clear
        
        Analytics.logEvent(Events.REPORT_SCREEN, parameters: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        done()
    }
    
    func barChart() {
        barChartView.delegate = self
         barChartView.noDataText = "You need to provide data for the chart."

         //legend
         let legend = barChartView.legend
         legend.enabled = true
         legend.horizontalAlignment = .right
         legend.verticalAlignment = .top
         legend.orientation = .vertical
         legend.drawInside = true
         legend.yOffset = 10.0;
         legend.xOffset = 10.0;
         legend.yEntrySpace = 0.0;


         let xaxis = barChartView.xAxis
         xaxis.drawGridLinesEnabled = true
         xaxis.labelPosition = .bottom
         xaxis.centerAxisLabelsEnabled = true
         xaxis.valueFormatter = IndexAxisValueFormatter(values:self.days)
         xaxis.granularity = 1
        if #available(iOS 13.0, *) {
            xaxis.labelTextColor = .label
        } else {
            xaxis.labelTextColor = .black
        }

         let leftAxisFormatter = NumberFormatter()
         leftAxisFormatter.maximumFractionDigits = 1

         let yaxis = barChartView.leftAxis
         yaxis.spaceTop = 0.35
         yaxis.axisMinimum = 0
         yaxis.drawGridLinesEnabled = false
        if #available(iOS 13.0, *) {
            yaxis.labelTextColor = .label
        } else {
            yaxis.labelTextColor = .black
        }

         barChartView.rightAxis.enabled = false

         setChart()
    }
    
    func setChart() {
        barChartView.noDataText = "You need to provide data for the chart."
        var dataEntries: [BarChartDataEntry] = []
        var dataEntries1: [BarChartDataEntry] = []

        for i in 0..<self.days.count {
            let dataEntry = BarChartDataEntry(x: Double(i) , y: Double(self.incomes[i]))
            dataEntries.append(dataEntry)

            let dataEntry1 = BarChartDataEntry(x: Double(i) , y: Double(self.self.expenses[i]))
            dataEntries1.append(dataEntry1)
        }

        let chartDataSet = BarChartDataSet(entries: dataEntries, label: LocalizationSystem.Localizer.localizedStringForKey(key: "income", comment: ""))
        chartDataSet.valueTextColor = .clear
        let chartDataSet1 = BarChartDataSet(entries: dataEntries1, label: LocalizationSystem.Localizer.localizedStringForKey(key: "expense", comment: ""))
        chartDataSet1.valueTextColor = .clear

        let dataSets: [BarChartDataSet] = [chartDataSet,chartDataSet1]
        chartDataSet.colors = [UIColor(hex: Color.colorPrimary.rawValue)]
        chartDataSet1.colors = [.red]

        let chartData = BarChartData(dataSets: dataSets)

        let groupSpace = 0.3
        let barSpace = 0.05
        let barWidth = 0.3

        let groupCount = self.days.count
        let startYear = 0


        chartData.barWidth = barWidth;
        barChartView.xAxis.axisMinimum = Double(startYear)
        let gg = chartData.groupWidth(groupSpace: groupSpace, barSpace: barSpace)
        print("Groupspace: \(gg)")
        barChartView.xAxis.axisMaximum = Double(startYear) + gg * Double(groupCount)

        chartData.groupBars(fromX: Double(startYear), groupSpace: groupSpace, barSpace: barSpace)
        barChartView.notifyDataSetChanged()

        barChartView.data = chartData

        //background color
        barChartView.backgroundColor = .clear

        //chart animation
        barChartView.animate(xAxisDuration: 1.5, yAxisDuration: 1.5, easingOption: .linear)
    }
    
    public func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight){
        var marker: BalloonMarker!
        if #available(iOS 13.0, *) {
            marker = BalloonMarker(color: UIColor.gray, font: UIFont(name: "Helvetica", size: 12)!, textColor: UIColor.label, insets: UIEdgeInsets(top: 7.0, left: 7.0, bottom: 7.0, right: 7.0))
        } else {
            marker = BalloonMarker(color: UIColor.gray, font: UIFont(name: "Helvetica", size: 12)!, textColor: UIColor.black, insets: UIEdgeInsets(top: 7.0, left: 7.0, bottom: 7.0, right: 7.0))
        }
        marker.minimumSize = CGSize(width: 75.0, height: 35.0)
        chartView.marker = marker
    }
    
    func done() {
        pages.removeAll()
        days.removeAll()
        expenses.removeAll()
        incomes.removeAll()
        reloadPagerTabStripView()
        updateIndicator(for: self, fromIndex: 0, toIndex: 0, withProgressPercentage: 0, indexWasChanged: false)
        barChart()
    }
    
    func tabbarSetting() {
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarHeight = 4.0
        settings.style.buttonBarMinimumLineSpacing = 0
        if #available(iOS 13.0, *) {
            settings.style.buttonBarItemTitleColor = .label
        } else {
            settings.style.buttonBarItemTitleColor = .black
        }
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = self.view.frame.size.width / 3
        settings.style.buttonBarRightContentInset = self.view.frame.size.width / 3
        settings.style.buttonBarMinimumInteritemSpacing = self.view.frame.size.width / 4
    }
    
    @IBAction func btnFilter(_ sender: Any) {
        let popUp = CalendarRangeView(nibName: "CalendarRangeView", bundle: nil)
        popUp.vc = self
        popUp.delegate = self
        calendarDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        calendarDialog.viewController = self
        self.present(calendarDialog, animated: true, completion: nil)
        Analytics.logEvent(Events.BTN_REPORT_FILTER, parameters: nil)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM, yyyy"
        
        dateFormatter.locale = Locale.init(identifier: "us")
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "dd MMM, yyyy"
        
        if self.fromDate == "" || self.toDate == "" {
            fromDate = dateFormatter.string(from: Date())
            toDate = dateFormatter.string(from: Date())
        }
        
        let dbFromDate = self.fromDate.dbDate(withFormat: "dd MMM, yyyy")!.todbString()
        let dbToDate = self.toDate.dbDate(withFormat: "dd MMM, yyyy")!.todbString()
        
        let rangeDates = db.readShortTimeBetweenDates(from: dbFromDate, to: dbToDate)
        
        let fromDate_usage = self.fromDate.dbDate(withFormat: "dd MMM, yyyy")!.todbString()
        let toDate_usage = self.toDate.dbDate(withFormat: "dd MMM, yyyy")!.todbString()
        let firstPage = BalanceModule().build(db: db, title: LocalizationSystem.Localizer.localizedStringForKey(key: "total_date", comment: ""), fromDate: fromDate_usage, toDate: toDate_usage) as! BalanceView
        firstPage.delegate = self
        pages.append(firstPage)
        
        rangeDates.forEach() {
            rangeDate in
            days.append(String(rangeDate.split(separator: ",")[0]))
            
            let (expense, income) = db.readUsageAmountsByDate(date: rangeDate)
            expenses.append(expense)
            incomes.append(income)
            
            let balanceView = BalanceModule().build(db: db, title: rangeDate, fromDate: self.fromDate, toDate: self.toDate) as! BalanceView
            balanceView.delegate = self
            
            pages.append(balanceView)
        }
        
        guard isReload else {
            return pages
        }
        
        return pages
    }
    
    override func reloadPagerTabStripView() {
        isReload = true
        if arc4random() % 2 == 0 {
            pagerBehaviour = .progressive(skipIntermediateViewControllers: arc4random() % 2 == 0, elasticIndicatorLimit: arc4random() % 2 == 0 )
        } else {
            pagerBehaviour = .common(skipIntermediateViewControllers: arc4random() % 2 == 0)
        }
        super.reloadPagerTabStripView()
    }
        
    func tableViewScrolled(scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = OverLayViewHeight.constant - y

        if newHeaderViewHeight > OverLayViewMaxHeight {
            self.barChartView.alpha = 1
            OverLayViewHeight.constant = OverLayViewMaxHeight
        } else if newHeaderViewHeight < OverLayViewMinHeight {
            self.barChartView.alpha = 0
            OverLayViewHeight.constant = OverLayViewMinHeight
        } else {
            self.barChartView.alpha = newHeaderViewHeight / 170
            OverLayViewHeight.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
        }
    }
}
