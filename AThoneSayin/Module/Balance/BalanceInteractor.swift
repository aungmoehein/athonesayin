//
//  BalanceInteractor.swift
//  Balance
//
//  Created by Aung Moe Hein on 07/05/2020.
//

import Foundation

final class BalanceInteractor: InteractorInterface {

    weak var presenter: BalancePresenterInteractorInterface!
    
    var db: DBHelper
    init(db: DBHelper) {
        self.db = db
    }
}

extension BalanceInteractor: BalanceInteractorPresenterInterface {
    func fetchUsagesByDate(date: String) {
        self.presenter.onFetchUsagesByDate(usages: db.readUsagesByDate(date: date))
    }
    
    func fetchUsagesBetweenDates(from: String, to: String) {
        self.presenter.onFetchUsagesBetweenDates(usages: db.readUsagesBetweenDates(fromDate: from, endDate: to))
    }
    
    func fetchCategoryById(id: Int) {
        self.presenter.onFetchCategoryById(category: db.readCategoryById(id: id))
    }
}
