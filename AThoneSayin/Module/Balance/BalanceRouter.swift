//
//  BalanceRouter.swift
//  Balance
//
//  Created by Aung Moe Hein on 07/05/2020.
//

import Foundation
import UIKit

final class BalanceRouter: RouterInterface {

    weak var presenter: BalancePresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension BalanceRouter: BalanceRouterPresenterInterface {

}