//
//  BalanceView.swift
//  Balance
//
//  Created by Aung Moe Hein on 07/05/2020.
//

import Foundation
import UIKit
import Lottie
import XLPagerTabStrip

protocol BalanceViewDelegate {
    func tableViewScrolled(scrollView: UIScrollView)
}

final class BalanceView: UIViewController, ViewInterface, IndicatorInfoProvider {
    @IBOutlet weak var reportTableView: UITableView!
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var lbNoUsage: UILabel!
    @IBOutlet weak var emptyView: UIView!
    
    var delegate: BalanceViewDelegate!
    var localizer = LocalizationSystem.Localizer
    var presenter: BalancePresenterViewInterface!
    var itemInfo = IndicatorInfo(title: "")
    
    var fromDate: String = ""
    var toDate: String = ""
    var totalAmount: Float = 0
    
    var usages: [Usage] = [] {
        didSet {
            self.reportTableView.isHidden = self.usages.count == 0
            self.emptyView.isHidden = self.usages.count != 0
            usages.forEach() {
                usage in
                if usage.usage_status == 0 {
                    totalAmount -= usage.usage_amount
                }else {
                    totalAmount += usage.usage_amount
                }
            }
            
            reportTableView.reloadData()
        }
    }
    
    var category: Category!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.reportTableView.isHidden = self.usages.count == 0
        self.emptyView.isHidden = self.usages.count != 0
        reportTableView.register(UINib(nibName: "ReportCell", bundle: nil), forCellReuseIdentifier: "report_cell")
        reportTableView.register(UINib(nibName: "ReportTotalCell", bundle: nil), forCellReuseIdentifier: "report_total_cell")
        reportTableView.dataSource = self
        reportTableView.delegate = self
        reportTableView.reloadData()

        self.animationView.playAnimation(filename: "animation_not_found")
        self.presenter.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        totalAmount = 0
        if itemInfo.title == localizer.localizedStringForKey(key: "total_date", comment: "") {
            self.lbNoUsage.text = localizer.localizedStringForKey(key: "no_usage_total", comment: "")
            self.presenter.getUsagesBetweenDates(from: fromDate, to: toDate)
        }else {
            self.lbNoUsage.text = String(format: localizer.localizedStringForKey(key: "no_usage_balance", comment: ""), itemInfo.title!) 
            self.presenter.getUsagesByDate(date: itemInfo.title!)
        }
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}

extension BalanceView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usages.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row != usages.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "report_cell", for: indexPath) as! ReportCell
            
            presenter.getCategoryById(id: usages[indexPath.row].category_id)
            cell.icon_name = category.category_icon
            cell.title = localizer.localizedStringForKey(key: category.category_name, comment: "")
            cell.lbAmount.textColor = usages[indexPath.row].usage_status == 0 ? .red : UIColor(hex: Color.colorPrimary.rawValue)
            cell.amount = usages[indexPath.row].usage_status == 0 ? "- " : "+ "
            cell.amount += String(format: localizer.localizedStringForKey(key: "ks", comment: ""), Int(usages[indexPath.row].usage_amount).formattedWithSeparator)
            cell.date = usages[indexPath.row].short_time
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "report_total_cell", for: indexPath) as! ReportTotalCell
            
            cell.lbAmount.textColor = totalAmount < 0 ? .red : UIColor(hex: Color.colorPrimary.rawValue)
            let localizedAmount = String(format: localizer.localizedStringForKey(key: "ks", comment: ""), totalAmount.formattedWithSeparator)
            cell.total = totalAmount > 0 ? "+" + localizedAmount : localizedAmount
            
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate.tableViewScrolled(scrollView: scrollView)
    }
}

extension BalanceView: BalanceViewPresenterInterface {
    func displayUsagesByDate(usages: [Usage]) {
        self.usages = usages
    }
    
    func displayUsagesBetweenDates(usages: [Usage]) {
        self.usages = usages
    }
    
    func displayCategoryById(category: Category) {
        self.category = category
    }
}
