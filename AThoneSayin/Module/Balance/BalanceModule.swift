//
//  BalanceModule.swift
//  Balance
//
//  Created by Aung Moe Hein on 07/05/2020.
//
import Foundation
import UIKit
import XLPagerTabStrip

// MARK: - router

protocol BalanceRouterPresenterInterface: RouterPresenterInterface {

}

// MARK: - presenter

protocol BalancePresenterRouterInterface: PresenterRouterInterface {

}

protocol BalancePresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchUsagesByDate(usages: [Usage])
    func onFetchUsagesBetweenDates(usages: [Usage])
    func onFetchCategoryById(category: Category)
}

protocol BalancePresenterViewInterface: PresenterViewInterface {
    func start()
    func getCategoryById(id: Int)
    func getUsagesByDate(date:String)
    func getUsagesBetweenDates(from: String, to: String)
}

// MARK: - interactor

protocol BalanceInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchCategoryById(id: Int)
    func fetchUsagesByDate(date: String)
    func fetchUsagesBetweenDates(from: String, to: String)
}

// MARK: - view

protocol BalanceViewPresenterInterface: ViewPresenterInterface {
    func displayCategoryById(category: Category)
    func displayUsagesByDate(usages: [Usage])
    func displayUsagesBetweenDates(usages: [Usage])
}


// MARK: - module builder

final class BalanceModule: ModuleInterface {

    typealias View = BalanceView
    typealias Presenter = BalancePresenter
    typealias Router = BalanceRouter
    typealias Interactor = BalanceInteractor

    func build(db: DBHelper, title: String, fromDate: String, toDate: String) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "balance") as! BalanceView
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router()

        view.fromDate = fromDate
        view.toDate = toDate
        view.itemInfo = IndicatorInfo(title: title)
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
