//
//  AddView+Locale.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension AddView {
    func localize() {
        lbOtherday.text = localizer.localizedStringForKey(key: "otherday", comment: "")
        btnShortText.setTitle(localizer.localizedStringForKey(key: "write_note", comment: ""), for: .normal)
        segmentControl.setTitle(localizer.localizedStringForKey(key: "expense", comment: ""), forSegmentAt: 0)
        segmentControl.setTitle(localizer.localizedStringForKey(key: "income", comment: ""), forSegmentAt: 1)
        lbSave.text = self.categoryType == Parameter.EXPENSE ? String(format: localizer.localizedStringForKey(key: "add_daily_use", comment: ""), localizer.localizedStringForKey(key: "expense", comment: "")) : String(format: localizer.localizedStringForKey(key: "add_daily_use", comment: ""), localizer.localizedStringForKey(key: "income", comment: ""))
    }
}
