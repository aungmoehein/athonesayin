//
//  AddRouter.swift
//  Add
//
//  Created by Aung Moe Hein on 10/05/2020.
//

import Foundation
import UIKit

final class AddRouter: RouterInterface {

    weak var presenter: AddPresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension AddRouter: AddRouterPresenterInterface {
    func goToHomeViewForAdded(db: DBHelper) {
        let vc = HomeModule().build(db: db) as! HomeView
        vc.modalPresentationStyle = .fullScreen
        viewController?.present(vc, animated: true, completion: nil)
    }
}
