//
//  AddView.swift
//  Add
//
//  Created by Aung Moe Hein on 10/05/2020.
//

import Foundation
import UIKit
import PopupDialog
import FloatingPanel
import Firebase

final class AddView: UIViewController, ViewInterface, UINavigationControllerDelegate {
    @IBOutlet weak var segmentControl: UISegmentedControl!
    @IBOutlet weak var segmentedControlOuterView: UIView!
    @IBOutlet weak var AmountText: UITextField!
    @IBOutlet weak var lbToday: UILabel!
    @IBOutlet weak var lbOtherday: UILabel!
    @IBOutlet weak var btnShortText: UIButton!
    @IBOutlet weak var btnCategoryIcon: UIButton!
    @IBOutlet weak var lbCategory: UILabel!
    @IBOutlet weak var dateText: UITextField!
    @IBOutlet weak var disableTextField: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var saveSwitch: UISwitch!
    @IBOutlet weak var lbSave: UILabel!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var btnEdit: UIButton!
    @IBOutlet weak var btnImgEdit: UIButton!
    
    var db: DBHelper!
    
    var presenter: AddPresenterViewInterface!
    var datePicker = UIDatePicker()
    var noteDialog: PopupDialog!
    let floatingPanel = FloatingPanelController()
    
    let localizer = LocalizationSystem.Localizer
    let nowDate = Date()
    let dateformatter = DateFormatter()
    let popUp = NoteDialog(nibName: "NoteDialog", bundle: nil)
    
    // For Update
    var usageID: Int!
    var updateCat: Category!
    var cat_id_update: Int = 0
    var updatedDate: String = ""
    var isUpdate: Bool = false
    var saveUpdate: Bool = false
    
    var categoryType: Int = 0
    var cat_id: Int = 0
    var cat_name: String = ""
    var icon_name: String = ""
    var amount: String = ""
    var createdDate: String = "ယနေ့"
    var shortNote: String = ""
    
    var selectedSegment: Int = Parameter.EXPENSE {
        didSet{
            self.categoryType = self.selectedSegment
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isUpdate {
            cat_id_update = self.cat_id
        }
        
        NavBarUI()
        
        let imgCheck = UIImage(named: "ic_check")?.withRenderingMode(.alwaysTemplate)
        btnSave.setImage(imgCheck, for: .normal)
        
        var image = UIImage(named: "ic_calendar")?.withRenderingMode(.alwaysTemplate)
        btnCalendar.setImage(image, for: .normal)
        btnCalendar.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        
        image = UIImage(named: "ic_pen_box")?.withRenderingMode(.alwaysTemplate)
        btnImgEdit.setImage(image, for: .normal)
        btnImgEdit.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        
        image = UIImage(named: "ic_arrow_back")?.withRenderingMode(.alwaysTemplate)
        btnBack.setImage(image, for: .normal)
        btnBack.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        
        segmentedControlUI()
        categoryIconUI(icon_name: self.icon_name)
        amountKeyboardUI()
        dateUI()
        
        lbCategory.text = localizer.localizedStringForKey(key: self.cat_name, comment: "")
        categoryIconUI(icon_name: self.icon_name)
        
        // Add and show the views managed by the `FloatingPanelController` object to self.view.
        floatingPanel.addPanel(toParent: self)
        
        if isUpdate {
            presenter.getCategoryById(id: cat_id_update)
            lbToday.text = updatedDate == dateformatter.string(from: nowDate) ? localizer.localizedStringForKey(key: "today", comment: "") : updatedDate
            lbCategory.text = localizer.localizedStringForKey(key: updateCat.category_name, comment: "")
            categoryIconUI(icon_name: updateCat.category_icon)
            saveUpdate = true
        }
        
        self.presenter.start()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        localize()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.navigationController!.viewControllers.count > 1
    }
    
    func NavBarUI() {
        self.navigationController?.navigationBar.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
        navigationItem.titleView = segmentedControlOuterView
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: btnSave)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: btnBack)
    }
    
    func segmentedControlUI() {
        self.segmentedControlOuterView.layer.cornerRadius = 18
        self.segmentedControlOuterView.layer.borderColor = UIColor(hex: Color.colorPrimary.rawValue).cgColor
        self.segmentedControlOuterView.layer.borderWidth = 1.0
        self.segmentedControlOuterView.layer.masksToBounds = true
        self.segmentControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], for: UIControl.State.selected)
        loadChooseCategoryView(type: self.categoryType)
        segmentControl.selectedSegmentIndex = self.categoryType
    }
    
    func categoryIconUI(icon_name: String) {
        let image = UIImage(named: icon_name)?.withRenderingMode(.alwaysTemplate)
        btnCategoryIcon.setImage(image, for: .normal)
        btnCategoryIcon.makeRounded()
        btnCategoryIcon.contentMode = .center
        btnCategoryIcon.backgroundColor = UIColor(hex: Color.colorPrimary.rawValue)
        btnCategoryIcon.tintColor = UIColor.white
    }
    
    func amountKeyboardUI() {
        let keyboardView = AmountKeyboard(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 300))
        keyboardView.delegate = self
        keyboardView.backgroundColor = UIColor.white
        AmountText.text = amount
        AmountText.delegate = self
        AmountText.inputView = keyboardView
        AmountText.selectedTextRange = AmountText.textRange(from: AmountText.endOfDocument, to: AmountText.endOfDocument)
        disableTextField.isHidden = true
    }
    
    func dateUI() {
        lbToday.text = localizer.localizedStringForKey(key: "today", comment: "")
        
        dateformatter.locale = Locale.init(identifier: "us")
        dateformatter.timeZone = .current
        dateformatter.dateStyle = .medium
        
        let selectedDate = dateformatter.date(from: createdDate) ?? nowDate
        self.createdDate = dateformatter.string(from: nowDate)
        self.dateText.setInputViewDatePicker(target: self, selectedDate: selectedDate, done: #selector(tapDatePickDone), cancel: #selector(tapDatePickCancel))
    }
    
    @objc func tapDatePickDone() {
        if let datePicker = self.dateText.inputView as? UIDatePicker {
            let selectedDate = dateformatter.string(from: datePicker.date)
            self.lbToday.text = selectedDate
            self.createdDate = selectedDate
            
            if createdDate == dateformatter.string(from: nowDate) {
                lbToday.text = localizer.localizedStringForKey(key: "today", comment: "")
            }
            
            if isUpdate {
                self.updatedDate = selectedDate
                lbToday.text = updatedDate == dateformatter.string(from: nowDate) ? localizer.localizedStringForKey(key: "today", comment: "") : updatedDate
            }
        }
        self.dateText.resignFirstResponder()
        self.dateText.text = ""
        self.disableTextField.isHidden = true
    }
    
    @objc func tapDatePickCancel() {
        self.dateText.resignFirstResponder()
        self.disableTextField.isHidden = true
    }
    
    @IBAction func segmentedControl(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex {
        case Parameter.EXPENSE:
            self.categoryType = Parameter.EXPENSE
            lbSave.text = String(format: localizer.localizedStringForKey(key: "add_daily_use", comment: ""), localizer.localizedStringForKey(key: "expense", comment: ""))
        default:
            self.categoryType = Parameter.INCOME
            lbSave.text = String(format: localizer.localizedStringForKey(key: "add_daily_use", comment: ""), localizer.localizedStringForKey(key: "income", comment: ""))
        }
        
        loadChooseCategoryView(type: self.categoryType)
    }
    
    func loadChooseCategoryView(type: Int) {
        let chooseCategoryVC = ChooseCategoryModule().build(db: self.db,categoryType: type, vc: self) as! ChooseCategoryView
        floatingPanel.delegate = self
        floatingPanel.set(contentViewController: chooseCategoryVC)
        floatingPanel.track(scrollView: chooseCategoryVC.categoryCollectionView)
    }
    
    @IBAction func btnChooseCategory(_ sender: Any) {
        self.disableTextField.isHidden = true
        AmountText.endEditing(true)
        floatingPanel.move(to: .half, animated: true)
    }
    
    @IBAction func btnShortText(_ sender: Any) {
        showNoteDialog()
    }
    
    @IBAction func saveSwitch(_ sender: Any) {
    }
    
    private func showNoteDialog() {
        popUp.note = shortNote
        popUp.addView = self
        noteDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let okButton = DefaultButton(title: "OK") {
            self.shortNote = self.popUp.textView.text
        }
        noteDialog.addButtons([okButton])
        self.present(noteDialog, animated: true, completion: nil)
    }
    
    @IBAction func btnSave(_ sender: Any) {
        onSave(update: saveUpdate)
    }
    
    func onSave(update: Bool) {
        var date = update ? updatedDate : createdDate
        saveUpdate = false
        let selectedYear = date.split(separator: ",")[1]
        let selectedMonth = date.split(separator: ",")[0].split(separator: " ")[0]
        let selectedDay = date.split(separator: ",")[0].split(separator: " ")[1]
        let short_time = "\(selectedDay) \(selectedMonth),\(selectedYear)"
        let amount = AmountText.text!.replacingOccurrences(of: ",", with: "").floatValue
        
        date = date.dbDate()!.todbString()
        if !update {
            presenter.insertIntoUsageDB(note: shortNote, amount: amount, cid: cat_id, status: self.categoryType, usage_time: date, short_time: String(short_time))
            
            if self.saveSwitch.isOn {
                presenter.insertIntoRecentlyUsedDB(note: shortNote, amount: amount, cid: cat_id, status: self.categoryType)
            }
            
            Analytics.logEvent(self.categoryType == Parameter.EXPENSE ? Events.ADD_EXPENCE : Events.ADD_INCOME, parameters: nil)
        } else {
            updatedDate = updatedDate.dbDate()!.todbString()
            presenter.updateForUsage(note: shortNote, amount: amount, cid: cat_id_update, status: self.categoryType, usage_time: updatedDate, short_time: String(short_time), id: usageID)
            
            Analytics.logEvent(self.categoryType == Parameter.EXPENSE ? Events.UPDATE_EXPENCE : Events.UPDATE_INCOME, parameters: nil)
        }
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
}

extension AddView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension AddView: FloatingPanelControllerDelegate {
    func floatingPanel(_ vc: FloatingPanelController, layoutFor newCollection: UITraitCollection) -> FloatingPanelLayout? {
        return MyFloatingPanelLayout()
    }
}

extension AddView: UITextFieldDelegate, AmountKeyboardDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if isUpdate {
            AmountText.text = ""
        }
        floatingPanel.move(to: .tip, animated: true)
        self.disableTextField.isHidden = false
        self.AmountText.tintColor = UIColor.clear
        let endPosition = textField.endOfDocument
        textField.selectedTextRange = textField.textRange(from: endPosition, to: endPosition)
    }
    
    func keyWasTapped(text character: String) {
        if Int(character) != nil {
            if character == "00" || character == "000" {
                if !(AmountText.text?.isEmpty)!{
                    AmountText.insertText(character)
                }
            }else {
                AmountText.insertText(character)
            }
        }
        
        if character == "C" {
            AmountText.text = ""
        }
        
        if character == "DELETE_CHARACTER" {
            if !(AmountText.text?.isEmpty)! {
                let beforeText = AmountText.text!
                let truncated = beforeText.substring(fromIndex: 0, count: beforeText.count - 1)
                AmountText.text = truncated
            }
        }
        
        if character == "DISMISS_KEYBOARD" {
            self.disableTextField.isHidden = true
            AmountText.endEditing(true)
        }
        
        AmountText.text = AmountText.text!.replacingOccurrences(of: ",", with: "")
        AmountText.text = Int(AmountText.text!)?.formattedWithSeparator
    }
}

class MyFloatingPanelLayout: FloatingPanelLayout {
    public var initialPosition: FloatingPanelPosition {
        return .tip
    }
    
    public var supportedPositions: Set<FloatingPanelPosition> {
        return [.full, .half, .tip]
    }

    public func insetFor(position: FloatingPanelPosition) -> CGFloat? {
        switch position {
            case .full: return 16.0 // A top inset from safe area
            case .half: return 270.0 // A bottom inset from the safe area
            case .tip: return 50.0 // A bottom inset from the safe area
            default: return nil // Or `case .hidden: return nil`
        }
    }
}

extension AddView: AddViewPresenterInterface {
    func displayCategoryById(category: Category) {
        self.updateCat = category
    }
}
