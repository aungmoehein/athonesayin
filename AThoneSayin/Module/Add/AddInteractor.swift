//
//  AddInteractor.swift
//  Add
//
//  Created by Aung Moe Hein on 10/05/2020.
//

import Foundation

final class AddInteractor: InteractorInterface {

    weak var presenter: AddPresenterInteractorInterface!
    var db: DBHelper!
    
    init(db: DBHelper) {
        self.db = db
    }
}

extension AddInteractor: AddInteractorPresenterInterface {
    func insertNewUsage(note: String, amount: Float, cid: Int, status: Int, usage_time: String, short_time: String) {
        db.insertUsage(usage_note: note, usage_amount: amount, category_id: cid, usage_status: status, usage_time: usage_time, short_time: short_time)
    }
    
    func insertNewRecentlyUsed(note: String, amount: Float, cid: Int, status: Int) {
        db.insertRecentlyUsed(usage_note: note, usage_amount: amount, category_id: cid, usage_status: status)
    }
    
    func fetchCategoryById(id: Int) {
        self.presenter.onFetchCategoryById(category: db.readCategoryById(id: id))
    }
    
    func updateUsage(note: String, amount: Float, cid: Int, status: Int, usage_time: String, short_time: String, id: Int){
        db.updateUsage(usage_note: note, usage_amount: amount, category_id: cid, usage_status: status, usage_time: usage_time, short_time: short_time, usage_id: id)
    }
}
