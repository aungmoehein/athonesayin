//
//  AddPresenter.swift
//  Add
//
//  Created by Aung Moe Hein on 10/05/2020.
//

import Foundation

final class AddPresenter: PresenterInterface {

    var router: AddRouterPresenterInterface!
    var interactor: AddInteractorPresenterInterface!
    weak var view: AddViewPresenterInterface!

}

extension AddPresenter: AddPresenterRouterInterface {

}

extension AddPresenter: AddPresenterInteractorInterface {
    func onFetchCategoryById(category: Category) {
        self.view.displayCategoryById(category: category)
    }
}

extension AddPresenter: AddPresenterViewInterface {
    func start() {

    }
    
    func homeView(db: DBHelper) {
        router.goToHomeViewForAdded(db: db)
    }
    
    func insertIntoUsageDB(note: String, amount: Float, cid: Int, status: Int, usage_time: String, short_time: String) {
        interactor.insertNewUsage(note: note, amount: amount, cid: cid, status: status, usage_time: usage_time, short_time: short_time)
    }
    
    func insertIntoRecentlyUsedDB(note: String, amount: Float, cid: Int, status: Int) {
        interactor.insertNewRecentlyUsed(note: note, amount: amount, cid: cid, status: status)
    }
    
    func updateForUsage(note: String, amount: Float, cid: Int, status: Int, usage_time: String, short_time: String, id: Int) {
        interactor.updateUsage(note: note, amount: amount, cid: cid, status: status, usage_time: usage_time, short_time: short_time, id: id)
    }
    
    func getCategoryById(id: Int) {
        interactor.fetchCategoryById(id: id)
    }

}
