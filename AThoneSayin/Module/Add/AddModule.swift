//
//  AddModule.swift
//  Add
//
//  Created by Aung Moe Hein on 10/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol AddRouterPresenterInterface: RouterPresenterInterface {
    func goToHomeViewForAdded(db: DBHelper)
}

// MARK: - presenter

protocol AddPresenterRouterInterface: PresenterRouterInterface {

}

protocol AddPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchCategoryById(category: Category)
}

protocol AddPresenterViewInterface: PresenterViewInterface {
    func start()
    func homeView(db: DBHelper)
    func getCategoryById(id: Int)
    func insertIntoUsageDB(note: String, amount: Float, cid: Int, status: Int, usage_time: String, short_time: String)
    func insertIntoRecentlyUsedDB(note: String, amount: Float, cid: Int, status: Int)
    func updateForUsage(note: String, amount: Float, cid: Int, status: Int, usage_time: String, short_time: String, id: Int)
}

// MARK: - interactor

protocol AddInteractorPresenterInterface: InteractorPresenterInterface {
    func insertNewUsage(note: String, amount: Float, cid: Int, status: Int, usage_time: String, short_time: String)
    func insertNewRecentlyUsed(note: String, amount: Float, cid: Int, status: Int)
    func updateUsage(note: String, amount: Float, cid: Int, status: Int, usage_time: String, short_time: String, id: Int)
    func fetchCategoryById(id: Int)
}

// MARK: - view

protocol AddViewPresenterInterface: ViewPresenterInterface {
    func displayCategoryById(category: Category)
}


// MARK: - module builder

final class AddModule: ModuleInterface {

    typealias View = AddView
    typealias Presenter = AddPresenter
    typealias Router = AddRouter
    typealias Interactor = AddInteractor

    func build(db: DBHelper) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "add") as! View
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router()

        view.db = db
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
