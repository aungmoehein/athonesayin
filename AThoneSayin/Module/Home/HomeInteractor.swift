//
//  HomeInteractor.swift
//  Home
//
//  Created by Aung Moe Hein on 07/05/2020.
//

import Foundation

final class HomeInteractor: InteractorInterface {

    weak var presenter: HomePresenterInteractorInterface!
    
    var db: DBHelper!
    
    init(db: DBHelper) {
        self.db = db
    }
}

extension HomeInteractor: HomeInteractorPresenterInterface {
    func fetchUsagesByStatus_SDate(status: Int, date: String) {
        let (cat_id, cat_id_count, amount, date) = db.readUsageByStatus_SDate(status: status, short_date: date)
        self.presenter.onFetchUsagesByStatus_SDate(cat_id: cat_id, cat_id_count: cat_id_count, amount: amount, date: date)
    }
    
    func deleteUsagesByStatus_SDate(status: Int, date: String, cid: Int) {
        db.deleteUsageByStatus_SDate(status: status, date: date, cid: cid)
    }
    
    func fetchCategoryById(id: Int) {
        self.presenter.onFetchCategoryById(category: db.readCategoryById(id: id))
    }
    
    func fetchIncomeExpenseByDate(date: String) {
        let (income, expense) = db.calcUsageAmountByDate(date: date)
        self.presenter.onFetchIncomeExpenseByDate(income: income, expense: expense)
    }
    
    func fetchRecentlyUsedsByStatus(status: Int) {
        self.presenter.onFetchRecentlyUsedsByStatus(recentlyUseds: db.readAllRecentlyUsedByStatus(status: status))
    }
}
