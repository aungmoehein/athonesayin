//
//  HomeView+Locale.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension HomeView {
    func localize() {
        lbIncome.text = localizer.localizedStringForKey(key: "income:", comment: "")
        lbExpense.text = localizer.localizedStringForKey(key: "expense:", comment: "")
        lbBalance.text = localizer.localizedStringForKey(key: "total_amount", comment: "")
        segmentedControl.setTitle(localizer.localizedStringForKey(key: "income", comment: ""), forSegmentAt: 1)
        segmentedControl.setTitle(localizer.localizedStringForKey(key: "expense", comment: ""), forSegmentAt: 0)
        lbNoData.text = localizer.localizedStringForKey(key: "info_no_expense", comment: "")
        btnNewExpense.setTitle(localizer.localizedStringForKey(key: "btn_add_expense", comment: ""), for: .normal)
    }
}
