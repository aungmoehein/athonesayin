//
//  HomeModule.swift
//  Home
//
//  Created by Aung Moe Hein on 07/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol HomeRouterPresenterInterface: RouterPresenterInterface {
    func goToAddView(status: Int)
    func goToUsagesDetail(status: Int, date: String, cid: Int)
    func showRecentlyUsedDialog(status: Int, recentlyUseds: [RecentlyUsed])
}

// MARK: - presenter

protocol HomePresenterRouterInterface: PresenterRouterInterface {
    
}

protocol HomePresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchUsagesByStatus_SDate(cat_id: [Int], cat_id_count: [Int], amount: [Float], date: [String])
    func onFetchCategoryById(category: Category)
    func onFetchIncomeExpenseByDate(income:Float, expense:Float)
    func onFetchRecentlyUsedsByStatus(recentlyUseds: [RecentlyUsed])
}

protocol HomePresenterViewInterface: PresenterViewInterface {
    func start()
    func addView(status: Int)
    func usageDetail(status: Int, date: String, cid: Int)
    func recentlyUsedDialog(status: Int, recentlyUseds: [RecentlyUsed])
    func getUsagesByStatus_SDate(status: Int, date: String)
    func getCategoryById(id: Int)
    func getIncomeExpenseByDate(date: String)
    func getRecentlyUsedsByStatus(status: Int)
    func removeUsagesByStatus_SDate(status: Int, date: String, cid: Int)
}

// MARK: - interactor

protocol HomeInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchUsagesByStatus_SDate(status: Int, date: String)
    func fetchCategoryById(id: Int)
    func fetchIncomeExpenseByDate(date: String)
    func fetchRecentlyUsedsByStatus(status: Int)
    func deleteUsagesByStatus_SDate(status: Int, date: String, cid: Int)
}

// MARK: - view

protocol HomeViewPresenterInterface: ViewPresenterInterface {
    func displayUsagesByStatus_SDate(cat_id: [Int], cat_id_count: [Int], amount: [Float], date: [String])
    func displayCategoryById(category: Category)
    func displayIncomeExpense(income: Float, expense: Float)
    func displayRecentlyUsed(recentlyUseds: [RecentlyUsed])
}


// MARK: - module builder

final class HomeModule: ModuleInterface {

    typealias View = HomeView
    typealias Presenter = HomePresenter
    typealias Router = HomeRouter
    typealias Interactor = HomeInteractor

    func build(db: DBHelper) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "home") as! HomeView
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router(db: db)

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view

        return view
    }
}
