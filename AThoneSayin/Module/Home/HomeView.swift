//
//  HomeView.swift
//  Home
//
//  Created by Aung Moe Hein on 07/05/2020.
//

import Foundation
import UIKit
import PopupDialog
import Lottie
import Firebase

final class HomeView: UIViewController, ViewInterface, UINavigationControllerDelegate {
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var btnNewExpense: UIButton!
    @IBOutlet weak var homeExpenseTable: UITableView!
    @IBOutlet weak var lbMonthYear: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var calendarView: UIStackView!
    @IBOutlet weak var balanceView: RoundCornerView!
    @IBOutlet weak var lbNoData: UILabel!
    @IBOutlet weak var lbIncome: UILabel!
    @IBOutlet weak var lbIncomeAmount: UILabel!
    @IBOutlet weak var lbExpense: UILabel!
    @IBOutlet weak var lbExpenseAmount: UILabel!
    @IBOutlet weak var lbBalance: UILabel!
    @IBOutlet weak var lbBalanceAmount: UILabel!
    @IBOutlet weak var emptyView: UIView!
    @IBOutlet weak var HeaderView: UIView!
    @IBOutlet weak var imgDollarCircle: UIImageView!
    @IBOutlet weak var btnCalendar: UIButton!
    
    let headerViewMaxHeight: CGFloat = 170
    let headerViewMinHeight: CGFloat = 0
    
    var calendarDialog: PopupDialog!
    var showMsgDialog: PopupDialog!
    let monthYearPicker = MonthYearPickerView()
    let dateFormatter = DateFormatter()
    var chosenMonth: String = "" {
        didSet{
            self.lbMonthYear.text = self.chosenMonth
            self.presenter.getIncomeExpenseByDate(date: self.chosenMonth)
            self.presenter.getUsagesByStatus_SDate(status: self.status, date: self.chosenMonth)
        }
    }

    var localizer = LocalizationSystem.Localizer
    var presenter: HomePresenterViewInterface!
    var category: Category!
    var isAdded: Bool = false
    var selectedStatus: Int = Parameter.EXPENSE
    var recentlyUseds: [RecentlyUsed] = []
    
    var income: Float = 0 {
        didSet {
            self.balance = self.income - self.expense
            lbIncomeAmount.text = String(format: localizer.localizedStringForKey(key: "ks", comment: ""), "\(Int(self.income).formattedWithSeparator)")
        }
    }
    
    var expense: Float = 0 {
        didSet {
            self.balance = self.income - self.expense
            lbExpenseAmount.text = String(format: localizer.localizedStringForKey(key: "ks", comment: ""), "\(Int(self.expense).formattedWithSeparator)")
        }
    }
    
    var balance: Float = 0 {
        didSet {
            lbBalanceAmount.text = String(format: localizer.localizedStringForKey(key: "ks", comment: ""), "\(Int(self.balance).formattedWithSeparator)")
        }
    }
    
    var status: Int = 0 {
        didSet {
            homeExpenseTable.reloadData()
        }
    }
    
    var cat_id: [Int] = [] {
        didSet {
            btnAdd.isHidden = self.cat_id.count == 0
            homeExpenseTable.isHidden = self.cat_id.count == 0
            emptyView.isHidden = self.cat_id.count != 0
            homeExpenseTable.reloadData()
        }
    }
    
    var cat_id_count: [Int] = [] {
        didSet {
            homeExpenseTable.reloadData()
        }
    }
    
    var amount: [Float] = [] {
        didSet {
            homeExpenseTable.reloadData()
        }
    }
    
    var date: [String] = [] {
        didSet {
            homeExpenseTable.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateFormatter.locale = Locale(identifier: "us")
        dateFormatter.timeZone = .current
        self.chosenMonth = "\(dateFormatter.monthSymbols[Calendar.current.component(.month, from: Date()) - 1].substring(fromIndex: 0, count: 3)), \(Calendar.current.component(.year, from: Date()))"
        self.lbMonthYear.text = self.chosenMonth
        
        self.navigationController?.delegate = self
        
        var image = UIImage(named: "ic_dollar_circle")?.withRenderingMode(.alwaysTemplate)
        imgDollarCircle.image = image
        imgDollarCircle.tintColor = .white
        
        image = UIImage(named: "ic_calendar")?.withRenderingMode(.alwaysTemplate)
        btnCalendar.setImage(image, for: .normal)
        btnCalendar.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        
        image = UIImage(named: "ic_plus")?.withRenderingMode(.alwaysTemplate)
        btnAdd.setImage(image, for: .normal)
        btnAdd.tintColor = .white
        btnAdd.makeRounded()
        btnAdd.backgroundColor = UIColor(hex: Color.colorAccent.rawValue)
        
        btnNewExpense.layer.cornerRadius = 5
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], for: UIControl.State.selected)
        
        self.homeExpenseTable.isHidden = self.cat_id.count == 0
        self.emptyView.isHidden = self.cat_id.count != 0
        self.homeExpenseTable.register(UINib(nibName: "HomeExpenseCell", bundle: nil), forCellReuseIdentifier: "home_expense_cell")
        self.homeExpenseTable.delegate = self
        self.homeExpenseTable.dataSource = self
        self.homeExpenseTable.allowsMultipleSelectionDuringEditing = false
        
        self.animationView.playAnimation(filename: "animation_not_found")
        self.presenter.start()
        Analytics.logEvent(Events.HOME_SCREEN, parameters: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        localize()
        
        self.presenter.getIncomeExpenseByDate(date: chosenMonth)
        self.presenter.getUsagesByStatus_SDate(status: self.status, date: chosenMonth)
        self.presenter.getRecentlyUsedsByStatus(status: self.status)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.navigationController!.viewControllers.count > 1
    }
    
    @IBAction func segmentedControl(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
        case Parameter.EXPENSE:
            self.status = Parameter.EXPENSE
            self.lbNoData.text = localizer.localizedStringForKey(key: "info_no_expense", comment: "")
            self.btnNewExpense.setTitle(localizer.localizedStringForKey(key: "btn_add_expense", comment: ""), for: .normal)
        default:
            self.status = Parameter.INCOME
            self.lbNoData.text = localizer.localizedStringForKey(key: "info_no_income", comment: "")
            self.btnNewExpense.setTitle(localizer.localizedStringForKey(key: "btn_add_income", comment: ""), for: .normal)
        }
        
        self.presenter.getIncomeExpenseByDate(date: chosenMonth)
        self.presenter.getUsagesByStatus_SDate(status: self.status, date: chosenMonth)
        self.presenter.getRecentlyUsedsByStatus(status: self.status)
    }
    
    @IBAction func btnCalendar(_ sender: Any) {
        let popUp = MonthYearPickerVC(nibName: "MonthYearPickerVC", bundle: nil)
        popUp.vc_name = "home"
        popUp.homeView = self
        calendarDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        calendarDialog.viewController = self
        self.present(calendarDialog, animated: true, completion: nil)
    }
    
    @IBAction func btnNewExpense(_ sender: Any) {
        if recentlyUseds.count == 0 {
            presenter.addView(status: self.status)
        }else {
            presenter.recentlyUsedDialog(status: self.status, recentlyUseds: recentlyUseds)
        }
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        if recentlyUseds.count == 0 {
            presenter.addView(status: self.status)
        }else {
            presenter.recentlyUsedDialog(status: self.status, recentlyUseds: recentlyUseds)
        }
    }
}

extension HomeView: MarkedUsagesViewDelegate {
    func recentlyUsedAdded() {
        self.presenter.getIncomeExpenseByDate(date: chosenMonth)
        self.presenter.getUsagesByStatus_SDate(status: self.status, date: chosenMonth)
    }
}

extension HomeView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cat_id.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "home_expense_cell", for: indexPath) as! HomeExpenseCell
        let backgroundView = UIView()
        backgroundView.backgroundColor = UIColor.clear
        cell.selectedBackgroundView = backgroundView
        
        cell.amount.textColor = UIColor.red
        if self.status == Parameter.INCOME {
            cell.amount.textColor = UIColor(hex: Color.colorPrimary.rawValue)
        }
        
        self.presenter.getCategoryById(id: self.cat_id[indexPath.row])
        cell.icon_name = category.category_icon
        cell.category_name = category.category_name
        cell.usage_amount = "\(Int(self.amount[indexPath.row]))"
        cell.count = self.cat_id_count[indexPath.row]
        cell.addedDate = self.date[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.usageDetail(status: self.status, date: self.date[indexPath.row], cid: self.cat_id[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                self.showMsg(msg: self.localizer.localizedStringForKey(key: "confirm_delete", comment: ""), tableView: tableView, indexPath: indexPath)
                
                completionHandler(true)
            }
                if #available(iOS 13.0, *) {
                    deleteAction.image = UIImage(systemName: "trash")
                } else {
                    deleteAction.image = UIImage(named: "ic_trash")
                }
            deleteAction.backgroundColor = .systemRed
            let configuration = UISwipeActionsConfiguration(actions: [deleteAction])
            return configuration
    }
    
    private func showMsg(msg: String, tableView: UITableView, indexPath: IndexPath) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
            self.presenter.removeUsagesByStatus_SDate(status: self.status, date: self.date[indexPath.row], cid: self.cat_id[indexPath.row])
            self.cat_id.remove(at: indexPath.row)
            self.cat_id_count.remove(at: indexPath.row)
            self.amount.remove(at: indexPath.row)
            self.date.remove(at: indexPath.row)
            tableView.reloadData()
            self.presenter.getIncomeExpenseByDate(date: self.chosenMonth)
        }
        let buttonCancel = CancelButton(title: NSLocalizedString("Cancel", comment: "")) {
        }
        showMsgDialog.addButtons([buttonOk, buttonCancel])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let y: CGFloat = scrollView.contentOffset.y
        let newHeaderViewHeight: CGFloat = headerViewHeight.constant - y

        if newHeaderViewHeight > headerViewMaxHeight {
            self.HeaderView.alpha = 1
            headerViewHeight.constant = headerViewMaxHeight
        } else if newHeaderViewHeight < headerViewMinHeight {
            self.HeaderView.alpha = 0
            headerViewHeight.constant = headerViewMinHeight
        } else {
            self.HeaderView.alpha = newHeaderViewHeight / 170
            headerViewHeight.constant = newHeaderViewHeight
            scrollView.contentOffset.y = 0 // block scroll view
        }
    }
}

extension HomeView: HomeViewPresenterInterface {
    func displayUsagesByStatus_SDate(cat_id: [Int], cat_id_count: [Int], amount: [Float], date: [String]) {
        self.cat_id = cat_id
        self.cat_id_count = cat_id_count
        self.amount = amount
        self.date = date
    }
    
    func displayCategoryById(category: Category) {
        self.category = category
    }
    
    func displayIncomeExpense(income: Float, expense: Float){
        self.income = income
        self.expense = expense
    }
    
    func displayRecentlyUsed(recentlyUseds: [RecentlyUsed]) {
        self.recentlyUseds = recentlyUseds
    }
}
