//
//  HomePresenter.swift
//  Home
//
//  Created by Aung Moe Hein on 07/05/2020.
//

import Foundation

final class HomePresenter: PresenterInterface {

    var router: HomeRouterPresenterInterface!
    var interactor: HomeInteractorPresenterInterface!
    weak var view: HomeViewPresenterInterface!

}

extension HomePresenter: HomePresenterRouterInterface {
    
}

extension HomePresenter: HomePresenterInteractorInterface {
    func onFetchUsagesByStatus_SDate(cat_id: [Int], cat_id_count: [Int], amount: [Float], date: [String]) {
        self.view.displayUsagesByStatus_SDate(cat_id: cat_id, cat_id_count: cat_id_count, amount: amount, date: date)
    }
    
    func onFetchCategoryById(category: Category) {
        self.view.displayCategoryById(category: category)
    }
    
    func onFetchIncomeExpenseByDate(income:Float, expense:Float) {
        self.view.displayIncomeExpense(income: income, expense: expense)
    }
    
    func onFetchRecentlyUsedsByStatus(recentlyUseds: [RecentlyUsed]) {
        self.view.displayRecentlyUsed(recentlyUseds: recentlyUseds)
    }
}

extension HomePresenter: HomePresenterViewInterface {
    func getUsagesByStatus_SDate(status: Int, date: String) {
        interactor.fetchUsagesByStatus_SDate(status: status, date: date)
    }
    
    func getCategoryById(id: Int) {
        interactor.fetchCategoryById(id: id)
    }
    
    func getIncomeExpenseByDate(date: String) {
        interactor.fetchIncomeExpenseByDate(date: date)
    }
    
    func getRecentlyUsedsByStatus(status: Int) {
        interactor.fetchRecentlyUsedsByStatus(status: status)
    }
    
    func removeUsagesByStatus_SDate(status: Int, date: String, cid: Int) {
        interactor.deleteUsagesByStatus_SDate(status: status, date: date, cid: cid)
    }
    
    func addView(status: Int) {
        router.goToAddView(status: status)
    }
    
    func usageDetail(status: Int, date: String, cid: Int) {
        router.goToUsagesDetail(status: status, date: date, cid: cid)
    }
    
    func recentlyUsedDialog(status: Int, recentlyUseds: [RecentlyUsed]) {
        router.showRecentlyUsedDialog(status: status, recentlyUseds: recentlyUseds)
    }
    
    func start() {

    }
}
