//
//  ChooseCategoryRouter.swift
//  ChooseCategory
//
//  Created by Aung Moe Hein on 10/05/2020.
//

import Foundation
import UIKit

final class ChooseCategoryRouter: RouterInterface {

    weak var presenter: ChooseCategoryPresenterRouterInterface!

    weak var viewController: UIViewController?
    
    var db: DBHelper!
    init(db: DBHelper) {
        self.db = db
    }
}

extension ChooseCategoryRouter: ChooseCategoryRouterPresenterInterface {
    func goToAddCategory(status: Int, vc: ChooseCategoryView) {
        let view = AddCategoryModule().build(db: self.db) as! AddCategoryView
        view.status = status
        view.delegate = vc
        viewController?.present(view, animated: true, completion: nil)
    }
}
