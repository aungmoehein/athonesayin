//
//  ChooseCategoryModule.swift
//  ChooseCategory
//
//  Created by Aung Moe Hein on 10/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol ChooseCategoryRouterPresenterInterface: RouterPresenterInterface {
    func goToAddCategory(status: Int, vc: ChooseCategoryView)
}

// MARK: - presenter

protocol ChooseCategoryPresenterRouterInterface: PresenterRouterInterface {

}

protocol ChooseCategoryPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchCategoriesByType(categories: [Category])
}

protocol ChooseCategoryPresenterViewInterface: PresenterViewInterface {
    func start()
    func addCategory(status: Int, vc: ChooseCategoryView)
    func getCategoriesByType(type: Int)
}

// MARK: - interactor

protocol ChooseCategoryInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchCategoriesByType(type: Int)
}

// MARK: - view

protocol ChooseCategoryViewPresenterInterface: ViewPresenterInterface {
    func displayCategoriesByType(categories: [Category])
}


// MARK: - module builder

final class ChooseCategoryModule: ModuleInterface {

    typealias View = ChooseCategoryView
    typealias Presenter = ChooseCategoryPresenter
    typealias Router = ChooseCategoryRouter
    typealias Interactor = ChooseCategoryInteractor

    func build(db: DBHelper, categoryType: Int, vc: AddView) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "choose_category") as! View
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router(db: db)

        view.categoryType = categoryType
        view.addViewVC = vc
        
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
