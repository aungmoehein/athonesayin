//
//  ChooseCategoryInteractor.swift
//  ChooseCategory
//
//  Created by Aung Moe Hein on 10/05/2020.
//

import Foundation

final class ChooseCategoryInteractor: InteractorInterface {

    weak var presenter: ChooseCategoryPresenterInteractorInterface!
    
    var db: DBHelper!
    
    init(db: DBHelper) {
        self.db = db
    }
}

extension ChooseCategoryInteractor: ChooseCategoryInteractorPresenterInterface {
    func fetchCategoriesByType(type: Int) {
        self.presenter.onFetchCategoriesByType(categories: db.readCategoryByType(type: type))
    }
}
