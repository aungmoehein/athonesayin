//
//  ChooseCategoryView+Locale.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension ChooseCategoryView {
    func locale() {
        lbTitle.text = localizer.localizedStringForKey(key: "choose", comment: "")
        btnAdd.setTitle(localizer.localizedStringForKey(key: "new_category", comment: ""), for: .normal)
    }
}
