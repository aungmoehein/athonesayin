//
//  ChooseCategoryPresenter.swift
//  ChooseCategory
//
//  Created by Aung Moe Hein on 10/05/2020.
//

import Foundation

final class ChooseCategoryPresenter: PresenterInterface {

    var router: ChooseCategoryRouterPresenterInterface!
    var interactor: ChooseCategoryInteractorPresenterInterface!
    weak var view: ChooseCategoryViewPresenterInterface!

}

extension ChooseCategoryPresenter: ChooseCategoryPresenterRouterInterface {

}

extension ChooseCategoryPresenter: ChooseCategoryPresenterInteractorInterface {
    func onFetchCategoriesByType(categories: [Category]) {
        self.view.displayCategoriesByType(categories: categories)
    }
}

extension ChooseCategoryPresenter: ChooseCategoryPresenterViewInterface {

    func start() {
        
    }
    
    func addCategory(status: Int, vc: ChooseCategoryView) {
        router.goToAddCategory(status: status, vc: vc)
    }

    func getCategoriesByType(type: Int) {
        interactor.fetchCategoriesByType(type: type)
    }
}
