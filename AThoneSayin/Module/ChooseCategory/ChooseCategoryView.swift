//
//  ChooseCategoryView.swift
//  ChooseCategory
//
//  Created by Aung Moe Hein on 10/05/2020.
//

import Foundation
import UIKit

final class ChooseCategoryView: UIViewController, ViewInterface {
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    
    var presenter: ChooseCategoryPresenterViewInterface!
    var categories: [Category] = [] {
        didSet {
            categoryCollectionView.reloadData()
        }
    }
    
    let localizer = LocalizationSystem.Localizer
    var addViewVC: AddView!
    var categoryType: Int!

    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 4,
        minimumInteritemSpacing: 0,
        minimumLineSpacing: 0,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    )

    override func viewDidLoad() {
        super.viewDidLoad()
        categoryCollectionView.register(UINib(nibName: "CategoryChooseCell", bundle: nil), forCellWithReuseIdentifier: "category_choose_cell")
        categoryCollectionView.dataSource = self
        categoryCollectionView.delegate = self
        categoryCollectionView.collectionViewLayout = columnLayout
        categoryCollectionView.reloadData()
        
        self.presenter.start()
        refreshView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        locale()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.navigationController!.viewControllers.count > 1
    }
    
    func refreshView() {
        self.presenter.getCategoriesByType(type: categoryType)
        
        self.addViewVC.cat_name = self.categories[0].category_name
        self.addViewVC.icon_name = self.categories[0].category_icon
        self.addViewVC.cat_id = self.categories[0].category_id
        self.addViewVC.lbCategory.text = localizer.localizedStringForKey(key: self.categories[0].category_name, comment: "")
        self.addViewVC.categoryIconUI(icon_name: self.categories[0].category_icon)
    }

    @IBAction func btnAdd(_ sender: Any) {
        self.presenter.addCategory(status: self.categoryType, vc: self)
    }
}

extension ChooseCategoryView: AddCategoryDelegate {
    func finishCategoryAdd() {
       self.presenter.getCategoriesByType(type: categoryType)
    }
}

extension ChooseCategoryView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = categoryCollectionView.dequeueReusableCell(withReuseIdentifier: "category_choose_cell", for: indexPath) as! CategoryChooseCell
        cell.category_name = categories[indexPath.row].category_name
        cell.icon_name = categories[indexPath.row].category_icon
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        addViewVC.cat_name = categories[indexPath.row].category_name
        addViewVC.icon_name = categories[indexPath.row].category_icon
        addViewVC.cat_id = categories[indexPath.row].category_id
        addViewVC.cat_id_update = categories[indexPath.row].category_id
        
        addViewVC.lbCategory.text = localizer.localizedStringForKey(key: categories[indexPath.row].category_name, comment: "")
        addViewVC.categoryIconUI(icon_name: categories[indexPath.row].category_icon)
        addViewVC.floatingPanel.move(to: .half, animated: true)
    }
}

extension ChooseCategoryView: ChooseCategoryViewPresenterInterface {
    func displayCategoriesByType(categories: [Category]) {
        self.categories = categories
    }
}
