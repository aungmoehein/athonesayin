//
//  ChooseThemePresenter.swift
//  ChooseTheme
//
//  Created by Aung Moe Hein on 24/07/2020.
//

import Foundation

final class ChooseThemePresenter: PresenterInterface {

    var router: ChooseThemeRouterPresenterInterface!
    var interactor: ChooseThemeInteractorPresenterInterface!
    weak var view: ChooseThemeViewPresenterInterface!

}

extension ChooseThemePresenter: ChooseThemePresenterRouterInterface {

}

extension ChooseThemePresenter: ChooseThemePresenterInteractorInterface {

}

extension ChooseThemePresenter: ChooseThemePresenterViewInterface {

    func start() {

    }

}
