//
//  ChooseThemeRouter.swift
//  ChooseTheme
//
//  Created by Aung Moe Hein on 24/07/2020.
//

import Foundation
import UIKit

final class ChooseThemeRouter: RouterInterface {

    weak var presenter: ChooseThemePresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension ChooseThemeRouter: ChooseThemeRouterPresenterInterface {

}