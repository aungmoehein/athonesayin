//
//  ChooseThemeView.swift
//  ChooseTheme
//
//  Created by Aung Moe Hein on 24/07/2020.
//

import Foundation
import UIKit

final class ChooseThemeView: UITableViewController, ViewInterface {

    var presenter: ChooseThemePresenterViewInterface!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.register(UINib(nibName: "ChooseThemeCell", bundle: nil), forCellReuseIdentifier: "choose_theme_cell")
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.reloadData()

        self.presenter.start()
    }

}

extension ChooseThemeView: ChooseThemeViewPresenterInterface {

}
