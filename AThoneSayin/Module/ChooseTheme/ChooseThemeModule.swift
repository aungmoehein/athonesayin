//
//  ChooseThemeModule.swift
//  ChooseTheme
//
//  Created by Aung Moe Hein on 24/07/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol ChooseThemeRouterPresenterInterface: RouterPresenterInterface {

}

// MARK: - presenter

protocol ChooseThemePresenterRouterInterface: PresenterRouterInterface {

}

protocol ChooseThemePresenterInteractorInterface: PresenterInteractorInterface {

}

protocol ChooseThemePresenterViewInterface: PresenterViewInterface {
    func start()
}

// MARK: - interactor

protocol ChooseThemeInteractorPresenterInterface: InteractorPresenterInterface {

}

// MARK: - view

protocol ChooseThemeViewPresenterInterface: ViewPresenterInterface {

}


// MARK: - module builder

final class ChooseThemeModule: ModuleInterface {

    typealias View = ChooseThemeView
    typealias Presenter = ChooseThemePresenter
    typealias Router = ChooseThemeRouter
    typealias Interactor = ChooseThemeInteractor

    func build() -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "choose_theme") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
