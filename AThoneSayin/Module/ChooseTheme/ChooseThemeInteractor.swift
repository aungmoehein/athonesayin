//
//  ChooseThemeInteractor.swift
//  ChooseTheme
//
//  Created by Aung Moe Hein on 24/07/2020.
//

import Foundation

final class ChooseThemeInteractor: InteractorInterface {

    weak var presenter: ChooseThemePresenterInteractorInterface!
}

extension ChooseThemeInteractor: ChooseThemeInteractorPresenterInterface {

}
