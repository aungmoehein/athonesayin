//
//  DashboardInteractor.swift
//  Dashboard
//
//  Created by Aung Moe Hein on 14/05/2020.
//

import Foundation

final class DashboardInteractor: InteractorInterface {

    weak var presenter: DashboardPresenterInteractorInterface!
    var db: DBHelper!
    
    init(db: DBHelper) {
        self.db = db
    }
}

extension DashboardInteractor: DashboardInteractorPresenterInterface {
    func fetchCategoryById(id: Int) {
        presenter.onFetchCategoryById(category: db.readCategoryById(id: id))
    }
    
    func fetctUsagesForDashboard(status: Int, date: String) {
        let (cids, amounts) = db.readUsagesForDashboard(status: status, short_date: date)
        presenter.onFetchUsagesForDashboard(cids: cids, amounts: amounts)
    }
    
    func fetchRecentlyUsedsByStatus(status: Int) {
        self.presenter.onFetchRecentlyUsedsByStatus(recentlyUseds: db.readAllRecentlyUsedByStatus(status: status))
    }
    
    func fetchCategoryCount() {
        self.presenter.onFetchCategoryCount(count: db.readCategoryCount())
    }
}
