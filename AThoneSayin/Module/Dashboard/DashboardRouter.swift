//
//  DashboardRouter.swift
//  Dashboard
//
//  Created by Aung Moe Hein on 14/05/2020.
//

import Foundation
import UIKit
import PopupDialog

final class DashboardRouter: RouterInterface {

    weak var presenter: DashboardPresenterRouterInterface!

    weak var viewController: UIViewController?
    var recentlyUsedDialog: PopupDialog!
    
    var db: DBHelper!
    init(db: DBHelper) {
        self.db = db
    }
}

extension DashboardRouter: DashboardRouterPresenterInterface {
    func goToAddView(status: Int) {
        let vc = AddModule().build(db: db) as! AddView
        vc.selectedSegment = status
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToUsagesDetail(status: Int, date: String, cid: Int) {
        let vc = UsageDetailModule().build(db: db, status: status, date: date, cid: cid) as! UsageDetailView
        vc.viaDashboard = true
        viewController?.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showRecentlyUsedDialog(status: Int, recentlyUseds: [RecentlyUsed]) {
        let popUp = MarkedUsagesView(nibName: "MarkedUsagesView", bundle: nil)
        popUp.db = db
        popUp.markedUsagesViewDelegate = viewController as! DashboardView
        popUp.viaDashboard = true
        popUp.dashboard = viewController as? DashboardView
        popUp.status = status
        popUp.recentlyUseds = recentlyUseds
        recentlyUsedDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        recentlyUsedDialog.viewController = viewController!
        viewController?.present(recentlyUsedDialog, animated: true, completion: nil)
    }
}
