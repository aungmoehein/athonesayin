//
//  DashboardModule.swift
//  Dashboard
//
//  Created by Aung Moe Hein on 14/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol DashboardRouterPresenterInterface: RouterPresenterInterface {
    func goToAddView(status: Int)
    func goToUsagesDetail(status: Int, date: String, cid: Int)
    func showRecentlyUsedDialog(status: Int, recentlyUseds: [RecentlyUsed])
}

// MARK: - presenter

protocol DashboardPresenterRouterInterface: PresenterRouterInterface {

}

protocol DashboardPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchCategoryById(category: Category)
    func onFetchUsagesForDashboard(cids: [Int], amounts: [Float])
    func onFetchRecentlyUsedsByStatus(recentlyUseds: [RecentlyUsed])
    func onFetchCategoryCount(count: Int)
}

protocol DashboardPresenterViewInterface: PresenterViewInterface {
    func start()
    func addView(status: Int)
    func usageDetail(status: Int, date: String, cid: Int)
    func recentlyUsedDialog(status: Int, recentlyUseds: [RecentlyUsed])
    func getUsagesForDashboard(status:Int, date:String)
    func getCategoryById(id: Int)
    func getRecentlyUsedsByStatus(status: Int)
    func getCategoryCount()
}

// MARK: - interactor

protocol DashboardInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchCategoryById(id: Int)
    func fetctUsagesForDashboard(status: Int, date: String)
    func fetchRecentlyUsedsByStatus(status: Int)
    func fetchCategoryCount() 
}

// MARK: - view

protocol DashboardViewPresenterInterface: ViewPresenterInterface {
    func displayUsagesForDashboard(cids: [Int], amounts: [Float])
    func displayCategoryById(category: Category)
    func displayRecentlyUsed(recentlyUseds: [RecentlyUsed])
    func displayCategoryCount(count: Int)
}


// MARK: - module builder

final class DashboardModule: ModuleInterface {

    typealias View = DashboardView
    typealias Presenter = DashboardPresenter
    typealias Router = DashboardRouter
    typealias Interactor = DashboardInteractor

    func build(db: DBHelper) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "dashboard") as! View
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router(db: db)

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
