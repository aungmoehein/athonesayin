//
//  DashboardPresenter.swift
//  Dashboard
//
//  Created by Aung Moe Hein on 14/05/2020.
//

import Foundation

final class DashboardPresenter: PresenterInterface {

    var router: DashboardRouterPresenterInterface!
    var interactor: DashboardInteractorPresenterInterface!
    weak var view: DashboardViewPresenterInterface!

}

extension DashboardPresenter: DashboardPresenterRouterInterface {

}

extension DashboardPresenter: DashboardPresenterInteractorInterface {
    func onFetchCategoryById(category: Category) {
        self.view.displayCategoryById(category: category)
    }
    
    func onFetchUsagesForDashboard(cids: [Int], amounts: [Float]) {
        self.view.displayUsagesForDashboard(cids: cids, amounts: amounts)
    }
    
    func onFetchRecentlyUsedsByStatus(recentlyUseds: [RecentlyUsed]) {
        self.view.displayRecentlyUsed(recentlyUseds: recentlyUseds)
    }
    
    func onFetchCategoryCount(count: Int) {
        self.view.displayCategoryCount(count: count)
    }
}

extension DashboardPresenter: DashboardPresenterViewInterface {

    func start() {

    }
    
    func addView(status: Int) {
        router.goToAddView(status: status)
    }
    
    func usageDetail(status: Int, date: String, cid: Int) {
        router.goToUsagesDetail(status: status, date: date, cid: cid)
    }
    
    func recentlyUsedDialog(status: Int, recentlyUseds: [RecentlyUsed]) {
        router.showRecentlyUsedDialog(status: status, recentlyUseds: recentlyUseds)
    }
    
    func getUsagesForDashboard(status: Int, date: String) {
        interactor.fetctUsagesForDashboard(status: status, date: date)
    }
    
    func getCategoryById(id: Int) {
        interactor.fetchCategoryById(id: id)
    }
    
    func getRecentlyUsedsByStatus(status: Int) {
        interactor.fetchRecentlyUsedsByStatus(status: status)
    }

    func getCategoryCount() {
        interactor.fetchCategoryCount()
    }
}
