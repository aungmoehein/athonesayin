//
//  DashboardView+Locale.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension DashboardView {
    func locale() {
        segmentedControl.setTitle(localizer.localizedStringForKey(key: "expense", comment: ""), forSegmentAt: 0)
        segmentedControl.setTitle(localizer.localizedStringForKey(key: "income", comment: ""), forSegmentAt: 1)
        btnAdd.setTitle(self.selectedStatus == Parameter.EXPENSE ? localizer.localizedStringForKey(key: "btn_add_expense", comment: "") : localizer.localizedStringForKey(key: "btn_add_income", comment: ""), for: .normal)
    }
}
