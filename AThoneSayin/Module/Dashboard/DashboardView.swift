//
//  DashboardView.swift
//  Dashboard
//
//  Created by Aung Moe Hein on 14/05/2020.
//

import Foundation
import UIKit
import Charts
import PopupDialog
import RandomColorSwift
import Firebase

final class DashboardView: UIViewController, ViewInterface, ChartViewDelegate {

    @IBOutlet weak var pieChart: PieChartView!
    @IBOutlet weak var lbMonthYear: UILabel!
    @IBOutlet weak var btnCalendar: UIButton!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var pieChartCV: UICollectionView!
    @IBOutlet weak var btnAdd: UIButton!
    
    var localizer = LocalizationSystem.Localizer
    var presenter: DashboardPresenterViewInterface!
    
    var selectedStatus: Int = Parameter.EXPENSE
    var calendarDialog: PopupDialog!
    
    let monthYearPicker = MonthYearPickerView()
    let dateFormatter = DateFormatter()
    var chosenMonth: String = "" {
        didSet{
            self.lbMonthYear.text = self.chosenMonth
            self.refresh()
        }
    }
    
    var cat_id: [Int] = [] {
        didSet {
            self.pieChartCV.isHidden = self.cat_id.count == 0
            cat_id.forEach() {
                id in
                presenter.getCategoryById(id: id)
            }
        }
    }
    
    var recentlyUseds: [RecentlyUsed] = []
    var cat_amount: [Int] = []
    var totalAmount: Int = 0
    var amount: [Float] = [] {
        didSet {
            amount.forEach() {
                amt in
                cat_amount.append(Int(amt))
                totalAmount += Int(amt)
            }
        }
    }

    var cat_names: [String] = []
    var cat_icons: [String] = []
    var category: Category! {
        didSet {
            cat_names.append(localizer.localizedStringForKey(key: category.category_name, comment: ""))
            cat_icons.append(category.category_icon)
        }
    }
    
    var categoryCount: Int = 0
    var colors: [UIColor] = []
    var shouldHideData: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        self.pieChartCV.isHidden = self.cat_id.count == 0
        
        let image = UIImage(named: "ic_calendar")?.withRenderingMode(.alwaysTemplate)
        btnCalendar.setImage(image, for: .normal)
        btnCalendar.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        
        btnAdd.layer.cornerRadius = 5
        dateFormatter.locale = Locale(identifier: "us")
        dateFormatter.timeZone = .current
        self.chosenMonth = "\(dateFormatter.monthSymbols[Calendar.current.component(.month, from: Date()) - 1].substring(fromIndex: 0, count: 3)), \(Calendar.current.component(.year, from: Date()))"
        self.lbMonthYear.text = self.chosenMonth
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], for: UIControl.State.selected)
        
        pieChartCV.register(UINib(nibName: "PieChartCollectionCell", bundle: nil), forCellWithReuseIdentifier: "pie_chart_collection_cell")
        pieChartCV.dataSource = self
        pieChartCV.delegate = self
        pieChartCV.reloadData()
        pieChart.holeColor = UIColor.clear
        pieChart.delegate = self
        pieChart.legend.enabled = false
        pieChart.setExtraOffsets(left: 0, top: 0, right: 0, bottom: 0)

        self.presenter.start()
        Analytics.logEvent(Events.DASHBOARD_SCREEN, parameters: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        presenter.getCategoryCount()
        colors = randomColors(count: categoryCount)
        locale()
        refresh()
    }
    
    override func viewDidAppear(_ animated: Bool) {
         self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.navigationController!.viewControllers.count > 1
     }
    
    func refresh() {
        self.cat_names = []
        self.cat_amount = []
        self.cat_icons = []
        self.totalAmount = 0
        self.presenter.getUsagesForDashboard(status: selectedStatus, date: chosenMonth)
        self.presenter.getRecentlyUsedsByStatus(status: selectedStatus)
        shouldHideData = cat_names.count == 0
        pieChart.animate(xAxisDuration: 0.8)
        pieChart.setup(date: chosenMonth, amount: String(format: localizer.localizedStringForKey(key: "ks", comment: ""), totalAmount.formattedWithSeparator))
        pieChart.updateChartData(hideData: shouldHideData, label: cat_names, value: cat_amount, colors: colors)
        pieChartCV.reloadData()
    }
    
    @IBAction func segmentedControl(_ sender: Any) {
        switch segmentedControl.selectedSegmentIndex {
        case Parameter.EXPENSE:
            self.selectedStatus = Parameter.EXPENSE
            btnAdd.setTitle(localizer.localizedStringForKey(key: "btn_add_expense", comment: ""), for: .normal)
        default:
            self.selectedStatus = Parameter.INCOME
            btnAdd.setTitle(localizer.localizedStringForKey(key: "btn_add_income", comment: ""), for: .normal)
        }
        
        refresh()
    }
    
    @IBAction func btnCalendar(_ sender: Any) {
        let popUp = MonthYearPickerVC(nibName: "MonthYearPickerVC", bundle: nil)
        popUp.vc_name = "dashboard"
        popUp.dashboardView = self
        calendarDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        calendarDialog.viewController = self
        self.present(calendarDialog, animated: true, completion: nil)
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        if recentlyUseds.count == 0 {
            presenter.addView(status: selectedStatus)
        }else {
            presenter.recentlyUsedDialog(status: self.selectedStatus, recentlyUseds: recentlyUseds)
        }
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        if let dataSet = chartView.data?.dataSets[ highlight.dataSetIndex] {
            let sliceIndex: Int = dataSet.entryIndex( entry: entry)
            pieChart.setup(date: chosenMonth, amount: String(format: localizer.localizedStringForKey(key: "ks", comment: ""), Int(amount[sliceIndex]).formattedWithSeparator))
        }
    }
    
    func chartValueNothingSelected(_ chartView: ChartViewBase) {
        pieChart.setup(date: chosenMonth, amount: String(format: localizer.localizedStringForKey(key: "ks", comment: ""), totalAmount.formattedWithSeparator))
    }
}

extension DashboardView: MarkedUsagesViewDelegate {
    func recentlyUsedAdded() {
        self.refresh()
    }
}

extension DashboardView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.cat_names.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pie_chart_collection_cell", for: indexPath) as! PieChartCollectionCell
        cell.icon_name = cat_icons[indexPath.row]
        cell.name = cat_names[indexPath.row]
        cell.amount = amount[indexPath.row]
        cell.color = colors[indexPath.row]
        cell.totalAmount = Float(totalAmount)
        cell.progress(current: amount[indexPath.row], max: Float(totalAmount))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.usageDetail(status: self.selectedStatus, date: chosenMonth, cid: cat_id[indexPath.row])
    }
}

extension DashboardView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 20, right: 10)
    }
}

extension DashboardView: DashboardViewPresenterInterface {
    func displayUsagesForDashboard(cids: [Int], amounts: [Float]) {
        self.cat_id = cids
        self.amount = amounts
    }
    
    func displayCategoryById(category: Category) {
        self.category = category
    }
    
    func displayRecentlyUsed(recentlyUseds: [RecentlyUsed]) {
        self.recentlyUseds = recentlyUseds
    }
    
    func displayCategoryCount(count: Int) {
        self.categoryCount = count
    }
}
