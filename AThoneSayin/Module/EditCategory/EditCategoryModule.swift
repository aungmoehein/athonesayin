//
//  EditCategoryModule.swift
//  EditCategory
//
//  Created by Aung Moe Hein on 16/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol EditCategoryRouterPresenterInterface: RouterPresenterInterface {
    func goToAddCategory(status: Int, vc: EditCategoryView)
    func goToUpdateCategory(id: Int, name: String, icon: String, vc: EditCategoryView)
}

// MARK: - presenter

protocol EditCategoryPresenterRouterInterface: PresenterRouterInterface {

}

protocol EditCategoryPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchCategoriesForEdit(categories: [Category])
}

protocol EditCategoryPresenterViewInterface: PresenterViewInterface {
    func start()
    func getCategories(status: Int)
    func removeCategoryById(id: Int)
    func removeUsagesByCategoryId(id: Int)
    func addCategory(status: Int, vc: EditCategoryView)
    func updateCategory(id: Int, name: String, icon: String, vc: EditCategoryView)
}

// MARK: - interactor

protocol EditCategoryInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchCategoriesForEdit(status: Int)
    func deleteCategoryById(id: Int)
    func deleteUsagesByCategoryId(id: Int)
}

// MARK: - view

protocol EditCategoryViewPresenterInterface: ViewPresenterInterface {
    func displayCategories(categories: [Category])
}


// MARK: - module builder

final class EditCategoryModule: ModuleInterface {

    typealias View = EditCategoryView
    typealias Presenter = EditCategoryPresenter
    typealias Router = EditCategoryRouter
    typealias Interactor = EditCategoryInteractor

    func build(db: DBHelper) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "edit_category") as! View
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router(db: db)

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
