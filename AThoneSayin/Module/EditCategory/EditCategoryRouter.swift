//
//  EditCategoryRouter.swift
//  EditCategory
//
//  Created by Aung Moe Hein on 16/05/2020.
//

import Foundation
import UIKit

final class EditCategoryRouter: RouterInterface {

    weak var presenter: EditCategoryPresenterRouterInterface!

    weak var viewController: UIViewController?
    
    var db: DBHelper!
    init(db: DBHelper) {
        self.db = db
    }
}

extension EditCategoryRouter: EditCategoryRouterPresenterInterface {
    func goToAddCategory(status: Int, vc: EditCategoryView) {
        let view = AddCategoryModule().build(db: self.db) as! AddCategoryView
        view.status = status
        view.delegate = vc
        viewController?.present(view, animated: true, completion: nil)
    }
    
    func goToUpdateCategory(id: Int, name: String, icon: String, vc: EditCategoryView) {
        let view = AddCategoryModule().build(db: db) as! AddCategoryView
        view.isUpdate = true
        view.updateName = name
        view.updateIcon = icon
        view.updateID = id
        view.delegate = vc
        viewController?.present(view, animated: true, completion: nil)
    }
}
