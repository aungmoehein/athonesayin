//
//  EditCategoryPresenter.swift
//  EditCategory
//
//  Created by Aung Moe Hein on 16/05/2020.
//

import Foundation

final class EditCategoryPresenter: PresenterInterface {

    var router: EditCategoryRouterPresenterInterface!
    var interactor: EditCategoryInteractorPresenterInterface!
    weak var view: EditCategoryViewPresenterInterface!

}

extension EditCategoryPresenter: EditCategoryPresenterRouterInterface {

}

extension EditCategoryPresenter: EditCategoryPresenterInteractorInterface {
    
    func onFetchCategoriesForEdit(categories: [Category]) {
        self.view.displayCategories(categories: categories)
    }

}

extension EditCategoryPresenter: EditCategoryPresenterViewInterface {
    func start() {

    }
    
    func getCategories(status: Int) {
        interactor.fetchCategoriesForEdit(status: status)
    }
    
    func addCategory(status: Int, vc: EditCategoryView) {
        router.goToAddCategory(status: status, vc: vc)
    }
    
    func updateCategory(id: Int, name: String, icon: String, vc: EditCategoryView) {
        router.goToUpdateCategory(id: id, name: name, icon: icon, vc: vc)
    }
    
    func removeCategoryById(id: Int) {
        interactor.deleteCategoryById(id: id)
    }

    func removeUsagesByCategoryId(id: Int) {
        interactor.deleteUsagesByCategoryId(id: id)
    }
}
