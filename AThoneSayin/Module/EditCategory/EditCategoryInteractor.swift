//
//  EditCategoryInteractor.swift
//  EditCategory
//
//  Created by Aung Moe Hein on 16/05/2020.
//

import Foundation

final class EditCategoryInteractor: InteractorInterface {

    weak var presenter: EditCategoryPresenterInteractorInterface!
    var db: DBHelper!
    
    init(db: DBHelper) {
        self.db = db
    }
}

extension EditCategoryInteractor: EditCategoryInteractorPresenterInterface {
    func fetchCategoriesForEdit(status: Int) {
        presenter.onFetchCategoriesForEdit(categories: db.readCategoryForEdit(status: status))
    }
    
    func deleteCategoryById(id: Int) {
        db.deleteCategoryById(category_id: id)
    }
    
    func deleteUsagesByCategoryId(id: Int) {
        db.deleteCategoryById(category_id: id)
    }
}
