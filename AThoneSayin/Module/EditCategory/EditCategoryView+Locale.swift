//
//  EditCategoryView+Locale.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension EditCategoryView {
    func locale () {
        self.navigationItem.title = localizer.localizedStringForKey(key: "edit_category", comment: "")
        segmentedControl.setTitle(localizer.localizedStringForKey(key: "expense", comment: ""), forSegmentAt: 0)
        segmentedControl.setTitle(localizer.localizedStringForKey(key: "income", comment: ""), forSegmentAt: 1)
    }
}
