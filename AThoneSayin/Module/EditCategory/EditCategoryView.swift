//
//  EditCategoryView.swift
//  EditCategory
//
//  Created by Aung Moe Hein on 16/05/2020.
//

import Foundation
import UIKit
import PopupDialog
import Firebase

final class EditCategoryView: UIViewController, ViewInterface, UINavigationControllerDelegate {
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var editCV: UICollectionView!
    @IBOutlet weak var doneBtn: UIButton!
    @IBOutlet weak var addBtn: UIButton!
    
    var presenter: EditCategoryPresenterViewInterface!
    
    var longPressGesture: UILongPressGestureRecognizer!
    var longPressedEnabled: Bool = false
    
    var showMsgDialog: PopupDialog!
    var localizer = LocalizationSystem.Localizer
    var categories: [Category] = [] {
        didSet {
            editCV.reloadData()
        }
    }
    
    var status: Int = Parameter.EXPENSE {
        didSet {
            self.presenter.getCategories(status: status)
            editCV.reloadData()
        }
    }

    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 3,
        minimumInteritemSpacing: 3,
        minimumLineSpacing: 3,
        sectionInset: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    )

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navBar()
        self.addBtn.layer.cornerRadius = 10
        self.doneBtn.isHidden = true
        self.doneBtn.layer.cornerRadius = 10
        self.doneBtn.layer.borderWidth = 1
        self.doneBtn.layer.borderColor = UIColor(hex: Color.colorAccent.rawValue).cgColor
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
        self.navigationController?.navigationBar.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)], for: UIControl.State.selected)
        
        editCV.register(UINib(nibName: "EditCategoryCell", bundle: nil), forCellWithReuseIdentifier: "edit_category_cell")
        editCV.dataSource = self
        editCV.delegate = self
        editCV.collectionViewLayout = columnLayout
        editCV.reloadData()
        
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.longTap(_:)))
        editCV.addGestureRecognizer(longPressGesture)

        self.presenter.start()
        self.presenter.getCategories(status: status)
        
        Analytics.logEvent(Events.SETTING_CATEGORY_SCREEN, parameters: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        locale()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = self.navigationController!.viewControllers.count > 1
    }
    
    func navBar() {
        self.navigationController?.navigationBar.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        self.navigationController?.delegate = self
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
        
        let image = UIImage(named: "ic_plus")?.withRenderingMode(.alwaysTemplate)
        addBtn.setImage(image, for: .normal)
        addBtn.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: addBtn)
    }
    
    @IBAction func segmentedControl(_ sender: Any) {
        self.status = segmentedControl.selectedSegmentIndex
    }
    
    private func showMsg(msg: String, sender: UIButton) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
            let hitPoint = sender.convert(CGPoint.zero, to: self.editCV)
            let hitIndex = self.editCV.indexPathForItem(at: hitPoint)
            
            //remove the image and refresh the collection view
            self.categories.remove(at: (hitIndex?.row)!)
            self.presenter.removeCategoryById(id: sender.tag)
            self.presenter.removeUsagesByCategoryId(id: sender.tag)
            self.editCV.reloadData()
            
            Analytics.logEvent(self.status == Parameter.EXPENSE ? Events.DELETE_EXPENCE_CATEGORY : Events.DELETE_INCOME_CATEGORY, parameters: nil)
        }
        let buttonCancel = CancelButton(title: NSLocalizedString("Cancel", comment: "")) {
        }
        showMsgDialog.addButtons([buttonOk, buttonCancel])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    @IBAction func removeBtnClick(_ sender: UIButton)   {
        showMsg(msg: "အမျိုးအစားကိုဖြတ်ပါက သက်ဆိုင်ရာအသုံးပြုမှုအချက်အလက်များပါပျက်ပါမည်။", sender: sender)
    }
    
    @IBAction func doneBtnClick(_ sender: Any) {
        //disable the shake and hide done button
        doneBtn.isHidden = true
        longPressedEnabled = false
        
        self.editCV.reloadData()
    }
    
    @IBAction func addBtnClick(_ sender: Any) {
        presenter.addCategory(status: status, vc: self)
    }
    
    @objc func longTap(_ gesture: UIGestureRecognizer){
        
        switch(gesture.state) {
        case .began:
            guard let selectedIndexPath = editCV.indexPathForItem(at: gesture.location(in: editCV)) else {
                return
            }
            editCV.beginInteractiveMovementForItem(at: selectedIndexPath)
            editCV.endInteractiveMovement()
            self.doneBtn.isHidden = false
            self.longPressedEnabled = true
            self.editCV.reloadData()
        case .changed:
            editCV.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        default:
            editCV.cancelInteractiveMovement()
        }
    }
}

extension EditCategoryView: AddCategoryDelegate {
    func finishCategoryAdd() {
        self.presenter.getCategories(status: status)
        editCV.reloadData()
    }
}

extension EditCategoryView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension EditCategoryView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "edit_category_cell", for: indexPath) as! EditCategoryCell
        cell.icon_name = categories[indexPath.row].category_icon
        cell.title = localizer.localizedStringForKey(key: categories[indexPath.row].category_name, comment: "")
        cell.removeBtn.tag = categories[indexPath.row].category_id
        cell.removeBtn.addTarget(self, action: #selector(removeBtnClick(_:)), for: .touchUpInside)
        
        if longPressedEnabled   {
            cell.startAnimate()
        }else{
            cell.stopAnimate()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.updateCategory(id: categories[indexPath.row].category_id, name: categories[indexPath.row].category_name, icon: categories[indexPath.row].category_icon, vc: self)
    }
}

extension EditCategoryView: EditCategoryViewPresenterInterface {
    func displayCategories(categories: [Category]) {
        self.categories = categories
    }
}
