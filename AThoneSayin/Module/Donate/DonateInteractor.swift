//
//  DonateInteractor.swift
//  Donate
//
//  Created by Aung Moe Hein on 31/05/2020.
//

import Foundation

final class DonateInteractor: InteractorInterface {

    weak var presenter: DonatePresenterInteractorInterface!
}

extension DonateInteractor: DonateInteractorPresenterInterface {

}
