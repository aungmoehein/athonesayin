//
//  DonateModule.swift
//  Donate
//
//  Created by Aung Moe Hein on 31/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol DonateRouterPresenterInterface: RouterPresenterInterface {

}

// MARK: - presenter

protocol DonatePresenterRouterInterface: PresenterRouterInterface {

}

protocol DonatePresenterInteractorInterface: PresenterInteractorInterface {

}

protocol DonatePresenterViewInterface: PresenterViewInterface {
    func start()
}

// MARK: - interactor

protocol DonateInteractorPresenterInterface: InteractorPresenterInterface {

}

// MARK: - view

protocol DonateViewPresenterInterface: ViewPresenterInterface {

}


// MARK: - module builder

final class DonateModule: ModuleInterface {

    typealias View = DonateView
    typealias Presenter = DonatePresenter
    typealias Router = DonateRouter
    typealias Interactor = DonateInteractor

    func build() -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "donate") as! View
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
