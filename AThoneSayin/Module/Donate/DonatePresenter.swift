//
//  DonatePresenter.swift
//  Donate
//
//  Created by Aung Moe Hein on 31/05/2020.
//

import Foundation

final class DonatePresenter: PresenterInterface {

    var router: DonateRouterPresenterInterface!
    var interactor: DonateInteractorPresenterInterface!
    weak var view: DonateViewPresenterInterface!

}

extension DonatePresenter: DonatePresenterRouterInterface {

}

extension DonatePresenter: DonatePresenterInteractorInterface {

}

extension DonatePresenter: DonatePresenterViewInterface {

    func start() {

    }

}
