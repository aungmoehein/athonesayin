//
//  DonateView.swift
//  Donate
//
//  Created by Aung Moe Hein on 31/05/2020.
//

import Foundation
import UIKit
import Lottie
import Firebase

final class DonateView: UIViewController, ViewInterface {
    @IBOutlet weak var animationView: AnimationView!
    @IBOutlet weak var ayaAccount: UILabel!
    @IBOutlet weak var kbzAccount: UILabel!
    @IBOutlet weak var cbAccount: UILabel!
    @IBOutlet weak var btnCopyAYA: UIButton!
    @IBOutlet weak var btnCopyKBZ: UIButton!
    @IBOutlet weak var btnCopyCB: UIButton!
    
    var presenter: DonatePresenterViewInterface!


    override func viewDidLoad() {
        super.viewDidLoad()
        
        animationView.playAnimation(filename: "coffee-like")
        
        ayaAccount.text = "0096223010010669"
        kbzAccount.text = "23430123400709201"
        cbAccount.text =  "0010600100483966"
        
        let image = UIImage(named: "ic_clipboard")?.withRenderingMode(.alwaysTemplate)
        btnCopyAYA.setImage(image, for: .normal)
        btnCopyAYA.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        btnCopyKBZ.setImage(image, for: .normal)
        btnCopyKBZ.tintColor = UIColor(hex: Color.colorPrimary.rawValue)
        btnCopyCB.setImage(image, for: .normal)
        btnCopyCB.tintColor = UIColor(hex: Color.colorPrimary.rawValue)

        self.presenter.start()
        Analytics.logEvent(Events.DONATION_SCREEN, parameters: nil)
    }

    @IBAction func btnCopyAYA(_ sender: Any) {
        UIPasteboard.general.string = ayaAccount.text
        self.view.makeToast("Copied to clipboard", duration: 2, position: .bottom)
        Analytics.logEvent(Events.BTN_COPY_AYA, parameters: nil)
    }
    
    @IBAction func btnCopyKBZ(_ sender: Any) {
        UIPasteboard.general.string = kbzAccount.text
        self.view.makeToast("Copied to clipboard", duration: 2, position: .bottom)
        Analytics.logEvent(Events.BTN_COPY_KBZ, parameters: nil)
    }
    
    @IBAction func btnCopyCB(_ sender: Any) {
        UIPasteboard.general.string = cbAccount.text
        self.view.makeToast("Copied to clipboard", duration: 2, position: .bottom)
        Analytics.logEvent(Events.BTN_COPY_CB, parameters: nil)
    }
}

extension DonateView: DonateViewPresenterInterface {

}
