//
//  DonateRouter.swift
//  Donate
//
//  Created by Aung Moe Hein on 31/05/2020.
//

import Foundation
import UIKit

final class DonateRouter: RouterInterface {

    weak var presenter: DonatePresenterRouterInterface!

    weak var viewController: UIViewController?
}

extension DonateRouter: DonateRouterPresenterInterface {

}