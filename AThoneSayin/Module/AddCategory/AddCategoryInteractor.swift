//
//  AddCategoryInteractor.swift
//  AddCategory
//
//  Created by Aung Moe Hein on 13/05/2020.
//

import Foundation

final class AddCategoryInteractor: InteractorInterface {

    weak var presenter: AddCategoryPresenterInteractorInterface!
    
    var db: DBHelper
    init(db: DBHelper) {
        self.db = db
    }
}

extension AddCategoryInteractor: AddCategoryInteractorPresenterInterface {
    func fetchCategoryIcons() {
        self.presenter.onFetchCategoryIcons(icons: db.readCategoryIcons())
    }
    
    func insertNewCategory(name: String, icon: String, type: Int) {
        db.insertCategory(category_name: name, category_icon: icon, category_type: type)
    }
    
    func updateCategory(id: Int, name: String, icon: String) {
        db.updateCategory(category_id: id, category_name: name, category_icon: icon)
    }
}
