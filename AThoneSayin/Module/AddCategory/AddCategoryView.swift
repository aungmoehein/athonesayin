//
//  AddCategoryView.swift
//  AddCategory
//
//  Created by Aung Moe Hein on 13/05/2020.
//

import Foundation
import PopupDialog
import UIKit
import Firebase

protocol AddCategoryDelegate{
    func finishCategoryAdd()
}

final class AddCategoryView: UIViewController, ViewInterface {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var iconCollectionView: UICollectionView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var localizer = LocalizationSystem.Localizer
    var presenter: AddCategoryPresenterViewInterface!
    var showMsgDialog: PopupDialog!
    var delegate : AddCategoryDelegate?
    var selectedIcon: String = ""
    
    var isUpdate: Bool = false
    var updateName: String = ""
    var updateIcon: String = ""
    var updateID: Int = 0
    var selectedIndex: Int = 0
    
    var status: Int = 0
    var icons: [String] = [] {
        didSet {
            iconCollectionView.reloadData()
        }
    }

    let columnLayout = ColumnFlowLayout(
        cellsPerRow: 5,
        minimumInteritemSpacing: 3,
        minimumLineSpacing: 3,
        sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    )

    override func viewDidLoad() {
        super.viewDidLoad()
        
        iconCollectionView.register(UINib(nibName: "AddCategoryCell", bundle: nil), forCellWithReuseIdentifier: "add_category_cell")
        iconCollectionView.dataSource = self
        iconCollectionView.delegate = self
        iconCollectionView.collectionViewLayout = columnLayout
        iconCollectionView.reloadData()
        
        btnAdd.layer.cornerRadius = 10
        btnAdd.backgroundColor = UIColor(hex: Color.colorPrimary.rawValue)
        let btnTitle = status == 0 ? localizer.localizedStringForKey(key: "expense", comment: "") : localizer.localizedStringForKey(key: "income", comment: "")
        btnAdd.setTitle(String(format: localizer.localizedStringForKey(key: "btn_category_add", comment: ""), btnTitle), for: .normal)
        btnAdd.layer.shadowColor = UIColor.gray.cgColor
        btnAdd.layer.shadowOffset = CGSize(width: 2, height: 2)
        btnAdd.layer.shadowOpacity = 0.7
        btnAdd.layer.shadowRadius = 2.0
        
        nameTextField.delegate = self

        self.presenter.start()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewDidLoad()
        
        locale()
        
        if isUpdate {
            nameTextField.text = updateName
            btnAdd.setTitle(localizer.localizedStringForKey(key: "update_category", comment: ""), for: .normal)
        }
        self.presenter.getCategoryIcons()
    }
    
    @IBAction func btnAdd(_ sender: Any) {
        if nameTextField.text != "" && selectedIcon != "" {
            if !isUpdate {
                self.presenter.addNewCategory(name: nameTextField.text!, icon: selectedIcon, type: status)
                UserDefaults.standard.set(selectedIndex, forKey: selectedIcon)
                Analytics.logEvent(status == Parameter.EXPENSE ? Events.ADD_EXPENCE_CATEGORY : Events.ADD_INCOME_CATEGORY, parameters: nil)
                delegate?.finishCategoryAdd()
            }else {
                self.presenter.updateExistingCategory(id: updateID, name: nameTextField.text!, icon: selectedIcon)
                UserDefaults.standard.set(selectedIndex, forKey: selectedIcon)
                Analytics.logEvent(status == Parameter.EXPENSE ? Events.UPDATE_EXPENCE_CATEGORY : Events.UPDATE_INCOME_CATEGORY, parameters: nil)
                delegate?.finishCategoryAdd()
            }
            self.dismiss(animated: true, completion: nil)
        }
        
        if nameTextField.text == "" {
            self.showMsg(msg: localizer.localizedStringForKey(key: "error_name", comment: ""))
        }
        
        if selectedIcon == "" {
            self.showMsg(msg: localizer.localizedStringForKey(key: "error_icon", comment: ""))
        }
    }
    
    private func showMsg(msg: String) {
        let popUp = ShowMsgDialog(nibName: "ShowMsgDialog", bundle: nil)
        popUp.msg = msg
        showMsgDialog = PopupDialog(viewController: popUp, buttonAlignment: .horizontal, transitionStyle: .bounceDown, tapGestureDismissal: false)
        let buttonOk = DefaultButton(title: NSLocalizedString("OK", comment: "")) {
            
        }
        showMsgDialog.addButtons([buttonOk])
        showMsgDialog.viewController = self
        showMsgDialog.buttonAlignment = .horizontal
        
        self.present(showMsgDialog, animated: true, completion: nil)
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension AddCategoryView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddCategoryView: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return icons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "add_category_cell", for: indexPath) as! AddCategoryCell
        cell.icon_name = icons[indexPath.row]

        if isUpdate {
            if (indexPath.row == UserDefaults.standard.integer(forKey: updateIcon)){
                selectedIcon = updateIcon
                selectedIndex = indexPath.row
                collectionView.selectItem(at: indexPath, animated: true, scrollPosition: UICollectionView.ScrollPosition.centeredHorizontally)
                cell.catView.backgroundColor = UIColor(hex: Color.colorAccent.rawValue)
                cell.icon.tintColor = UIColor.white
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIcon = icons[indexPath.row]
        selectedIndex = indexPath.row
    }
}

extension AddCategoryView: AddCategoryViewPresenterInterface {
    func displayCategoryIcons(icons: [String]) {
        self.icons = icons
    }
}
