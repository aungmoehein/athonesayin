//
//  AddCategoryView+Locale.swift
//  AThoneSayin
//
//  Created by Aung Moe Hein on 16/05/2020.
//  Copyright © 2020 Frontiir. All rights reserved.
//

import Foundation

extension AddCategoryView {
     func locale() {
        nameTextField.placeholder = localizer.localizedStringForKey(key: "new_category_name", comment: "")
    }
}
