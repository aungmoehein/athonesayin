//
//  AddCategoryRouter.swift
//  AddCategory
//
//  Created by Aung Moe Hein on 13/05/2020.
//

import Foundation
import UIKit

final class AddCategoryRouter: RouterInterface {

    weak var presenter: AddCategoryPresenterRouterInterface!

    weak var viewController: UIViewController?
    
    var db: DBHelper!
    init(db: DBHelper) {
        self.db = db
    }
}

extension AddCategoryRouter: AddCategoryRouterPresenterInterface {
    
}
