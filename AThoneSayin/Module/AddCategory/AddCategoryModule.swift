//
//  AddCategoryModule.swift
//  AddCategory
//
//  Created by Aung Moe Hein on 13/05/2020.
//
import Foundation
import UIKit

// MARK: - router

protocol AddCategoryRouterPresenterInterface: RouterPresenterInterface {

}

// MARK: - presenter

protocol AddCategoryPresenterRouterInterface: PresenterRouterInterface {

}

protocol AddCategoryPresenterInteractorInterface: PresenterInteractorInterface {
    func onFetchCategoryIcons(icons: [String])
}

protocol AddCategoryPresenterViewInterface: PresenterViewInterface {
    func start()
    func getCategoryIcons()
    func addNewCategory(name: String, icon: String, type: Int)
    func updateExistingCategory(id: Int, name: String, icon: String)
}

// MARK: - interactor

protocol AddCategoryInteractorPresenterInterface: InteractorPresenterInterface {
    func fetchCategoryIcons()
    func insertNewCategory(name: String, icon: String, type: Int)
    func updateCategory(id: Int, name: String, icon: String)
}

// MARK: - view

protocol AddCategoryViewPresenterInterface: ViewPresenterInterface {
    func displayCategoryIcons(icons: [String])
}


// MARK: - module builder

final class AddCategoryModule: ModuleInterface {

    typealias View = AddCategoryView
    typealias Presenter = AddCategoryPresenter
    typealias Router = AddCategoryRouter
    typealias Interactor = AddCategoryInteractor

    func build(db: DBHelper) -> UIViewController {
        let Storyboard  = UIStoryboard(name: "Main", bundle: nil)
        let view = Storyboard.instantiateViewController(withIdentifier: "add_category") as! View
        let interactor = Interactor(db: db)
        let presenter = Presenter()
        let router = Router(db: db)

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)

        router.viewController = view

        return view
    }
}
