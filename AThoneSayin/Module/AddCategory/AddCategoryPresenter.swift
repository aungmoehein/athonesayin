//
//  AddCategoryPresenter.swift
//  AddCategory
//
//  Created by Aung Moe Hein on 13/05/2020.
//

import Foundation

final class AddCategoryPresenter: PresenterInterface {

    var router: AddCategoryRouterPresenterInterface!
    var interactor: AddCategoryInteractorPresenterInterface!
    weak var view: AddCategoryViewPresenterInterface!

}

extension AddCategoryPresenter: AddCategoryPresenterRouterInterface {

}

extension AddCategoryPresenter: AddCategoryPresenterInteractorInterface {
    func onFetchCategoryIcons(icons: [String]) {
        self.view.displayCategoryIcons(icons: icons)
    }
}

extension AddCategoryPresenter: AddCategoryPresenterViewInterface {
    func start() {

    }
    
    func addNewCategory(name: String, icon: String, type: Int) {
        interactor.insertNewCategory(name: name, icon: icon, type: type)
    }
    
    func updateExistingCategory(id: Int, name: String, icon: String) {
        interactor.updateCategory(id: id, name: name, icon: icon)
    }

    func getCategoryIcons() {
        interactor.fetchCategoryIcons()
    }
}
